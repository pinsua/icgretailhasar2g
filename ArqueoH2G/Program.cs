﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using Hasar2daGen;

namespace ArqueoH2G
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            string _server = "";
            string _database = "";
            string _user = "";
            string _codVendedor = "";
            string _tipodoc = "";
            string _tipoArqueo = "";
            string _fo = "";
            string _z = "";
            string _caja = "";
            string _ip = "";
            string _keyIcg = "";
            bool _hasarLog = false;

            if (File.Exists("arqueo.xml"))
            {
                XmlDocument xDoc = new XmlDocument();
                xDoc.Load("arqueo.xml");

                XmlNodeList _List1 = xDoc.SelectNodes("/doc/bd");
                foreach (XmlNode xn in _List1)
                {
                    _server = xn["server"].InnerText;
                    _database = xn["database"].InnerText;
                    _user = xn["user"].InnerText;
                }

                XmlNodeList _List2 = xDoc.SelectNodes("/doc");
                foreach (XmlNode xn in _List2)
                {
                    _codVendedor = xn["codvendedor"].InnerText;
                    _tipodoc = xn["tipodoc"].InnerText;
                    _tipoArqueo = xn["tipoarqueo"].InnerText;
                    _fo = xn["fo"].InnerText;
                    _z = xn["z"].InnerText;
                    _caja = xn["caja"].InnerText;
                }

                if (_tipodoc.ToUpper() == "CIERRECAJA")
                {
                    if (File.Exists("Config2daGeneracion.xml"))
                    {
                        XmlDocument xConfig = new XmlDocument();
                        xConfig.Load("Config2daGeneracion.xml");

                        XmlNodeList _ListCfg = xConfig.SelectNodes("/config");
                        foreach (XmlNode xn in _ListCfg)
                        {
                            _ip = xn["ip"].InnerText;
                            _hasarLog = Convert.ToBoolean(xn["hasarlog"].InnerText);
                            _keyIcg = xn["keyICG"].InnerText;
                        }

                        string _key = IcgVarios.LicenciaIcg.Value();
                        string _cod = IcgVarios.NuevaLicencia.Value(_key);

                        if (_keyIcg == _cod)
                        {
                            //Armamos el stringConnection.
                            string strConnection = "Data Source=" + _server + ";Initial Catalog=" + _database + ";User Id=" + _user + ";Password=masterkey;";
                            //Conectamos.
                            using (SqlConnection _connection = new SqlConnection(strConnection))
                            {
                                try
                                {
                                    _connection.Open();
                                    if (_tipoArqueo.ToUpper() == "Z")
                                    {
                                        if (FuncionesVarias.TengoComprobantesSinImprimmir(_connection, _caja))
                                        {
                                            string _msj = "Existen comprobantes sin fiscalizar." + Environment.NewLine +
                                                "No se puede realizar el cierre Z con comprobantes sin fiscalizar." + Environment.NewLine +
                                                "Por favor fiscalicelos desde la consola y luego fuerze un cierre Z luego.";
                                            MessageBox.Show(new Form { TopMost = true }, _msj, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        }
                                        else
                                        {
                                            string _msj = Cierres.ImprimirCierreZ(_ip, _hasarLog, _caja,_connection);
                                            MessageBox.Show(new Form { TopMost = true }, _msj, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                                        }
                                    }
                                    if (_tipoArqueo.ToUpper() == "X")
                                    {
                                        hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();
                                        hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarJornadaFiscal _cierre = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarJornadaFiscal();
                                        hfl.argentina.HasarImpresoraFiscalRG3561.CerrarJornadaFiscalX _equis = new hfl.argentina.HasarImpresoraFiscalRG3561.CerrarJornadaFiscalX();

                                        string _msj;
                                        //Conectamos.
                                        Impresiones.Conectar(hasar, false, _ip);
                                        //ejecutamos el cierre Z.
                                        _cierre = hasar.CerrarJornadaFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.TipoReporte.REPORTE_X);
                                        _equis = _cierre.X;

                                        _msj = "INFORME DIARIO DE CIERRE :" + Environment.NewLine +
                                          "Cierre 'X' Nº            =[" + _equis.getNumero() + "]" + Environment.NewLine +
                                          "Fecha del Cierre         =[" + _equis.getFechaCierre() + "]" + Environment.NewLine +
                                          "DF Emitidos              =[" + _equis.getDF_CantidadEmitidos() + "]" + Environment.NewLine +
                                          "DF Total                 =[" + _equis.getDF_Total() + "]" + Environment.NewLine +
                                          "DF Total IVA             =[" + _equis.getDF_TotalIVA() + "]" + Environment.NewLine +
                                          "DF Total Otros Tributos  =[" + _equis.getDF_TotalTributos() + "]" + Environment.NewLine +
                                          "NC Emitidas              =[" + _equis.getNC_CantidadEmitidos() + "]" + Environment.NewLine +
                                          "NC Total                 =[" + _equis.getNC_Total() + "]" + Environment.NewLine +
                                          "NC Total IVA             =[" + _equis.getNC_TotalIVA() + "]" + Environment.NewLine +
                                          "NC Total Otros Tributos  =[" + _equis.getNC_TotalTributos() + "]" + Environment.NewLine +
                                          "DNFH Emitidos            =[" + _equis.getDNFH_CantidadEmitidos() + "]";
                                        MessageBox.Show(new Form { TopMost = true }, _msj, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error: " + ex.Message + Environment.NewLine + "Por favor comuniquese con ICG Argentina.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                                }
                            }
                        }
                        else
                            MessageBox.Show(new Form { TopMost = true }, "Licencia no valida!!!." + Environment.NewLine + "Por favor comuniquese con ICG Argentina.",
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                    }
                    else
                        MessageBox.Show(new Form { TopMost = true }, "El archivo de configuración no existe." + Environment.NewLine + "Por favor comuniquese con ICG Argentina.",
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                }
            }
            else
                MessageBox.Show(new Form { TopMost = true }, "El archivo arqueo.xml no existe." + Environment.NewLine + "Por favor comuniquese con ICG Argentina.",
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
        }
    }
}
