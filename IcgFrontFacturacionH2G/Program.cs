﻿    using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Configuration;
using Hasar2daGen;

namespace IcgFrontFacturacionH2G
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //aca Comienzo
            hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();
            //Estilo de Texto.
            hfl.argentina.Hasar_Funcs.AtributosDeTexto _estilo = new hfl.argentina.Hasar_Funcs.AtributosDeTexto();

            string _server = "";
            string _database = "";
            string _user = "";
            string _codVendedor = "";
            string _tipodoc = "";
            string _serie = "";
            string _numero = "";
            string _n = "";
            int _tipo = 12;
            int _accion = 0;
            string _textoRegalo1 = "";
            string _textoRegalo2 = "";
            string _textoRegalo3 = "";
            string _caja = "";
            string _terminal = "";
            string _ip = "";
            string _tef = "0";
            string _ptoVtaManual = "";
            InfoIrsa _infoIrsa = new InfoIrsa();
            string _pathCaballito = "";
            string _pathOlmos = "";
            bool _hasarLog = false;
            string _tipoComprobante = "";
            bool _tikcketRegalo = false;
            bool _checkFiServ = false;
            string _cuit = "";

            if (File.Exists("fiscal10.xml"))
            {
                XmlDocument xDoc = new XmlDocument();
                xDoc.Load("fiscal10.xml");

                XmlNodeList _List1 = xDoc.SelectNodes("/doc/bd");
                foreach (XmlNode xn in _List1)
                {
                    _server = xn["server"].InnerText;
                    _database = xn["database"].InnerText;
                    _user = xn["user"].InnerText;
                }

                int _tengoTef = xDoc.GetElementsByTagName("GuardandoTef").Count;

                XmlNodeList _List2 = xDoc.SelectNodes("/doc");
                foreach (XmlNode xn in _List2)
                {
                    _codVendedor = xn["codvendedor"].InnerText;
                    _tipodoc = xn["tipodoc"].InnerText;
                    _serie = xn["serie"].InnerText;
                    _numero = xn["numero"].InnerText;
                    _n = xn["n"].InnerText;
                    if(_tengoTef > 0)
                        _tef = xn["GuardandoTef"].InnerText;
                }

                //Salimos si es un pedido.
                if (_tipodoc.ToUpper() == "PEDVENTA")
                {
                    return;
                }
                //Salimos si es un remito.
                if (_tipodoc.ToUpper() == "ALBCOMPRA")
                {
                    return;
                }
                //Salimos si es un remito.
                if (_tipodoc.ToUpper() == "ALBVENTA")
                {
                    return;
                }

                if (File.Exists("Config2daGeneracion.xml"))
                {
                    XmlDocument xConfig = new XmlDocument();
                    xConfig.Load("Config2daGeneracion.xml");
                    //Nueva lectura
                    _tikcketRegalo = false;
                    //IP
                    XmlNodeList _IP = xConfig.GetElementsByTagName("ip");
                    if (_IP.Count > 0)
                        _ip = _IP[0].InnerText;
                    //CAJA
                    XmlNodeList _CAJA = xConfig.GetElementsByTagName("caja");
                    if (_CAJA.Count > 0)
                        _caja = _CAJA[0].InnerText;
                    //tktregalo1
                    XmlNodeList _TKTREGALO1 = xConfig.GetElementsByTagName("tktregalo1");
                    if (_TKTREGALO1.Count > 0)
                        _textoRegalo1 = _TKTREGALO1[0].InnerText;
                    //tktregalo2
                    XmlNodeList _TKTREGALO2 = xConfig.GetElementsByTagName("tktregalo2");
                    if (_TKTREGALO2.Count > 0)
                        _textoRegalo2 = _TKTREGALO2[0].InnerText;
                    //tktregalo3
                    XmlNodeList _TKTREGALO3 = xConfig.GetElementsByTagName("tktregalo3");
                    if (_TKTREGALO3.Count > 0)
                        _textoRegalo3 = _TKTREGALO3[0].InnerText;
                    //HasarLog
                    XmlNodeList _HASARLOG = xConfig.GetElementsByTagName("hasarlog");
                    if (_HASARLOG.Count > 0)
                        _hasarLog = Convert.ToBoolean(_HASARLOG[0].InnerText);
                    else
                        _hasarLog = false;
                    //PtoVtaManual
                    XmlNodeList _PtoVtaManual = xConfig.GetElementsByTagName("ptovtamanual");
                    if (_PtoVtaManual.Count > 0)
                        _ptoVtaManual = _PtoVtaManual[0].InnerText;
                    else
                        _ptoVtaManual = "0";
                    
                    //IRSA CATALOG
                    XmlNodeList _CONTRATO = xConfig.GetElementsByTagName("Contrato");
                    if (_CONTRATO.Count > 0)
                        _infoIrsa.contrato = _CONTRATO[0].InnerText;
                    //IRSA CATALOG
                    XmlNodeList _LOCAL = xConfig.GetElementsByTagName("Local");
                    if (_LOCAL.Count > 0)
                        _infoIrsa.local = _LOCAL[0].InnerText;
                    //IRSA CATALOG
                    XmlNodeList PATHSALIDAIRSA = xConfig.GetElementsByTagName("PathSalidaIrsa");
                    if (PATHSALIDAIRSA.Count > 0)
                        _infoIrsa.pathSalida = PATHSALIDAIRSA[0].InnerText;
                    //IRSA POS
                    XmlNodeList POS = xConfig.GetElementsByTagName("Pos");
                    if (POS.Count > 0)
                        _infoIrsa.pos = POS[0].InnerText;
                    //IRSA RUBRO
                    XmlNodeList RUBRO = xConfig.GetElementsByTagName("Rubro");
                    if (RUBRO.Count > 0)
                        _infoIrsa.rubro = RUBRO[0].InnerText;
                    //Datos Caballito
                    XmlNodeList PATHSALIDACABALLITO = xConfig.GetElementsByTagName("PathSalidaCaballito");
                    if (PATHSALIDACABALLITO.Count > 0)
                        _pathCaballito = PATHSALIDACABALLITO[0].InnerText;
                    //Datos Olmos
                    XmlNodeList PATHSALIDAOLMOS = xConfig.GetElementsByTagName("PathSalidaOlmos");
                    if (PATHSALIDAOLMOS.Count > 0)
                        _pathOlmos = PATHSALIDAOLMOS[0].InnerText;
                    //Check Fiserv
                    XmlNodeList CHECKFISERV = xConfig.GetElementsByTagName("CheckFiServ");
                    if (CHECKFISERV.Count > 0)
                        _checkFiServ = Convert.ToBoolean(CHECKFISERV[0].InnerText);
                    //CUIT
                    XmlNodeList CUIT = xConfig.GetElementsByTagName("Cuit");
                    if (CUIT.Count > 0)
                        _cuit = CUIT[0].InnerText;

                    //Solo imprimimos las que N = B
                    if (_n.ToUpper() == "B")
                    {
                        //veo si tengo la tef en espera.
                        if (_tef == "0")
                        {
                            //Armamos el stringConnection.
                            string strConnection = "Data Source=" + _server + ";Initial Catalog=" + _database + ";User Id=" + _user + ";Password=masterkey;";
                            //Conectamos.
                            using (SqlConnection _connection = new SqlConnection(strConnection))
                            {
                                int _nroComprobante = 0;
                                int _nroPos = 0;
                                bool _cancelada = false;
                                Hasar2daGen.Cabecera _cab = new Cabecera();
                                try
                                {
                                    _connection.Open();

                                    //Busco el comprobante en FacturasVentaSerieResol.
                                    Hasar2daGen.FacturasVentaSerieResol _fsr = Hasar2daGen.FacturasVentaSerieResol.GetTiquet(_serie, Convert.ToInt32(_numero), _n, _connection);
                                    //Vemos si no existe y la imprimos, si existe la ReImprimimos.
                                    if (_fsr.numeroFiscal == 0)
                                    {
                                        //Recuperamos los datos de la cabecera.
                                        _cab = Hasar2daGen.Cabecera.GetCabecera(_serie, Convert.ToInt32(_numero), _n, _connection);
                                        List<Hasar2daGen.Items> _items = Hasar2daGen.Items.GetItems(_cab.numserie, _cab.n, _cab.numalbaran, _connection);
                                        List<Hasar2daGen.OtrosTributos> _oTributos = Hasar2daGen.OtrosTributos.GetOtrosTributos(_serie, _n, Convert.ToInt32(_numero), _connection);
                                        List<Hasar2daGen.Recargos> _recargos = Hasar2daGen.Recargos.GetRecargos(_serie, _n, Convert.ToInt32(_numero), _connection);
                                        //Obtengo en nombre de la terminal
                                        _terminal = Environment.MachineName;

                                        if (_items.Count > 0)
                                        {
                                            List<Hasar2daGen.Pagos> _pagos = Hasar2daGen.Pagos.GetPagos(_cab.numserie, _cab.numfac, _cab.n, _connection);

                                            if (_pagos.Count > 0)
                                            {
                                                bool _cuitOK = true;
                                                bool _rSocialOK = true;
                                                bool _hasChange = false;
                                                bool _faltaPapel = false;

                                                //Veo para 2019 si el regimen de facturación es distinto de N
                                                if (_cab.regfacturacioncliente.ToUpper() != "N" && _cab.regfacturacioncliente.ToUpper() != "R")
                                                {
                                                    //Vemos si debemos validar el cuit.
                                                    if (_cab.regfacturacioncliente != "4")
                                                    {
                                                        int _digitoValidador = FuncionesVarias.CalcularDigitoCuit(_cab.nrodoccliente.Replace("-", ""));
                                                        int _digitorecibido = Convert.ToInt16(_cab.nrodoccliente.Substring(_cab.nrodoccliente.Length - 1));
                                                        if (_digitorecibido == _digitoValidador)
                                                            _cuitOK = true;
                                                        else
                                                        {
                                                            frmCuit _frm = new frmCuit();
                                                            _frm.ShowDialog();
                                                            if (String.IsNullOrEmpty(_frm._cuit))
                                                                _cuitOK = false;
                                                            else
                                                            {
                                                                _cab.nrodoccliente = _frm._cuit;
                                                                _cuitOK = true;
                                                                _hasChange = true;
                                                            }
                                                            _frm.Dispose();
                                                        }
                                                    }
                                                    else
                                                    {
                                                        //si el cliente es Hardcode los valores.
                                                        if (_cab.codcliente == 0)
                                                        {
                                                            _cab.direccioncliente = ".";
                                                            //_cab.client.tipodoc = "96_DNI";
                                                            _cab.nombrecliente = "Consumidor Final";
                                                            _cab.nrodoccliente = "0";
                                                        }
                                                        else
                                                        {
                                                            if (String.IsNullOrEmpty(_cab.nrodoccliente))
                                                                throw new Exception("El cliente no posee Número de Documento." + Environment.NewLine +
                                                                    "Por favor ingrese el Número y luego imprímalo desde la consola.");
                                                        }
                                                    }

                                                    //Validamos la direccion.
                                                    if (String.IsNullOrEmpty(_cab.direccioncliente))
                                                        _cab.direccioncliente = ".";

                                                    //Vaidamos la razon social.
                                                    if (String.IsNullOrEmpty(_cab.nombrecliente))
                                                    {
                                                        frmRazonSocial _frm = new frmRazonSocial();
                                                        _frm.ShowDialog();
                                                        if (String.IsNullOrEmpty(_frm._rSocial))
                                                            _rSocialOK = false;
                                                        else
                                                        {
                                                            _cab.nombrecliente = _frm._rSocial;
                                                            _rSocialOK = true;
                                                        }
                                                        _frm.Dispose();
                                                    }

                                                    switch (_cab.descripcionticket.Substring(0, 3))
                                                    {
                                                        case "003":
                                                        case "008":
                                                            {
                                                                string _respImpre;
                                                                if (_cuitOK)
                                                                {
                                                                    if (_rSocialOK)
                                                                    {
                                                                        //Busco documentos Asociados.
                                                                        List<Hasar2daGen.DocumentoAsociado> _docAsoc = Hasar2daGen.DocumentoAsociado.GetDocumentoAsociado(_items[0]._abonoDeNumseie, _items[0]._abonoDe_N, _items[0]._abonoDeNumAlbaran, _connection);
                                                                        //Valido que tenga al menos un comprobante asociado.
                                                                        if (_docAsoc.Count() == 0)
                                                                        {
                                                                            frmComprobanteAsociado _frm = new frmComprobanteAsociado(_cab.descripcionticket.Substring(0, 3));
                                                                            _frm.ShowDialog();
                                                                            if (_frm._nroComprobante > 0)
                                                                            {
                                                                                Hasar2daGen.DocumentoAsociado _newDoc = new DocumentoAsociado();
                                                                                _newDoc._seriefiscal1 = _frm._puntoVenta.ToString().PadLeft(4, '0');
                                                                                _newDoc._seriefiscal2 = _frm._tipoComprobante;
                                                                                _newDoc._numerofiscal = _frm._nroComprobante.ToString();
                                                                                _docAsoc.Add(_newDoc);
                                                                                _frm.Dispose();
                                                                            }
                                                                            else
                                                                            {
                                                                                _frm.Dispose();
                                                                                MessageBox.Show(new Form { TopMost = true }, "La Nota de Crédito no se imprimirá." + Environment.NewLine +
                                                                                    "Por no tener Facturas asociadas."
                                                                                    , "ICG Argentina",
                                                                                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                                                                                break;
                                                                            }

                                                                        }
                                                                        bool _necesitaCierreZ = false;
                                                                        _tipoComprobante = "C";
                                                                        _respImpre = Hasar2daGen.Impresiones.ImprimirNotaCreditoAB(_cab, _items, _pagos, _oTributos, _recargos, _docAsoc, _ip, _connection, _hasarLog,
                                                                            out _faltaPapel, out _necesitaCierreZ, out _nroComprobante, out _nroPos, out _cancelada);

                                                                        if (_necesitaCierreZ)
                                                                        {
                                                                            string _msg = "Ha llegado al horario del FINAL DE LA JORNADA FISCAL y debe realizar un Cierre Z. Desea realizarlo ahora y luego imprimir el tiquet?.";
                                                                            //Preguntamos.
                                                                            if (MessageBox.Show(new Form { TopMost = true }, _msg, "ICG Argentina", MessageBoxButtons.YesNo,
                                                                                MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                                                                            {
                                                                                //Sacamos el Cierre Z.
                                                                                MessageBox.Show(new Form { TopMost = true }, Hasar2daGen.Cierres.ImprimirCierreZ(_ip, _hasarLog, _caja, _connection), "ICG Argentina",
                                                                                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                                                                                //Volvemos a Lanzar la impresion
                                                                                _respImpre = Hasar2daGen.Impresiones.ImprimirNotaCreditoAB(_cab, _items, _pagos, _oTributos, _recargos, _docAsoc,
                                                                                    _ip, _connection, _hasarLog, out _faltaPapel, out _necesitaCierreZ, out _nroComprobante, out _nroPos, out _cancelada);
                                                                            }
                                                                            else
                                                                            {
                                                                                //Cambiar texto de _respImprime.
                                                                                _respImpre = "Recuerde que para seguir facturando debe SI o SI realizar un CIERRE Z.";
                                                                            }
                                                                        }

                                                                        if (!String.IsNullOrEmpty(_respImpre))
                                                                        {
                                                                            MessageBox.Show(new Form { TopMost = true }, _respImpre, "ICG Argentina", MessageBoxButtons.OK,
                                                                                MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                                        }
                                                                    }
                                                                    else
                                                                        MessageBox.Show(new Form { TopMost = true }, "El Cliente no posee Razon Social. Por favor ingresela y luego reimprimar el comprobante.",
                                                                           "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);

                                                                }
                                                                else
                                                                    MessageBox.Show(new Form { TopMost = true }, "El CUIT ingresado no es correcto. Por favor ingreselo correctamente y luego reimprimar el comprobante.",
                                                                           "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);

                                                                break;
                                                            }
                                                        case "001":
                                                        case "006":
                                                            {
                                                                string _respImpre;
                                                                if (_cuitOK)
                                                                {
                                                                    if (_rSocialOK)
                                                                    {
                                                                        bool _necesitaCierreZ = false;
                                                                        _tipoComprobante = "D";
                                                                        _respImpre = Hasar2daGen.Impresiones.ImprimirFacturasAB(_cab, _items, _pagos, _oTributos, _recargos,
                                                                            _ip, _connection, _hasarLog, _textoRegalo1, _textoRegalo2, _textoRegalo3, _tikcketRegalo, out _faltaPapel, out _necesitaCierreZ, 
                                                                            out _nroComprobante, out _nroPos, out _cancelada);

                                                                        if (_necesitaCierreZ)
                                                                        {
                                                                            string _msg = "Ha llegado al horario del FINAL DE LA JORNADA FISCAL y debe realizar un Cierre Z. Desea realizarlo ahora y luego imprimir el tiquet?.";
                                                                            //Preguntamos.
                                                                            if (MessageBox.Show(new Form { TopMost = true }, _msg, "ICG Argentina", MessageBoxButtons.YesNo,
                                                                                MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                                                                            {
                                                                                //Sacamos el Cierre Z.
                                                                                MessageBox.Show(new Form { TopMost = true }, Hasar2daGen.Cierres.ImprimirCierreZ(_ip, _hasarLog, _caja, _connection), "ICG Argentina",
                                                                                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                                                                                //Volvemos a Lanzar la impresion
                                                                                _respImpre = Hasar2daGen.Impresiones.ImprimirFacturasAB(_cab, _items, _pagos, _oTributos, _recargos,
                                                                                        _ip, _connection, _hasarLog, _textoRegalo1, _textoRegalo2, _textoRegalo3, _tikcketRegalo, out _faltaPapel, out _necesitaCierreZ,
                                                                                        out _nroComprobante, out _nroPos, out _cancelada);
                                                                            }
                                                                            else
                                                                            {
                                                                                //Cambiar texto de _respImprime.
                                                                                _respImpre = "Recuerde que para seguir facturando debe SI o SI realizar un CIERRE Z.";
                                                                            }
                                                                        }

                                                                        if (!String.IsNullOrEmpty(_respImpre))
                                                                        {
                                                                            MessageBox.Show(new Form { TopMost = true }, _respImpre, "ICG Argentina", MessageBoxButtons.OK,
                                                                                MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                                        }
                                                                    }
                                                                    else
                                                                        MessageBox.Show(new Form { TopMost = true }, "El Cliente no posee Razon Social. Por favor ingresela y luego reimprimar el comprobante.",
                                                                           "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                                }
                                                                else
                                                                    MessageBox.Show(new Form { TopMost = true }, "El CUIT ingresado no es correcto. Por favor ingreselo correctamente y luego reimprimar el comprobante.",
                                                                           "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                                break;
                                                            }
                                                        case "002":
                                                        case "007":
                                                            {
                                                                //Notas de Debito.
                                                                MessageBox.Show(new Form { TopMost = true }, "Notas de Debito. Aún no estan configuradas.",
                                                                           "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                                break;
                                                            }
                                                        default:
                                                            {
                                                                MessageBox.Show(new Form { TopMost = true }, "Tipo de Documento no configurado para su fiscalización.",
                                                                           "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                                break;
                                                            }
                                                    }
                                                    //vemos si tenemos modificacion.
                                                    if (_hasChange)
                                                    {
                                                        Hasar2daGen.Clientes.UpdateCliente(_cab.nombrecliente, _cab.nrodoccliente, _cab.codcliente, _connection);
                                                        Hasar2daGen.RemTransacciones.GraboRemTransacciones(_terminal, _caja, _cab.z.ToString(), _tipo, _accion, "", _cab.codcliente.ToString(), _cab.n, _connection);
                                                    }
                                                    //Informamos la falta de papel en la fiscal
                                                    if (_faltaPapel)
                                                        MessageBox.Show(new Form { TopMost = true }, "La impresora fiscal informa que se esta quedando sin papel.Por favor revise el papel.",
                                                            "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                    //Comprobamos si hay tiquets sin imprimir
                                                    if (FuncionesVarias.TengoComprobantesSinImprimmir(_connection, _caja))
                                                        MessageBox.Show(new Form { TopMost = true }, "Existen comprobantes sin fiscalizar. Por favor fiscalicelos o anulelos.",
                                                            "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);

                                                    #region Shoppings
                                                    //Vemos si tenemos que lanzar IRSA.
                                                    if (!String.IsNullOrEmpty(_infoIrsa.pathSalida))
                                                    {
                                                        if (!_checkFiServ)
                                                        {
                                                            string _comprobante = _nroPos.ToString().PadLeft(4, '0') + "-" + _nroComprobante.ToString().PadLeft(8, '0');
                                                            Irsa.GenerarIrsa(_cab, _pagos, _infoIrsa, _tipoComprobante, _comprobante, _connection);
                                                        }
                                                    }
                                                    //Vemos si tenemos que lanzar Shoppping Caballito.
                                                    if (!String.IsNullOrEmpty(_pathCaballito))
                                                    {
                                                        string _comprobante = _nroPos.ToString().PadLeft(4, '0') + "-" + _nroComprobante.ToString().PadLeft(8, '0');
                                                        ShoppingCaballito.GenerarShoppingCaballito(_serie, _numero, _n, _connection, _pathCaballito, _comprobante);
                                                    }
                                                    //Vemos si tenemos que lanzar Shoppping Olmos.
                                                    if (!String.IsNullOrEmpty(_pathOlmos))
                                                    {
                                                        MessageBox.Show(new Form { TopMost = true }, "Función Olmos no implementada.");
                                                    }
                                                    //Vemos si tenemos que mandar a IRSA SITEF.
                                                    if (_checkFiServ)
                                                    {
                                                        if (!String.IsNullOrEmpty(_infoIrsa.pathSalida))
                                                        {
                                                            //Validamos que tenemos numero fiscal.
                                                            if (_nroComprobante > 0)
                                                            {
                                                                Hasar2daGen.SiTefService.SiTefServiceRetail.SendNormalInvoiceSql(_serie, _numero.ToString(), _n, _infoIrsa.local.PadLeft(8,'0')
                                                                    , _infoIrsa.pos.PadLeft(8, '0'), _cuit, "30711277249", _infoIrsa.pathSalida, false, _connection);
                                                                
                                                            }
                                                        }
                                                    }
                                                    #endregion
                                                }
                                                else
                                                {
                                                    if (_cab.regfacturacioncliente == "n" || _cab.regfacturacioncliente == "r")
                                                    {
                                                        MessageBox.Show(new Form { TopMost = true }, "Ha ingresado un regimen de Facturación con n o r minúscula." + Environment.NewLine +
                                                            "Por favor cambie el regimen de facturación del cliente a Consumidor Final o Anule el comprobante.",
                                                               "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                    }
                                                    else
                                                        Transacciones.TransaccionBlack(_cab, _connection);
                                                }
                                            }
                                            else
                                            {
                                                //MessageBox.Show("No existen Pagos registrados, verifique que el importe sea mayor a cero. Por favor revise el comprobante.",
                                                //               "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                            }
                                        }
                                        else
                                        {
                                            MessageBox.Show(new Form { TopMost = true }, "No existen Items registrados. Por favor revise el comprobante.",
                                                               "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                        }
                                    }
                                    else
                                    {

                                    }
                                }
                                catch (Exception ex)
                                {

                                    MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error: " + ex.Message,
                                                               "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                    if (_nroComprobante > 0)
                                    {
                                        #region Shoppings
                                        //Vemos si tenemos que lanzar IRSA.
                                        if (!String.IsNullOrEmpty(_infoIrsa.pathSalida))
                                        {
                                            if (!_checkFiServ)
                                            {
                                                string _comprobante = _nroPos.ToString().PadLeft(4, '0') + "-" + _nroComprobante.ToString().PadLeft(8, '0');
                                                List<Hasar2daGen.Pagos> _pagos = Hasar2daGen.Pagos.GetPagos(_cab.numserie, _cab.numfac, _cab.n, _connection);
                                                Irsa.GenerarIrsa(_cab, _pagos, _infoIrsa, _tipoComprobante, _comprobante, _connection);
                                            }
                                        }
                                        //Vemos si tenemos que lanzar Shoppping Caballito.
                                        if (!String.IsNullOrEmpty(_pathCaballito))
                                        {
                                            string _comprobante = _nroPos.ToString().PadLeft(4, '0') + "-" + _nroComprobante.ToString().PadLeft(8, '0');
                                            ShoppingCaballito.GenerarShoppingCaballito(_serie, _numero, _n, _connection, _pathCaballito, _comprobante);
                                        }
                                        //Vemos si tenemos que lanzar Shoppping Olmos.
                                        if (!String.IsNullOrEmpty(_pathOlmos))
                                        {
                                            MessageBox.Show(new Form { TopMost = true }, "Función Olmos no implementada.");
                                        }
                                        //Vemos si tenemos que mandar a IRSA SITEF.
                                        if (_checkFiServ)
                                        {
                                            if (!String.IsNullOrEmpty(_infoIrsa.pathSalida))
                                            {
                                                Hasar2daGen.SiTefService.SiTefServiceRetail.SendNormalInvoiceSql(_serie, _numero.ToString(), _n, _infoIrsa.local.PadLeft(8, '0')
                                                    , _infoIrsa.pos.PadLeft(8, '0'), _cuit, "30711277249", _infoIrsa.pathSalida, false, _connection);

                                            }
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        if (!ex.Message.StartsWith("Exento no puede poser IVA."))
                                        {
                                            if (MessageBox.Show(new Form { TopMost = true }, "No se pudo establecer comunicacion con la Controladora Fiscal."+
                                                Environment.NewLine +"Desea ingresar el Nro. del Talonario?.",
                                              "ICG Argentina", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                                            {
                                                //Veo que tipo de comprobante es.
                                                string _descripcion = _cab.descripcionticket;
                                                string _serieFiscalMan;
                                                SeriesResolucion _serieResol = SeriesResolucion.GetContador(_ptoVtaManual, _descripcion.Substring(0, 3), _connection);
                                                imgresoManual frm = new imgresoManual(_descripcion, _ptoVtaManual, _serieResol.contador, _connection);
                                                frm.ShowDialog();
                                                int _nroCbte = frm._nroCbte;
                                                frm.Dispose();
                                                if (_nroCbte > 0)
                                                {
                                                    try
                                                    {
                                                        List<DocumentoAsociado> _lst = new List<DocumentoAsociado>();
                                                        switch(_cab.descripcionticket.Substring(0, 3))
                                                        {
                                                            case "003":
                                                                {
                                                                    _serieFiscalMan = "003_NC_Preimpreso_A";
                                                                    break;
                                                                }
                                                            case "008":
                                                                {
                                                                    _serieFiscalMan = "008_NC_Preimpreso_B";
                                                                    break;
                                                                }
                                                            case "001":
                                                                {
                                                                    _serieFiscalMan = "001_FC_Preimpreso_A";
                                                                    break;
                                                                }
                                                            case "006":
                                                                {
                                                                    _serieFiscalMan = "006_FC_Preimpreso_B";
                                                                    break;
                                                                }
                                                            default:
                                                                {
                                                                    _serieFiscalMan = "Sin_definirPreimpreso";
                                                                    break;
                                                                }
                                                        }
                                                        Transacciones.TransaccionOK(_cab, _ptoVtaManual, _serieFiscalMan, _nroCbte, 0, _lst, _connection);
                                                        _serieResol.contador = _nroCbte;
                                                        SeriesResolucion.UpdateContadorSerieResolucion(_serieResol, _connection);
                                                    }
                                                    catch(Exception ex2)
                                                    {
                                                        string _numeroError = _ptoVtaManual.PadLeft(4, '0') + "-" + _nroCbte.ToString().PadLeft(8, '0');
                                                        string _rta = "Hubo un Error y no se pudo grabar el numero de comprobante, " + _numeroError + Environment.NewLine +
                                                            ", por favor ingreselo manualmente." + Environment.NewLine + "Error: " + ex2.Message;
                                                        MessageBox.Show(new Form { TopMost = true }, _rta, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                    }                                                    
                                                }
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show(new Form { TopMost = true }, "No se encuentra el archivo de configuración. Por favor comuniquese con ICG Argentina.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                }
            }
            DeleteLog();
        }

       private static void DeleteLog()
        {
            if(File.Exists("Fiscal.log"))
            {
                File.Delete("Fiscal.log");
            }

            if(File.Exists("HasarLog.log"))
            {
                File.Delete("HasarLog.log");
            }
        }
    }
}
