﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace IcgInvocarCierreZ
{
    static class Program
    {
        [DllImport("user32.dll")]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new Form1());

            string _caja ="";
            string _ip = "";
            string _server = "";
            string _password = "";

            string _user = "";

            string _catalog = "";

            string strConnection = "";

            bool _rtaComprobantesOk = false;

            try
            {
                if (File.Exists("Config2daGeneracion.xml"))
                {
                    XmlDocument xDoc = new XmlDocument();
                    xDoc.Load("Config2daGeneracion.xml");

                    XmlNodeList _List1 = xDoc.SelectNodes("/config");
                    foreach (XmlNode xn in _List1)
                    {
                        _ip = xn["ip"].InnerText;
                        _caja = xn["caja"].InnerText;
                        _password = xn["super"].InnerText;
                    }

                    XmlNodeList _Listdb = xDoc.SelectNodes("/config/bd");
                    foreach (XmlNode xn in _Listdb)
                    {
                        _server = xn["server"].InnerText;
                        _user = xn["user"].InnerText;
                        _catalog = xn["database"].InnerText;
                    }

                    //Recuperado los datos vamos por la conexion.
                    //Armamos el stringConnection.
                    strConnection = "Data Source=" + _server + ";Initial Catalog=" + _catalog + ";User Id=" + _user + ";Password=masterkey;";

                    using (SqlConnection _con = new SqlConnection(strConnection))
                    {
                        _con.Open();

                        _rtaComprobantesOk = Hasar2daGen.FuncionesVarias.TengoComprobantesSinImprimmir(_con, _caja);

                        if (_con.State == System.Data.ConnectionState.Open)
                            _con.Close();
                    }

                    if(!_rtaComprobantesOk)
                    {
                        Process p = Process.GetProcessesByName("FrontRetail").FirstOrDefault();
                        if (p != null)
                        {
                            IntPtr h = p.MainWindowHandle;
                            SetForegroundWindow(h);
                            SendKeys.SendWait("%(z)");
                            SendKeys.Flush();
                        }
                    }
                    else
                    {
                        MessageBox.Show(new Form { TopMost = true }, "Posee comprobantes sin fiscalizar. Por favor fiscalize o anule los comprobantes para poder realizar el cierre Z.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                    }

                }
                else
                {
                    //DeshabilitarBotones();
                    MessageBox.Show(new Form { TopMost = true }, "No se encuentra el archivo de configuración. Por favor comuniquese con ICG Argentina.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error: " + ex.Message + ". Por favor comuniquese con ICG Argentina.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
        }
    }
}
