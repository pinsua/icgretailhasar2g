﻿using Hasar2daGen;
using hfl.argentina;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace IcgRetailAbrirCajon
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {            
            string _ip = "";
            string _keyIcg = "";
            bool _hasarLog = false;

            if (File.Exists("Config2daGeneracion.xml"))
            {
                XmlDocument xConfig = new XmlDocument();
                xConfig.Load("Config2daGeneracion.xml");

                XmlNodeList _ListCfg = xConfig.SelectNodes("/config");
                foreach (XmlNode xn in _ListCfg)
                {
                    _ip = xn["ip"].InnerText;
                    _hasarLog = Convert.ToBoolean(xn["hasarlog"].InnerText);
                    _keyIcg = xn["keyICG"].InnerText;
                }

                string _key = IcgVarios.LicenciaIcg.Value();
                string _cod = IcgVarios.NuevaLicencia.Value(_key);

                if (_keyIcg == _cod)
                {
                    try
                    {
                        //aca Comienzo
                        hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();
                        //Conectamos con la hasar.
                        Impresiones.Conectar(hasar, _hasarLog, _ip);
                        hasar.AbrirCajonDinero();
                    }
                    catch (HasarException hx)
                    {
                        MessageBox.Show(new Form { TopMost = true }, hx.Message);
                    }
                }
            }
        }
    }
}
