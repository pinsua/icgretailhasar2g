﻿namespace IcgRetailConsolaH2G
{
    partial class frmReimpresion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.gbGrid = new System.Windows.Forms.GroupBox();
            this.grCierre = new System.Windows.Forms.DataGridView();
            this.cajaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Hora = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numeroDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Tiquets = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtsReimpresion = new System.Data.DataSet();
            this.dataTable1 = new System.Data.DataTable();
            this.dataColumn3 = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.dataColumn5 = new System.Data.DataColumn();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.dataColumn6 = new System.Data.DataColumn();
            this.gbCierres = new System.Windows.Forms.GroupBox();
            this.btClose = new System.Windows.Forms.Button();
            this.btCierreZ = new System.Windows.Forms.Button();
            this.gbGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grCierre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtsReimpresion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).BeginInit();
            this.gbCierres.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbGrid
            // 
            this.gbGrid.Controls.Add(this.grCierre);
            this.gbGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbGrid.Location = new System.Drawing.Point(0, 0);
            this.gbGrid.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gbGrid.Name = "gbGrid";
            this.gbGrid.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gbGrid.Size = new System.Drawing.Size(1013, 402);
            this.gbGrid.TabIndex = 3;
            this.gbGrid.TabStop = false;
            this.gbGrid.Text = "Cierres de Caja";
            // 
            // grCierre
            // 
            this.grCierre.AllowUserToAddRows = false;
            this.grCierre.AllowUserToDeleteRows = false;
            this.grCierre.AutoGenerateColumns = false;
            this.grCierre.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grCierre.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cajaDataGridViewTextBoxColumn,
            this.fechaDataGridViewTextBoxColumn,
            this.Hora,
            this.numeroDataGridViewTextBoxColumn,
            this.Total,
            this.Tiquets});
            this.grCierre.DataMember = "Reimpresion";
            this.grCierre.DataSource = this.dtsReimpresion;
            this.grCierre.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grCierre.Location = new System.Drawing.Point(4, 24);
            this.grCierre.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.grCierre.Name = "grCierre";
            this.grCierre.ReadOnly = true;
            this.grCierre.RowHeadersWidth = 62;
            this.grCierre.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grCierre.Size = new System.Drawing.Size(1005, 373);
            this.grCierre.TabIndex = 0;
            // 
            // cajaDataGridViewTextBoxColumn
            // 
            this.cajaDataGridViewTextBoxColumn.DataPropertyName = "Caja";
            this.cajaDataGridViewTextBoxColumn.HeaderText = "Caja";
            this.cajaDataGridViewTextBoxColumn.MinimumWidth = 8;
            this.cajaDataGridViewTextBoxColumn.Name = "cajaDataGridViewTextBoxColumn";
            this.cajaDataGridViewTextBoxColumn.ReadOnly = true;
            this.cajaDataGridViewTextBoxColumn.Width = 150;
            // 
            // fechaDataGridViewTextBoxColumn
            // 
            this.fechaDataGridViewTextBoxColumn.DataPropertyName = "Fecha";
            dataGridViewCellStyle1.Format = "d";
            dataGridViewCellStyle1.NullValue = null;
            this.fechaDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle1;
            this.fechaDataGridViewTextBoxColumn.HeaderText = "Fecha";
            this.fechaDataGridViewTextBoxColumn.MinimumWidth = 8;
            this.fechaDataGridViewTextBoxColumn.Name = "fechaDataGridViewTextBoxColumn";
            this.fechaDataGridViewTextBoxColumn.ReadOnly = true;
            this.fechaDataGridViewTextBoxColumn.Width = 150;
            // 
            // Hora
            // 
            this.Hora.DataPropertyName = "Hora";
            dataGridViewCellStyle2.Format = "T";
            dataGridViewCellStyle2.NullValue = null;
            this.Hora.DefaultCellStyle = dataGridViewCellStyle2;
            this.Hora.HeaderText = "Hora";
            this.Hora.MinimumWidth = 8;
            this.Hora.Name = "Hora";
            this.Hora.ReadOnly = true;
            this.Hora.Width = 150;
            // 
            // numeroDataGridViewTextBoxColumn
            // 
            this.numeroDataGridViewTextBoxColumn.DataPropertyName = "Numero";
            this.numeroDataGridViewTextBoxColumn.HeaderText = "Numero";
            this.numeroDataGridViewTextBoxColumn.MinimumWidth = 8;
            this.numeroDataGridViewTextBoxColumn.Name = "numeroDataGridViewTextBoxColumn";
            this.numeroDataGridViewTextBoxColumn.ReadOnly = true;
            this.numeroDataGridViewTextBoxColumn.Width = 150;
            // 
            // Total
            // 
            this.Total.DataPropertyName = "Total";
            dataGridViewCellStyle3.Format = "C2";
            dataGridViewCellStyle3.NullValue = null;
            this.Total.DefaultCellStyle = dataGridViewCellStyle3;
            this.Total.HeaderText = "Total";
            this.Total.MinimumWidth = 8;
            this.Total.Name = "Total";
            this.Total.ReadOnly = true;
            this.Total.Width = 150;
            // 
            // Tiquets
            // 
            this.Tiquets.DataPropertyName = "Tiquets";
            this.Tiquets.HeaderText = "Tiquets";
            this.Tiquets.MinimumWidth = 8;
            this.Tiquets.Name = "Tiquets";
            this.Tiquets.ReadOnly = true;
            this.Tiquets.Width = 150;
            // 
            // dtsReimpresion
            // 
            this.dtsReimpresion.DataSetName = "NewDataSet";
            this.dtsReimpresion.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable1});
            // 
            // dataTable1
            // 
            this.dataTable1.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn3,
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn6});
            this.dataTable1.TableName = "Reimpresion";
            // 
            // dataColumn3
            // 
            this.dataColumn3.ColumnName = "Caja";
            // 
            // dataColumn4
            // 
            this.dataColumn4.ColumnName = "Fecha";
            this.dataColumn4.DataType = typeof(System.DateTime);
            // 
            // dataColumn5
            // 
            this.dataColumn5.ColumnName = "Numero";
            this.dataColumn5.DataType = typeof(int);
            // 
            // dataColumn1
            // 
            this.dataColumn1.ColumnName = "Hora";
            // 
            // dataColumn2
            // 
            this.dataColumn2.ColumnName = "Total";
            this.dataColumn2.DataType = typeof(decimal);
            // 
            // dataColumn6
            // 
            this.dataColumn6.ColumnName = "Tiquets";
            this.dataColumn6.DataType = typeof(int);
            // 
            // gbCierres
            // 
            this.gbCierres.Controls.Add(this.btClose);
            this.gbCierres.Controls.Add(this.btCierreZ);
            this.gbCierres.Dock = System.Windows.Forms.DockStyle.Right;
            this.gbCierres.Location = new System.Drawing.Point(1013, 0);
            this.gbCierres.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gbCierres.Name = "gbCierres";
            this.gbCierres.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gbCierres.Size = new System.Drawing.Size(237, 402);
            this.gbCierres.TabIndex = 2;
            this.gbCierres.TabStop = false;
            this.gbCierres.Text = "Cierres";
            // 
            // btClose
            // 
            this.btClose.Location = new System.Drawing.Point(63, 143);
            this.btClose.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btClose.Name = "btClose";
            this.btClose.Size = new System.Drawing.Size(112, 35);
            this.btClose.TabIndex = 1;
            this.btClose.Text = "Salir";
            this.btClose.UseVisualStyleBackColor = true;
            this.btClose.Click += new System.EventHandler(this.btClose_Click);
            // 
            // btCierreZ
            // 
            this.btCierreZ.Location = new System.Drawing.Point(63, 54);
            this.btCierreZ.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btCierreZ.Name = "btCierreZ";
            this.btCierreZ.Size = new System.Drawing.Size(112, 35);
            this.btCierreZ.TabIndex = 0;
            this.btCierreZ.Text = "Reimprimir Z";
            this.btCierreZ.UseVisualStyleBackColor = true;
            this.btCierreZ.Click += new System.EventHandler(this.btCierreZ_Click);
            // 
            // frmReimpresion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1250, 402);
            this.Controls.Add(this.gbGrid);
            this.Controls.Add(this.gbCierres);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmReimpresion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ICG Argentina - Reimpresion Cierre Z";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.frmReimpresion_Load);
            this.gbGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grCierre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtsReimpresion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).EndInit();
            this.gbCierres.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbGrid;
        private System.Windows.Forms.DataGridView grCierre;
        private System.Windows.Forms.GroupBox gbCierres;
        private System.Windows.Forms.Button btClose;
        private System.Windows.Forms.Button btCierreZ;
        private System.Data.DataSet dtsReimpresion;
        private System.Data.DataTable dataTable1;
        private System.Data.DataColumn dataColumn3;
        private System.Data.DataColumn dataColumn4;
        private System.Data.DataColumn dataColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn cajaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Hora;
        private System.Windows.Forms.DataGridViewTextBoxColumn numeroDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Total;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tiquets;
        private System.Data.DataColumn dataColumn1;
        private System.Data.DataColumn dataColumn2;
        private System.Data.DataColumn dataColumn6;
    }
}