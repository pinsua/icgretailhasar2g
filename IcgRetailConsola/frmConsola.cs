﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using hfl.argentina;
using System.IO;
using System.Net.NetworkInformation;
using System.Security.Cryptography;
using System.Diagnostics;

namespace IcgRetailConsolaH2G
{
    public partial class frmConsola : Form
    {
        string _ip = "";
        string _KeyICG = "";
        bool _estado = false;
        string _pathFile = @"C:\AFIP\";
        public hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();

        public frmConsola(string _IP)
        {
            InitializeComponent();

            this.Text = this.Text + " - V." + Application.ProductVersion;
            _ip = _IP;
        }

        private void btnAditoria_Click(object sender, EventArgs e)
        {
                string msj = "REPORTE DE AUDITORIA AFIP."+ Environment.NewLine+
                    "Se ejecutará el reporte del controlador fiscal, el mismo puede tardar." + Environment.NewLine+
                    "Durante el proceso no se puede utilizar el controlador fiscal." + Environment.NewLine + 
                    "¿ Desea continuar ?";

            if (MessageBox.Show(new Form { TopMost = true }, msj, "ICG Argentina", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {
                try
                {
                    this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
                    bool p = false;

                    string sourcePath = Application.StartupPath;
                    string _fDesde = dtpDesde.Value.Year.ToString().Substring(2, 2) + dtpDesde.Value.Month.ToString().PadLeft(2, '0') + dtpDesde.Value.Day.ToString().PadLeft(2, '0');
                    string _fHasta = dtpHasta.Value.Year.ToString().Substring(2, 2) + dtpHasta.Value.Month.ToString().PadLeft(2, '0') + dtpHasta.Value.Day.ToString().PadLeft(2, '0');
                    string _arg = "-p " + _ip + " -i xml -o auditafip.zip -a " + _fDesde + " " + _fHasta;

                    ProcessStartInfo _pi = new ProcessStartInfo();
                    _pi.FileName = "getaudar.exe";
                    _pi.Arguments = _arg;

                    using (Process exeProcess = Process.Start(_pi))
                    {
                        exeProcess.WaitForExit();
                    }

                    string srcFileName = "auditafip.zip";
                    string fileName = "auditafip_" + _fDesde + "_" + _fHasta + ".zip";

                    string sourceFile = System.IO.Path.Combine(sourcePath, srcFileName);
                    string destFile = System.IO.Path.Combine(_pathFile, fileName);

                    if (System.IO.File.Exists(sourceFile))
                    {
                        if (!System.IO.Directory.Exists(_pathFile))
                        {
                            System.IO.Directory.CreateDirectory(_pathFile);
                        }

                        System.IO.File.Copy(sourceFile, destFile, true);

                        this.Cursor = System.Windows.Forms.Cursors.Default;

                        MessageBox.Show(new Form { TopMost = true }, "El proceso termino correctamente. El archivo se encuentra en " + destFile,
                           "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                    }
                    else
                    {
                        this.Cursor = System.Windows.Forms.Cursors.Default;

                        MessageBox.Show(new Form { TopMost = true }, "El proceso no termino correctamente. ",
                           "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                    }
                }
                catch (Exception ex)
                {
                    this.Cursor = System.Windows.Forms.Cursors.Default;

                    MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente ERROR: " + ex.Message,
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            btConectar.Enabled = true;
            btTerminar.Enabled = false;
            gbAuditoria.Enabled = false;
            gbCierres.Enabled = false;
            gbReporteComprobantes.Enabled = false;
            gbCtd.Enabled = false;

            dtpDesde.Value = DateTime.Now;
            dtpHasta.Value = DateTime.Now;

            cbFechas.Checked = true;

            try
            {
                if (!Directory.Exists(_pathFile))
                    Directory.CreateDirectory(_pathFile);

                txtIp.Text = _ip;
            }
            catch (Exception ex)
            {
                MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error:" + ex.Message,
                       "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                this.Close();
            }
        }        

        private void btConectar_Click(object sender, EventArgs e)
        {

            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarVersion resp = new HasarImpresoraFiscalRG3561.RespuestaConsultarVersion();
            hasar.establecerTiempoDeEspera(30000);                             // Por defecto: 10000
            hasar.establecerTiempoDeEsperaLecturaEscritura(35000);             // Por defecto: 15000
            try
            {
                Application.DoEvents();
                this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

                hasar.conectar(txtIp.Text);
                resp = hasar.ConsultarVersion();

                txtProtocoloDll.Text = hasar.ObtenerVersionProtocolo().ToString();
                txtRevisionDll.Text = hasar.ObtenerRevision().ToString();
                txtProducto.Text = resp.getNombreProducto() + " -   Marca = " + resp.getMarca();
                txtModelo.Text = resp.getVersion();
                txtFirmware.Text = resp.getFechaFirmware();
                txtVersion.Text = resp.getVersionMotor();
                txtProtocoloCF.Text = resp.getVersionProtocolo().ToString();

                this.Cursor = System.Windows.Forms.Cursors.Arrow;

                btConectar.Enabled = false;
                btTerminar.Enabled = true;
                gbAuditoria.Enabled = true;
                gbCierres.Enabled = true;
                gbReporteComprobantes.Enabled = true;
                gbCtd.Enabled = true;

                _estado = true;
            }
            catch (Exception ex)
            {
                this.Cursor = System.Windows.Forms.Cursors.Arrow;
                MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente ERROR: " + ex.Message,
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);

            }
        }

        private void rbAfipCompleto_CheckedChanged(object sender, EventArgs e)
        {
            rbAfipMemFiscal.Checked = !rbAfipCompleto.Checked;
        }

        private void rbAfipMemFiscal_CheckedChanged(object sender, EventArgs e)
        {
            rbAfipMemFiscal.Checked = !rbAfipCompleto.Checked;
        }

        private void GrabarInfoBin(string archivo, string info)
        {
            if (File.Exists(archivo))
            {
                using (BinaryWriter writer = new BinaryWriter(File.Open(archivo, FileMode.Append)))
                {
                    writer.Write(info);
                }
            }
            else
            {
                using (BinaryWriter writer = new BinaryWriter(File.Open(archivo, FileMode.Create)))
                {
                    writer.Write(info);
                }
            }
        }

        public static Boolean IsNumeric(string valor)
        {
            int result;
            return int.TryParse(valor, out result);
        }

        private void rbComprimido_CheckedChanged(object sender, EventArgs e)
        {
            rbXml.Checked = !rbComprimido.Checked;
        }

        private void rbXml_CheckedChanged(object sender, EventArgs e)
        {
            rbComprimido.Checked = !rbXml.Checked;
        }

        private void btReporteCbte_Click(object sender, EventArgs e)
        {
                string msj = "REPORTE DE AUDITORIA AFIP." + Environment.NewLine +
                    "Se ejecutará el reporte del controlador fiscal, el mismo puede tardar." + Environment.NewLine +
                    "Durante el proceso no se puede utilizar el controlador fiscal." + Environment.NewLine +
                    "¿ Desea continuar ?";

                string doc = "";

            if (MessageBox.Show(new Form { TopMost = true }, msj, "ICG Argentina", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {
                try
                {
                    this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

                    switch (cboTipoCbte.SelectedItem)
                    {

                        case "COMPROBANTE_DONACION":
                            {
                                doc = "907";// hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.COMPROBANTE_DONACION;
                                break;
                            }
                        case "GENERICO":
                            {
                                doc = "910"; // hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.GENERICO;
                                break;
                            }
                        case "DETALLE_DE_VENTAS":
                            {
                                doc = "940"; // hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.DETALLE_DE_VENTAS;
                                break;
                            }
                        case "FACTURA_A":
                            {
                                doc = "1"; // hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_A;
                                break;
                            }
                        case "FACTURA_B":
                            {
                                doc = "6";// hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_B.ToString();
                                break;
                            }
                        case "FACTURA_C":
                            {
                                doc = "11"; // hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_C.ToString();
                                break;
                            }
                        case "FACTURA_M":
                            {
                                doc = "51"; // hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_M.ToString();
                                break;
                            }
                        case "INFORME_DE_AUDITORIA":
                            {
                                doc = "904"; // hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.INFORME_DE_AUDITORIA.ToString();
                                break;
                            }
                        case "INFORME_DIARIO_DE_CIERRE":
                            {
                                doc = "80"; // hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.INFORME_DIARIO_DE_CIERRE.ToString();
                                break;
                            }
                        case "NOTA_DE_CREDITO_A":
                            {
                                doc = "3"; // hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NOTA_DE_CREDITO_A.ToString();
                                break;
                            }
                        case "NOTA_DE_CREDITO_B":
                            {
                                doc = "8"; // hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NOTA_DE_CREDITO_B.ToString();
                                break;
                            }
                        case "NOTA_DE_CREDITO_C":
                            {
                                doc = "13"; // hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NOTA_DE_CREDITO_C.ToString();
                                break;
                            }
                        case "NOTA_DE_CREDITO_M":
                            {
                                doc = "53"; // hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NOTA_DE_CREDITO_M.ToString();
                                break;
                            }
                        case "NOTA_DE_DEBITO_A":
                            {
                                doc = "2"; // hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NOTA_DE_DEBITO_A.ToString();
                                break;
                            }
                        case "NOTA_DE_DEBITO_B":
                            {
                                doc = "7"; // hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NOTA_DE_DEBITO_B.ToString();
                                break;
                            }
                        case "NOTA_DE_DEBITO_C":
                            {
                                doc = "12"; // hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NOTA_DE_DEBITO_C.ToString();
                                break;
                            }
                        case "NOTA_DE_DEBITO_M":
                            {
                                doc = "52"; // hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NOTA_DE_DEBITO_M.ToString();
                                break;
                            }
                        case "PRESUPUESTO_X":
                            {
                                doc = "903"; // hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.PRESUPUESTO_X.ToString();
                                break;
                            }
                        case "RECIBO_A":
                            {
                                doc = "4"; // hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.RECIBO_A.ToString();
                                break;
                            }
                        case "RECIBO_B":
                            {
                                doc = "9"; // hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.RECIBO_B.ToString();
                                break;
                            }
                        case "RECIBO_C":
                            {
                                doc = "15"; // hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.RECIBO_C.ToString();
                                break;
                            }
                        case "RECIBO_M":
                            {
                                doc = "54"; // hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.RECIBO_M.ToString();
                                break;
                            }
                        case "RECIBO_X":
                            {
                                doc = "902";// hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.RECIBO_X.ToString();
                                break;
                            }
                        case "REMITO_R":
                            {
                                doc = "91";// hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.REMITO_R.ToString();
                                break;
                            }
                        case "REMITO_X":
                            {
                                doc = "901";// hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.REMITO_X.ToString();
                                break;
                            }
                        case "TIQUE":
                            {
                                doc = "83";// hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE.ToString();
                                break;
                            }
                        case "TIQUE_FACTURA_A":
                            {
                                doc = "81"; // hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_FACTURA_A.ToString();
                                break;
                            }
                        case "TIQUE_FACTURA_B":
                            {
                                doc = "82"; // hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_FACTURA_B.ToString();
                                break;
                            }
                        case "TIQUE_FACTURA_C":
                            {
                                doc = "111";// hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_FACTURA_C.ToString();
                                break;
                            }
                        case "TIQUE_FACTURA_M":
                            {
                                doc = "118"; // hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_FACTURA_M.ToString();
                                break;
                            }
                        case "TIQUE_NOTA_CREDITO":
                            {
                                doc = "110"; // hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO.ToString();
                                break;
                            }
                        case "TIQUE_NOTA_CREDITO_A":
                            {
                                doc = "112";// hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO_A.ToString();
                                break;
                            }
                        case "TIQUE_NOTA_CREDITO_B":
                            {
                                doc = "113"; // hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO_B.ToString();
                                break;
                            }
                        case "TIQUE_NOTA_CREDITO_C":
                            {
                                doc = "114"; // hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO_C.ToString();
                                break;
                            }
                        case "TIQUE_NOTA_CREDITO_M":
                            {
                                doc = "119"; // hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO_M.ToString();
                                break;
                            }
                        case "TIQUE_NOTA_DEBITO_A":
                            {
                                doc = "115"; // hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_DEBITO_A.ToString();
                                break;
                            }
                        case "TIQUE_NOTA_DEBITO_B":
                            {
                                doc = "116"; // hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_DEBITO_B.ToString();
                                break;
                            }
                        case "TIQUE_NOTA_DEBITO_C":
                            {
                                doc = "117"; // hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_DEBITO_C.ToString();
                                break;
                            }
                        case "TIQUE_NOTA_DEBITO_M":
                            {
                                doc = "120"; // hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_DEBITO_M.ToString();
                                break;
                            }
                    }

                    // Se descarga el primer bloque de información.
                    int _desde = Convert.ToInt32(txtNroDesde.Text);
                    int _hasta = Convert.ToInt32(txtNroHasta.Text);

                    string _arg = "-p " + _ip + " -i xml -o repcomph2g.zip " + doc + " " + _desde + " " + _hasta;

                    ProcessStartInfo _pi = new ProcessStartInfo();
                    _pi.FileName = "getdocar.exe";
                    _pi.Arguments = _arg;

                    using (Process exeProcess = Process.Start(_pi))
                    {
                        exeProcess.WaitForExit();
                    }

                    string fileName = "repcomph2g.zip";
                    string sourcePath = Application.StartupPath;

                    string sourceFile = System.IO.Path.Combine(sourcePath, fileName);
                    string destFile = System.IO.Path.Combine(_pathFile, fileName);

                    if (System.IO.File.Exists(sourceFile))
                    {
                        if (!System.IO.Directory.Exists(_pathFile))
                        {
                            System.IO.Directory.CreateDirectory(_pathFile);
                        }

                        System.IO.File.Copy(sourceFile, destFile, true);

                        this.Cursor = System.Windows.Forms.Cursors.Default;

                        MessageBox.Show(new Form { TopMost = true }, "El proceso termino correctamente. El archivo se encuentra en " + destFile,
                           "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                    }
                    else
                    {
                        this.Cursor = System.Windows.Forms.Cursors.Default;

                        MessageBox.Show(new Form { TopMost = true }, "El proceso no termino correctamente. ",
                           "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                    }
                }
                catch (Exception ex)
                {
                    this.Cursor = System.Windows.Forms.Cursors.Default;

                    MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente ERROR: " + ex.Message,
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                }
            }
        }

        private void btnCierreZ_Click(object sender, EventArgs e)
        {
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarJornadaFiscal _cierre = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarJornadaFiscal();
            hfl.argentina.HasarImpresoraFiscalRG3561.CerrarJornadaFiscalZ _zeta = new hfl.argentina.HasarImpresoraFiscalRG3561.CerrarJornadaFiscalZ();
            string _msj;

            try
            {
                //Conectamos.
                hasar.conectar(_ip);
                //ejecutamos el cierre Z.
                _cierre = hasar.CerrarJornadaFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.TipoReporte.REPORTE_Z);
                _zeta = _cierre.Z;

                _msj = "INFORME DIARIO DE CIERRE :" + Environment.NewLine +
                  "Cierre 'Z' Nº            =[" + _zeta.getNumero() + "]" + Environment.NewLine +
                  "Fecha del Cierre         =[" + _zeta.getFecha() + "]" + Environment.NewLine +
                  "DF Emitidos              =[" + _zeta.getDF_CantidadEmitidos() + "]" + Environment.NewLine +
                  "DF Cancelados            =[" + _zeta.getDF_CantidadCancelados() + "]" + Environment.NewLine +
                  "DF Total                 =[" + string.Format("{0:0.00}", _zeta.getDF_Total() / 100) + "]" + Environment.NewLine +
                  "DF Total Gravado         =[" + string.Format("{0:0.00}", _zeta.getDF_TotalGravado() / 100) + "]" + Environment.NewLine +
                  "DF Total No Gravado      =[" + string.Format("{0:0.00}", _zeta.getDF_TotalNoGravado() / 100) + "]" + Environment.NewLine +
                  "DF Total Exento          =[" + string.Format("{0:0.00}", _zeta.getDF_TotalExento() / 100) + "]" + Environment.NewLine +
                  "DF Total IVA             =[" + string.Format("{0:0.00}", _zeta.getDF_TotalIVA() / 100) + "]" + Environment.NewLine +
                  "DF Total Otros Tributos  =[" + string.Format("{0:0.00}", _zeta.getDF_TotalTributos() / 100) + "]" + Environment.NewLine +
                  "NC Emitidas              =[" + _zeta.getNC_CantidadEmitidos() + "]" + Environment.NewLine +
                  "NC Canceladas            =[" + _zeta.getNC_CantidadCancelados() + "]" + Environment.NewLine +
                  "NC Total                 =[" + string.Format("{0:0.00}", _zeta.getNC_Total() / 100) + "]" + Environment.NewLine +
                  "NC Total Gravado         =[" + string.Format("{0:0.00}", _zeta.getNC_TotalGravado() / 100) + "]" + Environment.NewLine +
                  "NC Total No Gravado      =[" + string.Format("{0:0.00}", _zeta.getNC_TotalNoGravado() / 100) + "]" + Environment.NewLine +
                  "NC Total Exento          =[" + string.Format("{0:0.00}", _zeta.getNC_TotalExento() / 100) + "]" + Environment.NewLine +
                  "NC Total IVA             =[" + string.Format("{0:0.00}", _zeta.getNC_TotalIVA() / 100) + "]" + Environment.NewLine +
                  "NC Total Otros Tributos  =[" + string.Format("{0:0.00}", _zeta.getNC_TotalTributos() / 100) + "]" + Environment.NewLine +
                  "DNFH Emitidos            =[" + _zeta.getDNFH_CantidadEmitidos() + "]" + Environment.NewLine +
                  "DNFH Total               =[" + _zeta.getDNFH_Total() + "]";

                MessageBox.Show(new Form { TopMost = true }, _msj, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);

            }
            catch (Exception ex)
            {
                MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente Error : " + ex.Message,
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        private void btnCierreX_Click(object sender, EventArgs e)
        {
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarJornadaFiscal _cierre = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarJornadaFiscal();
            hfl.argentina.HasarImpresoraFiscalRG3561.CerrarJornadaFiscalX _equis = new hfl.argentina.HasarImpresoraFiscalRG3561.CerrarJornadaFiscalX();

            string _msj;

            try
            {
                //Conectamos.
                hasar.conectar(_ip);
                //ejecutamos el cierre Z.
                _cierre = hasar.CerrarJornadaFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.TipoReporte.REPORTE_X);
                _equis = _cierre.X;

                _msj = "INFORME DIARIO DE CIERRE :" + Environment.NewLine +
                  "Cierre 'X' Nº            =[" + _equis.getNumero() + "]" + Environment.NewLine +
                  "Fecha del Cierre         =[" + _equis.getFechaCierre() + "]" + Environment.NewLine +
                  "DF Emitidos              =[" + _equis.getDF_CantidadEmitidos() + "]" + Environment.NewLine +
                  "DF Total                 =[" + _equis.getDF_Total() + "]" + Environment.NewLine +
                  "DF Total IVA             =[" + _equis.getDF_TotalIVA() + "]" + Environment.NewLine +
                  "DF Total Otros Tributos  =[" + _equis.getDF_TotalTributos() + "]" + Environment.NewLine +
                  "NC Emitidas              =[" + _equis.getNC_CantidadEmitidos() + "]" + Environment.NewLine +
                  "NC Total                 =[" + _equis.getNC_Total() + "]" + Environment.NewLine +
                  "NC Total IVA             =[" + _equis.getNC_TotalIVA() + "]" + Environment.NewLine +
                  "NC Total Otros Tributos  =[" + _equis.getNC_TotalTributos() + "]" + Environment.NewLine +
                  "DNFH Emitidos            =[" + _equis.getDNFH_CantidadEmitidos() + "]";

                MessageBox.Show(new Form { TopMost = true }, _msj, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente Error : " + ex.Message,
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        private void rbComprimidoCtd_CheckedChanged(object sender, EventArgs e)
        {
            rbXmlCtd.Checked = !rbComprimidoCtd.Checked;
        }

        private void rbXmlCtd_CheckedChanged(object sender, EventArgs e)
        {
            rbComprimidoCtd.Checked = !rbXmlCtd.Checked;
        }

        private void btCtd_Click(object sender, EventArgs e)
        {
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaObtenerPrimerBloqueAuditoria bloque1 = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaObtenerPrimerBloqueAuditoria();
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaObtenerSiguienteBloqueAuditoria bloque2 = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaObtenerSiguienteBloqueAuditoria();
                hfl.argentina.HasarImpresoraFiscalRG3561.CompresionDeInformacion comp = new hfl.argentina.HasarImpresoraFiscalRG3561.CompresionDeInformacion();
                hfl.argentina.HasarImpresoraFiscalRG3561.XMLsPorBajada equisml = new hfl.argentina.HasarImpresoraFiscalRG3561.XMLsPorBajada();
                hfl.argentina.HasarImpresoraFiscalRG3561.TipoReporteZ zetapor = new hfl.argentina.HasarImpresoraFiscalRG3561.TipoReporteZ();
                string msj = "";                                                         // A mostrar en MsgBox()
                string arch = "prueba.dat";                                                        // Archivo donde almacenar las descarga.
                string fileReader = "";                                                  // Para leer el archivo ".a85"
                System.IO.FileStream oFileStream;                                   // Para generar el ZIP
                string archzip = _pathFile + "MemAudit.zip";                                    // Resultado final de la descarga. Opción por defecto.
                string arch85 = _pathFile + "Memaudit.a85";                                     // Archivo intermedio. Contenido en ASCII85.
                string archxml = _pathFile + "Memaudit.xml";                                    // Resultado final de la descarga. Opción no disponible.
                byte[] pp;                                                          // Almacenamiento de ASCII85 decodificado a binario
                Ascii85 ja = new Ascii85();                                           // Instancia de la clase de decodificación
                string desde;                                                       // Comienzo del rango de selección
                string hasta;                                                       // Final del rango de selección

            try
            {
                if (System.IO.File.Exists(archxml) || System.IO.File.Exists(archzip) || System.IO.File.Exists(arch85))
                {
                    msj = "Descarga CTD : Se encontraron archivos de descargas anteriores. Si continúa se perderá dicha información. " +
                        "¿ Desea continuar ?";
                    if (MessageBox.Show(new Form { TopMost = true }, msj, "ICG Argentina", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                    {
                        if (System.IO.File.Exists(archzip))
                            System.IO.File.Delete(archzip);
                        if (System.IO.File.Exists(arch))
                            System.IO.File.Delete(arch);
                        if (System.IO.File.Exists(arch85))
                            System.IO.File.Delete(arch85);
                    }
                    else
                        return;
                }

                msj = "CTD (Cinta Testigo Digital). A descargar información. Si acepta, ... POR FAVOR ESPERE ...Esta operación puede durar algunos minutos ...";
                if (MessageBox.Show(new Form { TopMost = true }, msj, "ICG Argentina", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.No)
                {
                    MessageBox.Show(new Form { TopMost = true }, "DESCARGA CANCELADA ! ...", "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (cbNumeros.Checked)
                {
                    if (!IsNumeric(txtDesdeCtd.Text) || !IsNumeric(txtHastaCtd.Text))
                    {
                        MessageBox.Show(new Form { TopMost = true }, "RANGO NUMÉRICO INCORRECTO !. Por favor, verifique que ambos datos sean numéricos.", "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
                else
                {
                    //Valido las fechas.
                    //No hago nada ya que vienen de dtp.
                }

                if (rbComprimidoCtd.Checked)
                {
                    comp = hfl.argentina.HasarImpresoraFiscalRG3561.CompresionDeInformacion.COMPRIME;
                    arch = arch85;
                }
                else
                {
                    comp = hfl.argentina.HasarImpresoraFiscalRG3561.CompresionDeInformacion.NO_COMPRIME;
                    arch = archxml;
                }

                if (rbXmlCtd.Checked)
                    equisml = hfl.argentina.HasarImpresoraFiscalRG3561.XMLsPorBajada.XML_UNICO;
                else
                    equisml = hfl.argentina.HasarImpresoraFiscalRG3561.XMLsPorBajada.VARIOS_XMLS;

                if (cbFechas.Checked)
                {
                    zetapor = hfl.argentina.HasarImpresoraFiscalRG3561.TipoReporteZ.REPORTE_Z_FECHA;
                    desde = dtpDesde.Value.Year.ToString().Substring(2, 2) + dtpDesde.Value.Month.ToString().PadLeft(2, '0') + dtpDesde.Value.Day.ToString().PadLeft(2, '0');
                    hasta = dtpHasta.Value.Year.ToString().Substring(2, 2) + dtpHasta.Value.Month.ToString().PadLeft(2, '0') + dtpHasta.Value.Day.ToString().PadLeft(2, '0');
                }
                else
                {
                    zetapor = hfl.argentina.HasarImpresoraFiscalRG3561.TipoReporteZ.REPORTE_Z_NUMERO;
                    desde = txtNroDesde.Text;
                    hasta = txtNroHasta.Text;
                }

                this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

                bloque1 = hasar.ObtenerPrimerBloqueAuditoria(desde, hasta, zetapor, comp, equisml);

                if (bloque1.getRegistro() == hfl.argentina.HasarImpresoraFiscalRG3561.IdentificadorBloque.BLOQUE_FINAL)
                {
                    MessageBox.Show(new Form { TopMost = true }, "CTD (Cinta testigo Digital)  No hay información disponible ! ...", "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    this.Cursor = System.Windows.Forms.Cursors.Arrow;
                }

                GrabarInfoBin(arch, bloque1.getInformacion());
                bloque2 = hasar.ObtenerSiguienteBloqueAuditoria();

                while (bloque2.getRegistro() != hfl.argentina.HasarImpresoraFiscalRG3561.IdentificadorBloque.BLOQUE_FINAL)
                {
                    GrabarInfoBin(arch, bloque2.getInformacion());
                    bloque2 = hasar.ObtenerSiguienteBloqueAuditoria();
                    Application.DoEvents();
                }

                if (bloque2.getRegistro() == hfl.argentina.HasarImpresoraFiscalRG3561.IdentificadorBloque.BLOQUE_FINAL)
                {
                    GrabarInfoBin(arch, bloque2.getInformacion());
                }

                this.Cursor = System.Windows.Forms.Cursors.Arrow;

                MessageBox.Show(new Form { TopMost = true }, "CTD (Cinta testigo Digital)  Descarga finalizada ! ", "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information);

                if (rbComprimidoCtd.Checked)
                {
                    fileReader = System.IO.File.ReadAllText(arch85);
                    pp = ja.Decode(fileReader);                // Convierte ASCII85 a binario
                    oFileStream = new FileStream(archzip, System.IO.FileMode.Create);
                    oFileStream.Write(pp, 0, pp.Length);
                    oFileStream.Close();
                }
            }
            catch (Exception ex)
            {
                this.Cursor = System.Windows.Forms.Cursors.Arrow;

                MessageBox.Show(new Form { TopMost = true }, "CTD (Cinta testigo Digital) Se produjo el siguiente Error: " + ex.Message,
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void cbNumeros_CheckedChanged(object sender, EventArgs e)
        {
            cbFechas.Checked = !cbNumeros.Checked;
            if (cbNumeros.Checked)
            {
                dtpDesdeCtd.Enabled = false;
                dtpHastaCtd.Enabled = false;
                txtDesdeCtd.Enabled = true;
                txtHastaCtd.Enabled = true;
            }
        }

        private void cbFechas_CheckedChanged(object sender, EventArgs e)
        {
            cbNumeros.Checked = !cbFechas.Checked;
            if (cbFechas.Checked)
            {
                dtpDesdeCtd.Enabled = true;
                dtpHastaCtd.Enabled = true;
                txtDesdeCtd.Enabled = false;
                txtHastaCtd.Enabled = false;
            }
        }

        private void btTerminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(new Form { TopMost = true }, "Desea Salir de la Consola del Controlador Fiscal?.", "ICG Argentina", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {
                this.Close();
            }
        }

    }
}
