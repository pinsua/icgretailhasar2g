﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IcgRetailConsolaH2G
{
    public partial class frmDocumentoAsociado : Form
    {
        public string _tipoComprobante;
        public int _puntoVenta;
        public int _nroComprobante;

        public frmDocumentoAsociado()
        {
            InitializeComponent();
        }

        private void btOk_Click(object sender, EventArgs e)
        {
            if (cboTipoComprobante.SelectedIndex == -1)
                MessageBox.Show(new Form { TopMost = true }, "Debe Seleccionar un Tipo de Comprobante", "ICG-Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
            {
                if (txtPtoVta.Text.Trim() == "")
                    MessageBox.Show(new Form { TopMost = true }, "Debe ngresar un Punto de Venta", "ICG-Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    if (txtNroComprobante.Text.Trim() == "")
                        MessageBox.Show(new Form { TopMost = true }, "Debe ingresar un Numero de Comprobante", "ICG-Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    else
                    {
                        //Proceso.
                        _tipoComprobante = cboTipoComprobante.SelectedItem.ToString();
                        _puntoVenta = Convert.ToInt32(txtPtoVta.Text);
                        _nroComprobante = Convert.ToInt32(txtNroComprobante.Text);
                        //Cierro
                        this.Close();
                    }
                }
            }
        }

        private void txtPtoVta_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Para obligar a que sólo se introduzcan números 
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
              if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso 
            {
                e.Handled = false;
            }
            else
            {
                //el resto de teclas pulsadas se desactivan 
                e.Handled = true;
            }
        }

        private void btClose_Click(object sender, EventArgs e)
        {

        }
    }
}
