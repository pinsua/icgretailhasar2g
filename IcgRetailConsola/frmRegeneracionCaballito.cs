﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Hasar2daGen;

namespace IcgRetailConsolaH2G
{
    public partial class frmRegeneracionCaballito : Form
    {
        public string _stringConnection;
        public string _pathSalida;
        public frmRegeneracionCaballito(string _strCon, string _path)
        {
            InitializeComponent();
            _stringConnection = _strCon;
            _pathSalida = _path;
        }

        private void btnRegenerar_Click(object sender, EventArgs e)
        {
            //Recupero los datos de las fechas.
            DateTime _desde = dateTimePicker1.Value;
            DateTime _hasta = dateTimePicker2.Value;
            string _TipoComprobante = "";
            string _NroComprobante = "";
            string _tipoCompronateCab = "";
            try
            {
                using (SqlConnection _conexion = new SqlConnection(_stringConnection))
                {
                    //Abrimos la conexion.
                    _conexion.Open();
                    //Recupero la lista de ticketsl
                    List<Tickets> _lst = Irsa.GetTikets(_desde, _hasta, _conexion);
                    //Recorro la lista.
                    foreach (Tickets tck in _lst)
                    {
                        _TipoComprobante = "";
                        _tipoCompronateCab = "";
                        Cabecera _cabecera = Cabecera.GetCabecera(tck.NumSerie, tck.NumFactura, tck.N, _conexion);
                        List<Pagos> _pagos = Pagos.GetPagos(tck.NumSerie, tck.NumFactura, tck.N, _conexion);
                        _NroComprobante = tck.SerieFiscal1.PadLeft(4, '0') + "-" + tck.NumeroFiscal.ToString().PadLeft(8, '0');
                        //Vemos el tipo de comprobante.
                        switch (_cabecera.descripcionticket.Substring(0, 3))
                        {
                            case "003":
                            case "008":
                                {
                                    _TipoComprobante = "C";
                                    _tipoCompronateCab = "C";
                                    break;
                                }
                            case "001":
                            case "006":
                                {
                                    _TipoComprobante = "D";
                                    _tipoCompronateCab = "V";
                                    break;
                                }
                        }
                        //Vemos si vamos por Caballito.
                        if (!String.IsNullOrEmpty(_pathSalida))
                            ShoppingCaballito.LanzarShoppingCaballito(_cabecera, _pagos, _pathSalida, _tipoCompronateCab, _NroComprobante, _conexion);

                    }
                }

                MessageBox.Show(new Form { TopMost = true }, "El proceso termino correctamente. El archivo se encuentra en " + _pathSalida,
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);

            }
            catch (Exception ex)
            {
                MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error: " + ex.Message + Environment.NewLine + ". Por favor comuniquese con ICG Argentina.",
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
