﻿using Hasar2daGen;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace IcgRetailConsolaH2G
{
    public partial class Form1 : Form
    {
        public static string _ip;
        public static string _server;
        public static string _user;
        public static string _catalog;
        public static SqlConnection _conn = new SqlConnection();
        public string _N = "B";
        public string _numfiscal;
        public static string _textoRegalo1;
        public static string _textoRegalo2;
        public static string _textoRegalo3;
        public static bool _tikcketRegalo = false;
        public static string _caja;
        public static string _terminal;
        public int _tipo = 12;
        public int _accion = 1;
        public static bool _hasarLog = false;
        public string strConnection;
        public static string _password;
        public static string _keyIcg = "";
        public static bool _KeyIsOk;
        public static string _ptoVtaManual = "";        
        public static InfoIrsa _infoIrsa = new InfoIrsa();
        public static string _pathCaballito = "";
        public static string _pathOlmos = "";
        public static bool _checkFiServ = false;
        public static string _cuit;
        Hasar2daGen.DatosControlador _datosControlador = new DatosControlador();

        [DllImport("user32.dll")]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        public Form1()
        {
            InitializeComponent();

            this.Text = this.Text + " Consola Retail - V." + Application.ProductVersion;
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (File.Exists("Config2daGeneracion.xml"))
            {
                XmlDocument xDoc = new XmlDocument();
                xDoc.Load("Config2daGeneracion.xml");
                
                //Nueva lectura
                _tikcketRegalo = false;
                //IP
                XmlNodeList _IP = xDoc.GetElementsByTagName("ip");
                if (_IP.Count > 0)
                    _ip = _IP[0].InnerText; ;
                
                //CAJA
                XmlNodeList _CAJA = xDoc.GetElementsByTagName("caja");
                if (_CAJA.Count > 0)
                    _caja = _CAJA[0].InnerText;
                //Password
                XmlNodeList _PASSWORD = xDoc.GetElementsByTagName("super");
                if (_PASSWORD.Count > 0)
                    _password = _PASSWORD[0].InnerText;
                //tktregalo1
                XmlNodeList _TKTREGALO1 = xDoc.GetElementsByTagName("tktregalo1");
                if (_TKTREGALO1.Count > 0)
                    _textoRegalo1 = _TKTREGALO1[0].InnerText;
                //tktregalo2
                XmlNodeList _TKTREGALO2 = xDoc.GetElementsByTagName("tktregalo2");
                if (_TKTREGALO2.Count > 0)
                    _textoRegalo2 = _TKTREGALO2[0].InnerText;
                //tktregalo3
                XmlNodeList _TKTREGALO3 = xDoc.GetElementsByTagName("tktregalo3");
                if (_TKTREGALO3.Count > 0)
                    _textoRegalo3 = _TKTREGALO3[0].InnerText;
                //keyICG
                XmlNodeList _KEYICG = xDoc.GetElementsByTagName("keyICG");
                if (_KEYICG.Count > 0)
                    _keyIcg = _KEYICG[0].InnerText;
                //HasarLog
                XmlNodeList _HASARLOG = xDoc.GetElementsByTagName("hasarlog");
                if (_HASARLOG.Count > 0)
                    _hasarLog = Convert.ToBoolean(_HASARLOG[0].InnerText);
                //PtoVtaManual
                XmlNodeList _PtoVtaManual = xDoc.GetElementsByTagName("ptovtamanual");
                if (_PtoVtaManual.Count > 0)
                    _ptoVtaManual = _PtoVtaManual[0].InnerText;               
                //Nueva Lectura
                //SEver
                XmlNodeList _SEVER = xDoc.GetElementsByTagName("server");
                if (_SEVER.Count > 0)
                    _server = _SEVER[0].InnerText;
                //USER
                XmlNodeList _USER = xDoc.GetElementsByTagName("user");
                if (_USER.Count > 0)
                    _user = _USER[0].InnerText;
                //CATALOG
                XmlNodeList _CATALOG = xDoc.GetElementsByTagName("database");
                if (_CATALOG.Count > 0)
                    _catalog = _CATALOG[0].InnerText;
                //Datos de Irsa.                
                //Nueva Lectura
                //IRSA CATALOG
                XmlNodeList _CONTRATO = xDoc.GetElementsByTagName("Contrato");
                if (_CONTRATO.Count > 0)
                    _infoIrsa.contrato = _CONTRATO[0].InnerText;
                //IRSA CATALOG
                XmlNodeList _LOCAL = xDoc.GetElementsByTagName("Local");
                if (_LOCAL.Count > 0)
                    _infoIrsa.local = _LOCAL[0].InnerText;
                //IRSA CATALOG
                XmlNodeList PATHSALIDAIRSA = xDoc.GetElementsByTagName("PathSalidaIrsa");
                if (PATHSALIDAIRSA.Count > 0)
                    _infoIrsa.pathSalida = PATHSALIDAIRSA[0].InnerText;
                //IRSA POS
                XmlNodeList POS = xDoc.GetElementsByTagName("Pos");
                if (POS.Count > 0)
                    _infoIrsa.pos = POS[0].InnerText;
                //IRSA RUBRO
                XmlNodeList RUBRO = xDoc.GetElementsByTagName("Rubro");
                if (RUBRO.Count > 0)
                    _infoIrsa.rubro = RUBRO[0].InnerText;
                //Datos Caballito
                XmlNodeList PATHSALIDACABALLITO = xDoc.GetElementsByTagName("PathSalidaCaballito");
                if (PATHSALIDACABALLITO.Count > 0)
                    _pathCaballito = PATHSALIDACABALLITO[0].InnerText;
                //Datos Olmos
                XmlNodeList PATHSALIDAOLMOS = xDoc.GetElementsByTagName("PathSalidaOlmos");
                if (PATHSALIDAOLMOS.Count > 0)
                    _pathOlmos = PATHSALIDAOLMOS[0].InnerText;
                //Check Fiserv
                XmlNodeList CHECKFISERV = xDoc.GetElementsByTagName("CheckFiServ");
                if (CHECKFISERV.Count > 0)
                    _checkFiServ = Convert.ToBoolean(CHECKFISERV[0].InnerText);
                //CUIT
                XmlNodeList CUIT = xDoc.GetElementsByTagName("Cuit");
                if (CUIT.Count > 0)
                    _cuit = CUIT[0].InnerText;
                //Armamos el stringConnection.
                strConnection = "Data Source=" + _server + ";Initial Catalog=" + _catalog + ";User Id=" + _user + ";Password=masterkey;";
                //Validamos el KeySoftware.
                //_KeyIsOk = ValidoKeySoftware(_keyIcg);
                _KeyIsOk = true;
                //Conectamos.
                _conn.ConnectionString = strConnection;
                //recupero el nombre de la Terminal.
                _terminal = Environment.MachineName;
                if (String.IsNullOrEmpty(_ip))
                {
                    //Recuperamos los datos del controlador.
                    _datosControlador = FuncionesVarias.GetDatosControlador(_ip);
                }
                try
                {
                    _conn.Open();
                    //Recuperamos los datos.
                    GetDataset(_conn);
                    //Actualizo los botones.
                    BotonesRegeneracion();
                    //Valido Licencia
                    //ValidateKey();
                }
                catch(Exception ex)
                {
                    MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error al intentar acceder a los datos." + 
                        Environment.NewLine + "Error: " + ex.Message + Environment.NewLine + "Por favor comuniquese con ICG Argentina.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                }
            }
            else
            {
                DeshabilitarBotones();
                MessageBox.Show(new Form { TopMost = true }, "No se encuentra el archivo de configuración. Por favor comuniquese con ICG Argentina.",
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
        }

        private bool ValidoKeySoftware(string keyIcg)
        {
            string _key = IcgVarios.LicenciaIcg.Value();
            //IcgVarios.LicenciaIcg.Value();

            string _cod = IcgVarios.NuevaLicencia.Value(_key);

            if (keyIcg == _cod)
                return true;
            else
                return false;
        }

        private void DeshabilitarBotones()
        {
            //Desabilito los botones.
            btImprimir.Enabled = false;
            btReimprimir.Enabled = false;
            btTicketRegalo.Enabled = false;
        }

        private void BotonesRegeneracion()
        {
            if (String.IsNullOrEmpty(_pathCaballito))
            {
                btnCaballito.Enabled = false;
                btnCaballito.Visible = false;
            }
            if(String.IsNullOrEmpty(_infoIrsa.pathSalida))
            {
                btnIrsa.Enabled = false;
                btnIrsa.Visible = false;
            }
            if(String.IsNullOrEmpty(_pathOlmos))
            {
                btnOlmos.Enabled = false;
                btnOlmos.Visible = false;
            }
        }

        private void btExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (_conn.State == ConnectionState.Open)
                _conn.Close();
        }

        #region Tickets
        private void GetDataset(SqlConnection _con)
        {
            string _sql; 

            if (rbSinImprimir.Checked)
            {
                _sql = "SELECT ALBVENTACAB.NUMSERIE, ALBVENTACAB.NUMALBARAN, ALBVENTACAB.N, ALBVENTACAB.NUMFAC as NUMFACTURA," +
                "ALBVENTACAB.TOTALBRUTO,  ALBVENTACAB.TOTALNETO, CLIENTES.NOMBRECLIENTE, FACTURASVENTASERIESRESOL.SERIEFISCAL1, FACTURASVENTASERIESRESOL.SERIEFISCAL2, " +
                "FACTURASVENTASERIESRESOL.NUMEROFISCAL, TIPOSDOC.DESCRIPCION, ALBVENTACAB.FECHA, ALBVENTACAB.Z " +
                "FROM FACTURASVENTASERIESRESOL RIGHT JOIN ALBVENTACAB on FACTURASVENTASERIESRESOL.NUMSERIE = ALBVENTACAB.NUMSERIE AND " +
                "FACTURASVENTASERIESRESOL.NUMFACTURA = ALBVENTACAB.NUMFAC AND FACTURASVENTASERIESRESOL.N = ALBVENTACAB.N " +
                "INNER JOIN CLIENTES ON ALBVENTACAB.CODCLIENTE = CLIENTES.CODCLIENTE " +
                "INNER JOIN TIPOSDOC ON ALBVENTACAB.TIPODOC = TIPOSDOC.TIPODOC " +
                "WHERE (FACTURASVENTASERIESRESOL.NUMEROFISCAL is null or FACTURASVENTASERIESRESOL.NUMEROFISCAL = '') " +
                "AND UPPER(CLIENTES.REGIMFACT) <> 'N' AND UPPER(CLIENTES.REGIMFACT) <> 'R' " +
               "AND ALBVENTACAB.NUMFAC > -1 AND  ALBVENTACAB.FECHA = @date AND ALBVENTACAB.N = @N AND ALBVENTACAB.CAJA = @caja " +
                "ORDER BY ALBVENTACAB.FECHA";
            }
            else
            {
                _sql = "SELECT ALBVENTACAB.NUMSERIE, ALBVENTACAB.NUMALBARAN, ALBVENTACAB.N, ALBVENTACAB.NUMFAC as NUMFACTURA," +
                "ALBVENTACAB.TOTALBRUTO,  ALBVENTACAB.TOTALNETO, CLIENTES.NOMBRECLIENTE, FACTURASVENTASERIESRESOL.SERIEFISCAL1, FACTURASVENTASERIESRESOL.SERIEFISCAL2, " +
                "FACTURASVENTASERIESRESOL.NUMEROFISCAL, TIPOSDOC.DESCRIPCION, ALBVENTACAB.FECHA, ALBVENTACAB.Z " +
                "FROM FACTURASVENTASERIESRESOL RIGHT JOIN ALBVENTACAB on FACTURASVENTASERIESRESOL.NUMSERIE = ALBVENTACAB.NUMSERIE AND " +
                "FACTURASVENTASERIESRESOL.NUMFACTURA = ALBVENTACAB.NUMFAC AND FACTURASVENTASERIESRESOL.N = ALBVENTACAB.N " +
                "INNER JOIN CLIENTES ON ALBVENTACAB.CODCLIENTE = CLIENTES.CODCLIENTE " +
                "INNER JOIN TIPOSDOC ON ALBVENTACAB.TIPODOC = TIPOSDOC.TIPODOC " +
                "WHERE (FACTURASVENTASERIESRESOL.NUMEROFISCAL is not null or FACTURASVENTASERIESRESOL.NUMEROFISCAL != '') AND " +
                "UPPER(CLIENTES.REGIMFACT) <> 'N' AND UPPER(CLIENTES.REGIMFACT) <> 'R' " +
                "AND ALBVENTACAB.NUMFAC > -1 AND ALBVENTACAB.FECHA = @date AND ALBVENTACAB.N = @N AND ALBVENTACAB.CAJA = @caja " +
                "ORDER BY ALBVENTACAB.FECHA";
            }

            //Limpio el dataset
            dtsImpresiones.Tables["Impresiones"].Clear();

            using (SqlCommand _cmd = new SqlCommand(_sql, _con))
            {
                _cmd.Parameters.AddWithValue("@date", dtpSeleccion.Value.Date);
                _cmd.Parameters.AddWithValue("@N", _N);
                _cmd.Parameters.AddWithValue("@caja", _caja);

                using (SqlDataAdapter _sda = new SqlDataAdapter(_cmd))
                {
                    _sda.Fill(dtsImpresiones, "Impresiones");
                }
            }
        }

        private void dtpSeleccion_ValueChanged(object sender, EventArgs e)
        {
            GetDataset(_conn);
        }

        private void grImpresiones_SelectionChanged(object sender, EventArgs e)
        {
            if (grImpresiones.RowCount > 0)
            {
                if (grImpresiones.SelectedRows.Count > 0)
                {
                    //object _numfiscal = grImpresiones.SelectedRows[0].Cells["NUMEROFISCAL"].Value;
                    _numfiscal = grImpresiones.SelectedRows[0].Cells[3].Value.ToString();
                    if (String.IsNullOrEmpty(_numfiscal))
                    {
                        btReimprimir.Enabled = false;
                        btTicketRegalo.Enabled = false;
                        btImprimir.Enabled = true;
                    }
                    else
                    {
                        if (grImpresiones.SelectedRows[0].Cells[4].Value.ToString().ToUpper().Contains("FACTURA")
                            || grImpresiones.SelectedRows[0].Cells[4].Value.ToString().ToUpper().Contains("TIQUET"))
                        {
                            btImprimir.Enabled = false;
                            btReimprimir.Enabled = true;
                            btTicketRegalo.Enabled = true;
                        }
                        else
                        {
                            btImprimir.Enabled = false;
                            btReimprimir.Enabled = true;
                            btTicketRegalo.Enabled = false;
                        }
                    }
                }
            }
        }
        
        private void btReimprimir_Click(object sender, EventArgs e)
        {
            try
            {
                if (_KeyIsOk)
                {
                    if (grImpresiones.RowCount > 0)
                    {
                        if (grImpresiones.SelectedRows.Count > 0)
                        {
                            string _descrip = grImpresiones.SelectedRows[0].Cells[4].Value.ToString();
                            string _numFactura = grImpresiones.SelectedRows[0].Cells[3].Value.ToString();
                            if (_descrip.Contains("Preimpreso"))
                            {
                                MessageBox.Show(new Form { TopMost = true }, "El comprobante seleccionado es un comprobante PREIMPRESO de carga manual." + Environment.NewLine +
                                    "Estos comprobante no se pueden REIMPIMIR, ya que no estan en la memoria del Controlador.",
                                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                            }
                            else
                            {
                                bool _faltaPapel;
                                string _resp = Hasar2daGen.Impresiones.Reimprimir(Convert.ToInt32(_numFactura), _descrip, _ip, out _faltaPapel);

                                if (!String.IsNullOrEmpty(_resp))
                                    MessageBox.Show(new Form { TopMost = true }, _resp, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);

                                if (_faltaPapel)
                                    MessageBox.Show(new Form { TopMost = true }, "La impresora fiscal informa que se esta quedando sin papel.Por favor revise el papel.",
                                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                            }
                        }
                    }
                }
                else
                    MessageBox.Show(new Form { TopMost = true }, "La licencia de ICG Argentina no es correcta." + Environment.NewLine + "Por favor comuniquese con ICG Argentina",
                                                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
            catch(Exception ex)
            {
                MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error: " + ex.Message,
                                           "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        private void btImprimir_Click(object sender, EventArgs e)
        {
            if (_KeyIsOk)
            {
                if (grImpresiones.RowCount > 0)
                {
                    if (grImpresiones.SelectedRows.Count > 0)
                    {
                        string _numserie = grImpresiones.SelectedRows[0].Cells[0].Value.ToString();
                        string _N = grImpresiones.SelectedRows[0].Cells[8].Value.ToString();
                        string _numFactura = grImpresiones.SelectedRows[0].Cells[1].Value.ToString();
                        string _ptoVta = grImpresiones.SelectedRows[0].Cells[2].Value.ToString();
                        string _nroFiscal = grImpresiones.SelectedRows[0].Cells[3].Value.ToString();
                        string _compro = _ptoVta + "-" + _nroFiscal.PadLeft(8, '0');
                        string _z = grImpresiones.SelectedRows[0].Cells[12].Value.ToString();
                        int _nroComprobante = 0;
                        int _nroPos = 0;

                        try
                        {
                            //Recuperamos los datos de la cabecera.
                            Hasar2daGen.Cabecera _cab = Hasar2daGen.Cabecera.GetCabecera(_numserie, Convert.ToInt32(_numFactura), _N, _conn);
                            List<Hasar2daGen.Items> _items = Hasar2daGen.Items.GetItems(_cab.numserie, _cab.n, _cab.numalbaran, _conn);
                            List<Hasar2daGen.OtrosTributos> _oTributos = Hasar2daGen.OtrosTributos.GetOtrosTributos(_numserie, _N, Convert.ToInt32(_numFactura), _conn);
                            List<Hasar2daGen.Recargos> _recargos = Hasar2daGen.Recargos.GetRecargos(_numserie, _N, Convert.ToInt32(_numFactura), _conn);

                            if (_items.Count > 0)
                            {
                                List<Hasar2daGen.Pagos> _pagos = Hasar2daGen.Pagos.GetPagos(_cab.numserie, _cab.numfac, _cab.n, _conn);
                                List<Hasar2daGen.Items> _lstItems = Hasar2daGen.Items.GetItems(_numserie, _N, _cab.numalbaran, _conn);

                                if (_pagos.Count > 0)
                                {
                                    bool _cuitOK = true;
                                    bool _rSocialOK = true;
                                    bool _hasChange = false;
                                    bool _faltaPapel = false;
                                    bool _cancelada = false;

                                    if (_cab.regfacturacioncliente.ToUpper() != "N" && _cab.regfacturacioncliente.ToUpper() != "R")
                                    {
                                        //Vemos si debemos validar el cuit.
                                        if (_cab.regfacturacioncliente != "4")
                                        {

                                            int _digitoValidador = Hasar2daGen.FuncionesVarias.CalcularDigitoCuit(_cab.nrodoccliente.Replace("-", ""));
                                            int _digitorecibido = Convert.ToInt16(_cab.nrodoccliente.Substring(_cab.nrodoccliente.Length - 1));
                                            if (_digitorecibido == _digitoValidador)
                                                _cuitOK = true;
                                            else
                                            {
                                                frmCuit _frm = new frmCuit();
                                                _frm.ShowDialog(this);
                                                if (String.IsNullOrEmpty(_frm._cuit))
                                                    _cuitOK = false;
                                                else
                                                {
                                                    _cab.nrodoccliente = _frm._cuit;
                                                    _cuitOK = true;
                                                }
                                                _frm.Dispose();
                                                _hasChange = true;
                                            }
                                        }
                                        else
                                        {
                                            //si el cliente es Hardcode los valores.
                                            if (_cab.codcliente == 0)
                                            {
                                                _cab.direccioncliente = ".";
                                                _cab.nombrecliente = "Consumidor Final";
                                                _cab.nrodoccliente = "0";
                                            }
                                            else
                                            {
                                                if (String.IsNullOrEmpty(_cab.nrodoccliente))
                                                    throw new Exception("El cliente no posee Número de Documento." + Environment.NewLine +
                                                        "Por favor ingrese el Número y luego imprímalo desde la consola.");
                                            }
                                        }
                                        //Validamos el campo direccion del cliente.
                                        if (String.IsNullOrEmpty(_cab.direccioncliente))
                                            _cab.direccioncliente = ".";
                                        //Validamos la Razon Social.
                                        if (String.IsNullOrEmpty(_cab.nombrecliente))
                                        {
                                            frmRazonSocial _frm = new frmRazonSocial();
                                            _frm.ShowDialog(this);
                                            if (String.IsNullOrEmpty(_frm._rSocial))
                                                _rSocialOK = false;
                                            else
                                            {
                                                _cab.nombrecliente = _frm._rSocial;
                                                _rSocialOK = true;
                                            }
                                            _frm.Dispose();
                                            _hasChange = true;
                                        }

                                        switch (_cab.descripcionticket.Substring(0, 3))
                                        {
                                            case "003":
                                            case "008":
                                                {
                                                    if (_cuitOK)
                                                    {
                                                        if (_rSocialOK)
                                                        {
                                                            //Busco documentos Asociados.
                                                            List<Hasar2daGen.DocumentoAsociado> _docAsoc = Hasar2daGen.DocumentoAsociado.GetDocumentoAsociado(_items[0]._abonoDeNumseie, _items[0]._abonoDe_N, _items[0]._abonoDeNumAlbaran, _conn);
                                                            //Valido que tenga al menos un comprobante asociado.
                                                            if (_docAsoc.Count() == 0)
                                                            {
                                                                frmDocumentoAsociado _frm = new frmDocumentoAsociado();
                                                                _frm.ShowDialog();
                                                                if (_frm._nroComprobante > 0)
                                                                {
                                                                    Hasar2daGen.DocumentoAsociado _newDoc = new DocumentoAsociado();
                                                                    _newDoc._seriefiscal1 = _frm._puntoVenta.ToString().PadLeft(4, '0');
                                                                    _newDoc._seriefiscal2 = _frm._tipoComprobante;
                                                                    _newDoc._numerofiscal = _frm._nroComprobante.ToString();
                                                                    _docAsoc.Add(_newDoc);
                                                                    _frm.Dispose();
                                                                }
                                                                else
                                                                {
                                                                    _frm.Dispose();
                                                                    MessageBox.Show(new Form { TopMost = true}, "La Nota de Crédito no se imprimirá." + Environment.NewLine +
                                                                                    "Por no tener Facturas asociadas."
                                                                                    , "ICG Argentina",
                                                                                    MessageBoxButtons.OK, MessageBoxIcon.Information);                                                                    
                                                                    break;
                                                                }
                                                            }

                                                            bool _necesitaCierreZ = false;
                                                            this.Cursor = Cursors.WaitCursor;
                                                            string _respImpre = Hasar2daGen.Impresiones.ImprimirNotaCreditoAB(_cab, _items, _pagos, _oTributos,
                                                                _recargos, _docAsoc, _ip, _conn, _hasarLog, 
                                                                out _faltaPapel, out _necesitaCierreZ, out _nroComprobante, out _nroPos, out _cancelada);
                                                            this.Cursor = Cursors.Default;

                                                            if (_necesitaCierreZ)
                                                            {
                                                                string _msg = "Ha llegado al horario del FINAL DE LA JORNADA FISCAL y debe realizar un Cierre Z. Desea realizarlo ahora y luego imprimir el tiquet?.";
                                                                //Preguntamos.
                                                                if (MessageBox.Show(new Form { TopMost = true }, _msg, "ICG Argentina", MessageBoxButtons.YesNo,
                                                                    MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                                                                {
                                                                    //Sacamos el Cierre Z.
                                                                    MessageBox.Show(new Form { TopMost = true }, Hasar2daGen.Cierres.ImprimirCierreZ(_ip, _hasarLog, _caja, _conn), "ICG Argentina", 
                                                                        MessageBoxButtons.OK,MessageBoxIcon.Information);
                                                                    //Volvemos a Lanzar la impresion
                                                                    this.Cursor = Cursors.WaitCursor;
                                                                    _respImpre = Hasar2daGen.Impresiones.ImprimirNotaCreditoAB(_cab, _items, _pagos, _oTributos, _recargos, _docAsoc,
                                                                        _ip, _conn, _hasarLog, 
                                                                        out _faltaPapel, out _necesitaCierreZ, out _nroComprobante, out _nroPos, out _cancelada);
                                                                    this.Cursor = Cursors.Default;
                                                                }
                                                                else
                                                                {
                                                                    //Cambiar texto de _respImprime.
                                                                    _respImpre = "Recuerde que para seguir facturando debe SI o SI realizar un CIERRE Z.";
                                                                }
                                                            }
                                                            if (!String.IsNullOrEmpty(_respImpre))
                                                            {
                                                                MessageBox.Show(new Form { TopMost = true }, _respImpre, "ICG Argentina",
                                                                       MessageBoxButtons.OK, MessageBoxIcon.Information);
                                                            }
                                                        }
                                                        else
                                                            MessageBox.Show(new Form { TopMost = true }, "El Cliente no posee Razon Social. Por favor ingresela y luego reimprimar el comprobante.",
                                                               "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                    }
                                                    else
                                                        MessageBox.Show(new Form { TopMost = true }, "El CUIT ingresado no es correcto. Por favor ingreselo correctamente y luego reimprimar el comprobante.",
                                                               "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);

                                                    break;
                                                }
                                            case "001":
                                            case "006":
                                                {
                                                    if (_cuitOK)
                                                    {
                                                        if (_rSocialOK)
                                                        {
                                                            bool _necesitaCierreZ = false;
                                                            this.Cursor = Cursors.WaitCursor;
                                                            string _respImpre = Hasar2daGen.Impresiones.ImprimirFacturasAB(_cab, _items, _pagos, _oTributos, _recargos, 
                                                                _ip, _conn, _hasarLog, _textoRegalo1, _textoRegalo2, _textoRegalo3, _tikcketRegalo,
                                                                out _faltaPapel, out _necesitaCierreZ, out _nroComprobante, out _nroPos, out _cancelada);
                                                            this.Cursor = Cursors.Default;

                                                            if (_necesitaCierreZ)
                                                            {
                                                                string _msg = "Ha llegado al horario del FINAL DE LA JORNADA FISCAL y debe realizar un Cierre Z. Desea realizarlo ahora y luego imprimir el tiquet?.";
                                                                //Preguntamos.
                                                                if (MessageBox.Show(new Form { TopMost = true }, _msg, "ICG Argentina", MessageBoxButtons.YesNo,
                                                                    MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                                                                {
                                                                    //Sacamos el Cierre Z.
                                                                    MessageBox.Show(new Form { TopMost = true }, Hasar2daGen.Cierres.ImprimirCierreZ(_ip, _hasarLog, _caja, _conn), "ICG Argentina",
                                                                       MessageBoxButtons.OK, MessageBoxIcon.Information);
                                                                    //Volvemos a Lanzar la impresion
                                                                    this.Cursor = Cursors.WaitCursor;
                                                                    _respImpre = Hasar2daGen.Impresiones.ImprimirFacturasAB(_cab, _items, _pagos, _oTributos, _recargos, 
                                                                        _ip, _conn, _hasarLog, _textoRegalo1, _textoRegalo2, _textoRegalo3, _tikcketRegalo,
                                                                        out _faltaPapel, out _necesitaCierreZ, out _nroComprobante, out _nroPos, out _cancelada);
                                                                    this.Cursor = Cursors.Default;
                                                                }
                                                                else
                                                                {
                                                                    //Cambiar texto de _respImprime.
                                                                    _respImpre = "Recuerde que para seguir facturando debe SI o SI realizar un CIERRE Z.";
                                                                }
                                                            }
                                                            if(!String.IsNullOrEmpty(_respImpre))
                                                            {
                                                                MessageBox.Show(new Form { TopMost = true }, _respImpre, "ICG Argentina",
                                                                       MessageBoxButtons.OK, MessageBoxIcon.Information);
                                                            }
                                                        }
                                                        else
                                                            MessageBox.Show(new Form { TopMost = true }, "El Cliente no posee Razon Social. Por favor ingresela y luego reimprimar el comprobante.",
                                                               "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                    }
                                                    else
                                                        MessageBox.Show(new Form { TopMost = true }, "El CUIT ingresado no es correcto. Por favor ingreselo correctamente y luego reimprimar el comprobante.",
                                                               "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                    break;
                                                }
                                            case "002":
                                            case "007":
                                                {
                                                    //Notas de Debito.
                                                    MessageBox.Show(new Form { TopMost = true }, "Notas de Debito. Aún no estan configuradas.",
                                                               "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                    break;
                                                }
                                            default:
                                                {
                                                    MessageBox.Show(new Form { TopMost = true }, "Tipo de Documento no configurado para su fiscalización.",
                                                               "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                    break;
                                                }
                                        }
                                        //vemos si tenemos modificacion.
                                        if (_hasChange)
                                        {
                                            Hasar2daGen.Clientes.UpdateCliente(_cab.nombrecliente, _cab.nrodoccliente, _cab.codcliente, _conn);
                                            Hasar2daGen.RemTransacciones.GraboRemTransacciones(_terminal, _caja, _z, _tipo, _accion, "", _cab.codcliente.ToString(), _N, _conn);
                                        }
                                        #region Shoppings
                                        //Vemos si tenemos que lanzar IRSA.
                                        if (!String.IsNullOrEmpty(_infoIrsa.pathSalida))
                                        {
                                            string _tipoComprobante = FuncionesVarias.QueComprobanteEs(_numserie, _numFactura, _N, _cab.codvendedor.ToString(), _conn);
                                            string _tipcompro = _tipoComprobante.Substring(0, 3) == "003" || _tipoComprobante.Substring(0, 3) == "008" ? "C" : "D";
                                            string _comprobante = _nroPos.ToString().PadLeft(4, '0') + "-" + _nroComprobante.ToString().PadLeft(8, '0');
                                            Irsa.GenerarIrsa(_cab, _pagos, _infoIrsa, _tipcompro, _comprobante, _conn);
                                        }
                                        //Vemos si tenemos que lanzar Shoppping Caballito.
                                        if (!String.IsNullOrEmpty(_pathCaballito))
                                        {
                                            string _comprobante = _nroPos.ToString().PadLeft(4, '0') + "-" + _nroComprobante.ToString().PadLeft(8, '0');
                                            ShoppingCaballito.GenerarShoppingCaballito(_numserie, _numFactura, _N, _conn, _pathCaballito, _comprobante);
                                        }
                                        //Vemos si tenemos que lanzar Shoppping Olmos.
                                        if (!String.IsNullOrEmpty(_pathOlmos))
                                        {
                                            MessageBox.Show(new Form { TopMost = true }, "Función Olmos no implementada.");
                                        }
                                        //Vemos si tenemos que mandar a IRSA SITEF.
                                        if (_checkFiServ)
                                        {
                                            if (!String.IsNullOrEmpty(_infoIrsa.pathSalida))
                                            {
                                                Hasar2daGen.SiTefService.SiTefServiceRetail.SendNormalInvoiceSql(_numserie, _numFactura, _N, _infoIrsa.local.PadLeft(8, '0')
                                                    , _infoIrsa.pos.PadLeft(8, '0'), _cuit, "30711277249", _infoIrsa.pathSalida, false, _conn);

                                            }
                                        }
                                        #endregion
                                        //Informamos la falta de papel.
                                        if (_faltaPapel)
                                            MessageBox.Show(new Form { TopMost = true }, "La impresora fiscal informa que se esta quedando sin papel.Por favor revise el papel.",
                                                "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);

                                        //Refrescamos al grid.
                                        GetDataset(_conn);
                                    }
                                }
                                else
                                {
                                    MessageBox.Show(new Form { TopMost = true }, "No existen Pagos registrados, verifique que el importe sea mayor a cero. Por favor revise el comprobante.",
                                                   "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                }
                            }
                            else
                            {
                                MessageBox.Show(new Form { TopMost = true }, "No existen Items registrados. Por favor revise el comprobante.",
                                                   "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                            }
                        }
                        catch(Exception ex)
                        {
                            if (ex.Message.StartsWith("El cliente no posee Número de Documento"))
                            {
                                MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error: " + ex.Message,
                                                       "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                            }
                            else
                            {
                                //ingreso Manual
                                if (MessageBox.Show(new Form { TopMost = true }, "No se pudo establecer comunicacion con la Controladora Fiscal." +
                                                    Environment.NewLine + "Desea ingresar el Nro. del Talonario?.",
                                                  "ICG Argentina", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                                {
                                    //Veo que tipo de comprobante es.
                                    //Recuperamos los datos de la cabecera.
                                    Hasar2daGen.Cabecera _cab = Hasar2daGen.Cabecera.GetCabecera(_numserie, Convert.ToInt32(_numFactura), _N, _conn);
                                    string _descripcion = _cab.descripcionticket;
                                    string _serieFiscalMan;
                                    SeriesResolucion _serieResol = SeriesResolucion.GetContador(_ptoVtaManual, _descripcion.Substring(0, 3), _conn);
                                    frmIngresoManual frm = new frmIngresoManual(_descripcion, _ptoVtaManual, _serieResol.contador, _conn);
                                    frm.ShowDialog();
                                    int _nroCbte = frm._nroCbte;
                                    frm.Dispose();
                                    if (_nroCbte > 0)
                                    {
                                        try
                                        {
                                            List<DocumentoAsociado> _lst = new List<DocumentoAsociado>();
                                            switch (_cab.descripcionticket.Substring(0, 3))
                                            {
                                                case "003":
                                                    {
                                                        _serieFiscalMan = "003_NC_Preimpreso_A";
                                                        break;
                                                    }
                                                case "008":
                                                    {
                                                        _serieFiscalMan = "008_NC_Preimpreso_B";
                                                        break;
                                                    }
                                                case "001":
                                                    {
                                                        _serieFiscalMan = "001_FC_Preimpreso_A";
                                                        break;
                                                    }
                                                case "006":
                                                    {
                                                        _serieFiscalMan = "006_FC_Preimpreso_B";
                                                        break;
                                                    }
                                                default:
                                                    {
                                                        _serieFiscalMan = "Sin_definirPreimpreso";
                                                        break;
                                                    }
                                            }
                                            Transacciones.TransaccionOK(_cab, _ptoVtaManual, _serieFiscalMan, _nroCbte, 0, _lst, _conn);
                                            _serieResol.contador = _nroCbte;
                                            SeriesResolucion.UpdateContadorSerieResolucion(_serieResol, _conn);
                                        }
                                        catch (Exception ex2)
                                        {
                                            string _numeroError = _ptoVtaManual.PadLeft(4, '0') + "-" + _nroCbte.ToString().PadLeft(8, '0');
                                            string _rta = "Hubo un Error y no se pudo grabar el numero de comprobante, " + _numeroError + Environment.NewLine +
                                                ", por favor ingreselo manualmente." + Environment.NewLine + "Error: " + ex2.Message;
                                            MessageBox.Show(new Form { TopMost = true }, _rta, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                        }
                                        //Refrescamos al grid.
                                        GetDataset(_conn);
                                    }
                                }
                            }
                        }
                    }

                }
            }
            else
                MessageBox.Show(new Form { TopMost = true }, "La licencia de ICG Argentina no es correcta." + Environment.NewLine + "Por favor comuniquese con ICG Argentina",
                                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
        }

        private void btTicketRegalo_Click(object sender, EventArgs e)
        {
            if (_KeyIsOk)
            {
                if (grImpresiones.RowCount > 0)
                {
                    if (grImpresiones.SelectedRows.Count > 0)
                    {
                        if (grImpresiones.SelectedRows[0].Cells[4].Value.ToString().ToUpper().StartsWith("001")
                            || grImpresiones.SelectedRows[0].Cells[4].Value.ToString().ToUpper().StartsWith("006")
                            || grImpresiones.SelectedRows[0].Cells[4].Value.ToString().ToUpper().StartsWith("011"))
                        {
                            string _numserie = grImpresiones.SelectedRows[0].Cells[0].Value.ToString();
                            string _N = grImpresiones.SelectedRows[0].Cells[8].Value.ToString();
                            string _numFactura = grImpresiones.SelectedRows[0].Cells[1].Value.ToString();
                            string _ptoVta = grImpresiones.SelectedRows[0].Cells[2].Value.ToString();
                            string _nroFiscal = grImpresiones.SelectedRows[0].Cells[3].Value.ToString();
                            string _compro = _ptoVta + "-" + _nroFiscal.PadLeft(8, '0');
                            Hasar2daGen.Cabecera _cabecera = Hasar2daGen.Cabecera.GetCabecera(_numserie, Convert.ToInt32(_numFactura), _N, _conn);
                            List<Hasar2daGen.Items> _lstItems = Hasar2daGen.Items.GetItems(_numserie, _N, _cabecera.numalbaran, _conn);

                            bool _faltaPapel;
                            Hasar2daGen.Impresiones.ImprimirTicketRegalo(_cabecera, _lstItems, _ip, _textoRegalo1, _textoRegalo2, _textoRegalo3,
                                _compro, _hasarLog, out _faltaPapel);

                            if (_faltaPapel)
                                MessageBox.Show(new Form { TopMost = true }, "La impresora fiscal informa que se esta quedando sin papel.Por favor revise el papel.",
                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                        }
                        else
                        {
                            MessageBox.Show(new Form { TopMost = true }, "Los tickets de regalo/cambio solo se pueden imprimir sobre una factura A o B.",
                                "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                        }
                    }
                }
            }
            else
                MessageBox.Show(new Form { TopMost = true }, "La licencia de ICG Argentina no es correcta." + Environment.NewLine + "Por favor comuniquese con ICG Argentina",
                                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
        }

        private void gbTipoB_CheckedChanged(object sender, EventArgs e)
        {
            if(rbTipoB.Checked)
                rbTipoN.Checked = false;
            //Cambio variable.
            _N = "B";
            //Cargo Dataset.
            GetDataset(_conn);

        }

        private void rbTipoN_CheckedChanged(object sender, EventArgs e)
        {
            if (rbTipoN.Checked)
                rbTipoB.Checked = false;
            //Cambio Variable.
            _N = "N";
            //Cargo Dataset.
            GetDataset(_conn);
        }

        private void rbImpresas_CheckedChanged(object sender, EventArgs e)
        {
            if (rbImpresas.Checked)
                rbSinImprimir.Checked = false;

            GetDataset(_conn);
        }

        private void rbSinImprimir_CheckedChanged(object sender, EventArgs e)
        {
            if (rbSinImprimir.Checked)
                rbImpresas.Checked = false;

            GetDataset(_conn);
        }
        #endregion

        private void GraboRemTransacciones(string _z, string _serie, string _numero, string _N)
        {
            string _sql = "INSERT INTO REM_TRANSACCIONES(TERMINAL, CAJA, CAJANUM, Z, TIPO, ACCION, SERIE, NUMERO, N, FECHA, HORA, FO, IDCENTRAL, TALLA, COLOR, CODIGOSTR) " +
                    "VALUES(@terminal, @caja, 0, @Z, 0, 1, @serie, @numero, @N, @date, @hora, 0, 1, '.', '.', '')";

            using (SqlCommand _cmd = new SqlCommand(_sql, _conn))
            {
                _cmd.Parameters.AddWithValue("@terminal", _terminal);
                _cmd.Parameters.AddWithValue("@caja", _caja);
                _cmd.Parameters.AddWithValue("@Z", Convert.ToInt32(_z));
                _cmd.Parameters.AddWithValue("@serie", _serie);
                _cmd.Parameters.AddWithValue("@numero", Convert.ToInt32(_numero));
                _cmd.Parameters.AddWithValue("@N", _N);
                _cmd.Parameters.AddWithValue("@date", DateTime.Now.Date);
                _cmd.Parameters.AddWithValue("@hora", DateTime.Now.Date);

                int rta =_cmd.ExecuteNonQuery();
                
            }
        }

        #region Cierres
        private void btInvocar_Click(object sender, EventArgs e)
        {
            try
            {
                if (_KeyIsOk)
                {
                    bool _rtaComprobantesOk = false;

                    _rtaComprobantesOk = Hasar2daGen.FuncionesVarias.TengoComprobantesSinImprimmir(_conn, _caja);

                    if (!_rtaComprobantesOk)
                    {
                        Process p = Process.GetProcessesByName("FrontRetail").FirstOrDefault();
                        if (p != null)
                        {
                            IntPtr h = p.MainWindowHandle;
                            SetForegroundWindow(h);
                            SendKeys.SendWait("%(z)");
                            SendKeys.Flush();
                        }
                    }
                    else
                    {
                        MessageBox.Show(new Form { TopMost = true }, "Posee comprobantes sin fiscalizar. Por favor fiscalize o anule los comprobantes para poder realizar el cierre Z.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                    }
                }
                else
                    MessageBox.Show(new Form { TopMost = true }, "La licencia de ICG Argentina no es correcta." + Environment.NewLine + "Por favor comuniquese con ICG Argentina",
                                                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error al intentar Realizar el Cierre. " + ex.Message,
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        private void btReimpresionZ_Click(object sender, EventArgs e)
        {
            if (_KeyIsOk)
            {
                frmReimpresion frm = new frmReimpresion(_caja, "Z", _ip, strConnection);
                frm.ShowDialog(this);
                frm.Dispose();
            }
            else
                MessageBox.Show(new Form { TopMost = true }, "La licencia de ICG Argentina no es correcta." + Environment.NewLine + "Por favor comuniquese con ICG Argentina",
                                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
        }

        private void btCierreX_Click(object sender, EventArgs e)
        {
            if (_KeyIsOk)
            {
                DialogResult result = MessageBox.Show(new Form { TopMost = true }, "Seguro que desea realizar un Cierre X?.", "ICG - Argentina",
                    MessageBoxButtons.YesNo);
                switch (result)
                {
                    case DialogResult.Yes:
                        {
                            hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();
                            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarJornadaFiscal _cierre = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarJornadaFiscal();
                            hfl.argentina.HasarImpresoraFiscalRG3561.CerrarJornadaFiscalX _equis = new hfl.argentina.HasarImpresoraFiscalRG3561.CerrarJornadaFiscalX();

                            string _msj;

                            try
                            {
                                //Conectamos.
                                Hasar2daGen.Impresiones.Conectar(hasar, false, _ip);
                                //ejecutamos el cierre Z.
                                _cierre = hasar.CerrarJornadaFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.TipoReporte.REPORTE_X);
                                _equis = _cierre.X;

                                _msj = "INFORME DIARIO DE CIERRE :" + Environment.NewLine +
                                  "Cierre 'X' Nº            =[" + _equis.getNumero() + "]" + Environment.NewLine +
                                  "Fecha del Cierre         =[" + _equis.getFechaCierre() + "]" + Environment.NewLine +
                                  "DF Emitidos              =[" + _equis.getDF_CantidadEmitidos() + "]" + Environment.NewLine +
                                  "DF Total                 =[" + _equis.getDF_Total() + "]" + Environment.NewLine +
                                  "DF Total IVA             =[" + _equis.getDF_TotalIVA() + "]" + Environment.NewLine +
                                  "DF Total Otros Tributos  =[" + _equis.getDF_TotalTributos() + "]" + Environment.NewLine +
                                  "NC Emitidas              =[" + _equis.getNC_CantidadEmitidos() + "]" + Environment.NewLine +
                                  "NC Total                 =[" + _equis.getNC_Total() + "]" + Environment.NewLine +
                                  "NC Total IVA             =[" + _equis.getNC_TotalIVA() + "]" + Environment.NewLine +
                                  "NC Total Otros Tributos  =[" + _equis.getNC_TotalTributos() + "]" + Environment.NewLine +
                                  "DNFH Emitidos            =[" + _equis.getDNFH_CantidadEmitidos() + "]";

                                MessageBox.Show(new Form { TopMost = true }, _msj, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                            }
                            catch (Exception ex)
                            {
                                Hasar2daGen.Impresiones.Cancelar(hasar);
                                MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente Error : " + ex.Message,
                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                            }

                            break;
                        }
                    case DialogResult.No:
                        {
                            this.Text = "[Cancel]";
                            break;
                        }
                }
            }
            else
                MessageBox.Show(new Form { TopMost = true }, "La licencia de ICG Argentina no es correcta." + Environment.NewLine + "Por favor comuniquese con ICG Argentina",
                                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
        }

        private void btCierreZ_Click(object sender, EventArgs e)
        {
            if (_KeyIsOk)
            {
                frmCierreZ frm = new frmCierreZ(_ip, strConnection, _caja, false);
                frm.ShowDialog(this);
                frm.Dispose();
            }
            else
                MessageBox.Show(new Form { TopMost = true }, "La licencia de ICG Argentina no es correcta." + Environment.NewLine + "Por favor comuniquese con ICG Argentina",
                                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
        }

        private void btForzarZ_Click(object sender, EventArgs e)
        {
            bool _pasoOk = true;
            string _msjCbte;

            if (_KeyIsOk)
            {
                if (!String.IsNullOrEmpty(_password))
                {
                    frmPassword frm = new frmPassword();
                    frm.ShowDialog();
                    string _super = frm._Text;
                    if (_password != _super)
                        _pasoOk = false;

                }
                if (_pasoOk)
                {
                    try
                    {
                        //Vemos si tenemos comprobantes sin fiscalizar y armamos el MSJ.
                        if (Hasar2daGen.FuncionesVarias.TengoComprobantesSinImprimmir(_conn, _caja))
                            _msjCbte = "POSEE COMPROBANTES SIN FISCALIZAR. " + Environment.NewLine + "Desea continuar con la impresión del Z fiscal?.";
                        else
                            _msjCbte = "Desea continuar con la impresión del Z fiscal?.";

                        if (MessageBox.Show(new Form { TopMost = true }, _msjCbte, "ICG Argentina", MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                            MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                        {
                            string _msj = Cierres.ImprimirCierreZ(_ip, _hasarLog, _caja, _conn);
                            MessageBox.Show(new Form { TopMost = true }, _msj, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                        }
                    }
                    catch(Exception ex)
                    {
                        MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error: " + Environment.NewLine + ex.Message,
                                                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                    }
                }
                else
                    MessageBox.Show(new Form { TopMost = true }, "No posee permisos para esta acción." + Environment.NewLine + "Por favor comuniquese con ICG Argentina",
                                                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
            else
                MessageBox.Show(new Form { TopMost = true }, "La licencia de ICG Argentina no es correcta." + Environment.NewLine + "Por favor comuniquese con ICG Argentina",
                                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
        }
        #endregion

        #region Shoppings
        private void button1_Click(object sender, EventArgs e)
        {
            if (_KeyIsOk)
            {
                frmRegeneracionIrsa _frm = new frmRegeneracionIrsa(strConnection, _infoIrsa);
                _frm.ShowDialog(this);
                _frm.Dispose();
            }
            else
                MessageBox.Show(new Form { TopMost = true }, "La licencia de ICG Argentina no es correcta." + Environment.NewLine + "Por favor comuniquese con ICG Argentina",
                                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (_KeyIsOk)
            {
                frmRegeneracionCaballito _frm = new frmRegeneracionCaballito(strConnection, _pathCaballito);
                _frm.ShowDialog(this);
                _frm.Dispose();
            }
            else
                MessageBox.Show(new Form { TopMost = true }, "La licencia de ICG Argentina no es correcta." + Environment.NewLine + "Por favor comuniquese con ICG Argentina",
                                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
        }

        #endregion

        private void btnAFIP_Click(object sender, EventArgs e)
        {
            if (_KeyIsOk)
            {
                frmConsola _frm = new frmConsola(_ip);
                _frm.ShowDialog(this);
                _frm.Dispose();
            }
            else
                MessageBox.Show(new Form { TopMost = true }, "La licencia de ICG Argentina no es correcta." + Environment.NewLine + "Por favor comuniquese con ICG Argentina",
                                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
        }

        private void btnOlmos_Click(object sender, EventArgs e)
        {
            if (_KeyIsOk)
            {
                MessageBox.Show(new Form { TopMost = true }, "Metodo no implementado.");
            }
            else
                MessageBox.Show(new Form { TopMost = true }, "La licencia de ICG Argentina no es correcta." + Environment.NewLine + "Por favor comuniquese con ICG Argentina",
                                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            bool _pasoOk = true;

            if (!String.IsNullOrEmpty(_password))
            {
                frmPassword frm = new frmPassword();
                frm.ShowDialog();
                string _super = frm._Text;
                if (_password != _super)
                    _pasoOk = false;

            }
            if (_pasoOk)
            {
                frmConfig frm = new frmConfig();
                frm.ShowDialog(this);
                frm.Dispose();
            }
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void ValidateKey()
        {
            if (DateTime.Now.Day < 5)
            {
                if (!String.IsNullOrEmpty(_keyIcg))
                {
                    if (_datosControlador.Cuit != null)
                    {
                        try
                        {
                            Licencia lic = new Licencia();
#if DEBUG
                            lic.ClientCuit = "12345667";
#else
                            lic.ClientCuit = _datosControlador.Cuit;
#endif
                            lic.ClientName = _datosControlador.RazonSocial;
                            lic.ClientRazonSocial = _datosControlador.RazonSocial;
                            using (SqlCommand cmd = new SqlCommand())
                            {
                                cmd.Connection = _conn;
                                cmd.CommandType = CommandType.Text;
                                cmd.CommandText = "SELECT VERSION from [VERSION]";
                                using (SqlDataReader reader = cmd.ExecuteReader())
                                {
                                    if (reader.HasRows)
                                    {
                                        while (reader.Read())
                                        {
                                            lic.Version = reader["VERSION"].ToString();
                                        }
                                    }
                                }
                            }
                            lic.ClientKey = IcgVarios.LicenciaIcg.Value();
                            lic.password = "Pinsua.2730";
                            lic.Plataforma = "RETAIL";
                            lic.Release = Application.ProductVersion;
                            lic.user = "pinsua@yahoo.com";
                            lic.Tipo = "Retail H2G";
                            lic.TerminalName = Environment.MachineName;
                            lic.PointOfSale = _datosControlador.NroPos;
                            var json = JsonConvert.SerializeObject(lic);
#if DEBUG
                        var client = new RestClient("http://localhost:18459/api/Licencias/GetKey");
#else
                            var client = new RestClient("http://pkicg1.ddns.net:8095/api/Licencias/GetKey");
#endif
                            client.Timeout = -1;
                            var request = new RestRequest(Method.POST);
                            request.AddHeader("Content-Type", "application/json");
                            request.AddParameter("application/json", json, ParameterType.RequestBody);
                            IRestResponse response = client.Execute(request);
                            if (response.StatusCode == System.Net.HttpStatusCode.OK)
                            {
                                ResponcePostLicencia res = JsonConvert.DeserializeObject<ResponcePostLicencia>(response.Content);

                                //Recupero los datos.
                                XmlDocument xDoc = new XmlDocument();
                                xDoc.Load("Config2daGeneracion.xml");
                                //keyICG
                                XmlNodeList _KEYICG = xDoc.GetElementsByTagName("keyICG");
                                if (_KEYICG.Count > 0)
                                    _KEYICG[0].InnerXml = res.Key;
                                else
                                {
                                    XmlElement elem = xDoc.CreateElement("keyICG");
                                    XmlText text = xDoc.CreateTextNode(res.Key);
                                    xDoc.DocumentElement.AppendChild(elem);
                                    xDoc.DocumentElement.LastChild.AppendChild(text);
                                }
                                //Guardamos los datos.
                                xDoc.Save("Config2daGeneracion.xml");
                                if (String.IsNullOrEmpty(res.Key))
                                {
                                    //Muestro un Mensaje.
                                    MessageBox.Show(new Form { TopMost = true }, "Su licencia no esta habilitada." + Environment.NewLine +
                                        "Por favor comuniquese con ICG Argentina", "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    this.Close();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Se produjo el siguiente error: " + Environment.NewLine +
                                ex.Message + Environment.NewLine + "Por favor comuníquese con ICG Argentina para obtener una licencia.",
                            "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
            }
        }
    }
}
