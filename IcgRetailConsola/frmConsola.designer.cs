﻿namespace IcgRetailConsolaH2G
{
    partial class frmConsola
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbCierres = new System.Windows.Forms.GroupBox();
            this.btnCierreX = new System.Windows.Forms.Button();
            this.btnCierreZ = new System.Windows.Forms.Button();
            this.gbAuditoria = new System.Windows.Forms.GroupBox();
            this.rbAfipMemFiscal = new System.Windows.Forms.RadioButton();
            this.rbAfipCompleto = new System.Windows.Forms.RadioButton();
            this.lblFechaH = new System.Windows.Forms.Label();
            this.lblFechaD = new System.Windows.Forms.Label();
            this.dtpHasta = new System.Windows.Forms.DateTimePicker();
            this.dtpDesde = new System.Windows.Forms.DateTimePicker();
            this.btnAditoria = new System.Windows.Forms.Button();
            this.gbDatosCF = new System.Windows.Forms.GroupBox();
            this.btTerminar = new System.Windows.Forms.Button();
            this.btConectar = new System.Windows.Forms.Button();
            this.txtProtocoloCF = new System.Windows.Forms.TextBox();
            this.txtVersion = new System.Windows.Forms.TextBox();
            this.txtFirmware = new System.Windows.Forms.TextBox();
            this.txtModelo = new System.Windows.Forms.TextBox();
            this.txtProducto = new System.Windows.Forms.TextBox();
            this.txtRevisionDll = new System.Windows.Forms.TextBox();
            this.txtProtocoloDll = new System.Windows.Forms.TextBox();
            this.lblProtocoloCF = new System.Windows.Forms.Label();
            this.lblVersion = new System.Windows.Forms.Label();
            this.lblFirmware = new System.Windows.Forms.Label();
            this.lblModelo = new System.Windows.Forms.Label();
            this.lblProducto = new System.Windows.Forms.Label();
            this.lblRevision = new System.Windows.Forms.Label();
            this.lblProtocolo = new System.Windows.Forms.Label();
            this.txtIp = new System.Windows.Forms.TextBox();
            this.lblIP = new System.Windows.Forms.Label();
            this.gbReporteComprobantes = new System.Windows.Forms.GroupBox();
            this.btReporteCbte = new System.Windows.Forms.Button();
            this.lblTipoComprobante = new System.Windows.Forms.Label();
            this.cboTipoCbte = new System.Windows.Forms.ComboBox();
            this.rbXml = new System.Windows.Forms.RadioButton();
            this.rbComprimido = new System.Windows.Forms.RadioButton();
            this.txtNroHasta = new System.Windows.Forms.TextBox();
            this.txtNroDesde = new System.Windows.Forms.TextBox();
            this.lblNroHasta = new System.Windows.Forms.Label();
            this.lblNroDesde = new System.Windows.Forms.Label();
            this.gbCtd = new System.Windows.Forms.GroupBox();
            this.cbNumeros = new System.Windows.Forms.CheckBox();
            this.cbFechas = new System.Windows.Forms.CheckBox();
            this.btCtd = new System.Windows.Forms.Button();
            this.rbXmlCtd = new System.Windows.Forms.RadioButton();
            this.rbComprimidoCtd = new System.Windows.Forms.RadioButton();
            this.txtHastaCtd = new System.Windows.Forms.TextBox();
            this.txtDesdeCtd = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpHastaCtd = new System.Windows.Forms.DateTimePicker();
            this.dtpDesdeCtd = new System.Windows.Forms.DateTimePicker();
            this.lblHastaCtd = new System.Windows.Forms.Label();
            this.lblDesdeCtd = new System.Windows.Forms.Label();
            this.gbCierres.SuspendLayout();
            this.gbAuditoria.SuspendLayout();
            this.gbDatosCF.SuspendLayout();
            this.gbReporteComprobantes.SuspendLayout();
            this.gbCtd.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbCierres
            // 
            this.gbCierres.Controls.Add(this.btnCierreX);
            this.gbCierres.Controls.Add(this.btnCierreZ);
            this.gbCierres.Location = new System.Drawing.Point(12, 321);
            this.gbCierres.Name = "gbCierres";
            this.gbCierres.Size = new System.Drawing.Size(292, 57);
            this.gbCierres.TabIndex = 2;
            this.gbCierres.TabStop = false;
            this.gbCierres.Text = "Cierres";
            this.gbCierres.Visible = false;
            // 
            // btnCierreX
            // 
            this.btnCierreX.Location = new System.Drawing.Point(93, 19);
            this.btnCierreX.Name = "btnCierreX";
            this.btnCierreX.Size = new System.Drawing.Size(75, 23);
            this.btnCierreX.TabIndex = 1;
            this.btnCierreX.Text = "Cierre X";
            this.btnCierreX.UseVisualStyleBackColor = true;
            this.btnCierreX.Click += new System.EventHandler(this.btnCierreX_Click);
            // 
            // btnCierreZ
            // 
            this.btnCierreZ.Location = new System.Drawing.Point(12, 19);
            this.btnCierreZ.Name = "btnCierreZ";
            this.btnCierreZ.Size = new System.Drawing.Size(75, 23);
            this.btnCierreZ.TabIndex = 0;
            this.btnCierreZ.Text = "Cierre Z";
            this.btnCierreZ.UseVisualStyleBackColor = true;
            this.btnCierreZ.Click += new System.EventHandler(this.btnCierreZ_Click);
            // 
            // gbAuditoria
            // 
            this.gbAuditoria.Controls.Add(this.rbAfipMemFiscal);
            this.gbAuditoria.Controls.Add(this.rbAfipCompleto);
            this.gbAuditoria.Controls.Add(this.lblFechaH);
            this.gbAuditoria.Controls.Add(this.lblFechaD);
            this.gbAuditoria.Controls.Add(this.dtpHasta);
            this.gbAuditoria.Controls.Add(this.dtpDesde);
            this.gbAuditoria.Controls.Add(this.btnAditoria);
            this.gbAuditoria.Location = new System.Drawing.Point(310, 12);
            this.gbAuditoria.Name = "gbAuditoria";
            this.gbAuditoria.Size = new System.Drawing.Size(328, 125);
            this.gbAuditoria.TabIndex = 3;
            this.gbAuditoria.TabStop = false;
            this.gbAuditoria.Text = "Reporte AFIP (Obligatorio)";
            // 
            // rbAfipMemFiscal
            // 
            this.rbAfipMemFiscal.AutoSize = true;
            this.rbAfipMemFiscal.Location = new System.Drawing.Point(151, 54);
            this.rbAfipMemFiscal.Name = "rbAfipMemFiscal";
            this.rbAfipMemFiscal.Size = new System.Drawing.Size(121, 17);
            this.rbAfipMemFiscal.TabIndex = 6;
            this.rbAfipMemFiscal.Text = "AFIP Memoria Fiscal";
            this.rbAfipMemFiscal.UseVisualStyleBackColor = true;
            this.rbAfipMemFiscal.CheckedChanged += new System.EventHandler(this.rbAfipMemFiscal_CheckedChanged);
            // 
            // rbAfipCompleto
            // 
            this.rbAfipCompleto.AutoSize = true;
            this.rbAfipCompleto.Checked = true;
            this.rbAfipCompleto.Location = new System.Drawing.Point(43, 54);
            this.rbAfipCompleto.Name = "rbAfipCompleto";
            this.rbAfipCompleto.Size = new System.Drawing.Size(95, 17);
            this.rbAfipCompleto.TabIndex = 5;
            this.rbAfipCompleto.TabStop = true;
            this.rbAfipCompleto.Text = "AFIP Completo";
            this.rbAfipCompleto.UseVisualStyleBackColor = true;
            this.rbAfipCompleto.CheckedChanged += new System.EventHandler(this.rbAfipCompleto_CheckedChanged);
            // 
            // lblFechaH
            // 
            this.lblFechaH.AutoSize = true;
            this.lblFechaH.Location = new System.Drawing.Point(167, 28);
            this.lblFechaH.Name = "lblFechaH";
            this.lblFechaH.Size = new System.Drawing.Size(35, 13);
            this.lblFechaH.TabIndex = 4;
            this.lblFechaH.Text = "Hasta";
            // 
            // lblFechaD
            // 
            this.lblFechaD.AutoSize = true;
            this.lblFechaD.Location = new System.Drawing.Point(9, 28);
            this.lblFechaD.Name = "lblFechaD";
            this.lblFechaD.Size = new System.Drawing.Size(38, 13);
            this.lblFechaD.TabIndex = 3;
            this.lblFechaD.Text = "Desde";
            // 
            // dtpHasta
            // 
            this.dtpHasta.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpHasta.Location = new System.Drawing.Point(207, 23);
            this.dtpHasta.Name = "dtpHasta";
            this.dtpHasta.Size = new System.Drawing.Size(104, 20);
            this.dtpHasta.TabIndex = 2;
            // 
            // dtpDesde
            // 
            this.dtpDesde.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDesde.Location = new System.Drawing.Point(52, 23);
            this.dtpDesde.Name = "dtpDesde";
            this.dtpDesde.Size = new System.Drawing.Size(104, 20);
            this.dtpDesde.TabIndex = 1;
            // 
            // btnAditoria
            // 
            this.btnAditoria.Location = new System.Drawing.Point(50, 86);
            this.btnAditoria.Name = "btnAditoria";
            this.btnAditoria.Size = new System.Drawing.Size(208, 23);
            this.btnAditoria.TabIndex = 0;
            this.btnAditoria.Text = "Descargar Información";
            this.btnAditoria.UseVisualStyleBackColor = true;
            this.btnAditoria.Click += new System.EventHandler(this.btnAditoria_Click);
            // 
            // gbDatosCF
            // 
            this.gbDatosCF.Controls.Add(this.btTerminar);
            this.gbDatosCF.Controls.Add(this.btConectar);
            this.gbDatosCF.Controls.Add(this.txtProtocoloCF);
            this.gbDatosCF.Controls.Add(this.txtVersion);
            this.gbDatosCF.Controls.Add(this.txtFirmware);
            this.gbDatosCF.Controls.Add(this.txtModelo);
            this.gbDatosCF.Controls.Add(this.txtProducto);
            this.gbDatosCF.Controls.Add(this.txtRevisionDll);
            this.gbDatosCF.Controls.Add(this.txtProtocoloDll);
            this.gbDatosCF.Controls.Add(this.lblProtocoloCF);
            this.gbDatosCF.Controls.Add(this.lblVersion);
            this.gbDatosCF.Controls.Add(this.lblFirmware);
            this.gbDatosCF.Controls.Add(this.lblModelo);
            this.gbDatosCF.Controls.Add(this.lblProducto);
            this.gbDatosCF.Controls.Add(this.lblRevision);
            this.gbDatosCF.Controls.Add(this.lblProtocolo);
            this.gbDatosCF.Controls.Add(this.txtIp);
            this.gbDatosCF.Controls.Add(this.lblIP);
            this.gbDatosCF.Location = new System.Drawing.Point(12, 12);
            this.gbDatosCF.Name = "gbDatosCF";
            this.gbDatosCF.Size = new System.Drawing.Size(292, 303);
            this.gbDatosCF.TabIndex = 5;
            this.gbDatosCF.TabStop = false;
            this.gbDatosCF.Text = "Datos Controlador Fiscal";
            // 
            // btTerminar
            // 
            this.btTerminar.Location = new System.Drawing.Point(101, 254);
            this.btTerminar.Name = "btTerminar";
            this.btTerminar.Size = new System.Drawing.Size(75, 23);
            this.btTerminar.TabIndex = 17;
            this.btTerminar.Text = "&Terminar";
            this.btTerminar.UseVisualStyleBackColor = true;
            this.btTerminar.Click += new System.EventHandler(this.btTerminar_Click);
            // 
            // btConectar
            // 
            this.btConectar.Location = new System.Drawing.Point(20, 254);
            this.btConectar.Name = "btConectar";
            this.btConectar.Size = new System.Drawing.Size(75, 23);
            this.btConectar.TabIndex = 16;
            this.btConectar.Text = "&Conectar";
            this.btConectar.UseVisualStyleBackColor = true;
            this.btConectar.Click += new System.EventHandler(this.btConectar_Click);
            // 
            // txtProtocoloCF
            // 
            this.txtProtocoloCF.Enabled = false;
            this.txtProtocoloCF.Location = new System.Drawing.Point(95, 213);
            this.txtProtocoloCF.Name = "txtProtocoloCF";
            this.txtProtocoloCF.Size = new System.Drawing.Size(177, 20);
            this.txtProtocoloCF.TabIndex = 15;
            // 
            // txtVersion
            // 
            this.txtVersion.Enabled = false;
            this.txtVersion.Location = new System.Drawing.Point(95, 187);
            this.txtVersion.Name = "txtVersion";
            this.txtVersion.Size = new System.Drawing.Size(177, 20);
            this.txtVersion.TabIndex = 14;
            // 
            // txtFirmware
            // 
            this.txtFirmware.Enabled = false;
            this.txtFirmware.Location = new System.Drawing.Point(94, 161);
            this.txtFirmware.Name = "txtFirmware";
            this.txtFirmware.Size = new System.Drawing.Size(177, 20);
            this.txtFirmware.TabIndex = 13;
            // 
            // txtModelo
            // 
            this.txtModelo.Enabled = false;
            this.txtModelo.Location = new System.Drawing.Point(94, 135);
            this.txtModelo.Name = "txtModelo";
            this.txtModelo.Size = new System.Drawing.Size(177, 20);
            this.txtModelo.TabIndex = 12;
            // 
            // txtProducto
            // 
            this.txtProducto.Enabled = false;
            this.txtProducto.Location = new System.Drawing.Point(95, 109);
            this.txtProducto.Name = "txtProducto";
            this.txtProducto.Size = new System.Drawing.Size(177, 20);
            this.txtProducto.TabIndex = 11;
            // 
            // txtRevisionDll
            // 
            this.txtRevisionDll.Enabled = false;
            this.txtRevisionDll.Location = new System.Drawing.Point(94, 83);
            this.txtRevisionDll.Name = "txtRevisionDll";
            this.txtRevisionDll.Size = new System.Drawing.Size(177, 20);
            this.txtRevisionDll.TabIndex = 10;
            // 
            // txtProtocoloDll
            // 
            this.txtProtocoloDll.Enabled = false;
            this.txtProtocoloDll.Location = new System.Drawing.Point(95, 57);
            this.txtProtocoloDll.Name = "txtProtocoloDll";
            this.txtProtocoloDll.Size = new System.Drawing.Size(177, 20);
            this.txtProtocoloDll.TabIndex = 9;
            // 
            // lblProtocoloCF
            // 
            this.lblProtocoloCF.AutoSize = true;
            this.lblProtocoloCF.Location = new System.Drawing.Point(15, 216);
            this.lblProtocoloCF.Name = "lblProtocoloCF";
            this.lblProtocoloCF.Size = new System.Drawing.Size(68, 13);
            this.lblProtocoloCF.TabIndex = 8;
            this.lblProtocoloCF.Text = "Protocolo CF";
            // 
            // lblVersion
            // 
            this.lblVersion.AutoSize = true;
            this.lblVersion.Location = new System.Drawing.Point(17, 190);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(42, 13);
            this.lblVersion.TabIndex = 7;
            this.lblVersion.Text = "Version";
            // 
            // lblFirmware
            // 
            this.lblFirmware.AutoSize = true;
            this.lblFirmware.Location = new System.Drawing.Point(17, 164);
            this.lblFirmware.Name = "lblFirmware";
            this.lblFirmware.Size = new System.Drawing.Size(49, 13);
            this.lblFirmware.TabIndex = 6;
            this.lblFirmware.Text = "Firmware";
            // 
            // lblModelo
            // 
            this.lblModelo.AutoSize = true;
            this.lblModelo.Location = new System.Drawing.Point(17, 138);
            this.lblModelo.Name = "lblModelo";
            this.lblModelo.Size = new System.Drawing.Size(42, 13);
            this.lblModelo.TabIndex = 5;
            this.lblModelo.Text = "Modelo";
            // 
            // lblProducto
            // 
            this.lblProducto.AutoSize = true;
            this.lblProducto.Location = new System.Drawing.Point(17, 112);
            this.lblProducto.Name = "lblProducto";
            this.lblProducto.Size = new System.Drawing.Size(50, 13);
            this.lblProducto.TabIndex = 4;
            this.lblProducto.Text = "Producto";
            // 
            // lblRevision
            // 
            this.lblRevision.AutoSize = true;
            this.lblRevision.Location = new System.Drawing.Point(17, 86);
            this.lblRevision.Name = "lblRevision";
            this.lblRevision.Size = new System.Drawing.Size(71, 13);
            this.lblRevision.TabIndex = 3;
            this.lblRevision.Text = "Revisión DLL";
            // 
            // lblProtocolo
            // 
            this.lblProtocolo.AutoSize = true;
            this.lblProtocolo.Location = new System.Drawing.Point(15, 60);
            this.lblProtocolo.Name = "lblProtocolo";
            this.lblProtocolo.Size = new System.Drawing.Size(75, 13);
            this.lblProtocolo.TabIndex = 2;
            this.lblProtocolo.Text = "Protocolo DLL";
            // 
            // txtIp
            // 
            this.txtIp.Enabled = false;
            this.txtIp.Location = new System.Drawing.Point(94, 31);
            this.txtIp.Name = "txtIp";
            this.txtIp.Size = new System.Drawing.Size(177, 20);
            this.txtIp.TabIndex = 1;
            // 
            // lblIP
            // 
            this.lblIP.AutoSize = true;
            this.lblIP.Location = new System.Drawing.Point(15, 34);
            this.lblIP.Name = "lblIP";
            this.lblIP.Size = new System.Drawing.Size(17, 13);
            this.lblIP.TabIndex = 0;
            this.lblIP.Text = "IP";
            // 
            // gbReporteComprobantes
            // 
            this.gbReporteComprobantes.Controls.Add(this.btReporteCbte);
            this.gbReporteComprobantes.Controls.Add(this.lblTipoComprobante);
            this.gbReporteComprobantes.Controls.Add(this.cboTipoCbte);
            this.gbReporteComprobantes.Controls.Add(this.rbXml);
            this.gbReporteComprobantes.Controls.Add(this.rbComprimido);
            this.gbReporteComprobantes.Controls.Add(this.txtNroHasta);
            this.gbReporteComprobantes.Controls.Add(this.txtNroDesde);
            this.gbReporteComprobantes.Controls.Add(this.lblNroHasta);
            this.gbReporteComprobantes.Controls.Add(this.lblNroDesde);
            this.gbReporteComprobantes.Location = new System.Drawing.Point(310, 150);
            this.gbReporteComprobantes.Name = "gbReporteComprobantes";
            this.gbReporteComprobantes.Size = new System.Drawing.Size(328, 153);
            this.gbReporteComprobantes.TabIndex = 6;
            this.gbReporteComprobantes.TabStop = false;
            this.gbReporteComprobantes.Text = "Reporte Comprobantes";
            // 
            // btReporteCbte
            // 
            this.btReporteCbte.Location = new System.Drawing.Point(52, 116);
            this.btReporteCbte.Name = "btReporteCbte";
            this.btReporteCbte.Size = new System.Drawing.Size(208, 23);
            this.btReporteCbte.TabIndex = 8;
            this.btReporteCbte.Text = "Descargar Información";
            this.btReporteCbte.UseVisualStyleBackColor = true;
            this.btReporteCbte.Click += new System.EventHandler(this.btReporteCbte_Click);
            // 
            // lblTipoComprobante
            // 
            this.lblTipoComprobante.AutoSize = true;
            this.lblTipoComprobante.Location = new System.Drawing.Point(12, 87);
            this.lblTipoComprobante.Name = "lblTipoComprobante";
            this.lblTipoComprobante.Size = new System.Drawing.Size(53, 13);
            this.lblTipoComprobante.TabIndex = 7;
            this.lblTipoComprobante.Text = "Tipo Cbte";
            // 
            // cboTipoCbte
            // 
            this.cboTipoCbte.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTipoCbte.FormattingEnabled = true;
            this.cboTipoCbte.Items.AddRange(new object[] {
            "COMPROBANTE_DONACION",
            "GENERICO",
            "DETALLE_DE_VENTAS",
            "FACTURA_A",
            "FACTURA_B",
            "FACTURA_C",
            "FACTURA_M",
            "INFORME_DE_AUDITORIA",
            "INFORME_DIARIO_DE_CIERRE",
            "NOTA_DE_CREDITO_A",
            "NOTA_DE_CREDITO_B",
            "NOTA_DE_CREDITO_C",
            "NOTA_DE_CREDITO_M",
            "NOTA_DE_DEBITO_A",
            "NOTA_DE_DEBITO_B",
            "NOTA_DE_DEBITO_C",
            "NOTA_DE_DEBITO_M",
            "PRESUPUESTO_X",
            "RECIBO_A",
            "RECIBO_B",
            "RECIBO_C",
            "RECIBO_M",
            "RECIBO_X",
            "REMITO_R",
            "REMITO_X",
            "TIQUE",
            "TIQUE_FACTURA_A",
            "TIQUE_FACTURA_B",
            "TIQUE_FACTURA_C",
            "TIQUE_FACTURA_M",
            "TIQUE_NOTA_CREDITO",
            "TIQUE_NOTA_CREDITO_A",
            "TIQUE_NOTA_CREDITO_B",
            "TIQUE_NOTA_CREDITO_C",
            "TIQUE_NOTA_CREDITO_M",
            "TIQUE_NOTA_DEBITO_A",
            "TIQUE_NOTA_DEBITO_B",
            "TIQUE_NOTA_DEBITO_C",
            "TIQUE_NOTA_DEBITO_M"});
            this.cboTipoCbte.Location = new System.Drawing.Point(87, 84);
            this.cboTipoCbte.Name = "cboTipoCbte";
            this.cboTipoCbte.Size = new System.Drawing.Size(124, 21);
            this.cboTipoCbte.TabIndex = 6;
            // 
            // rbXml
            // 
            this.rbXml.AutoSize = true;
            this.rbXml.Checked = true;
            this.rbXml.Location = new System.Drawing.Point(151, 59);
            this.rbXml.Name = "rbXml";
            this.rbXml.Size = new System.Drawing.Size(78, 17);
            this.rbXml.TabIndex = 5;
            this.rbXml.TabStop = true;
            this.rbXml.Text = "XML Unico";
            this.rbXml.UseVisualStyleBackColor = true;
            this.rbXml.CheckedChanged += new System.EventHandler(this.rbXml_CheckedChanged);
            // 
            // rbComprimido
            // 
            this.rbComprimido.AutoSize = true;
            this.rbComprimido.Location = new System.Drawing.Point(43, 59);
            this.rbComprimido.Name = "rbComprimido";
            this.rbComprimido.Size = new System.Drawing.Size(79, 17);
            this.rbComprimido.TabIndex = 4;
            this.rbComprimido.Text = "Comprimido";
            this.rbComprimido.UseVisualStyleBackColor = true;
            this.rbComprimido.CheckedChanged += new System.EventHandler(this.rbComprimido_CheckedChanged);
            // 
            // txtNroHasta
            // 
            this.txtNroHasta.Location = new System.Drawing.Point(231, 27);
            this.txtNroHasta.Name = "txtNroHasta";
            this.txtNroHasta.Size = new System.Drawing.Size(78, 20);
            this.txtNroHasta.TabIndex = 3;
            // 
            // txtNroDesde
            // 
            this.txtNroDesde.Location = new System.Drawing.Point(76, 27);
            this.txtNroDesde.Name = "txtNroDesde";
            this.txtNroDesde.Size = new System.Drawing.Size(78, 20);
            this.txtNroDesde.TabIndex = 2;
            // 
            // lblNroHasta
            // 
            this.lblNroHasta.AutoSize = true;
            this.lblNroHasta.Location = new System.Drawing.Point(167, 30);
            this.lblNroHasta.Name = "lblNroHasta";
            this.lblNroHasta.Size = new System.Drawing.Size(58, 13);
            this.lblNroHasta.TabIndex = 1;
            this.lblNroHasta.Text = "Nro. Hasta";
            // 
            // lblNroDesde
            // 
            this.lblNroDesde.AutoSize = true;
            this.lblNroDesde.Location = new System.Drawing.Point(9, 30);
            this.lblNroDesde.Name = "lblNroDesde";
            this.lblNroDesde.Size = new System.Drawing.Size(61, 13);
            this.lblNroDesde.TabIndex = 0;
            this.lblNroDesde.Text = "Nro. Desde";
            // 
            // gbCtd
            // 
            this.gbCtd.Controls.Add(this.cbNumeros);
            this.gbCtd.Controls.Add(this.cbFechas);
            this.gbCtd.Controls.Add(this.btCtd);
            this.gbCtd.Controls.Add(this.rbXmlCtd);
            this.gbCtd.Controls.Add(this.rbComprimidoCtd);
            this.gbCtd.Controls.Add(this.txtHastaCtd);
            this.gbCtd.Controls.Add(this.txtDesdeCtd);
            this.gbCtd.Controls.Add(this.label1);
            this.gbCtd.Controls.Add(this.label2);
            this.gbCtd.Controls.Add(this.dtpHastaCtd);
            this.gbCtd.Controls.Add(this.dtpDesdeCtd);
            this.gbCtd.Controls.Add(this.lblHastaCtd);
            this.gbCtd.Controls.Add(this.lblDesdeCtd);
            this.gbCtd.Location = new System.Drawing.Point(310, 309);
            this.gbCtd.Name = "gbCtd";
            this.gbCtd.Size = new System.Drawing.Size(328, 157);
            this.gbCtd.TabIndex = 7;
            this.gbCtd.TabStop = false;
            this.gbCtd.Text = "Cinta Testigo Digital ";
            // 
            // cbNumeros
            // 
            this.cbNumeros.AutoSize = true;
            this.cbNumeros.Location = new System.Drawing.Point(151, 73);
            this.cbNumeros.Name = "cbNumeros";
            this.cbNumeros.Size = new System.Drawing.Size(68, 17);
            this.cbNumeros.TabIndex = 14;
            this.cbNumeros.Text = "Numeros";
            this.cbNumeros.UseVisualStyleBackColor = true;
            this.cbNumeros.CheckedChanged += new System.EventHandler(this.cbNumeros_CheckedChanged);
            // 
            // cbFechas
            // 
            this.cbFechas.AutoSize = true;
            this.cbFechas.Location = new System.Drawing.Point(43, 73);
            this.cbFechas.Name = "cbFechas";
            this.cbFechas.Size = new System.Drawing.Size(61, 17);
            this.cbFechas.TabIndex = 13;
            this.cbFechas.Text = "Fechas";
            this.cbFechas.UseVisualStyleBackColor = true;
            this.cbFechas.CheckedChanged += new System.EventHandler(this.cbFechas_CheckedChanged);
            // 
            // btCtd
            // 
            this.btCtd.Location = new System.Drawing.Point(52, 119);
            this.btCtd.Name = "btCtd";
            this.btCtd.Size = new System.Drawing.Size(208, 23);
            this.btCtd.TabIndex = 12;
            this.btCtd.Text = "Descargar Información";
            this.btCtd.UseVisualStyleBackColor = true;
            this.btCtd.Click += new System.EventHandler(this.btCtd_Click);
            // 
            // rbXmlCtd
            // 
            this.rbXmlCtd.AutoSize = true;
            this.rbXmlCtd.Location = new System.Drawing.Point(151, 96);
            this.rbXmlCtd.Name = "rbXmlCtd";
            this.rbXmlCtd.Size = new System.Drawing.Size(78, 17);
            this.rbXmlCtd.TabIndex = 11;
            this.rbXmlCtd.Text = "XML Unico";
            this.rbXmlCtd.UseVisualStyleBackColor = true;
            this.rbXmlCtd.CheckedChanged += new System.EventHandler(this.rbXmlCtd_CheckedChanged);
            // 
            // rbComprimidoCtd
            // 
            this.rbComprimidoCtd.AutoSize = true;
            this.rbComprimidoCtd.Checked = true;
            this.rbComprimidoCtd.Location = new System.Drawing.Point(43, 96);
            this.rbComprimidoCtd.Name = "rbComprimidoCtd";
            this.rbComprimidoCtd.Size = new System.Drawing.Size(79, 17);
            this.rbComprimidoCtd.TabIndex = 10;
            this.rbComprimidoCtd.TabStop = true;
            this.rbComprimidoCtd.Text = "Comprimido";
            this.rbComprimidoCtd.UseVisualStyleBackColor = true;
            this.rbComprimidoCtd.CheckedChanged += new System.EventHandler(this.rbComprimidoCtd_CheckedChanged);
            // 
            // txtHastaCtd
            // 
            this.txtHastaCtd.Location = new System.Drawing.Point(237, 47);
            this.txtHastaCtd.Name = "txtHastaCtd";
            this.txtHastaCtd.Size = new System.Drawing.Size(78, 20);
            this.txtHastaCtd.TabIndex = 7;
            // 
            // txtDesdeCtd
            // 
            this.txtDesdeCtd.Location = new System.Drawing.Point(82, 47);
            this.txtDesdeCtd.Name = "txtDesdeCtd";
            this.txtDesdeCtd.Size = new System.Drawing.Size(78, 20);
            this.txtDesdeCtd.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(173, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Nro. Hasta";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Nro. Desde";
            // 
            // dtpHastaCtd
            // 
            this.dtpHastaCtd.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpHastaCtd.Location = new System.Drawing.Point(213, 19);
            this.dtpHastaCtd.Name = "dtpHastaCtd";
            this.dtpHastaCtd.Size = new System.Drawing.Size(104, 20);
            this.dtpHastaCtd.TabIndex = 3;
            // 
            // dtpDesdeCtd
            // 
            this.dtpDesdeCtd.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDesdeCtd.Location = new System.Drawing.Point(57, 19);
            this.dtpDesdeCtd.Name = "dtpDesdeCtd";
            this.dtpDesdeCtd.Size = new System.Drawing.Size(104, 20);
            this.dtpDesdeCtd.TabIndex = 2;
            // 
            // lblHastaCtd
            // 
            this.lblHastaCtd.AutoSize = true;
            this.lblHastaCtd.Location = new System.Drawing.Point(172, 23);
            this.lblHastaCtd.Name = "lblHastaCtd";
            this.lblHastaCtd.Size = new System.Drawing.Size(35, 13);
            this.lblHastaCtd.TabIndex = 1;
            this.lblHastaCtd.Text = "Hasta";
            // 
            // lblDesdeCtd
            // 
            this.lblDesdeCtd.AutoSize = true;
            this.lblDesdeCtd.Location = new System.Drawing.Point(13, 23);
            this.lblDesdeCtd.Name = "lblDesdeCtd";
            this.lblDesdeCtd.Size = new System.Drawing.Size(38, 13);
            this.lblDesdeCtd.TabIndex = 0;
            this.lblDesdeCtd.Text = "Desde";
            // 
            // frmConsola
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(650, 489);
            this.Controls.Add(this.gbCtd);
            this.Controls.Add(this.gbReporteComprobantes);
            this.Controls.Add(this.gbDatosCF);
            this.Controls.Add(this.gbAuditoria);
            this.Controls.Add(this.gbCierres);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmConsola";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Consola Controlador Fiscal Hasar 2da. Generación";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.gbCierres.ResumeLayout(false);
            this.gbAuditoria.ResumeLayout(false);
            this.gbAuditoria.PerformLayout();
            this.gbDatosCF.ResumeLayout(false);
            this.gbDatosCF.PerformLayout();
            this.gbReporteComprobantes.ResumeLayout(false);
            this.gbReporteComprobantes.PerformLayout();
            this.gbCtd.ResumeLayout(false);
            this.gbCtd.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbCierres;
        private System.Windows.Forms.Button btnCierreX;
        private System.Windows.Forms.Button btnCierreZ;
        private System.Windows.Forms.GroupBox gbAuditoria;
        private System.Windows.Forms.Button btnAditoria;
        private System.Windows.Forms.GroupBox gbDatosCF;
        private System.Windows.Forms.Button btTerminar;
        private System.Windows.Forms.Button btConectar;
        private System.Windows.Forms.TextBox txtProtocoloCF;
        private System.Windows.Forms.TextBox txtVersion;
        private System.Windows.Forms.TextBox txtFirmware;
        private System.Windows.Forms.TextBox txtModelo;
        private System.Windows.Forms.TextBox txtProducto;
        private System.Windows.Forms.TextBox txtRevisionDll;
        private System.Windows.Forms.TextBox txtProtocoloDll;
        private System.Windows.Forms.Label lblProtocoloCF;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.Label lblFirmware;
        private System.Windows.Forms.Label lblModelo;
        private System.Windows.Forms.Label lblProducto;
        private System.Windows.Forms.Label lblRevision;
        private System.Windows.Forms.Label lblProtocolo;
        private System.Windows.Forms.TextBox txtIp;
        private System.Windows.Forms.Label lblIP;
        private System.Windows.Forms.RadioButton rbAfipMemFiscal;
        private System.Windows.Forms.RadioButton rbAfipCompleto;
        private System.Windows.Forms.Label lblFechaH;
        private System.Windows.Forms.Label lblFechaD;
        private System.Windows.Forms.DateTimePicker dtpHasta;
        private System.Windows.Forms.DateTimePicker dtpDesde;
        private System.Windows.Forms.GroupBox gbReporteComprobantes;
        private System.Windows.Forms.Label lblTipoComprobante;
        private System.Windows.Forms.ComboBox cboTipoCbte;
        private System.Windows.Forms.RadioButton rbXml;
        private System.Windows.Forms.RadioButton rbComprimido;
        private System.Windows.Forms.TextBox txtNroHasta;
        private System.Windows.Forms.TextBox txtNroDesde;
        private System.Windows.Forms.Label lblNroHasta;
        private System.Windows.Forms.Label lblNroDesde;
        private System.Windows.Forms.Button btReporteCbte;
        private System.Windows.Forms.GroupBox gbCtd;
        private System.Windows.Forms.Button btCtd;
        private System.Windows.Forms.RadioButton rbXmlCtd;
        private System.Windows.Forms.RadioButton rbComprimidoCtd;
        private System.Windows.Forms.TextBox txtHastaCtd;
        private System.Windows.Forms.TextBox txtDesdeCtd;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpHastaCtd;
        private System.Windows.Forms.DateTimePicker dtpDesdeCtd;
        private System.Windows.Forms.Label lblHastaCtd;
        private System.Windows.Forms.Label lblDesdeCtd;
        private System.Windows.Forms.CheckBox cbNumeros;
        private System.Windows.Forms.CheckBox cbFechas;
    }
}

