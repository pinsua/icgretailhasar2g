﻿using Hasar2daGen;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace IcgRetailConsolaH2G
{
    public partial class frmConfig : Form
    {
        int _ticketRegalo;
        public frmConfig()
        {
            InitializeComponent();
        }

        private void frmConfig_Load(object sender, EventArgs e)
        {
            if (File.Exists("Config2daGeneracion.xml"))
            {                
                txtIp.Text = Form1._ip;
                txtCaja.Text = Form1._caja;
                txtTicketReg1.Text = Form1._textoRegalo1;
                txtTicketReg2.Text = Form1._textoRegalo2;
                txtTicketReg3.Text = Form1._textoRegalo3;
                //_password = xn["super"].InnerText;
                //_hasarLog = Convert.ToBoolean(xn["hasarlog"].InnerText);
                txtLicenciaIcg.Text = Form1._keyIcg;
                txtPtoVta.Text = Form1._ptoVtaManual;
                if (Form1._tikcketRegalo)
                    chkUsaTktRegalo.Checked = true;
                else
                    chkUsaTktRegalo.Checked = false;
                //Datos de conexion a la base de datos
                txtServidor.Text = Form1._server;
                txtUsuario.Text = Form1._user;
                txtBaseDatos.Text = Form1._catalog;
                //Datos de Irsa.
                txtContratoIrsa.Text = Form1._infoIrsa.contrato;
                txtLocalIrsa.Text = Form1._infoIrsa.local;
                txtPathIrsa.Text = Form1._infoIrsa.pathSalida;
                txtPosIrsa.Text = Form1._infoIrsa.pos;
                txtRubroIrsa.Text = Form1._infoIrsa.rubro;
                //Datos Caballito
                txtPathCaballito.Text = Form1._pathCaballito;
                //Datos Olmos
                txtPathOlmos.Text = Form1._pathOlmos;
                //Check Fiserv
                chkFiServ.Checked = Form1._checkFiServ;
                //Cuit
                txtCuit.Text = Form1._cuit;
            }
            else
            {
                MessageBox.Show(new Form { TopMost = true }, "No existe el archivo de configuración." +
                        Environment.NewLine + "Por favor comuniquese con ICG Argentina.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void txtUsuario_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                //Nuevo
                XmlDocument xDocNew = new XmlDocument();
                xDocNew.Load("Config2daGeneracion.xml");
                //IP
                XmlNodeList _IP = xDocNew.GetElementsByTagName("ip");
                if (_IP.Count > 0)
                    _IP[0].InnerXml = txtIp.Text.Trim();
                else
                {
                    XmlElement elem = xDocNew.CreateElement("ip");
                    XmlText text = xDocNew.CreateTextNode(txtIp.Text.Trim());
                    xDocNew.DocumentElement.AppendChild(elem);
                    xDocNew.DocumentElement.LastChild.AppendChild(text);
                }
                //CAJA
                XmlNodeList _CAJA = xDocNew.GetElementsByTagName("caja");
                if (_CAJA.Count > 0)
                    _CAJA[0].InnerXml = txtCaja.Text.Trim();
                else
                {
                    XmlElement elem = xDocNew.CreateElement("caja");
                    XmlText text = xDocNew.CreateTextNode(txtCaja.Text.Trim());
                    xDocNew.DocumentElement.AppendChild(elem);
                    xDocNew.DocumentElement.LastChild.AppendChild(text);
                }
                //Password
                XmlNodeList _PASSWORD = xDocNew.GetElementsByTagName("super");
                if (_PASSWORD.Count == 0)
                {
                    XmlElement elem = xDocNew.CreateElement("super");
                    XmlText text = xDocNew.CreateTextNode("hoguera");
                    xDocNew.DocumentElement.AppendChild(elem);
                    xDocNew.DocumentElement.LastChild.AppendChild(text);
                }
                //Monotributo
                XmlNodeList _MONOTRIBUTO = xDocNew.GetElementsByTagName("monotributo");
                if (_MONOTRIBUTO.Count == 0)
                {
                    XmlElement elem = xDocNew.CreateElement("monotributo");
                    XmlText text = xDocNew.CreateTextNode("false");
                    xDocNew.DocumentElement.AppendChild(elem);
                    xDocNew.DocumentElement.LastChild.AppendChild(text);
                }
                //tktregalo1
                XmlNodeList _TKTREGALO1 = xDocNew.GetElementsByTagName("tktregalo1");
                if (_TKTREGALO1.Count > 0)
                    _TKTREGALO1[0].InnerXml = txtTicketReg1.Text.Trim();
                else
                {
                    XmlElement elem = xDocNew.CreateElement("tktregalo1");
                    XmlText text = xDocNew.CreateTextNode(txtTicketReg1.Text.Trim());
                    xDocNew.DocumentElement.AppendChild(elem);
                    xDocNew.DocumentElement.LastChild.AppendChild(text);
                }
                //tktregalo2
                XmlNodeList _TKTREGALO2 = xDocNew.GetElementsByTagName("tktregalo2");
                if (_TKTREGALO2.Count > 0)
                    _TKTREGALO2[0].InnerXml = txtTicketReg2.Text.Trim();
                else
                {
                    XmlElement elem = xDocNew.CreateElement("tktregalo2");
                    XmlText text = xDocNew.CreateTextNode(txtTicketReg2.Text.Trim());
                    xDocNew.DocumentElement.AppendChild(elem);
                    xDocNew.DocumentElement.LastChild.AppendChild(text);
                }
                //tktregalo3
                XmlNodeList _TKTREGALO3 = xDocNew.GetElementsByTagName("tktregalo3");
                if (_TKTREGALO3.Count > 0)
                    _TKTREGALO3[0].InnerXml = txtTicketReg3.Text.Trim();
                else
                {
                    XmlElement elem = xDocNew.CreateElement("tktregalo3");
                    XmlText text = xDocNew.CreateTextNode(txtTicketReg3.Text.Trim());
                    xDocNew.DocumentElement.AppendChild(elem);
                    xDocNew.DocumentElement.LastChild.AppendChild(text);
                }
                //Pto Vta Manual
                XmlNodeList _ptovtamanual = xDocNew.GetElementsByTagName("ptovtamanual");
                if (_ptovtamanual.Count > 0)
                    _ptovtamanual[0].InnerXml = txtPtoVta.Text.Trim();
                else
                {
                    XmlElement elem = xDocNew.CreateElement("ptovtamanual");
                    XmlText text = xDocNew.CreateTextNode(txtPtoVta.Text.Trim());
                    xDocNew.DocumentElement.AppendChild(elem);
                    xDocNew.DocumentElement.LastChild.AppendChild(text);
                }

                //keyICG
                XmlNodeList _KEYICG = xDocNew.GetElementsByTagName("keyICG");
                if (_KEYICG.Count > 0)
                    _KEYICG[0].InnerXml = txtLicenciaIcg.Text.Trim();
                else
                {
                    XmlElement elem = xDocNew.CreateElement("keyICG");
                    XmlText text = xDocNew.CreateTextNode(txtLicenciaIcg.Text.Trim());
                    xDocNew.DocumentElement.AppendChild(elem);
                    xDocNew.DocumentElement.LastChild.AppendChild(text);
                }
                //HasarLog
                XmlNodeList _HASARLOG = xDocNew.GetElementsByTagName("hasarlog");
                if (_HASARLOG.Count > 0)
                    _HASARLOG[0].InnerXml = "false";
                else
                {
                    XmlElement elem = xDocNew.CreateElement("hasarlog");
                    XmlText text = xDocNew.CreateTextNode("false");
                    xDocNew.DocumentElement.AppendChild(elem);
                    xDocNew.DocumentElement.LastChild.AppendChild(text);
                }               

                //SEver
                XmlNodeList _SEVER = xDocNew.GetElementsByTagName("server");
                if (_SEVER.Count > 0)
                    _SEVER[0].InnerXml = txtServidor.Text.Trim();
                else
                {
                    XmlElement elem = xDocNew.CreateElement("server");
                    XmlText text = xDocNew.CreateTextNode(txtServidor.Text.Trim());
                    xDocNew.DocumentElement.AppendChild(elem);
                    xDocNew.DocumentElement.LastChild.AppendChild(text);
                }
                //USER
                XmlNodeList _USER = xDocNew.GetElementsByTagName("user");
                if (_USER.Count > 0)
                    _USER[0].InnerXml = txtUsuario.Text.Trim();
                else
                {
                    XmlElement elem = xDocNew.CreateElement("user");
                    XmlText text = xDocNew.CreateTextNode(txtUsuario.Text.Trim());
                    xDocNew.DocumentElement.AppendChild(elem);
                    xDocNew.DocumentElement.LastChild.AppendChild(text);
                }
                //CATALOG
                XmlNodeList _CATALOG = xDocNew.GetElementsByTagName("database");
                if (_CATALOG.Count > 0)
                    _CATALOG[0].InnerXml = txtBaseDatos.Text.Trim();
                else
                {
                    XmlElement elem = xDocNew.CreateElement("database");
                    XmlText text = xDocNew.CreateTextNode(txtBaseDatos.Text.Trim());
                    xDocNew.DocumentElement.AppendChild(elem);
                    xDocNew.DocumentElement.LastChild.AppendChild(text);
                }

                //IRSA CATALOG
                XmlNodeList _CONTRATO = xDocNew.GetElementsByTagName("Contrato");
                if (_CONTRATO.Count > 0)
                    _CONTRATO[0].InnerXml = txtContratoIrsa.Text.Trim();
                else
                {
                    XmlElement elem = xDocNew.CreateElement("Contrato");
                    XmlText text = xDocNew.CreateTextNode(txtContratoIrsa.Text.Trim());
                    xDocNew.DocumentElement.AppendChild(elem);
                    xDocNew.DocumentElement.LastChild.AppendChild(text);
                }
                //IRSA CATALOG
                XmlNodeList _LOCAL = xDocNew.GetElementsByTagName("Local");
                if (_LOCAL.Count > 0)
                    _LOCAL[0].InnerXml = txtLocalIrsa.Text.Trim();
                else
                {
                    XmlElement elem = xDocNew.CreateElement("Local");
                    XmlText text = xDocNew.CreateTextNode(txtLocalIrsa.Text.Trim());
                    xDocNew.DocumentElement.AppendChild(elem);
                    xDocNew.DocumentElement.LastChild.AppendChild(text);
                }
                //IRSA CATALOG
                XmlNodeList PATHSALIDAIRSA = xDocNew.GetElementsByTagName("PathSalidaIrsa");
                if (PATHSALIDAIRSA.Count > 0)
                    PATHSALIDAIRSA[0].InnerXml = txtPathIrsa.Text.Trim();
                else
                {
                    XmlElement elem = xDocNew.CreateElement("PathSalidaIrsa");
                    XmlText text = xDocNew.CreateTextNode(txtPathIrsa.Text.Trim());
                    xDocNew.DocumentElement.AppendChild(elem);
                    xDocNew.DocumentElement.LastChild.AppendChild(text);
                }
                //IRSA POS
                XmlNodeList POS = xDocNew.GetElementsByTagName("Pos");
                if (POS.Count > 0)
                    POS[0].InnerXml = txtPosIrsa.Text.Trim();
                else
                {
                    XmlElement elem = xDocNew.CreateElement("Pos");
                    XmlText text = xDocNew.CreateTextNode(txtPosIrsa.Text.Trim());
                    xDocNew.DocumentElement.AppendChild(elem);
                    xDocNew.DocumentElement.LastChild.AppendChild(text);
                }
                //IRSA RUBRO
                XmlNodeList RUBRO = xDocNew.GetElementsByTagName("Rubro");
                if (RUBRO.Count > 0)
                    RUBRO[0].InnerXml = txtRubroIrsa.Text.Trim();
                else
                {
                    XmlElement elem = xDocNew.CreateElement("Rubro");
                    XmlText text = xDocNew.CreateTextNode(txtRubroIrsa.Text.Trim());
                    xDocNew.DocumentElement.AppendChild(elem);
                    xDocNew.DocumentElement.LastChild.AppendChild(text);
                }

                //CABALLITO
                XmlNodeList PATHSALIDACABALLITO = xDocNew.GetElementsByTagName("PathSalidaCaballito");
                if (PATHSALIDACABALLITO.Count > 0)
                    PATHSALIDACABALLITO[0].InnerXml = txtPathCaballito.Text.Trim();
                else
                {
                    XmlElement elem = xDocNew.CreateElement("PathSalidaCaballito");
                    XmlText text = xDocNew.CreateTextNode(txtPathCaballito.Text.Trim());
                    xDocNew.DocumentElement.AppendChild(elem);
                    xDocNew.DocumentElement.LastChild.AppendChild(text);
                }

                //OLMOS
                XmlNodeList PATHSALIDAOLMOS = xDocNew.GetElementsByTagName("PathSalidaOlmos");
                if (PATHSALIDAOLMOS.Count > 0)
                    PATHSALIDAOLMOS[0].InnerXml = txtPathOlmos.Text.Trim();
                else
                {
                    XmlElement elem = xDocNew.CreateElement("PathSalidaOlmos");
                    XmlText text = xDocNew.CreateTextNode(txtPathOlmos.Text.Trim());
                    xDocNew.DocumentElement.AppendChild(elem);
                    xDocNew.DocumentElement.LastChild.AppendChild(text);
                }
                //Check FiServ
                XmlNodeList CHECKFISERV = xDocNew.GetElementsByTagName("CheckFiServ");
                if (CHECKFISERV.Count > 0)
                    CHECKFISERV[0].InnerXml = chkFiServ.Checked.ToString();
                else
                {
                    XmlElement elem = xDocNew.CreateElement("CheckFiServ");
                    XmlText text = xDocNew.CreateTextNode(chkFiServ.Checked.ToString());
                    xDocNew.DocumentElement.AppendChild(elem);
                    xDocNew.DocumentElement.LastChild.AppendChild(text);
                }
                //CUIT
                XmlNodeList CUIT = xDocNew.GetElementsByTagName("Cuit");
                if (CUIT.Count > 0)
                    CUIT[0].InnerXml = txtCuit.Text.Trim();
                else
                {
                    XmlElement elem = xDocNew.CreateElement("Cuit");
                    XmlText text = xDocNew.CreateTextNode(txtCuit.Text.Trim());
                    xDocNew.DocumentElement.AppendChild(elem);
                    xDocNew.DocumentElement.LastChild.AppendChild(text);
                }

                //Fin Nuevo
                xDocNew.Save("Config2daGeneracion.xml");

                this.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error al intentar guardar el archivo de configuración." +
                        Environment.NewLine + "Error: " + ex.Message + Environment.NewLine + "Por favor comuniquese con ICG Argentina.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnLicencia_Click(object sender, EventArgs e)
        {
            //frmKey _frm = new frmKey();
            //_frm.ShowDialog(this);
            //_frm.Dispose();

            if (!String.IsNullOrEmpty(txtIp.Text.Trim()))
            {
                //Recuperamos los datos del controlador.
                Hasar2daGen.DatosControlador _datosControlador = FuncionesVarias.GetDatosControlador(txtIp.Text.Trim());

                try
                {
                    Licencia lic = new Licencia();
                    lic.ClientCuit = _datosControlador.Cuit;
                    lic.ClientName = _datosControlador.RazonSocial;
                    lic.ClientRazonSocial = _datosControlador.RazonSocial;
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = Form1._conn;
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = "SELECT VERSION from [VERSION]";
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    lic.Version = reader["VERSION"].ToString();
                                }
                            }
                        }
                    }
                    lic.ClientKey = IcgVarios.LicenciaIcg.Value();
                    lic.password = "Pinsua.2730";
                    lic.Plataforma = "RETAIL";
                    lic.Release = Application.ProductVersion;
                    lic.user = "pinsua@yahoo.com";
                    lic.Tipo = "Retail H2G";
                    lic.TerminalName = Environment.MachineName;
                    lic.PointOfSale = _datosControlador.NroPos;
                    var json = JsonConvert.SerializeObject(lic);
//#if DEBUG
//                    var client = new RestClient("http://localhost:18459/api/Licencias/GetKey");
//#else
                    var client = new RestClient("http://pkicg1.ddns.net:8095/api/Licencias/GetKey");
//#endif
                    client.Timeout = -1;
                    var request = new RestRequest(Method.POST);
                    request.AddHeader("Content-Type", "application/json");
                    request.AddParameter("application/json", json, ParameterType.RequestBody);
                    IRestResponse response = client.Execute(request);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {                        
                        ResponcePostLicencia res = JsonConvert.DeserializeObject<ResponcePostLicencia>(response.Content);
                        if (res.Resultado == "OK")
                        {
                            txtLicenciaIcg.Text = res.Key;
                            MessageBox.Show(new Form { TopMost = true }, "Su licencia ha sido generada correctamente.",
                                "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else
                        {
                            MessageBox.Show(new Form { TopMost = true }, "Su licencia no se ha podido generada correctamente." +
                                Environment.NewLine + "Comuniquese con ICG Argentina",
                                "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Se produjo el siguiente error: " + Environment.NewLine +
                        ex.Message + Environment.NewLine + "Por favor comuníquese con ICG Argentina para obtener una licencia.",
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btValidar_Click(object sender, EventArgs e)
        {
            try
            {
                if (String.IsNullOrEmpty(txtBaseDatos.Text))
                    throw new Exception("No se ha definido ninguna base de datos. Por favor definala y luego reintente.");
                if (String.IsNullOrEmpty(txtServidor.Text))
                    throw new Exception("No se ha definido ningun servidor. Por favor definalo y luego reintente.");
                if (String.IsNullOrEmpty(txtUsuario.Text))
                    throw new Exception("No se ha definido ningun usuario. Por favor definalo y luego reintente.");
                string _connection = "Data Source=" + txtServidor.Text.Trim() + ";Initial Catalog=" + txtBaseDatos.Text.Trim() + ";User Id=" + txtUsuario.Text.Trim() + ";Password=masterkey;";

                //string _valcamposlibres = ValidarColumnasCamposLibres(_connection);
                //if (!String.IsNullOrEmpty(_valcamposlibres))
                //    MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error al intentar validar los campos." +
                //        Environment.NewLine + "Error: " + _valcamposlibres + Environment.NewLine + "Por favor comuniquese con ICG Argentina.",
                //        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error al intentar validar los campos." +
                        Environment.NewLine + "Error: " + ex.Message + Environment.NewLine + "Por favor comuniquese con ICG Argentina.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        private string ValidarColumnasCamposLibres(string _connection)
        {
            string _msj = "";
            try
            {
                using (SqlConnection _sqlConn = new SqlConnection(_connection))
                {
                    _sqlConn.Open();

                    if (!ValidarColumna("FACTURASVENTACAMPOSLIBRES", "Z_NRO", _sqlConn))
                    {
                        _msj = _msj + "No existe la Columna Z_NRO en la tabla FACTURASVENTACAMPOSLIBRES" + Environment.NewLine;
                    }
                    if (!ValidarColumna("FACTURASVENTACAMPOSLIBRES", "Z_TOTAL", _sqlConn))
                    {
                        _msj = _msj + "No existe la Columna Z_TOTAL en la tabla FACTURASVENTACAMPOSLIBRES" + Environment.NewLine;
                    }
                    if (!ValidarColumna("FACTURASVENTACAMPOSLIBRES", "Z_FECHA_HORA", _sqlConn))
                    {
                        _msj = _msj + "No existe la Columna Z_FECHA_HORA en la tabla FACTURASVENTACAMPOSLIBRES" + Environment.NewLine;
                    }
                    if (!ValidarColumna("FACTURASVENTACAMPOSLIBRES", "Z_IMPUESTOS", _sqlConn))
                    {
                        _msj = _msj + "No existe la Columna Z_IMPUESTOS en la tabla FACTURASVENTACAMPOSLIBRES" + Environment.NewLine;
                    }
                    if (!ValidarColumna("FACTURASVENTACAMPOSLIBRES", "Z_TIQUETS", _sqlConn))
                    {
                        _msj = _msj + "No existe la Columna Z_TIQUETS en la tabla FACTURASVENTACAMPOSLIBRES" + Environment.NewLine;
                    }
                    //if (!ValidarColumna("CLIENTESCAMPOSLIBRES", "TIPO_DOC", _sqlConn))
                    //{
                    //    _msj = _msj + "No existe la Columna TIPO_DOC en la tabla CLIENTESCAMPOSLIBRES" + Environment.NewLine;
                    //}
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error validando CamposLibres. " + ex.Message);
            }
            return _msj;
        }

        private bool ValidarColumna(string _table, string _columna, SqlConnection _sqlConn)
        {
            string _sql = @"IF NOT EXISTS(SELECT column_name FROM information_schema.columns WHERE table_name=@table and column_name=@column )
                BEGIN SELECT 'NO EXISTE' END ELSE BEGIN SELECT 'EXISTE' END";
            bool _rta = false;

            using (SqlCommand _cmd = new SqlCommand())
            {
                _cmd.Connection = _sqlConn;
                _cmd.CommandType = System.Data.CommandType.Text;
                _cmd.CommandText = _sql;
                _cmd.Parameters.AddWithValue("@table", _table);
                _cmd.Parameters.AddWithValue("column", _columna);

                if (_cmd.ExecuteScalar().ToString() == "EXISTE")
                    _rta = true;
            }
            return _rta;
        }

        private void btnGetRegestry_Click(object sender, EventArgs e)
        {
            try
            {
                string _serverSql = "";
                string _database = "";
                IcgVarios.IcgRegestry.Retail.GetDataBaseToConnect(out _serverSql, out _database);
                txtBaseDatos.Text = _database;
                txtServidor.Text = _serverSql;
            }
            catch (Exception ex)
            {
                MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error al intentar recuperar los datos de Conexión." +
                        Environment.NewLine + "Error: " + ex.Message + Environment.NewLine + "Por favor comuniquese con ICG Argentina, o ingreselos manualmente.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);

            }
        }

        private void chkFiServ_CheckedChanged(object sender, EventArgs e)
        {
            if (chkFiServ.Checked)
            {
                txtCuit.Enabled = true;
                txtContratoIrsa.Enabled = false;
                txtRubroIrsa.Enabled = false;
                txtContratoIrsa.Text = "";
                txtRubroIrsa.Text = "";
            }
            else
            {
                txtCuit.Enabled = false;
                txtContratoIrsa.Enabled = true;
                txtRubroIrsa.Enabled = true;
            }
        }
    }
}
