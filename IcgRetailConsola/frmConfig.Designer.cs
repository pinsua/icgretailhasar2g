﻿namespace IcgRetailConsolaH2G
{
    partial class frmConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnGetRegestry = new System.Windows.Forms.Button();
            this.btValidar = new System.Windows.Forms.Button();
            this.txtUsuario = new System.Windows.Forms.TextBox();
            this.txtBaseDatos = new System.Windows.Forms.TextBox();
            this.txtServidor = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkUsaTktRegalo = new System.Windows.Forms.CheckBox();
            this.txtTicketReg3 = new System.Windows.Forms.TextBox();
            this.txtTicketReg2 = new System.Windows.Forms.TextBox();
            this.txtTicketReg1 = new System.Windows.Forms.TextBox();
            this.txtPtoVta = new System.Windows.Forms.TextBox();
            this.txtCaja = new System.Windows.Forms.TextBox();
            this.txtIp = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtPathIrsa = new System.Windows.Forms.TextBox();
            this.txtRubroIrsa = new System.Windows.Forms.TextBox();
            this.txtPosIrsa = new System.Windows.Forms.TextBox();
            this.txtLocalIrsa = new System.Windows.Forms.TextBox();
            this.txtContratoIrsa = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtPathCaballito = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtPathOlmos = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txtLicenciaIcg = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.btnLicencia = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.chkFiServ = new System.Windows.Forms.CheckBox();
            this.txtCuit = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnGetRegestry);
            this.groupBox1.Controls.Add(this.btValidar);
            this.groupBox1.Controls.Add(this.txtUsuario);
            this.groupBox1.Controls.Add(this.txtBaseDatos);
            this.groupBox1.Controls.Add(this.txtServidor);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(18, 18);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Size = new System.Drawing.Size(1164, 123);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Base de Datos";
            // 
            // btnGetRegestry
            // 
            this.btnGetRegestry.Location = new System.Drawing.Point(274, 65);
            this.btnGetRegestry.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnGetRegestry.Name = "btnGetRegestry";
            this.btnGetRegestry.Size = new System.Drawing.Size(284, 35);
            this.btnGetRegestry.TabIndex = 7;
            this.btnGetRegestry.Text = "Recuperar Datos de Conexión";
            this.btnGetRegestry.UseVisualStyleBackColor = true;
            this.btnGetRegestry.Click += new System.EventHandler(this.btnGetRegestry_Click);
            // 
            // btValidar
            // 
            this.btValidar.Location = new System.Drawing.Point(614, 65);
            this.btValidar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btValidar.Name = "btValidar";
            this.btValidar.Size = new System.Drawing.Size(284, 35);
            this.btValidar.TabIndex = 6;
            this.btValidar.Text = "Validar Campos Libres";
            this.btValidar.UseVisualStyleBackColor = true;
            this.btValidar.Click += new System.EventHandler(this.btValidar_Click);
            // 
            // txtUsuario
            // 
            this.txtUsuario.Location = new System.Drawing.Point(850, 29);
            this.txtUsuario.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtUsuario.Name = "txtUsuario";
            this.txtUsuario.Size = new System.Drawing.Size(240, 26);
            this.txtUsuario.TabIndex = 5;
            this.txtUsuario.TextChanged += new System.EventHandler(this.txtUsuario_TextChanged);
            // 
            // txtBaseDatos
            // 
            this.txtBaseDatos.Location = new System.Drawing.Point(507, 29);
            this.txtBaseDatos.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtBaseDatos.Name = "txtBaseDatos";
            this.txtBaseDatos.Size = new System.Drawing.Size(232, 26);
            this.txtBaseDatos.TabIndex = 4;
            // 
            // txtServidor
            // 
            this.txtServidor.Location = new System.Drawing.Point(87, 29);
            this.txtServidor.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtServidor.Name = "txtServidor";
            this.txtServidor.Size = new System.Drawing.Size(268, 26);
            this.txtServidor.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(777, 34);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Usuario";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(382, 34);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(115, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Base de Datos";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 34);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Servidor";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chkUsaTktRegalo);
            this.groupBox2.Controls.Add(this.txtTicketReg3);
            this.groupBox2.Controls.Add(this.txtTicketReg2);
            this.groupBox2.Controls.Add(this.txtTicketReg1);
            this.groupBox2.Controls.Add(this.txtPtoVta);
            this.groupBox2.Controls.Add(this.txtCaja);
            this.groupBox2.Controls.Add(this.txtIp);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Location = new System.Drawing.Point(18, 148);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox2.Size = new System.Drawing.Size(1164, 172);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Controlador";
            // 
            // chkUsaTktRegalo
            // 
            this.chkUsaTktRegalo.AutoSize = true;
            this.chkUsaTktRegalo.Checked = true;
            this.chkUsaTktRegalo.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkUsaTktRegalo.Location = new System.Drawing.Point(74, 66);
            this.chkUsaTktRegalo.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.chkUsaTktRegalo.Name = "chkUsaTktRegalo";
            this.chkUsaTktRegalo.Size = new System.Drawing.Size(196, 24);
            this.chkUsaTktRegalo.TabIndex = 12;
            this.chkUsaTktRegalo.Text = "Usa Ticket de Regalo?";
            this.chkUsaTktRegalo.UseVisualStyleBackColor = true;
            // 
            // txtTicketReg3
            // 
            this.txtTicketReg3.Location = new System.Drawing.Point(508, 129);
            this.txtTicketReg3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtTicketReg3.Name = "txtTicketReg3";
            this.txtTicketReg3.Size = new System.Drawing.Size(582, 26);
            this.txtTicketReg3.TabIndex = 11;
            // 
            // txtTicketReg2
            // 
            this.txtTicketReg2.Location = new System.Drawing.Point(508, 94);
            this.txtTicketReg2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtTicketReg2.Name = "txtTicketReg2";
            this.txtTicketReg2.Size = new System.Drawing.Size(582, 26);
            this.txtTicketReg2.TabIndex = 10;
            // 
            // txtTicketReg1
            // 
            this.txtTicketReg1.Location = new System.Drawing.Point(508, 62);
            this.txtTicketReg1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtTicketReg1.Name = "txtTicketReg1";
            this.txtTicketReg1.Size = new System.Drawing.Size(582, 26);
            this.txtTicketReg1.TabIndex = 9;
            // 
            // txtPtoVta
            // 
            this.txtPtoVta.Location = new System.Drawing.Point(906, 29);
            this.txtPtoVta.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtPtoVta.Name = "txtPtoVta";
            this.txtPtoVta.Size = new System.Drawing.Size(184, 26);
            this.txtPtoVta.TabIndex = 8;
            // 
            // txtCaja
            // 
            this.txtCaja.Location = new System.Drawing.Point(507, 29);
            this.txtCaja.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCaja.Name = "txtCaja";
            this.txtCaja.Size = new System.Drawing.Size(232, 26);
            this.txtCaja.TabIndex = 7;
            // 
            // txtIp
            // 
            this.txtIp.Location = new System.Drawing.Point(87, 29);
            this.txtIp.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtIp.Name = "txtIp";
            this.txtIp.Size = new System.Drawing.Size(268, 26);
            this.txtIp.TabIndex = 6;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(375, 132);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(119, 20);
            this.label9.TabIndex = 5;
            this.label9.Text = "Ticket Regalo 3";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(375, 98);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(119, 20);
            this.label8.TabIndex = 4;
            this.label8.Text = "Ticket Regalo 2";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(375, 66);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(119, 20);
            this.label7.TabIndex = 3;
            this.label7.Text = "Ticket Regalo 1";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(777, 34);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(118, 20);
            this.label6.TabIndex = 2;
            this.label6.Text = "Pto Vta Manual";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(382, 34);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 20);
            this.label5.TabIndex = 1;
            this.label5.Text = "Caja ICG";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 34);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(24, 20);
            this.label4.TabIndex = 0;
            this.label4.Text = "IP";
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(808, 729);
            this.btnGuardar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(159, 35);
            this.btnGuardar.TabIndex = 2;
            this.btnGuardar.Text = "Guardar y Salir";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtCuit);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Controls.Add(this.chkFiServ);
            this.groupBox3.Controls.Add(this.txtPathIrsa);
            this.groupBox3.Controls.Add(this.txtRubroIrsa);
            this.groupBox3.Controls.Add(this.txtPosIrsa);
            this.groupBox3.Controls.Add(this.txtLocalIrsa);
            this.groupBox3.Controls.Add(this.txtContratoIrsa);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Location = new System.Drawing.Point(18, 326);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox3.Size = new System.Drawing.Size(1164, 154);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Shopping IRSA";
            // 
            // txtPathIrsa
            // 
            this.txtPathIrsa.Location = new System.Drawing.Point(159, 89);
            this.txtPathIrsa.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtPathIrsa.Name = "txtPathIrsa";
            this.txtPathIrsa.Size = new System.Drawing.Size(417, 26);
            this.txtPathIrsa.TabIndex = 9;
            // 
            // txtRubroIrsa
            // 
            this.txtRubroIrsa.Location = new System.Drawing.Point(927, 35);
            this.txtRubroIrsa.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtRubroIrsa.Name = "txtRubroIrsa";
            this.txtRubroIrsa.Size = new System.Drawing.Size(148, 26);
            this.txtRubroIrsa.TabIndex = 8;
            // 
            // txtPosIrsa
            // 
            this.txtPosIrsa.Location = new System.Drawing.Point(651, 35);
            this.txtPosIrsa.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtPosIrsa.Name = "txtPosIrsa";
            this.txtPosIrsa.Size = new System.Drawing.Size(148, 26);
            this.txtPosIrsa.TabIndex = 7;
            // 
            // txtLocalIrsa
            // 
            this.txtLocalIrsa.Location = new System.Drawing.Point(366, 35);
            this.txtLocalIrsa.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtLocalIrsa.Name = "txtLocalIrsa";
            this.txtLocalIrsa.Size = new System.Drawing.Size(148, 26);
            this.txtLocalIrsa.TabIndex = 6;
            // 
            // txtContratoIrsa
            // 
            this.txtContratoIrsa.Location = new System.Drawing.Point(88, 35);
            this.txtContratoIrsa.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtContratoIrsa.Name = "txtContratoIrsa";
            this.txtContratoIrsa.Size = new System.Drawing.Size(148, 26);
            this.txtContratoIrsa.TabIndex = 5;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(9, 94);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(135, 20);
            this.label14.TabIndex = 4;
            this.label14.Text = "Ubicación Archivo";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(864, 40);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(53, 20);
            this.label13.TabIndex = 3;
            this.label13.Text = "Rubro";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(598, 40);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(42, 20);
            this.label12.TabIndex = 2;
            this.label12.Text = "POS";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(308, 40);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(47, 20);
            this.label11.TabIndex = 1;
            this.label11.Text = "Local";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(9, 40);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(71, 20);
            this.label10.TabIndex = 0;
            this.label10.Text = "Contrato";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtPathCaballito);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Location = new System.Drawing.Point(18, 488);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox4.Size = new System.Drawing.Size(1164, 69);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Shopping Caballito";
            // 
            // txtPathCaballito
            // 
            this.txtPathCaballito.Location = new System.Drawing.Point(159, 28);
            this.txtPathCaballito.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtPathCaballito.Name = "txtPathCaballito";
            this.txtPathCaballito.Size = new System.Drawing.Size(481, 26);
            this.txtPathCaballito.TabIndex = 1;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(9, 32);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(135, 20);
            this.label15.TabIndex = 0;
            this.label15.Text = "Ubicación Archivo";
            // 
            // txtPathOlmos
            // 
            this.txtPathOlmos.Location = new System.Drawing.Point(159, 22);
            this.txtPathOlmos.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtPathOlmos.Name = "txtPathOlmos";
            this.txtPathOlmos.Size = new System.Drawing.Size(481, 26);
            this.txtPathOlmos.TabIndex = 1;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(9, 28);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(135, 20);
            this.label16.TabIndex = 0;
            this.label16.Text = "Ubicación Archivo";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.txtPathOlmos);
            this.groupBox5.Controls.Add(this.label16);
            this.groupBox5.Location = new System.Drawing.Point(18, 563);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox5.Size = new System.Drawing.Size(1164, 66);
            this.groupBox5.TabIndex = 5;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Shopping Olmos";
            // 
            // txtLicenciaIcg
            // 
            this.txtLicenciaIcg.Location = new System.Drawing.Point(159, 25);
            this.txtLicenciaIcg.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtLicenciaIcg.Name = "txtLicenciaIcg";
            this.txtLicenciaIcg.ReadOnly = true;
            this.txtLicenciaIcg.Size = new System.Drawing.Size(481, 26);
            this.txtLicenciaIcg.TabIndex = 1;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(9, 29);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(100, 20);
            this.label17.TabIndex = 0;
            this.label17.Text = "Licencia ICG";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.btnLicencia);
            this.groupBox6.Controls.Add(this.txtLicenciaIcg);
            this.groupBox6.Controls.Add(this.label17);
            this.groupBox6.Location = new System.Drawing.Point(18, 634);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox6.Size = new System.Drawing.Size(1164, 72);
            this.groupBox6.TabIndex = 6;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Licencia ICG";
            // 
            // btnLicencia
            // 
            this.btnLicencia.Location = new System.Drawing.Point(759, 22);
            this.btnLicencia.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnLicencia.Name = "btnLicencia";
            this.btnLicencia.Size = new System.Drawing.Size(267, 35);
            this.btnLicencia.TabIndex = 2;
            this.btnLicencia.Text = "Generar Licencia";
            this.btnLicencia.UseVisualStyleBackColor = true;
            this.btnLicencia.Click += new System.EventHandler(this.btnLicencia_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(45, 720);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(676, 50);
            this.label18.TabIndex = 7;
            this.label18.Text = "IMPORTANTE: Solo debe llenar los campos del Shopping que corresponda.\r\nPara la li" +
    "cencia GENERE LA KEY y comuniquesela a ICG Argentina";
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(992, 729);
            this.btnClose.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(156, 35);
            this.btnClose.TabIndex = 8;
            this.btnClose.Text = "Salir sin Guardar";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // chkFiServ
            // 
            this.chkFiServ.AutoSize = true;
            this.chkFiServ.Location = new System.Drawing.Point(603, 89);
            this.chkFiServ.Name = "chkFiServ";
            this.chkFiServ.Size = new System.Drawing.Size(150, 24);
            this.chkFiServ.TabIndex = 10;
            this.chkFiServ.Text = "Envío por FiServ";
            this.chkFiServ.UseVisualStyleBackColor = true;
            this.chkFiServ.CheckedChanged += new System.EventHandler(this.chkFiServ_CheckedChanged);
            // 
            // txtCuit
            // 
            this.txtCuit.Location = new System.Drawing.Point(927, 87);
            this.txtCuit.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCuit.Name = "txtCuit";
            this.txtCuit.Size = new System.Drawing.Size(148, 26);
            this.txtCuit.TabIndex = 12;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(864, 92);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(46, 20);
            this.label19.TabIndex = 11;
            this.label19.Text = "CUIT";
            // 
            // frmConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1200, 789);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmConfig";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ICG Argentina. Configuración Hasar 2G";
            this.Load += new System.EventHandler(this.frmConfig_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtUsuario;
        private System.Windows.Forms.TextBox txtBaseDatos;
        private System.Windows.Forms.TextBox txtServidor;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtTicketReg3;
        private System.Windows.Forms.TextBox txtTicketReg2;
        private System.Windows.Forms.TextBox txtTicketReg1;
        private System.Windows.Forms.TextBox txtPtoVta;
        private System.Windows.Forms.TextBox txtCaja;
        private System.Windows.Forms.TextBox txtIp;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtPathIrsa;
        private System.Windows.Forms.TextBox txtRubroIrsa;
        private System.Windows.Forms.TextBox txtPosIrsa;
        private System.Windows.Forms.TextBox txtLocalIrsa;
        private System.Windows.Forms.TextBox txtContratoIrsa;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txtPathCaballito;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtPathOlmos;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox txtLicenciaIcg;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button btnLicencia;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btValidar;
        private System.Windows.Forms.CheckBox chkUsaTktRegalo;
        private System.Windows.Forms.Button btnGetRegestry;
        private System.Windows.Forms.CheckBox chkFiServ;
        private System.Windows.Forms.TextBox txtCuit;
        private System.Windows.Forms.Label label19;
    }
}