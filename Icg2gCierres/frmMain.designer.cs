﻿namespace Icg2gCierres
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btCierreX = new System.Windows.Forms.Button();
            this.btCierreZ = new System.Windows.Forms.Button();
            this.btSalir = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btCierreX
            // 
            this.btCierreX.Location = new System.Drawing.Point(34, 30);
            this.btCierreX.Name = "btCierreX";
            this.btCierreX.Size = new System.Drawing.Size(75, 23);
            this.btCierreX.TabIndex = 0;
            this.btCierreX.Text = "Cierre X";
            this.btCierreX.UseVisualStyleBackColor = true;
            this.btCierreX.Click += new System.EventHandler(this.btCierreX_Click);
            // 
            // btCierreZ
            // 
            this.btCierreZ.Location = new System.Drawing.Point(128, 30);
            this.btCierreZ.Name = "btCierreZ";
            this.btCierreZ.Size = new System.Drawing.Size(75, 23);
            this.btCierreZ.TabIndex = 1;
            this.btCierreZ.Text = "Cierre Z";
            this.btCierreZ.UseVisualStyleBackColor = true;
            this.btCierreZ.Click += new System.EventHandler(this.btCierreZ_Click);
            // 
            // btSalir
            // 
            this.btSalir.Location = new System.Drawing.Point(328, 30);
            this.btSalir.Name = "btSalir";
            this.btSalir.Size = new System.Drawing.Size(75, 23);
            this.btSalir.TabIndex = 2;
            this.btSalir.Text = "Salir";
            this.btSalir.UseVisualStyleBackColor = true;
            this.btSalir.Click += new System.EventHandler(this.btSalir_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(222, 30);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(91, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "Reimpresión Z";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(463, 101);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btSalir);
            this.Controls.Add(this.btCierreZ);
            this.Controls.Add(this.btCierreX);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ICG - Cierres";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmMain_FormClosed);
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btCierreX;
        private System.Windows.Forms.Button btCierreZ;
        private System.Windows.Forms.Button btSalir;
        private System.Windows.Forms.Button button1;
    }
}