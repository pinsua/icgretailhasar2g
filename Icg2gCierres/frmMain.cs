﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Icg2gCierres
{
    public partial class frmMain : Form
    {
        public string _caja;
        public string _ip;
        public string _server;
        public static string _password;

        public string _user;

        public string _catalog;

        public string strConnection;

        public bool _habilitoPassord = false;

        public bool _hasarLog = false;

        //public SqlConnection _conn = new SqlConnection();
        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            string _boolPasword = "false";
            try
            {
                if (File.Exists("Config2daGeneracion.xml"))
                {
                    XmlDocument xDoc = new XmlDocument();
                    xDoc.Load("Config2daGeneracion.xml");

                    XmlNodeList _List1 = xDoc.SelectNodes("/config");
                    foreach (XmlNode xn in _List1)
                    {
                        _ip = xn["ip"].InnerText;
                        _caja = xn["caja"].InnerText;
                        _password = xn["super"].InnerText;
                        _boolPasword = xn["habilitoPassword"].InnerText;
                        _hasarLog = Convert.ToBoolean(xn["hasarlog"].InnerText);
                    }

                    XmlNodeList _Listdb = xDoc.SelectNodes("/config/bd");
                    foreach (XmlNode xn in _Listdb)
                    {
                        _server = xn["server"].InnerText;
                        _user = xn["user"].InnerText;
                        _catalog = xn["database"].InnerText;
                    }

                    _habilitoPassord = Convert.ToBoolean(_boolPasword);

                    //Recuperado los datos vamos por la conexion.
                    //Armamos el stringConnection.
                    strConnection = "Data Source=" + _server + ";Initial Catalog=" + _catalog + ";User Id=" + _user + ";Password=masterkey;";

                }
                else
                {
                    DeshabilitarBotones();
                    MessageBox.Show(new Form { TopMost = true }, "No se encuentra el archivo de configuracióm. Por favor comuniquese con ICG Argentina.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error: " + ex.Message +". Por favor comuniquese con ICG Argentina.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
        }

        private void DeshabilitarBotones()
        {
            btCierreX.Enabled = false;
            btCierreZ.Enabled = false;
            btSalir.Enabled = true;
        }

        private void btCierreX_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show(new Form { TopMost = true }, "Seguro que desea realizar un Cierre X?.", "ICG - Argentina",
                MessageBoxButtons.YesNo);
            switch (result)
            {
                case DialogResult.Yes:
                    {
                        hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();
                        hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarJornadaFiscal _cierre = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarJornadaFiscal();
                        hfl.argentina.HasarImpresoraFiscalRG3561.CerrarJornadaFiscalX _equis = new hfl.argentina.HasarImpresoraFiscalRG3561.CerrarJornadaFiscalX();

                        string _msj;

                        try
                        {
                            //Conectamos.
                            Conectar(hasar);
                            //ejecutamos el cierre Z.
                            _cierre = hasar.CerrarJornadaFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.TipoReporte.REPORTE_X);
                            _equis = _cierre.X;

                            _msj = "INFORME DIARIO DE CIERRE :" + Environment.NewLine +
                              "Cierre 'X' Nº            =[" + _equis.getNumero() + "]" + Environment.NewLine +
                              "Fecha del Cierre         =[" + _equis.getFechaCierre() + "]" + Environment.NewLine +
                              "DF Emitidos              =[" + _equis.getDF_CantidadEmitidos() + "]" + Environment.NewLine +
                              "DF Total                 =[" + _equis.getDF_Total() + "]" + Environment.NewLine +
                              "DF Total IVA             =[" + _equis.getDF_TotalIVA() + "]" + Environment.NewLine +
                              "DF Total Otros Tributos  =[" + _equis.getDF_TotalTributos() + "]" + Environment.NewLine +
                              "NC Emitidas              =[" + _equis.getNC_CantidadEmitidos() + "]" + Environment.NewLine +
                              "NC Total                 =[" + _equis.getNC_Total() + "]" + Environment.NewLine +
                              "NC Total IVA             =[" + _equis.getNC_TotalIVA() + "]" + Environment.NewLine +
                              "NC Total Otros Tributos  =[" + _equis.getNC_TotalTributos() + "]" + Environment.NewLine +
                              "DNFH Emitidos            =[" + _equis.getDNFH_CantidadEmitidos() + "]";

                            MessageBox.Show(new Form { TopMost = true }, _msj, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);


                        }
                        catch (Exception ex)
                        {
                            Cancelar(hasar);
                            MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente Error : " + ex.Message,
                                "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                        }

                        break;
                    }
                case DialogResult.No:
                    {
                        this.Text = "[Cancel]";
                        break;
                    }
            }
        }

            private void Conectar(hfl.argentina.HasarImpresoraFiscalRG3561 _cf)
            {
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarVersion _respVersion = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarVersion();
                try
                {
                    //Abrimos el log.
                    if(_hasarLog)
                        _cf.archivoRegistro("HasarLog.log");
                    //Conectamos con la hasar.
                    _cf.conectar(_ip);
                }
                catch (Exception ex)
                {
                    throw new Exception("No se pudo conectar con la Controladora Fiscal.");
                }
            }

        private void Cancelar(hfl.argentina.HasarImpresoraFiscalRG3561 _cf)
        {
            hfl.argentina.Estados_Fiscales_RG3561.EstadoFiscal stfiscal = new hfl.argentina.Estados_Fiscales_RG3561.EstadoFiscal();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarEstado resp = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarEstado();

            resp = _cf.ConsultarEstado();
            stfiscal = _cf.ObtenerUltimoEstadoFiscal();

            if (stfiscal.getDocumentoAbierto())
            {
                try
                {
                    _cf.Cancelar();
                }
                catch
                {
                    throw new Exception("Se produjo un error al cancelar un documento.");
                }
            }
        }

        private void btSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            //if (_conn.State == ConnectionState.Open)
            //    _conn.Close();
        }

        private void btCierreZ_Click(object sender, EventArgs e)
        {
            frmCierreX frm = new frmCierreX(_ip,strConnection, _caja, _habilitoPassord);
            frm.ShowDialog(this);
            frm.Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmReimpresion frm = new frmReimpresion(_caja, "Z", _ip, strConnection);
            frm.ShowDialog(this);
            frm.Dispose();

        }
    }
}
