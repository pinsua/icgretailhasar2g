﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Hasar2daGen
{
    public class Cabecera
    {
        public string numserie { get; set; }
        public int numalbaran { get; set; }
        public string n { get; set; }
        public int numfac { get; set; }
        public int codcliente { get; set; }
        public int codvendedor { get; set; }
        public double dtocomercial { get; set; }
        public double totdtocomercial { get; set; }
        public double totalbruto { get; set; }
        public double totalimpuestos { get; set; }
        public double totalneto { get; set; }
        public int tipodoc { get; set; }
        public string nombrecliente { get; set; }
        public string direccioncliente { get; set; }
        public string nrodoccliente { get; set; }
        public string codpostalcliente { get; set; }
        public string ciudadcliente { get; set; }
        public string provinciacliente { get; set; }
        public string regfacturacioncliente { get; set; }
        public string descripcionticket { get; set; }
        public double dtopp { get; set; }
        public double totdtopp { get; set; }
        public double entregado { get; set; }
        public DateTime fecha { get; set; }
        public int z { get; set; }
        public string telefono { get; set; }
        public string nomvendedor { get; set; }
        public DateTime hora { get; set; }

        public static Cabecera GetCabecera(string _serie, int _numero, string _N, SqlConnection _con)
        {
            string _sql = "SELECT ALBVENTACAB.NUMSERIE, ALBVENTACAB.NUMALBARAN, ALBVENTACAB.N, ALBVENTACAB.NUMFAC, ALBVENTACAB.CODCLIENTE, ALBVENTACAB.CODVENDEDOR, " +
                "ALBVENTACAB.DTOCOMERCIAL, ALBVENTACAB.TOTDTOCOMERCIAL, ALBVENTACAB.TOTALBRUTO, ALBVENTACAB.TOTALIMPUESTOS, ALBVENTACAB.TOTALNETO, ALBVENTACAB.TIPODOC, ALBVENTACAB.DTOPP, ALBVENTACAB.TOTDTOPP, " +
                "CLIENTES.NOMBRECLIENTE, CLIENTES.DIRECCION1, CLIENTES.NIF20, CLIENTES.CODPOSTAL, CLIENTES.POBLACION, CLIENTES.PROVINCIA, CLIENTES.REGIMFACT, " +
                "TIPOSDOC.DESCRIPCION, FACTURASVENTA.ENTREGADO, FACTURASVENTA.FECHA, ALBVENTACAB.Z, CLIENTES.TELEFONO1, VENDEDORES.NOMVENDEDOR, ALBVENTACAB.HORA  " +
                "FROM FACTURASVENTA INNER JOIN  ALBVENTACAB on FACTURASVENTA.NUMSERIE = ALBVENTACAB.NUMSERIE AND FACTURASVENTA.NUMFACTURA = ALBVENTACAB.NUMFAC AND FACTURASVENTA.N = ALBVENTACAB.N " +
                "INNER JOIN CLIENTES ON ALBVENTACAB.CODCLIENTE = CLIENTES.CODCLIENTE " +
                "INNER JOIN TIPOSDOC ON ALBVENTACAB.TIPODOC = TIPOSDOC.TIPODOC " +
                "INNER JOIN VENDEDORES ON FACTURASVENTA.CODVENDEDOR = VENDEDORES.CODVENDEDOR "+
                "WHERE FACTURASVENTA.NUMSERIE = @Numserie AND facturasventa.NUMFACTURA = @NumFact AND FACTURASVENTA.N = @N";

            Cabecera _cabecera = new Cabecera();

            using (SqlCommand _cmd = new SqlCommand())
            {
                _cmd.Connection = _con;
                _cmd.CommandType = System.Data.CommandType.Text;
                _cmd.CommandText = _sql;

                _cmd.Parameters.AddWithValue("@Numserie", _serie);
                _cmd.Parameters.AddWithValue("@N", _N);
                _cmd.Parameters.AddWithValue("@NumFact", _numero);

                using (SqlDataReader _reader = _cmd.ExecuteReader())
                {
                    if (_reader.HasRows)
                    {
                        while (_reader.Read())
                        {
                            _cabecera.ciudadcliente = _reader["POBLACION"].ToString();
                            _cabecera.codcliente = Convert.ToInt32(_reader["CODCLIENTE"]);
                            _cabecera.codpostalcliente = _reader["CODPOSTAL"].ToString();
                            _cabecera.codvendedor = Convert.ToInt32(_reader["CODVENDEDOR"]);
                            _cabecera.descripcionticket = _reader["DESCRIPCION"].ToString();
                            _cabecera.direccioncliente = _reader["DIRECCION1"].ToString();
                            _cabecera.dtocomercial = Convert.ToDouble(_reader["DTOCOMERCIAL"]);
                            _cabecera.n = _reader["N"].ToString();
                            _cabecera.nombrecliente = _reader["NOMBRECLIENTE"].ToString();
                            _cabecera.nrodoccliente = _reader["NIF20"].ToString();
                            _cabecera.numalbaran = Convert.ToInt32(_reader["NUMALBARAN"]);
                            _cabecera.numfac = Convert.ToInt32(_reader["NUMFAC"]);
                            _cabecera.numserie = _reader["NUMSERIE"].ToString();
                            _cabecera.provinciacliente = _reader["PROVINCIA"].ToString();
                            _cabecera.regfacturacioncliente = _reader["REGIMFACT"].ToString();
                            _cabecera.tipodoc = Convert.ToInt32(_reader["TIPODOC"]);
                            _cabecera.totalbruto = Convert.ToDouble(_reader["TOTALBRUTO"]);
                            _cabecera.totalimpuestos = Convert.ToDouble(_reader["TOTALIMPUESTOS"]);
                            _cabecera.totalneto = Convert.ToDouble(_reader["TOTALNETO"]);
                            _cabecera.totdtocomercial = Convert.ToDouble(_reader["TOTDTOCOMERCIAL"]);
                            _cabecera.dtopp = Convert.ToDouble(_reader["DTOPP"]);
                            _cabecera.totdtopp = Convert.ToDouble(_reader["TOTDTOPP"]);
                            _cabecera.entregado = Convert.ToDouble(_reader["ENTREGADO"]);
                            _cabecera.fecha = Convert.ToDateTime(_reader["FECHA"]);
                            _cabecera.z = Convert.ToInt32(_reader["Z"]);
                            _cabecera.telefono = _reader["TELEFONO1"].ToString();
                            _cabecera.nomvendedor = _reader["NOMVENDEDOR"].ToString();
                            _cabecera.hora = Convert.ToDateTime(_reader["HORA"]);
                        }
                    }
                }
            }

            return _cabecera;
        }

        public static bool GrabarZeta(string _serie, int _numero, string _N, int _zeta, SqlConnection _con)
        {
            string _sql = "UPDATE FACTURASVENTA SET CLEANCASHCONTROLCODE1 = @Zeta WHERE NUMSERIE = @Serie AND NUMFACTURA = @Numero AND N = @N";

            bool _rta = false;

            try
            {
                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;

                    _cmd.Parameters.AddWithValue("@Serie", _serie);
                    _cmd.Parameters.AddWithValue("@Numero", _numero);
                    _cmd.Parameters.AddWithValue("@N", _N);
                    _cmd.Parameters.AddWithValue("@Zeta", _zeta);

                    int _insert = _cmd.ExecuteNonQuery();
                    _rta = true;
                }
            }
            catch (Exception)
            {
                _rta = false;
            }

            return _rta;
        }

        public static void GrabarZetaNew(FacturasVentaCamposLibres _fvcl, SqlConnection _con, SqlTransaction _transac)
        {
            string _sql = "UPDATE FACTURASVENTA SET CLEANCASHCONTROLCODE1 = @Zeta WHERE NUMSERIE = @Serie AND NUMFACTURA = @Numero AND N = @N";

            try
            {
                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;
                    _cmd.Transaction = _transac;

                    _cmd.Parameters.AddWithValue("@Serie", _fvcl.numSerie);
                    _cmd.Parameters.AddWithValue("@Numero", _fvcl.numFactura);
                    _cmd.Parameters.AddWithValue("@N", _fvcl.N);
                    _cmd.Parameters.AddWithValue("@Zeta", _fvcl.zNro);

                    int _insert = _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("GrabarZetaNew. Error: " + ex.Message);
            }
        }
    }
}
