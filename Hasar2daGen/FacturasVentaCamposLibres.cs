﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hasar2daGen
{
    public class FacturasVentaCamposLibres
    {
        public string numSerie { get; set; }
        public int numFactura { get; set; }
        public string N { get; set; }
        public int zNro { get; set; }
        public double zTotal { get; set; }
        public DateTime zFechaHora { get; set; }
        public double zImpuestos { get; set; }
        public int zTiquets { get; set; }

        public static void UpdateFacturasVentaCamposLibres_Tiquet(FacturasVentaCamposLibres _fvcl, SqlConnection _sqlConn, SqlTransaction _sqlTransac)
        {
            string _sql = @"UPDATE FACTURASVENTACAMPOSLIBRES SET Z_NRO = @zNro WHERE NUMSERIE = @numSerie AND NUMFACTURA = @numFac AND N = @n";

            try
            {
                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _sqlConn;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;
                    _cmd.Transaction = _sqlTransac;

                    _cmd.Parameters.AddWithValue("@numSerie", _fvcl.numSerie);
                    _cmd.Parameters.AddWithValue("@numFac", _fvcl.numFactura);
                    _cmd.Parameters.AddWithValue("@n", _fvcl.N);
                    _cmd.Parameters.AddWithValue("@zNro", _fvcl.zNro);                    

                    int _insert = _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("UpdateFacturasVentaCamposLibres_Tiquet. Error: " + ex.Message);
            }
        }

        public static void UpdateFacturasVentaCamposLibres_CierreZ(FacturasVentaCamposLibres _fvcl, string _caja, SqlConnection _sqlConn, SqlTransaction _sqlTransac)
        {
            string _sql = @"UPDATE FACTURASVENTACAMPOSLIBRES SET FACTURASVENTACAMPOSLIBRES.Z_FECHA_HORA = @FECHA, FACTURASVENTACAMPOSLIBRES.Z_TOTAL = @TOTAL, 
                FACTURASVENTACAMPOSLIBRES.Z_IMPUESTOS = @IMPUESTOS, FACTURASVENTACAMPOSLIBRES.Z_TIQUETS = @TIQUETS
                FROM FACTURASVENTA INNER JOIN FACTURASVENTACAMPOSLIBRES on FACTURASVENTA.numserie = FACTURASVENTACAMPOSLIBRES.numserie 
                and FACTURASVENTA.numfactura = FACTURASVENTACAMPOSLIBRES.numfactura and FACTURASVENTA.n = FACTURASVENTACAMPOSLIBRES.n
                WHERE FACTURASVENTACAMPOSLIBRES.z_nro = @nroZeta and FACTURASVENTA.CAJA = @caja";

            try
            {
                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _sqlConn;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;
                    _cmd.Transaction = _sqlTransac;
                    _cmd.Parameters.AddWithValue("@zNro", _fvcl.zNro);
                    _cmd.Parameters.AddWithValue("@zTotal", _fvcl.zTotal);
                    _cmd.Parameters.AddWithValue("@zFechaHora", _fvcl.zFechaHora);
                    _cmd.Parameters.AddWithValue("@zImpuestos", _fvcl.zImpuestos);
                    _cmd.Parameters.AddWithValue("@zTiquets", _fvcl.zTiquets);
                    _cmd.Parameters.AddWithValue("@caja", _caja);

                    int _insert = _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("UpdateFacturasVentaCamposLibres_CierreZ. Error: " + ex.Message);
            }
        }

        public static void UpdateFacturasVentaCamposLibres_CierreZ(FacturasVentaCamposLibres _fvcl, string _caja, SqlConnection _sqlConn)
        {
            string _sql = @"UPDATE FACTURASVENTACAMPOSLIBRES SET FACTURASVENTACAMPOSLIBRES.Z_FECHA_HORA = @FECHA, FACTURASVENTACAMPOSLIBRES.Z_TOTAL = @TOTAL, 
                FACTURASVENTACAMPOSLIBRES.Z_IMPUESTOS = @IMPUESTOS, FACTURASVENTACAMPOSLIBRES.Z_TIQUETS = @TIQUETS
                FROM FACTURASVENTA INNER JOIN FACTURASVENTACAMPOSLIBRES on FACTURASVENTA.numserie = FACTURASVENTACAMPOSLIBRES.numserie 
                and FACTURASVENTA.numfactura = FACTURASVENTACAMPOSLIBRES.numfactura and FACTURASVENTA.n = FACTURASVENTACAMPOSLIBRES.n
                WHERE FACTURASVENTACAMPOSLIBRES.z_nro = @nroZeta and FACTURASVENTA.CAJA = @caja";

            try
            {
                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _sqlConn;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;
                    _cmd.Parameters.AddWithValue("@nroZeta", _fvcl.zNro);
                    _cmd.Parameters.AddWithValue("@TOTAL", _fvcl.zTotal);
                    _cmd.Parameters.AddWithValue("@FECHA", _fvcl.zFechaHora);
                    _cmd.Parameters.AddWithValue("@IMPUESTOS", _fvcl.zImpuestos);
                    _cmd.Parameters.AddWithValue("@TIQUETS", _fvcl.zTiquets);
                    _cmd.Parameters.AddWithValue("@caja", _caja);

                    int _insert = _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("UpdateFacturasVentaCamposLibres_CierreZ. Error: " + ex.Message);
            }
        }
    }
}
