﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Xml;
using System.ComponentModel.DataAnnotations;
using static System.Net.Mime.MediaTypeNames;

namespace Hasar2daGen
{
    public class FuncionesVarias
    {
        /// <summary>
        /// Calcula el dígito verificador dado un CUIT completo o sin él.
        /// </summary>
        /// <param name="cuit">El CUIT como String sin guiones</param>
        /// <returns>El valor del dígito verificador calculado.</returns>
        public static int CalcularDigitoCuit(string cuit)
        {
            int[] mult = { 5, 4, 3, 2, 7, 6, 5, 4, 3, 2 };
            char[] nums = cuit.ToCharArray();
            int total = 0;
            for (int i = 0; i < mult.Length; i++)
            {
                total += int.Parse(nums[i].ToString()) * mult[i];
            }
            int resto = total % 11;
            return resto == 0 ? 0 : resto == 1 ? 9 : 11 - resto;
        }

        public static bool FechaOk(hfl.argentina.HasarImpresoraFiscalRG3561 _cf, Cabecera _cab)
        {
            bool _rta = false;
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarFechaHora _respFechaHora = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarFechaHora();
            _respFechaHora = _cf.ConsultarFechaHora();

            DateTime _fecha = _respFechaHora.getFecha();

            if (_cab.fecha.Date == _fecha.Date)
                _rta = true;
            else
            {
                if (_cab.fecha.Date >= _fecha.Date.AddDays(-30) && _cab.fecha.Date < _fecha.Date)
                    _rta = true;
            }

            return _rta;
        }

        public static bool TengoComprobantesSinImprimmir(SqlConnection _conn, string _caja)
        {
            bool _rta = true;

            string _sql = "SELECT count(*) FROM FACTURASVENTASERIESRESOL RIGHT JOIN ALBVENTACAB on " +
                "FACTURASVENTASERIESRESOL.NUMSERIE = ALBVENTACAB.NUMSERIE AND FACTURASVENTASERIESRESOL.NUMFACTURA = ALBVENTACAB.NUMFAC " +
                "AND FACTURASVENTASERIESRESOL.N = ALBVENTACAB.N INNER JOIN CLIENTES ON ALBVENTACAB.CODCLIENTE = CLIENTES.CODCLIENTE " +
                "INNER JOIN TIPOSDOC ON ALBVENTACAB.TIPODOC = TIPOSDOC.TIPODOC " +
                "WHERE(FACTURASVENTASERIESRESOL.NUMEROFISCAL is null or FACTURASVENTASERIESRESOL.NUMEROFISCAL = '') " +
                "AND ALBVENTACAB.N = 'B' AND ALBVENTACAB.NUMFAC > -1 AND CLIENTES.REGIMFACT <> 'N' AND ALBVENTACAB.CAJA = @caja AND ALBVENTACAB.FECHA = @date";

            using (SqlCommand _cmd = new SqlCommand(_sql, _conn))
            {
                _cmd.Parameters.AddWithValue("@date", DateTime.Now.Date);
                _cmd.Parameters.AddWithValue("@caja", _caja);

                object _resp = _cmd.ExecuteScalar();

                if (_resp == null)
                    _rta = false;
                else
                {
                    if (Convert.ToInt32(_resp) > 0)
                        _rta = true;
                    else
                        _rta = false;
                }
            }

            return _rta;
        }

        public static bool FaltaPapel(hfl.argentina.HasarImpresoraFiscalRG3561 _cf)
        {
            if (_cf.ObtenerUltimoEstadoImpresora().getFaltaPapelReceipt())
            {
                return true;
            }
            else
                return false;
        }

        public static string QueComprobanteEs(string serie, string numero, string n, string vendedor,
           SqlConnection _con)
        {
            string _sql = @"select  TIPOSDOC.DESCRIPCION from FACTURASVENTA inner join TIPOSDOC on FACTURASVENTA.TIPODOC = TIPOSDOC.TIPODOC
                    WHERE FACTURASVENTA.NUMSERIE = @serie AND FACTURASVENTA.NUMFACTURA = @numero AND FACTURASVENTA.N = @n
                    AND FACTURASVENTA.CODVENDEDOR = @codVendedor";

            string _rta = "";

            using (SqlCommand _cmd = new SqlCommand())
            {
                _cmd.Connection = _con;
                _cmd.CommandType = System.Data.CommandType.Text;
                _cmd.CommandText = _sql;

                //_cmd.Parameters.AddWithValue("@fo", fo);
                _cmd.Parameters.AddWithValue("@serie", serie);
                _cmd.Parameters.AddWithValue("@n", n);
                _cmd.Parameters.AddWithValue("@numero", numero);
                _cmd.Parameters.AddWithValue("@codVendedor", vendedor);

                using (SqlDataReader _reader = _cmd.ExecuteReader())
                {
                    if (_reader.HasRows)
                    {
                        while (_reader.Read())
                        {
                            _rta = _reader["DESCRIPCION"].ToString();
                        }
                    }
                }
            }

            return _rta;
        }

        public static bool ComparoSubtotal(hfl.argentina.HasarImpresoraFiscalRG3561 _cf, double _totalNeto)
        {
            bool _rta = false;
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal _respConsultaSubtotal = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal();

            _respConsultaSubtotal = _cf.ConsultarSubtotal();
            double _subTotal = _respConsultaSubtotal.getSubtotal() / 100;
            if (_subTotal != Math.Abs(_totalNeto))
            {
                double _dif = _subTotal - Math.Abs(_totalNeto);
                double _pipo = double.Parse(".2", CultureInfo.InvariantCulture);
                if (_dif > _pipo)
                    _rta = false;
                else
                    _rta = true;
            }
            else
                _rta = true;
                return _rta;
        }

        public static bool LeerXmlConfiguracion(out string _ip, out string _caja,
            out string _password, out string _server, out string _user, out string _catalog, out bool _monotributo
            , out string _tktregalo1, out string _tktregalo2, out string _tktregalo3,
            out bool _hasarLog, out string _keyICG, out InfoIrsa _infoIrsa, out string _pathCaballito, out string _pathOlmos)
        {
            bool _rta = false;
            _ip = "";
            _caja = "";
            _password = "";
            _server = "";
            _user = "";
            _catalog = "";
            _monotributo = false;
            _hasarLog = false;
            _tktregalo1 = "";
            _tktregalo2 = "";
            _tktregalo3 = "";
            _keyICG = "";
            _pathCaballito = "";
            _infoIrsa = new InfoIrsa();
            _pathOlmos = "";

            try
            {
                if (File.Exists("Config2daGeneracion.xml"))
                {
                    XmlDocument xDoc = new XmlDocument();
                    xDoc.Load("Config2daGeneracion.xml");
                    //IP
                    XmlNodeList _IP = xDoc.GetElementsByTagName("ip");
                    if (_IP.Count > 0)
                        _ip = _IP[0].InnerXml;
                    else
                        _ip = "";
                    //CAJA
                    XmlNodeList _CAJA = xDoc.GetElementsByTagName("caja");
                    if (_CAJA.Count > 0)
                        _caja = _CAJA[0].InnerXml;
                    else
                        _caja = "";
                    //Super
                    XmlNodeList _PASSWORD = xDoc.GetElementsByTagName("super");
                    if (_CAJA.Count > 0)
                        _password = _PASSWORD[0].InnerXml;
                    else
                        _password = "";
                    //Monotributo
                    XmlNodeList _MONOTRIBUTO = xDoc.GetElementsByTagName("monotributo");
                    if (_MONOTRIBUTO.Count > 0)
                        _monotributo = Convert.ToBoolean(_MONOTRIBUTO[0].InnerXml);
                    else
                        _monotributo = false;
                    //tktregalo1
                    XmlNodeList _TKTREGALO1 = xDoc.GetElementsByTagName("tktregalo1");
                    if (_TKTREGALO1.Count > 0)
                        _tktregalo1 = _TKTREGALO1[0].InnerXml;
                    else
                        _tktregalo1 = "";
                    //tktregalo2
                    XmlNodeList _TKTREGALO2 = xDoc.GetElementsByTagName("tktregalo2");
                    if (_TKTREGALO2.Count > 0)
                        _tktregalo2 = _TKTREGALO2[0].InnerXml;
                    else
                        _tktregalo2 = "";
                    //tktregalo3
                    XmlNodeList _TKTREGALO3 = xDoc.GetElementsByTagName("tktregalo3");
                    if (_TKTREGALO3.Count > 0)
                        _tktregalo3 = _TKTREGALO3[0].InnerXml;
                    else
                        _tktregalo3 = "";
                    //keyICG
                    XmlNodeList _KEYICG = xDoc.GetElementsByTagName("keyICG");
                    if (_KEYICG.Count > 0)
                        _keyICG = _KEYICG[0].InnerXml;
                    else
                        _keyICG = "";
                    //HasarLog
                    XmlNodeList _HASARLOG = xDoc.GetElementsByTagName("hasarlog");
                    if (_HASARLOG.Count > 0)
                        _hasarLog = Convert.ToBoolean(_HASARLOG[0].InnerXml);
                    else
                        _hasarLog = false;
                    //SEver
                    XmlNodeList _SEVER = xDoc.GetElementsByTagName("server");
                    if (_SEVER.Count > 0)
                        _server = _SEVER[0].InnerXml;
                    else
                        _server = "";
                    //USER
                    XmlNodeList _USER = xDoc.GetElementsByTagName("user");
                    if (_USER.Count > 0)
                        _user = _USER[0].InnerXml;
                    else
                        _user = "";
                    //CATALOG
                    XmlNodeList _CATALOG = xDoc.GetElementsByTagName("database");
                    if (_CATALOG.Count > 0)
                        _catalog = _CATALOG[0].InnerXml;
                    else
                        _catalog = "";

                    //IRSA CATALOG
                    XmlNodeList _CONTRATO = xDoc.GetElementsByTagName("Contrato");
                    if (_CONTRATO.Count > 0)
                        _infoIrsa.contrato = _CONTRATO[0].InnerXml;
                    else
                        _infoIrsa.contrato = "";
                    //IRSA CATALOG
                    XmlNodeList _LOCAL = xDoc.GetElementsByTagName("Local");
                    if (_LOCAL.Count > 0)
                        _infoIrsa.local = _LOCAL[0].InnerXml;
                    else
                        _infoIrsa.local = "";
                    //IRSA CATALOG
                    XmlNodeList PATHSALIDAIRSA = xDoc.GetElementsByTagName("PathSalidaIrsa");
                    if (PATHSALIDAIRSA.Count > 0)
                        _infoIrsa.pathSalida = PATHSALIDAIRSA[0].InnerXml;
                    else
                        _infoIrsa.pathSalida = "";
                    //IRSA POS
                    XmlNodeList POSIRSA = xDoc.GetElementsByTagName("Pos");
                    if (POSIRSA.Count > 0)
                        _infoIrsa.pos = POSIRSA[0].InnerXml;
                    else
                        _infoIrsa.pos = "";
                    //IRSA RUBRO
                    XmlNodeList RUBROIRSA = xDoc.GetElementsByTagName("Rubro");
                    if (RUBROIRSA.Count > 0)
                        _infoIrsa.rubro = RUBROIRSA[0].InnerXml;
                    else
                        _infoIrsa.rubro = "";

                    //Caballito
                    XmlNodeList PathCaballito = xDoc.GetElementsByTagName("PathSalidaCaballito");
                    if (PathCaballito.Count > 0)
                        _pathCaballito = PathCaballito[0].InnerXml;
                    else
                        _pathCaballito = "";

                    //Caballito
                    XmlNodeList PathOlmos = xDoc.GetElementsByTagName("PathSalidaOlmos");
                    if (PathOlmos.Count > 0)
                        _pathOlmos = PathOlmos[0].InnerXml;
                    else
                        _pathOlmos = "";

                    _rta = true;
                }
                else
                    _rta = false;
            }
            catch (Exception)
            {
                throw new Exception("Se produjo un error leyendo el archivo de configuracion." + Environment.NewLine + "Por favor revise el archivo.");
            }
            return _rta;
        }

        public static DatosControlador GetDatosControlador(string _ip)
        {
            try
            {
                DatosControlador _controlador = new DatosControlador();
                hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarDatosInicializacion _respConsultarDatosInicializacion = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarDatosInicializacion();
                //Conectamos con la hasar.
                hasar.conectar(_ip);
                //Recupero los datos de la Inicializacion.
                _respConsultarDatosInicializacion = hasar.ConsultarDatosInicializacion();
                //Recupero los datos del Punto de venta.
                _controlador.NroPos = _respConsultarDatosInicializacion.getNumeroPos();
                //Recupero el CUIT
                _controlador.Cuit = _respConsultarDatosInicializacion.getCUIT();
                //Recupero la Razon Social
                _controlador.RazonSocial = _respConsultarDatosInicializacion.getRazonSocial();

                return _controlador;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al recuperar los datos del Controlador Fiscal.");
            }
        }        
    }
    public class DatosControlador
    {
        public int NroPos { get; set; }
        public string Cuit { get; set; }
        public string RazonSocial { get; set; }
    }
    public class Licencia
    {
        [Required]
        public string user { get; set; }
        [Required]
        public string password { get; set; }
        [Required]
        public string ClientKey { get; set; }
        [Required]
        public string Release { get; set; }
        [Required]
        public string Plataforma { get; set; }
        public string Version { get; set; }
        public string ClientName { get; set; }
        [Required]
        public string ClientCuit { get; set; }
        public string ClientRazonSocial { get; set; }
        public string Tipo { get; set; }
        public string TerminalName { get; set; }
        public bool Enabled { get; set; }
        public int PointOfSale { get; set; }
    }

    public class ResponcePostLicencia
    {
        public string Key { get; set; }
        public string Resultado { get; set; }
    }
}
