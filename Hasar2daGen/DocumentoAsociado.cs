﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hasar2daGen
{
    public class DocumentoAsociado
    {
        public string _numserie { get; set; }
        public int _numfactura { get; set; }
        public string _n { get; set; }
        public string _seriefiscal1 { get; set; }
        public string _seriefiscal2 { get; set; }
        public string _numerofiscal { get; set; }
        public DateTime _fecha { get; set; }

        public static List<DocumentoAsociado> GetDocumentoAsociado(string _numSerie, string _n, int _nroAlbaran, SqlConnection _con)
        {
            string _sql = "SELECT DISTINCT FACTURASVENTASERIESRESOL.NUMSERIE, FACTURASVENTASERIESRESOL.NUMFACTURA, FACTURASVENTASERIESRESOL.N, " +
                "FACTURASVENTASERIESRESOL.SERIEFISCAL1, FACTURASVENTASERIESRESOL.SERIEFISCAL2, FACTURASVENTASERIESRESOL.NUMEROFISCAL " +
                "FROM FACTURASVENTASERIESRESOL INNER JOIN  ALBVENTACAB on FACTURASVENTASERIESRESOL.NUMSERIE = ALBVENTACAB.NUMSERIE " +
                "AND FACTURASVENTASERIESRESOL.NUMFACTURA = ALBVENTACAB.NUMFAC AND FACTURASVENTASERIESRESOL.N = ALBVENTACAB.N " +
                "INNER JOIN ALBVENTALIN ON ALBVENTACAB.NUMALBARAN = ALBVENTALIN.NUMALBARAN AND ALBVENTACAB.NUMSERIE = ALBVENTALIN.NUMSERIE " +
                "AND ALBVENTACAB.N = ALBVENTALIN.N WHERE ALBVENTACAB.NUMSERIE = @NumSerie AND ALBVENTACAB.NUMALBARAN = @NumAlbaran AND ALBVENTACAB.N = @N";

            List<DocumentoAsociado> _items = new List<DocumentoAsociado>();

            using (SqlCommand _cmd = new SqlCommand())
            {
                _cmd.Connection = _con;
                _cmd.CommandType = System.Data.CommandType.Text;
                _cmd.CommandText = _sql;

                _cmd.Parameters.AddWithValue("@NumSerie", _numSerie);
                _cmd.Parameters.AddWithValue("@N", _n);
                _cmd.Parameters.AddWithValue("@NumAlbaran", _nroAlbaran);

                using (SqlDataReader _reader = _cmd.ExecuteReader())
                {
                    if (_reader.HasRows)
                    {
                        while (_reader.Read())
                        {
                            DocumentoAsociado _cls = new DocumentoAsociado();
                            _cls._numserie = _reader["NUMSERIE"].ToString();
                            _cls._numfactura = Convert.ToInt32(_reader["NUMFACTURA"]);
                            _cls._n = _reader["N"].ToString();
                            _cls._seriefiscal1 = _reader["SERIEFISCAL1"].ToString();
                            _cls._seriefiscal2 = _reader["SERIEFISCAL2"].ToString();
                            _cls._numerofiscal = _reader["NUMEROFISCAL"].ToString();
                            //_cls._fecha = GetDocumentoAsociadoFecha(_cls._numserie, _cls._n, _cls._numfactura, _con);
                            
                            _items.Add(_cls);
                        }
                    }
                }

                foreach (DocumentoAsociado da in _items)
                {
                    da._fecha = GetDocumentoAsociadoFecha(da._numserie, da._n, da._numfactura, _con);
                }
            }

            return _items;
        }

        public static DateTime GetDocumentoAsociadoFecha(string _numSerie, string _n, int _nroFactura, SqlConnection _con)
        {
            string _sql = "SELECT FECHA FROM FACTURASVENTA " +
                "WHERE FACTURASVENTA.NUMSERIE = @NumSerie AND FACTURASVENTA.NUMFACTURA = @NumFactura AND FACTURASVENTA.N = @N";

            DateTime _fec = DateTime.Now;

            using (SqlCommand _cmd = new SqlCommand())
            {
                _cmd.Connection = _con;
                _cmd.CommandType = System.Data.CommandType.Text;
                _cmd.CommandText = _sql;

                _cmd.Parameters.AddWithValue("@NumSerie", _numSerie);
                _cmd.Parameters.AddWithValue("@N", _n);
                _cmd.Parameters.AddWithValue("@NumFactura", _nroFactura);

                using (SqlDataReader _reader = _cmd.ExecuteReader())
                {
                    if (_reader.HasRows)
                    {
                        while (_reader.Read())
                        {
                            _fec = Convert.ToDateTime(_reader["FECHA"]);
                        }
                    }
                }
            }

            return _fec;
        }
    }
}
