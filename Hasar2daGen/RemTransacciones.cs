﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hasar2daGen
{
    public class RemTransacciones
    {
        public static void GraboRemTransacciones(string _terminal, string _caja, string _z, int _tipo, int _accion, 
            string _serie, string _numero, string _N, SqlConnection _con)
        {
            string _sql = "INSERT INTO REM_TRANSACCIONES(TERMINAL, CAJA, CAJANUM, Z, TIPO, ACCION, SERIE, NUMERO, N, FECHA, HORA, FO, IDCENTRAL, TALLA, COLOR, CODIGOSTR) " +
                    "VALUES(@terminal, @caja, 0, @Z, @tipo, @accion, @serie, @numero, @N, @date, @hora, 0, 1, '.', '.', '')";

            using (SqlCommand _cmd = new SqlCommand(_sql, _con))
            {
                _cmd.Parameters.AddWithValue("@terminal", _terminal);
                _cmd.Parameters.AddWithValue("@caja", _caja);
                _cmd.Parameters.AddWithValue("@Z", Convert.ToInt32(_z));
                _cmd.Parameters.AddWithValue("@tipo", _tipo);
                _cmd.Parameters.AddWithValue("@accion", _accion);
                _cmd.Parameters.AddWithValue("@serie", _serie);
                _cmd.Parameters.AddWithValue("@numero", Convert.ToInt32(_numero));
                _cmd.Parameters.AddWithValue("@N", _N);
                _cmd.Parameters.AddWithValue("@date", DateTime.Now.Date);
                _cmd.Parameters.AddWithValue("@hora", DateTime.Now.Date);

                int rta = _cmd.ExecuteNonQuery();

            }
        }

        public static bool InsertRemTransacciones(string _terminal, string _serie, int _numero, string _N, SqlConnection _con)
        {
            bool _rta = false;
            try
            {
                string _sql = @"INSERT INTO rem_transacciones(TERMINAL, TIPO, ACCION, CAJA, Z, SERIE, NUMERO, N, FO, IDCENTRAL)
                    SELECT @Terminal, 0, 1, t.CAJA, CASE WHEN isnull(t.Z, 0) = 0 THEN z.Z + 1 ELSE t.Z END, t.NUMSERIE, t.NUMFACTURA, t.N, '0', 1
                    FROM FACTURASVENTA t LEFT JOIN (SELECT Caja, max(Numero) Z FROM ARQUEOS WHERE Arqueo = 'Z' GROUP BY Caja) z on z.Caja = t.CAJA
                    WHERE t.NUMSERIE = @Serie AND t.NUMFACTURA = @Numero AND t.N = @N";

                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;

                    _cmd.Parameters.AddWithValue("@Terminal", _terminal.ToUpper());
                    _cmd.Parameters.AddWithValue("@Serie", _serie);
                    _cmd.Parameters.AddWithValue("@Numero", Convert.ToInt32(_numero));
                    _cmd.Parameters.AddWithValue("@N", _N);

                    _cmd.ExecuteNonQuery();
                }
                _rta = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return _rta;
        }

        public static void InsertRemTransacciones(string _terminal, string _serie, int _numero, string _N, int _accion, SqlConnection _con, SqlTransaction _tran)
        {
            try
            {
                string _sql = @"INSERT INTO rem_transacciones(TERMINAL, TIPO, ACCION, CAJA, Z, SERIE, NUMERO, N, FO, IDCENTRAL)
                    SELECT @Terminal, 0, @Accion, t.CAJA, CASE WHEN isnull(t.Z, 0) = 0 THEN z.Z + 1 ELSE t.Z END, t.NUMSERIE, t.NUMFACTURA, t.N, '0', 1
                    FROM FACTURASVENTA t LEFT JOIN (SELECT Caja, max(Numero) Z FROM ARQUEOS WHERE Arqueo = 'Z' GROUP BY Caja) z on z.Caja = t.CAJA
                    WHERE t.NUMSERIE = @Serie AND t.NUMFACTURA = @Numero AND t.N = @N";

                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;
                    _cmd.Transaction = _tran;

                    _cmd.Parameters.AddWithValue("@Terminal", _terminal.ToUpper());
                    _cmd.Parameters.AddWithValue("@Accion", _accion);
                    _cmd.Parameters.AddWithValue("@Serie", _serie);
                    _cmd.Parameters.AddWithValue("@Numero", Convert.ToInt32(_numero));
                    _cmd.Parameters.AddWithValue("@N", _N);

                    _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("InsertRemTransacciones. Error: " + ex.Message);
            }

        }

        public static bool InsertRemTransaccionesCierreZ(string _terminal, int _NroZ, int _caja, SqlConnection _con)
        {
            bool _rta = false;

            try
            {
                string _sql = @"insert into rem_transacciones(TERMINAL, TIPO, ACCION, CAJANUM, Z, SERIE, NUMERO, N, FO, IDCENTRAL)
                select @Terminal, 0, 1, t.CAJA, case when isnull(t.Z, 0) = 0 then z.Z + 1 else t.Z end, t.NUMSERIE, t.NUMFACTURA, t.N, '0', 1
                from FACTURASVENTA t left join (select Caja, max(Numero) Z from ARQUEOS where Arqueo = 'Z' group by Caja) z on z.Caja = t.CAJA
                inner join FACTURASVENTACAMPOSLIBRES cl on t.NUMSERIE =  cl.NUMSERIE and t.NUMFACTURA = cl.NUMFACTURA and t.N = cl.N
                where cl.Z_NRO = @NroZ and t.caja = @Caja";

                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;

                    _cmd.Parameters.AddWithValue("@Terminal", _terminal.ToUpper());
                    _cmd.Parameters.AddWithValue("@NroZ", _NroZ);
                    _cmd.Parameters.AddWithValue("@Caja", _caja);

                    _cmd.ExecuteNonQuery();
                }
                _rta = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return _rta;
        }

        public static bool InsertRemTransaccionesCierreZ(string _terminal, int _NroZ, string _caja, SqlConnection _con)
        {
            bool _rta = false;

            try
            {
                string _sql = @"insert into rem_transacciones(TERMINAL, TIPO, ACCION, CAJA, Z, SERIE, NUMERO, N, FO, IDCENTRAL)
                select @Terminal, 0, 1, t.CAJA, case when isnull(t.Z, 0) = 0 then z.Z + 1 else t.Z end, t.NUMSERIE, t.NUMFACTURA, t.N, '0', 1
                from FACTURASVENTA t left join (select Caja, max(Numero) Z from ARQUEOS where Arqueo = 'Z' group by Caja) z on z.Caja = t.CAJA
                inner join FACTURASVENTACAMPOSLIBRES cl on t.NUMSERIE =  cl.NUMSERIE and t.NUMFACTURA = cl.NUMFACTURA and t.N = cl.N
                where cl.Z_NRO = @NroZ and t.caja = @Caja";

                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;

                    _cmd.Parameters.AddWithValue("@Terminal", _terminal.ToUpper());
                    _cmd.Parameters.AddWithValue("@NroZ", _NroZ);
                    _cmd.Parameters.AddWithValue("@Caja", _caja);

                    _cmd.ExecuteNonQuery();
                }
                _rta = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return _rta;
        }

        public static bool InsertRemTransaccionesCierreZ_X(string _terminal, int _NroZ, string _caja, SqlConnection _con)
        {
            bool _rta = false;

            try
            {
                string _sql = @"insert into rem_transacciones(TERMINAL, TIPO, ACCION, CAJA, Z, SERIE, NUMERO, N, FO, IDCENTRAL)
                VALUES (@Terminal, 125, 0, @Caja, @NroZ, '', -1, '', '0',1)";

                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;

                    _cmd.Parameters.AddWithValue("@Terminal", _terminal.ToUpper());
                    _cmd.Parameters.AddWithValue("@NroZ", _NroZ);
                    _cmd.Parameters.AddWithValue("@Caja", _caja);

                    _cmd.ExecuteNonQuery();
                }
                _rta = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return _rta;
        }
    }
}
