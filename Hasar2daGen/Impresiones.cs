﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hasar2daGen
{
    public class Impresiones
    {
        public static string ImprimirNotaCreditoAB(Cabecera _cab, List<Items> _items, List<Pagos> _pagos, List<OtrosTributos> _oTributos,
                List<Recargos> _recargos, List<DocumentoAsociado> _docAsoc, string _ip, SqlConnection _sqlConn, bool _log, 
                out bool _faltaPapel, out bool _necesitaCierreZ, out int _nroComprobante, out int _nroPos, out bool _seCancelo)
        {
            bool _abrirCajon = false;
            string _rta = "";
            string _serieFiscal2 = "";
            _necesitaCierreZ = false;
            _faltaPapel = false;
            _nroComprobante = 0;
            _seCancelo = false;
            int _nroZeta = 0;
            //aca Comienzo
            hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();
            //Estilo de Texto.
            hfl.argentina.Hasar_Funcs.AtributosDeTexto _estilo = new hfl.argentina.Hasar_Funcs.AtributosDeTexto();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarDatosInicializacion _respConsultarDatosInicializacion = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarDatosInicializacion();
            //Conecatmos con el CF.
            Conectar(hasar, _log, _ip);
            //Recupero los datos de la Inicializacion.
            _respConsultarDatosInicializacion = hasar.ConsultarDatosInicializacion();
            _nroPos = _respConsultarDatosInicializacion.getNumeroPos();

            if (FuncionesVarias.FechaOk(hasar, _cab))
            {
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento _respAbrir = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento();
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento _respCerrar = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento();
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago _respPago = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago();
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal _respSubTotal = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal();
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarCapacidadZetas _seta = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarCapacidadZetas();
                //Recupero el ultimo Z le sumo 1.
                _seta = hasar.ConsultarCapacidadZetas();
                _nroZeta = _seta.getUltimaZeta() + 1;

                _estilo.setNegrita(false);
                _estilo.setDobleAncho(false);
                _estilo.setCentrado(false);
                _estilo.setBorradoTexto(false);                

                try
                {
                    switch (_cab.regfacturacioncliente)
                    {
                        case "1":
                            {
                                hasar.CargarDatosCliente(_cab.nombrecliente, _cab.nrodoccliente.Replace("-", ""),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_INSCRIPTO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT,
                                    _cab.direccioncliente, "", "", "");
                                _serieFiscal2 = "003_Credito_A";
                                break;
                            }
                        case "2":
                            {
                                hasar.CargarDatosCliente(_cab.nombrecliente, _cab.nrodoccliente.Replace("-", ""),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_EXENTO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT,
                                    _cab.direccioncliente, "", "", "");
                                _serieFiscal2 = "008_Credito_B";
                                break;
                            }
                        case "4":
                            {
                                hasar.CargarDatosCliente(_cab.nombrecliente, _cab.nrodoccliente.Replace("-", ""),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.CONSUMIDOR_FINAL,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_DNI,
                                    _cab.direccioncliente, "", "", "");
                                _serieFiscal2 = "008_Credito_B";
                                break;
                            }
                        case "6":
                        case "5":
                            {
                                //hasar.CargarDatosCliente(_cab.nombrecliente, _cab.nrodoccliente.Replace("-", ""),
                                //    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.MONOTRIBUTO,
                                //    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT,
                                //    _cab.direccioncliente, "", "", ""); ;
                                //_serieFiscal2 = "013_Credito_C";
                                hasar.CargarDatosCliente(_cab.nombrecliente, _cab.nrodoccliente.Replace("-", ""),
                                   hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_INSCRIPTO,
                                   hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT,
                                   "Receptor del comprobante - Responsable Monotributo", _cab.direccioncliente, "", "");
                                _serieFiscal2 = "003_Credito_A";
                                break;
                            }
                        default:
                            {
                                hasar.CargarDatosCliente(_cab.nombrecliente, _cab.nrodoccliente.Replace("-", ""),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.CONSUMIDOR_FINAL,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_DNI,
                                    _cab.direccioncliente, "", "", "");
                                _serieFiscal2 = "008_Credito_B";
                                break;
                            }
                    }
                    int _counter = 1;
                    //Vemos si tenemos comprobante asociado.
                    if (_docAsoc.Count > 0)
                    {
                        foreach (DocumentoAsociado _doc in _docAsoc)
                        {
                            switch (_doc._seriefiscal2.Substring(0, 3).ToUpper())
                            {
                                case "001":
                                    {
                                        hasar.CargarDocumentoAsociado(_counter, hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_A, Convert.ToInt32(_doc._seriefiscal1), Convert.ToInt32(_doc._numerofiscal), _doc._fecha);
                                        _respAbrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NOTA_DE_CREDITO_A);
                                        break;
                                    }
                                case "006":
                                    {
                                        hasar.CargarDocumentoAsociado(_counter, hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_B, Convert.ToInt32(_doc._seriefiscal1), Convert.ToInt32(_doc._numerofiscal), _doc._fecha);
                                        _respAbrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NOTA_DE_CREDITO_B);
                                        break;
                                    }
                                default:
                                    {
                                        hasar.CargarDocumentoAsociado(_counter, hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.GENERICO, Convert.ToInt32(_doc._seriefiscal1), Convert.ToInt32(_doc._numerofiscal), _doc._fecha);
                                        _respAbrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.GENERICO);
                                        break;
                                    }
                            }
                            _counter++;
                        }
                    }
                    else
                    {
                        switch (_serieFiscal2.Substring(0, 3).ToUpper())
                        {
                            case "003":
                                {
                                    hasar.CargarDocumentoAsociado(_counter, hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_A, _nroPos, 123456, DateTime.Now.AddDays(-10));
                                    _respAbrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NOTA_DE_CREDITO_A);
                                    break;
                                }
                            case "008":
                                {
                                    hasar.CargarDocumentoAsociado(_counter, hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_B, _nroPos, 123456, DateTime.Now.AddDays(-10));
                                    _respAbrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NOTA_DE_CREDITO_B);
                                    break;
                                }
                            default:
                                {
                                    hasar.CargarDocumentoAsociado(_counter, hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.GENERICO, _nroPos, 123456, DateTime.Now.AddDays(-10));
                                    _respAbrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.GENERICO);
                                    break;
                                }
                        }
                    }

                    _nroComprobante = _respAbrir.getNumeroComprobante();

                    foreach (Items _it in _items)
                    {
                        if(!String.IsNullOrEmpty(_it._referencia))
                            hasar.ImprimirTextoFiscal(_estilo, "Referencia " + _it._referencia, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);

                        if (String.IsNullOrEmpty(_it._descripcion))
                            _it._descripcion = "Articulo sin Descripcion";

                        //si tenemos precio = 0 o DTO2 = 100 entonces imprimo el texto de la promo.
                        if (_it._precio == 0 && _it._dto2 == 100)
                        {
                            string _txt = _it._textoPromo + " Importe: " + _it._precioDefecto.ToString();
                            hasar.ImprimirTextoFiscal(_estilo, _txt, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);
                        }
                        //Imprimo las promos
                        foreach (ItemsPromos ip in _it.Promosiones)
                        {
                            string _txt = ip.textoimprimir + " Importe: " + ip.importedtoiva.ToString();
                            hasar.ImprimirTextoFiscal(_estilo, _txt, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);
                        }
                        if (_it._iva > 0)
                        {
                            hasar.ImprimirItem(_it._descripcion,
                                Math.Abs(Convert.ToDouble(_it._cantidad)),
                                Math.Abs(Convert.ToDouble(_it._precio)),
                                hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO,
                                Math.Abs(Convert.ToDouble(_it._iva)),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_KIVA,
                                0.0,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                1, "", _it._codArticulo,
                                hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);
                        }
                        else
                        {
                            if (_it._tipoImpuesto == 9)
                            {
                                hasar.ImprimirItem(_it._descripcion,
                                 Math.Abs(Convert.ToDouble(_it._cantidad)),
                                 Math.Abs(Convert.ToDouble(_it._precio)),
                                 hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.NO_GRAVADO,
                                 Math.Abs(Convert.ToDouble(_it._iva)),
                                 hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_KIVA,
                                 0.0,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                 1, "", _it._codArticulo,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);
                            }
                            else
                            {
                                hasar.ImprimirItem(_it._descripcion,
                                 Math.Abs(Convert.ToDouble(_it._cantidad)),
                                 Math.Abs(Convert.ToDouble(_it._precio)),
                                 hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.EXENTO,
                                 Math.Abs(Convert.ToDouble(_it._iva)),
                                 hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_KIVA,
                                 0.0,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                 1, "", _it._codArticulo,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);
                            }
                        }
                        //Veo si tengo dto en el item.
                        if (_it._precio > 0)
                        {
                            if (Math.Abs(_it._descuento) == 100)
                            {
                                //Imprimo el item
                                hasar.ImprimirItem(_it._descripcion,
                                    Convert.ToDouble(_it._cantidad),
                                    Convert.ToDouble(_it._precio),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO,
                                    Convert.ToDouble(_it._iva),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_KIVA,
                                    0.0,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                    1, "", _it._codArticulo,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);
                            }
                            else
                            {
                                if (_it._descuento > 0)
                                {
                                    double _dto = Math.Abs(Convert.ToDouble(((_it._cantidad * _it._precio) * _it._descuento / 100)));
                                    //Valido que tengo texto descuento.
                                    if (String.IsNullOrEmpty(_it._textoPromo))
                                    {
                                        if (_it._descuento == 0)
                                            _it._textoPromo = "Descuento ";
                                        else
                                            _it._textoPromo = "Descuento " + _it._descuento.ToString();
                                    }
                                    hasar.ImprimirDescuentoItem(_it._textoPromo,
                                        _dto,
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE);
                                }
                                else
                                {
                                    //Recargo
                                    double _dto;
                                    _dto = Convert.ToDouble(((_it._cantidad * _it._precio) * _it._descuento / 100));
                                    hasar.ImprimirItem("Recargo del " + (_it._descuento * -1).ToString(),
                                   1,
                                   Math.Abs(_dto),
                                   hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO,
                                   Convert.ToDouble(_it._iva),
                                   hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                                   hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_KIVA,
                                   0.0,
                                   hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                   hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                   1, "", _it._codArticulo,
                                   hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);
                                }
                            }
                        }
                    }

                    //Vemos si tenemos dto en la cabecera.
                    if (_cab.dtocomercial > 0)
                    {
                        hasar.ImprimirAjuste("Bonificación Gral. (comercial) " + _cab.dtocomercial.ToString(),
                            Math.Abs(_cab.totdtocomercial),
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                            "",
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL);
                    }
                    if (_cab.dtopp != 0)
                    {
                        if (_cab.dtopp > 0)
                        {
                            hasar.ImprimirAjuste("Bonificación DTOPP " + _cab.dtopp.ToString(),
                                Math.Abs(_cab.totdtopp),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL);
                        }
                        else
                        {
                            double _dto = Math.Abs(_cab.totdtopp);
                            hasar.ImprimirAjuste("Recargo DTOPP " + _cab.dtopp.ToString(),
                                _dto,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.AJUSTE_POS);
                        }
                    }

                    //Recargos
                    foreach (Recargos _rec in _recargos)
                    {

                        if (_rec._recargo > 0)
                        {
                            double _importeRecargo = (_rec._bruto * _rec._recargo / 100);
                            if (_rec._iva > 0)
                            {
                                hasar.ImprimirAnticipoBonificacionEnvases(_rec._texto, _importeRecargo,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, _rec._iva,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_FIJO_MONTO, 0,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE, "7790001001047",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeOperacionesGlobalesIVA.RECARGO_IVA);
                            }
                            else
                            {
                                hasar.ImprimirAnticipoBonificacionEnvases(_rec._texto, _importeRecargo,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.EXENTO, _rec._iva,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_FIJO_MONTO, 0,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE, "7790001001047",
                                 hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeOperacionesGlobalesIVA.RECARGO_IVA);
                            }
                        }
                        else
                        {
                            if (_rec._iva > 0)
                            {
                                hasar.ImprimirAnticipoBonificacionEnvases(_rec._texto, Math.Abs(_rec._importe),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, _rec._iva,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_FIJO_MONTO, 0,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE, "7790001001047",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeOperacionesGlobalesIVA.RECARGO_IVA);
                            }
                            else
                            {
                                hasar.ImprimirAnticipoBonificacionEnvases(_rec._texto, Math.Abs(_rec._importe),
                                 hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.EXENTO, _rec._iva,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_FIJO_MONTO, 0,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE, "7790001001047",
                                 hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeOperacionesGlobalesIVA.RECARGO_IVA);
                            }
                        }

                    }

                    //Otros Tributos
                    foreach (OtrosTributos _ot in _oTributos)
                    {
                        double _impor = Math.Abs(_ot._importe);
                        double _bs = Math.Abs(_ot._base);
                        hasar.ImprimirOtrosTributos(hfl.argentina.HasarImpresoraFiscalRG3561.TiposTributos.IIBB,
                            _ot._nombre, _bs, _impor);
                    }

                    //Valido los importes.

                    if(!FuncionesVarias.ComparoSubtotal(hasar, _cab.totalneto))
                    {
                        throw new Exception("Los totales no coinciden. Revise el comprobante y luego reintente imprimirlo.");
                    }

                    //Pagos.
                    foreach (Pagos _pag in _pagos)
                    {
                        switch (_pag.tipopago.ToUpper())
                        {
                            case "TARJETA CRÉDITO":
                            case "TARJETA CREDITO":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Math.Abs(Convert.ToDouble(_pag.monto)),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.TARJETA_DE_CREDITO,
                                0, "", "");
                                    break;
                                }
                            case "PENDIENTE":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Math.Abs(Convert.ToDouble(_pag.monto)),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CUENTA_CORRIENTE,
                                0, "", "");
                                    break;
                                }
                            case "EFECTIVO":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                        Math.Abs(Convert.ToDouble(_pag.monto)),
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                        "",
                                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.EFECTIVO,
                                        0, "", "");
                                    break;
                                }
                            case "CHEQUE":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Math.Abs(Convert.ToDouble(_pag.monto)),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CHEQUE,
                                0, "", "");
                                    break;
                                }
                            case "TARJETA DEBITO":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Math.Abs(Convert.ToDouble(_pag.monto)),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.TARJETA_DE_DEBITO,
                                0, "", "");
                                    break;
                                }
                            default:
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Math.Abs(Convert.ToDouble(_pag.monto)),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.OTROS_MEDIOS_PAGO,
                                0, "", "");
                                    break;
                                }
                        }
                        if (_pag.AbrirCajon)
                            _abrirCajon = true;
                    }

                    _respCerrar = hasar.CerrarDocumento();

                    int _numeroTicket = _respCerrar.getNumeroComprobante();

                    string _numeroPos = hasar.ConsultarDatosInicializacion().getNumeroPos().ToString().PadLeft(4, '0');

                    bool _ok = FacturasVentaSerieResol.GrabarTiquet(_cab.numserie, _cab.numfac, _cab.n, _numeroPos, _serieFiscal2, _numeroTicket, _sqlConn);

                    if (!_ok)
                    {
                        string _numeroError = _numeroPos + "-" + _numeroTicket.ToString().PadLeft(8, '0');
                        _rta = "Hubo un Error y no se pudo grabar el numero de comprobante, " + _numeroError + ", por favor ingreselo manualmente.";
                    }
                    else
                    {
                        //Grabo el Zeta.
                        Cabecera.GrabarZeta(_cab.numserie, _cab.numfac, _cab.n, _nroZeta, _sqlConn);
                    }

                    if (hasar.ObtenerUltimoEstadoImpresora().getFaltaPapelReceipt())
                        _faltaPapel = true;

                    if (_abrirCajon)
                        hasar.AbrirCajonDinero();
                }
                catch (Exception ex)
                {
                    //Vemos si el error fue porque necesita el Cierre Z.
                    if (ex.Message.StartsWith("POS_DOCUMENT_BEYOND_FISCAL_DAY"))
                    {
                        _necesitaCierreZ = true;
                        _rta = "Cierre Z. ";
                    }
                    else
                    {
                        Impresiones.Cancelar(hasar);
                        throw new Exception("CANCELADA " + _rta + ex.Message);
                    }
                }
                //Valido si el comprobante se cancelo.
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarEstado _estado = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarEstado();
                _estado = hasar.ConsultarEstado();
                if (_estado.getNumeroUltimoComprobante() == _nroComprobante)
                {
                    _seCancelo = _estado.EstadoAuxiliar.getUltimoComprobanteFueCancelado();
                }
            }
            else
                _rta = "Las fechas del sistema y de la controladora fiscal no coinciden, debe realizar un Cierre Z. Por favor comuniquese con ICG Argentina.";

            return _rta;
        }

        public static string ImprimirNotaCreditoAB(Cabecera _cab, List<Items> _items, List<Pagos> _pagos, List<OtrosTributos> _oTributos,
        List<Recargos> _recargos, List<DocumentoAsociado> _docAsoc, string _ip, SqlConnection _sqlConn, bool _log,
        out bool _faltaPapel, out bool _necesitaCierreZ)
        {
            bool _abrirCajon = false;
            string _rta = "";
            string _serieFiscal2 = "";
            _necesitaCierreZ = false;
            _faltaPapel = false;
            int _nroComprobante = 0;
            int _nroZeta = 0;
            //aca Comienzo
            hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();
            //Estilo de Texto.
            hfl.argentina.Hasar_Funcs.AtributosDeTexto _estilo = new hfl.argentina.Hasar_Funcs.AtributosDeTexto();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarDatosInicializacion _respConsultarDatosInicializacion = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarDatosInicializacion();
            //Conecatmos con el CF.
            Conectar(hasar, _log, _ip);
            //Recupero los datos de la Inicializacion.
            _respConsultarDatosInicializacion = hasar.ConsultarDatosInicializacion();
            int _nroPos = _respConsultarDatosInicializacion.getNumeroPos();

            if (FuncionesVarias.FechaOk(hasar, _cab))
            {
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento _respAbrir = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento();
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento _respCerrar = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento();
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago _respPago = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago();
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal _respSubTotal = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal();
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarCapacidadZetas _seta = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarCapacidadZetas();
                //Recupero el ultimo Z le sumo 1.
                _seta = hasar.ConsultarCapacidadZetas();
                _nroZeta = _seta.getUltimaZeta() + 1;

                _estilo.setNegrita(false);
                _estilo.setDobleAncho(false);
                _estilo.setCentrado(false);
                _estilo.setBorradoTexto(false);

                try
                {
                    hasar.ConfigurarZona(1, _estilo, "Numero Interno: " + _cab.numserie + "/" + _cab.numfac.ToString() + string.Empty.PadLeft(5, ' ') + "VENDEDOR: " + _cab.nomvendedor,
                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO,
                        hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_1_ENCABEZADO);
                    switch (_cab.regfacturacioncliente)
                    {
                        case "1":
                            {
                                hasar.CargarDatosCliente(_cab.nombrecliente, _cab.nrodoccliente.Replace("-", ""),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_INSCRIPTO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT,
                                    _cab.direccioncliente, "", "", "");
                                _serieFiscal2 = "003_Credito_A";
                                break;
                            }
                        case "2":
                            {
                                hasar.CargarDatosCliente(_cab.nombrecliente, _cab.nrodoccliente.Replace("-", ""),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_EXENTO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT,
                                    _cab.direccioncliente, "", "", "");
                                _serieFiscal2 = "008_Credito_B";
                                break;
                            }
                        case "4":
                            {
                                hasar.CargarDatosCliente(_cab.nombrecliente, _cab.nrodoccliente.Replace("-", ""),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.CONSUMIDOR_FINAL,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_DNI,
                                    _cab.direccioncliente, "", "", "");
                                _serieFiscal2 = "008_Credito_B";
                                break;
                            }
                        case "6":
                        case "5":
                            {
                                //hasar.CargarDatosCliente(_cab.nombrecliente, _cab.nrodoccliente.Replace("-", ""),
                                //    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.MONOTRIBUTO,
                                //    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT,
                                //    _cab.direccioncliente, "", "", ""); 
                                //_serieFiscal2 = "013_Credito_C";
                                hasar.CargarDatosCliente(_cab.nombrecliente, _cab.nrodoccliente.Replace("-", ""),
                                   hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_INSCRIPTO,
                                   hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT,
                                   "Receptor del comprobante - Responsable Monotributo", _cab.direccioncliente, "", "");
                                _serieFiscal2 = "003_Credito_A";
                                break;
                            }
                        default:
                            {
                                hasar.CargarDatosCliente(_cab.nombrecliente, _cab.nrodoccliente.Replace("-", ""),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.CONSUMIDOR_FINAL,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_DNI,
                                    _cab.direccioncliente, "", "", "");
                                _serieFiscal2 = "008_Credito_B";
                                break;
                            }
                    }
                    int _counter = 1;
                    //Vemos si tenemos comprobante asociado.
                    if (_docAsoc.Count > 0)
                    {
                        foreach (DocumentoAsociado _doc in _docAsoc)
                        {
                            switch (_doc._seriefiscal2.Substring(0, 3).ToUpper())
                            {
                                case "001":
                                    {
                                        hasar.CargarDocumentoAsociado(_counter, hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_A, Convert.ToInt32(_doc._seriefiscal1), Convert.ToInt32(_doc._numerofiscal), _doc._fecha);
                                        _respAbrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NOTA_DE_CREDITO_A);
                                        break;
                                    }
                                case "006":
                                    {
                                        hasar.CargarDocumentoAsociado(_counter, hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_B, Convert.ToInt32(_doc._seriefiscal1), Convert.ToInt32(_doc._numerofiscal), _doc._fecha);
                                        _respAbrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NOTA_DE_CREDITO_B);
                                        break;
                                    }
                                case "011":
                                    {
                                        hasar.CargarDocumentoAsociado(_counter, hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_C, Convert.ToInt32(_doc._seriefiscal1), Convert.ToInt32(_doc._numerofiscal),_doc._fecha);
                                        _respAbrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NOTA_DE_CREDITO_B);
                                        break;
                                    }
                                default:
                                    {
                                        hasar.CargarDocumentoAsociado(_counter, hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.GENERICO, Convert.ToInt32(_doc._seriefiscal1), Convert.ToInt32(_doc._numerofiscal), _doc._fecha);
                                        _respAbrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.GENERICO);
                                        break;
                                    }
                            }
                            _counter++;
                        }
                    }
                    else
                    {
                        switch (_serieFiscal2.Substring(0, 3).ToUpper())
                        {
                            case "003":
                                {
                                    hasar.CargarDocumentoAsociado(_counter, hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_A, _nroPos, 123456, DateTime.Now.AddDays(-10));
                                    _respAbrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NOTA_DE_CREDITO_A);
                                    break;
                                }
                            case "008":
                                {
                                    hasar.CargarDocumentoAsociado(_counter, hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_B, _nroPos, 123456, DateTime.Now.AddDays(-10));
                                    _respAbrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NOTA_DE_CREDITO_B);
                                    break;
                                }
                            case "011":
                                {
                                    hasar.CargarDocumentoAsociado(_counter, hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_C, _nroPos, 123456, DateTime.Now.AddDays(-10));
                                    _respAbrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NOTA_DE_CREDITO_B);
                                    break;
                                }
                            default:
                                {
                                    hasar.CargarDocumentoAsociado(_counter, hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.GENERICO, _nroPos, 123456, DateTime.Now.AddDays(-10));
                                    _respAbrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.GENERICO);
                                    break;
                                }
                        }
                    }

                    _nroComprobante = _respAbrir.getNumeroComprobante();

                    foreach (Items _it in _items)
                    {
                        if (!String.IsNullOrEmpty(_it._referencia))
                            hasar.ImprimirTextoFiscal(_estilo, "Referencia " + _it._referencia, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);

                        if (String.IsNullOrEmpty(_it._descripcion))
                            _it._descripcion = "Articulo sin Descripcion";

                        //si tenemos precio = 0 o DTO2 = 100 entonces imprimo el texto de la promo.
                        if (_it._precio == 0 && _it._dto2 == 100)
                        {
                            string _txt = _it._textoPromo + " Importe: " + _it._precioDefecto.ToString();
                            hasar.ImprimirTextoFiscal(_estilo, _txt, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);
                        }
                        //Imprimo las promos
                        foreach (ItemsPromos ip in _it.Promosiones)
                        {
                            string _txt = ip.textoimprimir + " Importe: " + ip.importedtoiva.ToString();
                            hasar.ImprimirTextoFiscal(_estilo, _txt, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);
                        }
                        if (_it._iva > 0)
                        {
                            hasar.ImprimirItem(_it._descripcion,
                                Math.Abs(Convert.ToDouble(_it._cantidad)),
                                Math.Abs(Convert.ToDouble(_it._precio)),
                                hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO,
                                Math.Abs(Convert.ToDouble(_it._iva)),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_KIVA,
                                0.0,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                1, "", _it._codArticulo,
                                hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);
                        }
                        else
                        {
                            if (_it._tipoImpuesto == 9)
                            {
                                hasar.ImprimirItem(_it._descripcion,
                                 Math.Abs(Convert.ToDouble(_it._cantidad)),
                                 Math.Abs(Convert.ToDouble(_it._precio)),
                                 hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.NO_GRAVADO,
                                 Math.Abs(Convert.ToDouble(_it._iva)),
                                 hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_KIVA,
                                 0.0,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                 1, "", _it._codArticulo,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);
                            }
                            else
                            {
                                hasar.ImprimirItem(_it._descripcion,
                                 Math.Abs(Convert.ToDouble(_it._cantidad)),
                                 Math.Abs(Convert.ToDouble(_it._precio)),
                                 hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.EXENTO,
                                 Math.Abs(Convert.ToDouble(_it._iva)),
                                 hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_KIVA,
                                 0.0,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                 1, "", _it._codArticulo,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);
                            }
                        }
                        //Veo si tengo dto en el item.
                        if (_it._precio > 0)
                        {
                            if (Math.Abs(_it._descuento) == 100)
                            {
                                //Imprimo el item
                                hasar.ImprimirItem(_it._descripcion,
                                    //Convert.ToDouble(_it._cantidad * -1),
                                    Convert.ToDouble(_it._cantidad),
                                    Convert.ToDouble(_it._precio),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO,
                                    Convert.ToDouble(_it._iva),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_KIVA,
                                    0.0,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                    1, "", _it._codArticulo,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);
                            }
                            else
                            {
                                if (_it._descuento > 0)
                                {
                                    double _dto = Math.Abs(Convert.ToDouble(((_it._cantidad * _it._precio) * _it._descuento / 100)));
                                    //Valido que tengo texto descuento.
                                    if (String.IsNullOrEmpty(_it._textoPromo))
                                    {
                                        if (_it._descuento == 0)
                                            _it._textoPromo = "Descuento ";
                                        else
                                            _it._textoPromo = "Descuento " + _it._descuento.ToString();
                                    }
                                    hasar.ImprimirDescuentoItem(_it._textoPromo,
                                        _dto,
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE);
                                }
                                else
                                {
                                    if (_it._descuento != 0)
                                    {
                                        //Recargo
                                        double _dto;
                                        _dto = Convert.ToDouble(((_it._cantidad * _it._precio) * _it._descuento / 100));
                                        hasar.ImprimirItem("Recargo del " + (_it._descuento * -1).ToString(),
                                       1,
                                       Math.Abs(_dto),
                                       hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO,
                                       Convert.ToDouble(_it._iva),
                                       hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                                       hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_KIVA,
                                       0.0,
                                       hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                       hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                       1, "", _it._codArticulo,
                                       hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);
                                    }
                                }
                            }
                        }
                    }

                    //Vemos si tenemos dto en la cabecera.
                    if (_cab.dtocomercial > 0)
                    {
                        hasar.ImprimirAjuste("Bonificación Gral. (comercial) " + _cab.dtocomercial.ToString(),
                            Math.Abs(_cab.totdtocomercial),
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                            "",
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL);
                    }
                    if (_cab.dtopp != 0)
                    {
                        if (_cab.dtopp > 0)
                        {
                            hasar.ImprimirAjuste("Bonificación DTOPP " + _cab.dtopp.ToString(),
                                Math.Abs(_cab.totdtopp),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL);
                        }
                        else
                        {
                            double _dto = Math.Abs(_cab.totdtopp);
                            hasar.ImprimirAjuste("Recargo DTOPP " + _cab.dtopp.ToString(),
                                _dto,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.AJUSTE_POS);
                        }
                    }

                    //Recargos
                    foreach (Recargos _rec in _recargos)
                    {

                        if (_rec._recargo > 0)
                        {
                            double _importeRecargo = (_rec._bruto * _rec._recargo / 100);
                            if (_rec._iva > 0)
                            {
                                hasar.ImprimirAnticipoBonificacionEnvases(_rec._texto, _importeRecargo,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, _rec._iva,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_FIJO_MONTO, 0,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE, "7790001001047",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeOperacionesGlobalesIVA.RECARGO_IVA);
                            }
                            else
                            {
                                hasar.ImprimirAnticipoBonificacionEnvases(_rec._texto, _importeRecargo,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.EXENTO, _rec._iva,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_FIJO_MONTO, 0,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE, "7790001001047",
                                 hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeOperacionesGlobalesIVA.RECARGO_IVA);
                            }
                        }
                        else
                        {
                            if (_rec._iva > 0)
                            {
                                hasar.ImprimirAnticipoBonificacionEnvases(_rec._texto, Math.Abs(_rec._importe),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, _rec._iva,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_FIJO_MONTO, 0,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE, "7790001001047",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeOperacionesGlobalesIVA.RECARGO_IVA);
                            }
                            else
                            {
                                hasar.ImprimirAnticipoBonificacionEnvases(_rec._texto, Math.Abs(_rec._importe),
                                 hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.EXENTO, _rec._iva,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_FIJO_MONTO, 0,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE, "7790001001047",
                                 hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeOperacionesGlobalesIVA.RECARGO_IVA);
                            }
                        }

                    }

                    //Otros Tributos
                    foreach (OtrosTributos _ot in _oTributos)
                    {
                        double _impor = Math.Abs(_ot._importe);
                        double _bs = Math.Abs(_ot._base);
                        hasar.ImprimirOtrosTributos(hfl.argentina.HasarImpresoraFiscalRG3561.TiposTributos.IIBB,
                            _ot._nombre, _bs, _impor);
                    }

                    //Valido los importes.

                    if (!FuncionesVarias.ComparoSubtotal(hasar, _cab.totalneto))
                    {
                        throw new Exception("Los totales no coinciden. Revise el comprobante y luego reintente imprimirlo.");
                    }

                    //Pagos.
                    foreach (Pagos _pag in _pagos)
                    {
                        switch (_pag.tipopago.ToUpper())
                        {
                            case "TARJETA CRÉDITO":
                            case "TARJETA CREDITO":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Math.Abs(Convert.ToDouble(_pag.monto)),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.TARJETA_DE_CREDITO,
                                0, "", "");
                                    break;
                                }
                            case "PENDIENTE":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Math.Abs(Convert.ToDouble(_pag.monto)),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CUENTA_CORRIENTE,
                                0, "", "");
                                    break;
                                }
                            case "EFECTIVO":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                        Math.Abs(Convert.ToDouble(_pag.monto)),
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                        "",
                                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.EFECTIVO,
                                        0, "", "");
                                    break;
                                }
                            case "CHEQUE":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Math.Abs(Convert.ToDouble(_pag.monto)),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CHEQUE,
                                0, "", "");
                                    break;
                                }
                            case "TARJETA DEBITO":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Math.Abs(Convert.ToDouble(_pag.monto)),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.TARJETA_DE_DEBITO,
                                0, "", "");
                                    break;
                                }
                            default:
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Math.Abs(Convert.ToDouble(_pag.monto)),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.OTROS_MEDIOS_PAGO,
                                0, "", "");
                                    break;
                                }
                        }
                        if (_pag.AbrirCajon)
                            _abrirCajon = true;
                    }
                    //Limpiamos
                    hasar.ConfigurarZona(1, _estilo, " ",
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_1_ENCABEZADO);
                    _respCerrar = hasar.CerrarDocumento();

                    int _numeroTicket = _respCerrar.getNumeroComprobante();

                    string _numeroPos = hasar.ConsultarDatosInicializacion().getNumeroPos().ToString().PadLeft(4, '0');

                    //Valido si el comprobante se cancelo.
                    if (hasar.ConsultarEstado().getNumeroUltimoComprobante() == _nroComprobante)
                    {
                        if (hasar.ConsultarEstado().EstadoAuxiliar.getUltimoComprobanteFueCancelado())
                        {
                            //Disparo la cancelacion.
                            Transacciones.TransaccionCancelacion(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _sqlConn);
                            Impresiones.Cancelar(hasar);
                            _rta = "La venta se cancelo y NO SE FISCALIZO por problemas de conexion con el controlador fiscal." + Environment.NewLine +
                                    "Luego intente fiscalizarla nuevamente. ";
                        }
                        else
                        {                            
                            Transacciones.TransaccionOK(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _docAsoc, _sqlConn);
                            _rta = "";
                        }
                    }

                    if (hasar.ObtenerUltimoEstadoImpresora().getFaltaPapelReceipt())
                        _faltaPapel = true;

                    if (_abrirCajon)
                        hasar.AbrirCajonDinero();
                }
                catch (Exception ex)
                {
                    //Vemos si el error fue porque necesita el Cierre Z.
                    if (ex.Message.StartsWith("POS_DOCUMENT_BEYOND_FISCAL_DAY"))
                    {
                        _necesitaCierreZ = true;
                        _rta = "Cierre Z. ";
                    }
                    else
                    {
                        if (ex.Message.StartsWith("Los totales no coinciden"))
                        {
                            //Disparo la cancelacion.
                            Transacciones.TransaccionCancelacion(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _sqlConn);
                            Impresiones.Cancelar(hasar);
                            _rta = "La venta se cancelo y NO SE FISCALIZO por diferencias en los totales." + Environment.NewLine +
                            "Luego intente fiscalizarla nuevamente. ";
                        }
                        else
                        {
                            if (_nroComprobante > 0)
                            {
                                try
                                {
                                    if (hasar.ObtenerUltimoEstadoImpresora().getImpresoraOffLine())
                                        hasar.conectar(_ip);
                                    if (hasar.ConsultarEstado().EstadoAuxiliar.getUltimoComprobanteFueCancelado())
                                    {
                                        //Disparo la cancelacion.
                                        Transacciones.TransaccionCancelacion(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _sqlConn);
                                        Impresiones.Cancelar(hasar);
                                        _rta = "La venta se cancelo y NO SE FISCALIZO por problemas de conexion con el controlador fiscal." + Environment.NewLine +
                                        "Luego intente fiscalizarla nuevamente. ";
                                    }
                                    else
                                    {
                                        List<DocumentoAsociado> _lst = new List<DocumentoAsociado>();
                                        Transacciones.TransaccionOK(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _lst, _sqlConn);
                                        _rta = "";
                                    }
                                }
                                catch (Exception exc)
                                {
                                    _rta = "La venta NO SE FISCALIZO por problemas de conexion con el controlador fiscal." + Environment.NewLine +
                                        "Luego intente fiscalizarla nuevamente. Revise el error. Error: " + exc.Message;
                                }
                            }
                        }
                    }
                }
               
            }
            else
                _rta = "Las fechas del sistema y de la controladora fiscal no coinciden, debe realizar un Cierre Z. Por favor comuniquese con ICG Argentina.";

            return _rta;
        }

        public static string ImprimirFacturasAB(Cabecera _cab, List<Items> _items, List<Pagos> _pagos, List<OtrosTributos> _tributos,
            List<Recargos> _recargos, string _ip, SqlConnection _sqlConn, bool _log, string _txtRegalo1, string _txtRegalo2, string _txtRegalo3, bool _imprimeTktRegalo,
            out bool _faltaPapel, out bool _necesitaCierreZ, out int _nroComprobante, out int _nroPos, out bool _seCancelo)
        {
            bool _abrirCajon = false;            
            string _rta = "";
            string _serieFiscal2 = "";
            int _nroZeta = 0;
            //Valores de Salida Out.
            _faltaPapel = false;
            _necesitaCierreZ = false;
            _seCancelo = false;
            _nroComprobante = 0;
            _nroPos = 0;

            //aca Comienzo
            hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();
            //Estilo de Texto.
            hfl.argentina.Hasar_Funcs.AtributosDeTexto _estilo = new hfl.argentina.Hasar_Funcs.AtributosDeTexto();            
            //Hasar.
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarVersion _respVersion = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarVersion();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento _respAbrirDoc = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento _respCerrarDoc = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago _respPago = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal _respConsultaSubtotal = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarDatosInicializacion _respConsultarDatosInicializacion = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarDatosInicializacion();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarCapacidadZetas _seta = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarCapacidadZetas();
            
            _estilo.setNegrita(false);
            _estilo.setDobleAncho(false);
            _estilo.setCentrado(false);
            _estilo.setBorradoTexto(false);
            try
            {
                //Conectamos con la hasar.
                Conectar(hasar, _log, _ip);
                //Recupero los datos de la Inicializacion.
                _respConsultarDatosInicializacion = hasar.ConsultarDatosInicializacion();
                _nroPos = _respConsultarDatosInicializacion.getNumeroPos();
                //Recupero el ultimo Z le sumo 1.
                _seta = hasar.ConsultarCapacidadZetas();
                _nroZeta = _seta.getUltimaZeta() + 1;
                //Configuramos los datos extras a mostrar en el tiquet.
                hasar.ConfigurarZona(1, _estilo, "Nro Interno: " + _cab.numserie +"/" + _cab.numfac.ToString() + string.Empty.PadRight(5, ' ') + "VENDEDOR: " + _cab.nomvendedor,
                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO,
                        hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_1_ENCABEZADO);

                if (FuncionesVarias.FechaOk(hasar, _cab))
                {
                    switch(_cab.regfacturacioncliente)
                    {
                        case "1":
                            {
                                //Responsable inscripto.
                                hasar.CargarDatosCliente(_cab.nombrecliente, _cab.nrodoccliente.Replace("-", ""),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_INSCRIPTO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT,
                                    _cab.direccioncliente, "", "", "");

                                _respAbrirDoc = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_A);
                                _serieFiscal2 = "001_Factura_A";
                                break;
                            }
                        case "2":
                            {
                                hasar.CargarDatosCliente(_cab.nombrecliente, _cab.nrodoccliente.Replace("-", ""),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_EXENTO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT,
                                    _cab.direccioncliente, "", "", "");

                                _respAbrirDoc = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_B);
                                _serieFiscal2 = "006_Factura_B";
                                break;
                            }
                        case "4":
                            {
                                hasar.CargarDatosCliente(_cab.nombrecliente, _cab.nrodoccliente.Replace("-", ""),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.CONSUMIDOR_FINAL,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_DNI,
                                    _cab.direccioncliente, "", "", "");

                                _respAbrirDoc = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_B);
                                _serieFiscal2 = "006_Factura_B";
                                break;
                            }
                        case "6":
                        case "5":
                            {

                                hasar.CargarDatosCliente(_cab.nombrecliente, _cab.nrodoccliente.Replace("-", ""),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_INSCRIPTO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT,
                                    "Receptor del comprobante - Responsable Monotributo", _cab.direccioncliente, "", "");

                                _respAbrirDoc = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_A);
                                _serieFiscal2 = "001_Factura_A";
                                break;
                            }
                        default:
                            {
                                hasar.CargarDatosCliente(_cab.nombrecliente, _cab.nrodoccliente.Replace("-", ""),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.CONSUMIDOR_FINAL,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_DNI,
                                    _cab.direccioncliente, "", "", "");

                                _respAbrirDoc = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_B);
                                _serieFiscal2 = "006_Factura_B";
                                break;
                            }
                    }

                    //Recupero el Nro. de comprobante.
                    _nroComprobante = _respAbrirDoc.getNumeroComprobante();

                    foreach (Items _it in _items)
                    {
                        if (!String.IsNullOrEmpty(_it._referencia))
                            hasar.ImprimirTextoFiscal(_estilo, "Referencia " + _it._referencia, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);

                        if (String.IsNullOrEmpty(_it._descripcion))
                            _it._descripcion = "Articulo Sin Descripcion";

                        //si tenemos precio = 0 o DTO2 = 100 entonces imprimo el texto de la promo.
                        if (_it._precio == 0 && _it._dto2 == 100)
                        {
                            string _txt = _it._textoPromo + " Importe: " + _it._precioDefecto.ToString();
                            hasar.ImprimirTextoFiscal(_estilo, _txt, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);
                        }
                        //Imprimo las promos
                        foreach (ItemsPromos ip in _it.Promosiones)
                        {
                            string _txt = ip.textoimprimir + " Importe: " + ip.importedtoiva.ToString();
                            hasar.ImprimirTextoFiscal(_estilo, _txt, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);
                        }
                        if (_it._iva > 0)
                        {
                            //Imprimo el item
                            hasar.ImprimirItem(_it._descripcion,
                                Convert.ToDouble(_it._cantidad),
                                Convert.ToDouble(_it._precio),
                                hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO,
                                Convert.ToDouble(_it._iva),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_KIVA,
                                0.0,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                1, "", _it._codArticulo,
                                hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);
                        }
                        else
                        {
                            if (_it._tipoImpuesto == 9)
                            {
                                hasar.ImprimirItem(_it._descripcion,
                                 Convert.ToDouble(_it._cantidad),
                                 Convert.ToDouble(_it._precio),
                                 hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.NO_GRAVADO,
                                 Convert.ToDouble(_it._iva),
                                 hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_KIVA,
                                 0.0,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                 1, "", _it._codArticulo,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);
                            }
                            else
                            {
                                //Imprimo el item
                                hasar.ImprimirItem(_it._descripcion,
                                    Convert.ToDouble(_it._cantidad),
                                    Convert.ToDouble(_it._precio),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.EXENTO,
                                    Convert.ToDouble(_it._iva),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_KIVA,
                                    0.0,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                    1, "", _it._codArticulo,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);
                            }
                        }
                        //Si el Precio es 0 no imprimo el descuento del item.
                        if (_it._precio > 0)
                        {
                            if (Math.Abs(_it._descuento) == 100)
                            {
                                //Imprimo el item
                                hasar.ImprimirItem(_it._descripcion,
                                    Convert.ToDouble(_it._cantidad * -1),
                                    Convert.ToDouble(_it._precio),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO,
                                    Convert.ToDouble(_it._iva),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_KIVA,
                                    0.0,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                    1, "", _it._codArticulo,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);
                            }
                            else
                            {
                                if (_it._descuento > 0)
                                {
                                    double _dto;
                                    _dto = Convert.ToDouble(((_it._cantidad * _it._precio) * _it._descuento / 100));
                                    //Valido que tengo texto descuento.
                                    if (String.IsNullOrEmpty(_it._textoPromo))
                                    {
                                        if (_it._descuento == 0)
                                            _it._textoPromo = "Descuento ";
                                        else
                                            _it._textoPromo = "Descuento " + _it._descuento.ToString();
                                    }
                                    hasar.ImprimirDescuentoItem(_it._textoPromo,
                                        _dto,
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE);
                                }
                                else
                                {
                                    //Recargo
                                    double _dto;
                                    _dto = Convert.ToDouble(((_it._cantidad * _it._precio) * _it._descuento / 100));
                                    hasar.ImprimirItem("Recargo del " + (_it._descuento * -1).ToString(),
                                   1,
                                   Math.Abs(_dto),
                                   hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO,
                                   Convert.ToDouble(_it._iva),
                                   hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                                   hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_KIVA,
                                   0.0,
                                   hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                   hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                   1, "", _it._codArticulo,
                                   hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);
                                }
                            }
                        }
                    }

                    //Vemos si tenemos dto en la cabecera.
                    if (_cab.dtocomercial > 0)
                    {
                        hasar.ImprimirAjuste("Bonificación Gral. (comercial) " + _cab.dtocomercial.ToString(),
                            _cab.totdtocomercial,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                            "",
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL);
                    }
                    if (_cab.dtopp != 0)
                    {
                        if (_cab.dtopp > 0)
                        {
                            hasar.ImprimirAjuste("Bonificación DTOPP " + _cab.dtopp.ToString(),
                                _cab.totdtopp,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL);
                        }
                        else
                        {
                            double _dto = Math.Abs(_cab.totdtopp);
                            hasar.ImprimirAjuste("Recargo DTOPP " + _cab.dtopp.ToString(),
                                _dto,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.AJUSTE_POS);
                        }
                    }

                    //Recargos
                    foreach (Recargos _rec in _recargos)
                    {
                        if (_rec._recargo > 0)
                        {
                            double _importeRecargo = (_rec._bruto * _rec._recargo / 100);
                            if (_rec._iva > 0)
                            {
                                hasar.ImprimirAnticipoBonificacionEnvases(_rec._texto, _importeRecargo,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, _rec._iva,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_FIJO_MONTO, 0,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE, "7790001001047",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeOperacionesGlobalesIVA.RECARGO_IVA);
                            }
                            else
                            {
                                hasar.ImprimirAnticipoBonificacionEnvases(_rec._texto, _importeRecargo,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.EXENTO, _rec._iva,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_FIJO_MONTO, 0,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE, "7790001001047",
                                 hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeOperacionesGlobalesIVA.RECARGO_IVA);
                            }
                        }
                        else
                        {
                            if (_rec._iva > 0)
                            {
                                hasar.ImprimirAnticipoBonificacionEnvases(_rec._texto, _rec._importe,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, _rec._iva,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_FIJO_MONTO, 0,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE, "7790001001047",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeOperacionesGlobalesIVA.RECARGO_IVA);
                            }
                            else
                            {
                                hasar.ImprimirAnticipoBonificacionEnvases(_rec._texto, _rec._importe,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.EXENTO, _rec._iva,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_FIJO_MONTO, 0,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE, "7790001001047",
                                 hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeOperacionesGlobalesIVA.RECARGO_IVA);
                            }
                        }
                    }

                    //Otros Tributos.
                    foreach (OtrosTributos _ot in _tributos)
                    {
                        double _impor = Math.Abs(_ot._importe);
                        hasar.ImprimirOtrosTributos(hfl.argentina.HasarImpresoraFiscalRG3561.TiposTributos.IIBB,
                            _ot._nombre, _ot._base, _impor);
                    }
                    //Comparo los totales
                    if (!FuncionesVarias.ComparoSubtotal(hasar, _cab.totalneto))
                    {
                        throw new Exception("Los totales no coinciden. Revise el comprobante y luego reintente imprimirlo.");
                    }
                    //Pagos.
                    foreach (Pagos _pag in _pagos)
                    {
                        switch (_pag.tipopago.ToUpper())
                        {
                            case "TARJETA CRÉDITO":
                            case "TARJETA CREDITO":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                    Convert.ToDouble(_pag.monto),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.TARJETA_DE_CREDITO,
                                    0, "", "");
                                    break;
                                }
                            case "PENDIENTE":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                    Convert.ToDouble(_pag.monto),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CUENTA_CORRIENTE,
                                    0, "", "");
                                    break;
                                }
                            case "EFECTIVO":
                                {
                                    if (_pag.monto > 0)
                                    {
                                        if (_cab.entregado == 0)
                                        {
                                            _respPago = hasar.ImprimirPago(_pag.descripcion,
                                            Convert.ToDouble(_pag.monto),
                                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                            "",
                                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.EFECTIVO,
                                            0, "", "");
                                        }
                                        else
                                        {
                                            _respPago = hasar.ImprimirPago(_pag.descripcion,
                                            Convert.ToDouble(_cab.entregado),
                                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                            "",
                                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.EFECTIVO,
                                            0, "", "");
                                        }
                                    }
                                    break;
                                }
                            case "CHEQUE":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                    Convert.ToDouble(_pag.monto),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CHEQUE,
                                    0, "", "");
                                    break;
                                }
                            case "TARJETA DEBITO":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                    Convert.ToDouble(_pag.monto),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.TARJETA_DE_DEBITO,
                                    0, "", "");
                                    break;
                                }
                            default:
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                    Convert.ToDouble(_pag.monto),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    //_pag.descripcion,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.OTROS_MEDIOS_PAGO,
                                    0, "", "");

                                    break;
                                }
                        }
                        if (_pag.AbrirCajon)
                            _abrirCajon = true;
                    }

                    _respCerrarDoc = hasar.CerrarDocumento();

                    int _numeroTicket = _respCerrarDoc.getNumeroComprobante();

                    string _numeroPos = hasar.ConsultarDatosInicializacion().getNumeroPos().ToString().PadLeft(4, '0');

                    //Valido si el comprobante se cancelo.                    
                    if (hasar.ConsultarEstado().getNumeroUltimoComprobante() == _nroComprobante)
                    {
                        if (hasar.ConsultarEstado().EstadoAuxiliar.getUltimoComprobanteFueCancelado())
                        {
                            //Disparo la cancelacion.
                            Transacciones.TransaccionCancelacion(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _sqlConn);
                            Impresiones.Cancelar(hasar);
                        }
                        else
                        {
                            List<DocumentoAsociado> _lst = new List<DocumentoAsociado>();
                            Transacciones.TransaccionOK(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _lst, _sqlConn);
                            //Preguntamos por el ticket de regalo.
                            if (_imprimeTktRegalo)
                            {
                                if (MessageBox.Show(new Form { TopMost = true }, "Desea imprimir un ticket de regalo?.",
                                        "ICG Argentina", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                {
                                    string _numeroComprobante = _nroPos.ToString().PadLeft(4, '0') + "-" + _nroComprobante.ToString().PadLeft(8, '0');
                                    Hasar2daGen.Impresiones.ImprimirTicketRegalo(_cab, _items, _ip, _txtRegalo1, _txtRegalo2, _txtRegalo3,
                                        _numeroComprobante, _log, hasar, out _faltaPapel);
                                }
                            }
                        }
                    }

                    if (hasar.ObtenerUltimoEstadoImpresora().getFaltaPapelReceipt())
                    {
                        _faltaPapel = true;                        
                    }

                    //Vemos si abrimos el cajon
                    if (_abrirCajon)
                        hasar.AbrirCajonDinero();                    
                }
                else
                {
                    _rta = "La fecha del comprobante que va imprimir y de la controladora fiscal no coinciden." + Environment.NewLine + 
                        "Debe cambiar la fecha para poder imprimirlo."+ Environment.NewLine + "Por favor comuniquese con ICG Argentina.";
                }
                //Limpiamos
                hasar.ConfigurarZona(1, _estilo, " ",
                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO,
                        hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_1_ENCABEZADO);
            }
            catch (Exception ex)
            {
                _rta = "La venta quedará registrada PENDIENTE de fiscalizar. Revise el error y REINTENTE IMPRIMIR el ticket. ";
                //Vemos si el error fue porque necesita el Cierre Z.
                if (ex.Message.StartsWith("POS_DOCUMENT_BEYOND_FISCAL_DAY"))
                {
                    _necesitaCierreZ = true;
                    _rta = "Cierre Z. ";
                }
                else
                {
                    if (ex.Message.StartsWith("Los totales no coinciden"))
                    {
                        //Disparo la cancelacion.
                        Transacciones.TransaccionCancelacion(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _sqlConn);
                        Impresiones.Cancelar(hasar);
                        _rta = "La venta se cancelo y NO SE FISCALIZO por diferencias en los totales." + Environment.NewLine +
                        "Luego intente fiscalizarla nuevamente. ";
                    }
                    else
                    {
                        if (_nroComprobante > 0)
                        {
                            try
                            {
                                if (hasar.ObtenerUltimoEstadoImpresora().getImpresoraOffLine())
                                    hasar.conectar(_ip);
                                if (hasar.ObtenerUltimoEstadoFiscal().getDocumentoAbierto())
                                {
                                    //Disparo la cancelacion.
                                    Transacciones.TransaccionCancelacion(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _sqlConn);
                                    Impresiones.Cancelar(hasar);
                                    _rta = "La venta se cancelo y NO SE FISCALIZO por problemas de conexion con el controlador fiscal." + Environment.NewLine +
                                    "Luego intente fiscalizarla nuevamente.";
                                }
                                if (hasar.ConsultarEstado().EstadoAuxiliar.getUltimoComprobanteFueCancelado())
                                {
                                    //Disparo la cancelacion.
                                    Transacciones.TransaccionCancelacion(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _sqlConn);
                                    Impresiones.Cancelar(hasar);
                                    _rta = "CANCELADA";
                                }
                                else
                                {
                                    List<DocumentoAsociado> _lst = new List<DocumentoAsociado>();
                                    Transacciones.TransaccionOK(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _lst, _sqlConn);
                                    //Preguntamos por el ticket de regalo.
                                    if (_imprimeTktRegalo)
                                    {
                                        if (MessageBox.Show(new Form { TopMost = true }, "Desea imprimir un ticket de regalo?.",
                                                "ICG Argentina", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                        {
                                            string _numeroComprobante = _nroPos.ToString().PadLeft(4, '0') + "-" + _nroComprobante.ToString().PadLeft(8, '0');
                                            Hasar2daGen.Impresiones.ImprimirTicketRegalo(_cab, _items, _ip, _txtRegalo1, _txtRegalo2, _txtRegalo3, _numeroComprobante
                                                , _log, hasar, out _faltaPapel);
                                        }
                                    }
                                }
                            }
                            catch (Exception exc)
                            {
                                _rta = "La venta NO SE FISCALIZO por problemas de conexion con el controlador fiscal." + Environment.NewLine +
                                    "Luego intente fiscalizarla nuevamente. Revise el error. Error: ";
                                throw new Exception(_rta + exc.Message);
                            }
                        }
                        else
                        {
                            //Solo cancelo.
                            Impresiones.Cancelar(hasar);
                        }
                    }
                }
            }            

            return _rta;
        }

        public static string ImprimirFacturasAB(Cabecera _cab, List<Items> _items, List<Pagos> _pagos, List<OtrosTributos> _tributos,
           List<Recargos> _recargos, string _ip, SqlConnection _sqlConn, bool _log, string _txtRegalo1, string _txtRegalo2, string _txtRegalo3, bool _imprimeTktRegalo,
           out bool _faltaPapel, out bool _necesitaCierreZ)
        {
            bool _abrirCajon = false;
            string _rta = "";
            string _serieFiscal2 = "";
            int _nroZeta = 0;
            int _nroComprobante = 0;
            int _nroPos = 0;
            //Valores de Salida Out.
            _faltaPapel = false;
            _necesitaCierreZ = false;
            

            //aca Comienzo
            hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();
            //Estilo de Texto.
            hfl.argentina.Hasar_Funcs.AtributosDeTexto _estilo = new hfl.argentina.Hasar_Funcs.AtributosDeTexto();
            //Hasar.
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarVersion _respVersion = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarVersion();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento _respAbrirDoc = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento _respCerrarDoc = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago _respPago = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal _respConsultaSubtotal = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarDatosInicializacion _respConsultarDatosInicializacion = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarDatosInicializacion();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarCapacidadZetas _seta = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarCapacidadZetas();

            _estilo.setNegrita(false);
            _estilo.setDobleAncho(false);
            _estilo.setCentrado(false);
            _estilo.setBorradoTexto(false);
            try
            {
                //Conectamos con la hasar.
                Conectar(hasar, _log, _ip);
                //Recupero los datos de la Inicializacion.
                _respConsultarDatosInicializacion = hasar.ConsultarDatosInicializacion();
                _nroPos = _respConsultarDatosInicializacion.getNumeroPos();
                //Recupero el ultimo Z le sumo 1.
                _seta = hasar.ConsultarCapacidadZetas();
                _nroZeta = _seta.getUltimaZeta() + 1;
               

                if (FuncionesVarias.FechaOk(hasar, _cab))
                {
                    hasar.ConfigurarZona(1, _estilo, "Numero Interno: " + _cab.numserie + "/" + _cab.numfac.ToString() + string.Empty.PadLeft(5, ' ') + "VENDEDOR: " + _cab.nomvendedor,
                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO,
                        hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_1_ENCABEZADO);
                    switch (_cab.regfacturacioncliente)
                    {
                        case "1":
                            {
                                //Responsable inscripto.
                                hasar.CargarDatosCliente(_cab.nombrecliente, _cab.nrodoccliente.Replace("-", ""),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_INSCRIPTO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT,
                                    _cab.direccioncliente, "", "", "");

                                _respAbrirDoc = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_A);
                                _serieFiscal2 = "001_Factura_A";
                                break;
                            }
                        case "2":
                            {
                                hasar.CargarDatosCliente(_cab.nombrecliente, _cab.nrodoccliente.Replace("-", ""),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_EXENTO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT,
                                    _cab.direccioncliente, "", "", "");

                                _respAbrirDoc = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_B);
                                _serieFiscal2 = "006_Factura_B";
                                break;
                            }
                        case "4":
                            {
                                hasar.CargarDatosCliente(_cab.nombrecliente, _cab.nrodoccliente.Replace("-", ""),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.CONSUMIDOR_FINAL,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_DNI,
                                    _cab.direccioncliente, "", "", "");

                                _respAbrirDoc = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_B);
                                _serieFiscal2 = "006_Factura_B";
                                break;
                            }
                        case "6":
                        case "5":
                            {

                                hasar.CargarDatosCliente(_cab.nombrecliente, _cab.nrodoccliente.Replace("-", ""),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_INSCRIPTO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT,
                                    "Receptor del comprobante - Responsable Monotributo", _cab.direccioncliente, "", "");

                                _respAbrirDoc = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_A);
                                _serieFiscal2 = "001_Factura_A";
                                break;
                            }
                        default:
                            {
                                hasar.CargarDatosCliente(_cab.nombrecliente, _cab.nrodoccliente.Replace("-", ""),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.CONSUMIDOR_FINAL,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_DNI,
                                    _cab.direccioncliente, "", "", "");

                                _respAbrirDoc = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_B);
                                _serieFiscal2 = "006_Factura_B";
                                break;
                            }
                    }

                    //Recupero el Nro. de comprobante.
                    _nroComprobante = _respAbrirDoc.getNumeroComprobante();

                    foreach (Items _it in _items)
                    {
                        if (!String.IsNullOrEmpty(_it._referencia))
                            hasar.ImprimirTextoFiscal(_estilo, "Referencia " + _it._referencia, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);

                        if (String.IsNullOrEmpty(_it._descripcion))
                            _it._descripcion = "Articulo Sin Descripcion";

                        //si tenemos precio = 0 o DTO2 = 100 entonces imprimo el texto de la promo.
                        if (_it._precio == 0 && _it._dto2 == 100)
                        {
                            string _txt = _it._textoPromo + " Importe: " + _it._precioDefecto.ToString();
                            hasar.ImprimirTextoFiscal(_estilo, _txt, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);
                        }
                        //Imprimo las promos
                        foreach (ItemsPromos ip in _it.Promosiones)
                        {
                            string _txt = ip.textoimprimir + " Importe: " + ip.importedtoiva.ToString();
                            hasar.ImprimirTextoFiscal(_estilo, _txt, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);
                        }
                        if (_it._iva > 0)
                        {
                            //Imprimo el item
                            hasar.ImprimirItem(_it._descripcion,
                                Convert.ToDouble(_it._cantidad),
                                Convert.ToDouble(_it._precio),
                                hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO,
                                Convert.ToDouble(_it._iva),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_KIVA,
                                0.0,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                1, "", _it._codArticulo,
                                hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);
                        }
                        else
                        {
                            if (_it._tipoImpuesto == 9)
                            {
                                hasar.ImprimirItem(_it._descripcion,
                                 Convert.ToDouble(_it._cantidad),
                                 Convert.ToDouble(_it._precio),
                                 hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.NO_GRAVADO,
                                 Convert.ToDouble(_it._iva),
                                 hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_KIVA,
                                 0.0,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                 1, "", _it._codArticulo,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);
                            }
                            else
                            {
                                //Imprimo el item
                                hasar.ImprimirItem(_it._descripcion,
                                    Convert.ToDouble(_it._cantidad),
                                    Convert.ToDouble(_it._precio),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.EXENTO,
                                    Convert.ToDouble(_it._iva),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_KIVA,
                                    0.0,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                    1, "", _it._codArticulo,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);
                            }
                        }
                        //Si el Precio es 0 no imprimo el descuento del item.
                        if (_it._precio > 0)
                        {
                            if (Math.Abs(_it._descuento) > 0)
                            {
                                if (Math.Abs(_it._descuento) == 100)
                                {
                                    hasar.ImprimirItem(_it._descripcion,
                                        Convert.ToDouble(_it._cantidad * -1),
                                        Convert.ToDouble(_it._precio),
                                        hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO,
                                        Convert.ToDouble(_it._iva),
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_KIVA,
                                        0.0,
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                        1, "", _it._codArticulo,
                                        hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);
                                }
                                else
                                {
                                    if (_it._descuento > 0)
                                    {
                                        //Descuento.
                                        double _dto;
                                        _dto = Convert.ToDouble(((_it._cantidad * _it._precio) * _it._descuento / 100));
                                        //Valido que tengo texto descuento.
                                        if (String.IsNullOrEmpty(_it._textoPromo))
                                        {
                                            if (_it._descuento == 0)
                                                _it._textoPromo = "Descuento ";
                                            else
                                                _it._textoPromo = "Descuento " + _it._descuento.ToString();
                                        }
                                        hasar.ImprimirDescuentoItem(_it._textoPromo,
                                            //Math.Abs(_dto),
                                            _dto,
                                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE);
                                    }
                                    else
                                    {
                                        //Recargo
                                        double _dto;
                                        _dto = Convert.ToDouble(((_it._cantidad * _it._precio) * _it._descuento / 100));
                                        hasar.ImprimirItem("Recargo del " + (_it._descuento * -1).ToString(),
                                       1,
                                       Math.Abs(_dto),
                                       hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO,
                                       Convert.ToDouble(_it._iva),
                                       hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                                       hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_KIVA,
                                       0.0,
                                       hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                       hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                       1, "", _it._codArticulo,
                                       hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);
                                    }
                                }
                            }
                        }
                    }

                    //Vemos si tenemos dto en la cabecera.
                    if (_cab.dtocomercial > 0)
                    {
                        hasar.ImprimirAjuste("Bonificación Gral. (comercial) " + _cab.dtocomercial.ToString(),
                            _cab.totdtocomercial,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                            "",
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL);
                    }
                    if (_cab.dtopp != 0)
                    {
                        if (_cab.dtopp > 0)
                        {
                            hasar.ImprimirAjuste("Bonificación DTOPP " + _cab.dtopp.ToString(),
                                _cab.totdtopp,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL);
                        }
                        else
                        {
                            double _dto = Math.Abs(_cab.totdtopp);
                            hasar.ImprimirAjuste("Recargo DTOPP " + _cab.dtopp.ToString(),
                                _dto,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.AJUSTE_POS);
                        }
                    }

                    //Recargos
                    foreach (Recargos _rec in _recargos)
                    {
                        if (_rec._recargo > 0)
                        {
                            double _importeRecargo = (_rec._bruto * _rec._recargo / 100);
                            if (_rec._iva > 0)
                            {
                                hasar.ImprimirAnticipoBonificacionEnvases(_rec._texto, _importeRecargo,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, _rec._iva,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_FIJO_MONTO, 0,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE, "7790001001047",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeOperacionesGlobalesIVA.RECARGO_IVA);
                            }
                            else
                            {
                                hasar.ImprimirAnticipoBonificacionEnvases(_rec._texto, _importeRecargo,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.EXENTO, _rec._iva,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_FIJO_MONTO, 0,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE, "7790001001047",
                                 hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeOperacionesGlobalesIVA.RECARGO_IVA);
                            }
                        }
                        else
                        {
                            if (_rec._iva > 0)
                            {
                                hasar.ImprimirAnticipoBonificacionEnvases(_rec._texto, _rec._importe,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, _rec._iva,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_FIJO_MONTO, 0,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE, "7790001001047",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeOperacionesGlobalesIVA.RECARGO_IVA);
                            }
                            else
                            {
                                hasar.ImprimirAnticipoBonificacionEnvases(_rec._texto, _rec._importe,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.EXENTO, _rec._iva,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_FIJO_MONTO, 0,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                 hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE, "7790001001047",
                                 hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeOperacionesGlobalesIVA.RECARGO_IVA);
                            }
                        }
                    }

                    //Otros Tributos.
                    foreach (OtrosTributos _ot in _tributos)
                    {
                        double _impor = Math.Abs(_ot._importe);
                        hasar.ImprimirOtrosTributos(hfl.argentina.HasarImpresoraFiscalRG3561.TiposTributos.IIBB,
                            _ot._nombre, _ot._base, _impor);
                    }
                    //Comparo los totales
                    if (!FuncionesVarias.ComparoSubtotal(hasar, _cab.totalneto))
                    {
                        throw new Exception("Los totales no coinciden. Revise el comprobante y luego reintente imprimirlo.");
                    }
                    //Pagos.
                    foreach (Pagos _pag in _pagos)
                    {
                        switch (_pag.tipopago.ToUpper())
                        {
                            case "TARJETA CRÉDITO":
                            case "TARJETA CREDITO":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                    Convert.ToDouble(_pag.monto),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.TARJETA_DE_CREDITO,
                                    0, "", "");
                                    break;
                                }
                            case "PENDIENTE":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                    Convert.ToDouble(_pag.monto),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CUENTA_CORRIENTE,
                                    0, "", "");
                                    break;
                                }
                            case "EFECTIVO":
                                {
                                    if (_pag.monto > 0)
                                    {
                                        if (_cab.entregado == 0)
                                        {
                                            _respPago = hasar.ImprimirPago(_pag.descripcion,
                                            Convert.ToDouble(_pag.monto),
                                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                            "",
                                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.EFECTIVO,
                                            0, "", "");
                                        }
                                        else
                                        {
                                            _respPago = hasar.ImprimirPago(_pag.descripcion,
                                            Convert.ToDouble(_cab.entregado),
                                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                            "",
                                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.EFECTIVO,
                                            0, "", "");
                                        }
                                    }
                                    break;
                                }
                            case "CHEQUE":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                    Convert.ToDouble(_pag.monto),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CHEQUE,
                                    0, "", "");
                                    break;
                                }
                            case "TARJETA DEBITO":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                    Convert.ToDouble(_pag.monto),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.TARJETA_DE_DEBITO,
                                    0, "", "");
                                    break;
                                }
                            default:
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                    Convert.ToDouble(_pag.monto),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    //_pag.descripcion,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.OTROS_MEDIOS_PAGO,
                                    0, "", "");

                                    break;
                                }
                        }
                        if (_pag.AbrirCajon)
                            _abrirCajon = true;
                    }
                    //Limpiamos
                    hasar.ConfigurarZona(1, _estilo, " ",
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_1_ENCABEZADO);
                    _respCerrarDoc = hasar.CerrarDocumento();

                    int _numeroTicket = _respCerrarDoc.getNumeroComprobante();

                    string _numeroPos = hasar.ConsultarDatosInicializacion().getNumeroPos().ToString().PadLeft(4, '0');

                    if (hasar.ConsultarEstado().getNumeroUltimoComprobante() == _nroComprobante)
                    {
                        if (hasar.ConsultarEstado().EstadoAuxiliar.getUltimoComprobanteFueCancelado())
                        {
                            //Disparo la cancelacion.
                            Transacciones.TransaccionCancelacion(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _sqlConn);
                            Impresiones.Cancelar(hasar);
                            _rta = "La venta se cancelo y NO SE FISCALIZO por problemas de conexion con el controlador fiscal." + Environment.NewLine +
                                    "Luego intente fiscalizarla nuevamente. ";
                        }
                        else
                        {
                            List<DocumentoAsociado> _lst = new List<DocumentoAsociado>();
                            Transacciones.TransaccionOK(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _lst, _sqlConn);
                            //Preguntamos por el ticket de regalo.
                            if (_imprimeTktRegalo)
                            {
                                if (MessageBox.Show(new Form { TopMost = true }, "Desea imprimir un ticket de regalo?.",
                                        "ICG Argentina", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                {
                                    string _numeroComprobante = _numeroPos + "-" + _numeroTicket.ToString().PadLeft(8, '0');
                                    Hasar2daGen.Impresiones.ImprimirTicketRegalo(_cab, _items, _ip, _txtRegalo1, _txtRegalo2, _txtRegalo3, _numeroComprobante
                                        , _log, hasar, out _faltaPapel);
                                }
                            }
                            _rta = "";
                        }
                    }
                    
                    if (hasar.ObtenerUltimoEstadoImpresora().getFaltaPapelReceipt())
                    {
                        _faltaPapel = true;
                    }

                    //Vemos si abrimos el cajon
                    if (_abrirCajon)
                        hasar.AbrirCajonDinero();
                }
                else
                {
                    _rta = "La fecha del comprobante que va imprimir y de la controladora fiscal no coinciden." + Environment.NewLine +
                        "Debe cambiar la fecha para poder imprimirlo." + Environment.NewLine + "Por favor comuniquese con ICG Argentina.";
                }
                //Limpiamos
                hasar.ConfigurarZona(1, _estilo, " ",
                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO,
                        hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_1_ENCABEZADO);
            }
            catch (Exception ex)
            {
                _rta = "La venta quedará registrada PENDIENTE de fiscalizar. Revise el error y REINTENTE IMPRIMIR el ticket. ";
                //Vemos si el error fue porque necesita el Cierre Z.
                if (ex.Message.StartsWith("POS_DOCUMENT_BEYOND_FISCAL_DAY"))
                {
                    _necesitaCierreZ = true;
                    _rta = "Cierre Z. ";
                }
                else
                {
                    if (_nroComprobante > 0)
                    {
                        try
                        {
                            if (hasar.ObtenerUltimoEstadoImpresora().getImpresoraOffLine())
                                hasar.conectar(_ip);
                            if(hasar.ObtenerUltimoEstadoFiscal().getDocumentoAbierto())
                            {
                                //Disparo la cancelacion.
                                Transacciones.TransaccionCancelacion(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _sqlConn);
                                Impresiones.Cancelar(hasar);
                                _rta = "La venta se cancelo y NO SE FISCALIZO por problemas de Impresión." + Environment.NewLine +
                                "Revise el ticket antes de reimprimirlo.";
                                //no hago nada mas y salgo.
                                return _rta;
                            }
                            if (hasar.ConsultarEstado().EstadoAuxiliar.getUltimoComprobanteFueCancelado())
                            {
                                //Disparo la cancelacion.
                                Transacciones.TransaccionCancelacion(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _sqlConn);
                                Impresiones.Cancelar(hasar);
                                _rta = "La venta se cancelo y NO SE FISCALIZO por problemas de conexion con el controlador fiscal." + Environment.NewLine +
                                "Luego intente fiscalizarla nuevamente.";                                
                            }
                            else
                            {

                                List<DocumentoAsociado> _lst = new List<DocumentoAsociado>();
                                Transacciones.TransaccionOK(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _lst, _sqlConn);
                                //Preguntamos por el ticket de regalo.
                                if (_imprimeTktRegalo)
                                {
                                    if (MessageBox.Show(new Form { TopMost = true }, "Desea imprimir un ticket de regalo?.",
                                            "ICG Argentina", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                    {
                                        string _numeroComprobante = _nroPos.ToString().PadLeft(4, '0') + "-" + _nroComprobante.ToString().PadLeft(8, '0');
                                        Hasar2daGen.Impresiones.ImprimirTicketRegalo(_cab, _items, _ip, _txtRegalo1, _txtRegalo2, _txtRegalo3, _numeroComprobante
                                            , _log, hasar, out _faltaPapel);
                                    }
                                }
                                _rta = "";
                            }
                        }
                        catch (Exception exc)
                        {
                            _rta = "La venta NO SE FISCALIZO por problemas de conexion con el controlador fiscal." + Environment.NewLine +
                                "Luego intente fiscalizarla nuevamente." + Environment.NewLine +"Revise el error. Error: "+ exc.Message;
                        }
                    }
                    else
                    {
                        //Solo cancelo el comprobante en curso.
                        Impresiones.Cancelar(hasar);
                    }
                }
            }

            return _rta;
        }

        public static void ImprimirTicketRegalo(Cabecera _cab, List<Items> _items, string _ip
            , string _txtRegalo1, string _txtRegalo2, string _txtRegalo3,
            string _comprobante, bool _log, out bool _faltaPapel)
        {            
            _faltaPapel = false;
            //aca Comienzo
            hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();
            //Estilo de Texto.
            hfl.argentina.Hasar_Funcs.AtributosDeTexto _estilo = new hfl.argentina.Hasar_Funcs.AtributosDeTexto();

            //Hasar.
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarVersion _respVersion = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarVersion();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento _respAbrirDoc = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento _respCerrarDoc = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago _respPago = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal _respConsultaSubtotal = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal();
            _estilo.setNegrita(false);
            _estilo.setDobleAncho(false);
            _estilo.setCentrado(false);
            _estilo.setBorradoTexto(false);

            try
            {
                //Conectamos con la hasar.
                Conectar(hasar, _log, _ip);

                //Abro el documento.
                _respAbrirDoc = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.GENERICO);

                _estilo.setNegrita(true);
                //Titulo.
                _estilo.setDobleAncho(true);
                _estilo.setNegrita(true);
                _estilo.setCentrado(true);
                hasar.ImprimirTextoGenerico(_estilo, "Ticket Regalo", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);
                hasar.ImprimirTextoGenerico(_estilo, "Comprobante: " +_comprobante, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);
                _estilo.setCentrado(false);
                _estilo.setNegrita(false);
                _estilo.setDobleAncho(false);

                //Cabecera de Items.
                hasar.ImprimirTextoGenerico(_estilo, "Descripcion Articulo", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);
                //Linea separadora.
                hasar.ImprimirTextoGenerico(_estilo, "----------------------------------------------------------------", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);
                _estilo.setNegrita(false);
                //Recorro los items.
                decimal _cantidad = 0;
                foreach (Items _it in _items)
                {
                    //Imprimo items
                    hasar.ImprimirTextoGenerico(_estilo, _it._descripcion, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);
                    _cantidad = _cantidad + _it._cantidad;
                }
                _estilo.setNegrita(true);
                //Linea separadora.
                hasar.ImprimirTextoGenerico(_estilo, "----------------------------------------------------------------", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);
                hasar.ImprimirTextoGenerico(_estilo, "Total unidades: " + Convert.ToInt32(_cantidad).ToString(), hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);
                _estilo.setNegrita(false);
                //Imprimo texto de regalo.
                //_estilo.setDobleAncho(true);
                if(!String.IsNullOrEmpty(_txtRegalo1))
                    hasar.ImprimirTextoGenerico(_estilo, _txtRegalo1, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);
                if (!String.IsNullOrEmpty(_txtRegalo2))
                    hasar.ImprimirTextoGenerico(_estilo, _txtRegalo2, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);
                if (!String.IsNullOrEmpty(_txtRegalo3))
                    hasar.ImprimirTextoGenerico(_estilo, _txtRegalo3, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);
                //_estilo.setDobleAncho(false);

                _respCerrarDoc = hasar.CerrarDocumento();

                if (hasar.ObtenerUltimoEstadoImpresora().getFaltaPapelReceipt())
                    _faltaPapel = true;
                    //_rta = "La impresora fiscal informa que se esta quedando sin papel. Por favor revise el papel.";
            }
            catch(Exception ex)
            {
                Impresiones.Cancelar(hasar);
                throw new Exception(ex.Message);
            }
        }

        public static void ImprimirTicketRegalo(Cabecera _cab, List<Items> _items, string _ip
            , string _txtRegalo1, string _txtRegalo2, string _txtRegalo3,
            string _comprobante, bool _log, hfl.argentina.HasarImpresoraFiscalRG3561 _cf, out bool _faltaPapel)
        {
            _faltaPapel = false;
            //Estilo de Texto.
            hfl.argentina.Hasar_Funcs.AtributosDeTexto _estilo = new hfl.argentina.Hasar_Funcs.AtributosDeTexto();

            //Hasar.
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarVersion _respVersion = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarVersion();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento _respAbrirDoc = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento _respCerrarDoc = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago _respPago = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal _respConsultaSubtotal = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal();
            _estilo.setNegrita(false);
            _estilo.setDobleAncho(false);
            _estilo.setCentrado(false);
            _estilo.setBorradoTexto(false);

            try
            {
                if (_cf.ObtenerUltimoEstadoImpresora().getImpresoraOffLine())
                    //Conectamos con la hasar.
                    Conectar(_cf, _log, _ip);

                //Abro el documento.
                _respAbrirDoc = _cf.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.GENERICO);

                _estilo.setNegrita(true);
                //Titulo.
                _estilo.setDobleAncho(true);
                _estilo.setNegrita(true);
                _estilo.setCentrado(true);
                _cf.ImprimirTextoGenerico(_estilo, "Ticket Regalo", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);
                _cf.ImprimirTextoGenerico(_estilo, "Comprobante: " + _comprobante, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);
                _estilo.setCentrado(false);
                _estilo.setNegrita(false);
                _estilo.setDobleAncho(false);

                //Cabecera de Items.
                _cf.ImprimirTextoGenerico(_estilo, "Descripcion Articulo", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);
                //Linea separadora.
                _cf.ImprimirTextoGenerico(_estilo, "----------------------------------------------------------------", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);
                _estilo.setNegrita(false);
                //Recorro los items.
                decimal _cantidad = 0;
                foreach (Items _it in _items)
                {
                    //Imprimo items
                    _cf.ImprimirTextoGenerico(_estilo, _it._descripcion, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);
                    _cantidad = _cantidad + _it._cantidad;
                }
                _estilo.setNegrita(true);
                //Linea separadora.
                _cf.ImprimirTextoGenerico(_estilo, "----------------------------------------------------------------", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);
                _cf.ImprimirTextoGenerico(_estilo, "Total unidades: " + Convert.ToInt32(_cantidad).ToString(), hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);
                _estilo.setNegrita(false);
                //Imprimo texto de regalo.
                //_estilo.setDobleAncho(true);
                if (!String.IsNullOrEmpty(_txtRegalo1))
                    _cf.ImprimirTextoGenerico(_estilo, _txtRegalo1, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);
                if (!String.IsNullOrEmpty(_txtRegalo2))
                    _cf.ImprimirTextoGenerico(_estilo, _txtRegalo2, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);
                if (!String.IsNullOrEmpty(_txtRegalo3))
                    _cf.ImprimirTextoGenerico(_estilo, _txtRegalo3, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);
                //_estilo.setDobleAncho(false);

                _respCerrarDoc = _cf.CerrarDocumento();

                if (_cf.ObtenerUltimoEstadoImpresora().getFaltaPapelReceipt())
                    _faltaPapel = true;
                //_rta = "La impresora fiscal informa que se esta quedando sin papel. Por favor revise el papel.";
            }
            catch (Exception ex)
            {
                Impresiones.Cancelar(_cf);
                throw new Exception(ex.Message);
            }
        }
        public static string Reimprimir(int _nro, string _descrip, string _ip, out bool _faltaPapel)
        {
            string _rta = "";
            _faltaPapel = false;
            hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();

            //hasar.conectar(_ip);
            Conectar(hasar, false, _ip);

            try
            {
                switch (_descrip.Substring(0, 3))
                {
                    //case "NOTA DE CREDITO A":
                    case "003":
                        {
                            hasar.CopiarComprobante(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NOTA_DE_CREDITO_A, _nro);
                            break;
                        }
                    //case "NOTA DE CREDITO B":
                    case "008":
                        {
                            hasar.CopiarComprobante(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NOTA_DE_CREDITO_B, _nro);
                            break;
                        }
                    //case "TIQUET FACTURA A":
                    case "001":
                        {
                            hasar.CopiarComprobante(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_A, _nro);
                            break;
                        }
                    //case "TIQUET FACTURA B":
                    case "006":
                        {
                            hasar.CopiarComprobante(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_B, _nro);
                            break;
                        }
                    case "002":
                        {
                            //Nota de debito A
                            hasar.CopiarComprobante(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NOTA_DE_DEBITO_A, _nro);
                            break;
                        }
                    case "007":
                        {
                            //Nota de debito A
                            hasar.CopiarComprobante(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NOTA_DE_DEBITO_B, _nro);
                            break;
                        }
                    default:
                        {
                            _rta = "Tipo de Documento no configurado para su fiscalización.";
                            break;
                        }
                }

                if (hasar.ObtenerUltimoEstadoImpresora().getFaltaPapelReceipt())
                    _faltaPapel = true;
            }
            catch (Exception ex)
            {
                Cancelar(hasar);
                throw new Exception(ex.Message);
            }

            return _rta;
        }

        public static void Cancelar(hfl.argentina.HasarImpresoraFiscalRG3561 _cf)
        {
            hfl.argentina.Estados_Fiscales_RG3561.EstadoFiscal _estadoFiscal = new hfl.argentina.Estados_Fiscales_RG3561.EstadoFiscal();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarEstado _respConsultarEstado = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarEstado();
 
            _respConsultarEstado = _cf.ConsultarEstado();
            _estadoFiscal = _cf.ObtenerUltimoEstadoFiscal();

            if (_estadoFiscal.getDocumentoAbierto())
            {
                try
                {
                    _cf.Cancelar();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
            }
        }

        public static void Conectar(hfl.argentina.HasarImpresoraFiscalRG3561 _cf, bool _hasarLog, string _ip)
        {
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarVersion _respVersion = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarVersion();
            try
            {
                //Abrimos el log.
                if (_hasarLog)
                    _cf.archivoRegistro("HasarLog.log");
                //Conectamos con la hasar.
                bool _conectado = false;
                do
                {
                    try
                    {
                        _cf.conectar(_ip);
                        _conectado = true;
                    }
                    catch
                    {
                        if (MessageBox.Show(new Form { TopMost = true }, "No podemos conectar con el controlador fisca?."
                            + Environment.NewLine + "Desea REINTENTAR la conexión?.",
                            "ICG Argentina", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            _conectado = false;
                        else
                            throw new Exception();
                            //_conectado = true;
                    }
                } while (!_conectado);
                if (!_conectado)
                    throw new Exception("No se conecto al controlador Fiscal.");
                                
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo conectar con la Controladora Fiscal. Error:" + ex.Message);
            }
        }
    }
}
