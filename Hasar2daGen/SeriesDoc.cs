﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hasar2daGen
{
    public class SeriesDoc
    {
        public int tipodoc { get; set; }
        public string serie { get; set; }
        public int posicion { get; set; }
        public int contadorb { get; set; }
        public int contadorn { get; set; }

        public static SeriesDoc GetSeriesDoc(string _serie, int _tipodoc, SqlConnection _con)
        {
            string _sql = @"select TIPODOC, SERIE, POSICION, CONTADORB, CONTADORN from SERIESDOC where SERIE = @Serie and TIPODOC = @TipoDoc";

            SeriesDoc _cls = new SeriesDoc();

            try
            {
                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;

                    _cmd.Parameters.AddWithValue("@Serie", _serie);
                    _cmd.Parameters.AddWithValue("@TipoDoc", _tipodoc);

                    using (SqlDataReader _reader = _cmd.ExecuteReader())
                    {
                        if (_reader.HasRows)
                        {
                            while (_reader.Read())
                            {
                                _cls.contadorb = Convert.ToInt32(_reader["CONTADORB"]);
                                _cls.contadorn = Convert.ToInt32(_reader["CONTADORN"]);
                                _cls.posicion = Convert.ToInt32(_reader["POSICION"]);
                                _cls.serie = _reader["SERIE"].ToString();
                                _cls.tipodoc = Convert.ToInt32(_reader["TIPODOC"]);
                            }
                        }
                    }
                }

                return _cls;
            }
            catch(Exception ex)
            {
                throw new Exception("GetSeriesDoc. " +ex.Message);
            }
        }

        public static SeriesDoc GetSeriesDoc(string _serie, int _tipodoc, SqlConnection _con, SqlTransaction _transac)
        {
            string _sql = @"select TIPODOC, SERIE, POSICION, CONTADORB, CONTADORN from SERIESDOC where SERIE = @Serie and TIPODOC = @TipoDoc";

            SeriesDoc _cls = new SeriesDoc();

            try
            {
                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;
                    _cmd.Transaction = _transac;

                    _cmd.Parameters.AddWithValue("@Serie", _serie);
                    _cmd.Parameters.AddWithValue("@TipoDoc", _tipodoc);

                    using (SqlDataReader _reader = _cmd.ExecuteReader())
                    {
                        if (_reader.HasRows)
                        {
                            while (_reader.Read())
                            {
                                _cls.contadorb = Convert.ToInt32(_reader["CONTADORB"]);
                                _cls.contadorn = Convert.ToInt32(_reader["CONTADORN"]);
                                _cls.posicion = Convert.ToInt32(_reader["POSICION"]);
                                _cls.serie = _reader["SERIE"].ToString();
                                _cls.tipodoc = Convert.ToInt32(_reader["TIPODOC"]);
                            }
                        }
                    }
                }

                return _cls;
            }
            catch (Exception ex)
            {
                throw new Exception("GetSeriesDoc. Error: " + ex.Message);
            }
        }

        public static bool UpdateSeriesDoc(SeriesDoc _serieDoc, SqlConnection _con)
        {
            string _sql = @"UPDATE SERIESDOC SET CONTADORB = @ContaB, CONTADORN = @ContaN WHERE TIPODOC = @TipoDoc and SERIE = @Serie";

            bool _rta = false;

            try
            {
                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;

                    _cmd.Parameters.AddWithValue("@ContaB", _serieDoc.contadorb);
                    _cmd.Parameters.AddWithValue("@ContaN", _serieDoc.contadorn);
                    _cmd.Parameters.AddWithValue("@TipoDoc", _serieDoc.tipodoc);
                    _cmd.Parameters.AddWithValue("@Serie", _serieDoc.serie);

                    int _Q = _cmd.ExecuteNonQuery();
                    if (_Q > 0)
                        _rta = true;
                    else
                        _rta = false;

                }

                return _rta;
            }
            catch (Exception ex)
            {
                throw new Exception("UpdateSeriesDoc. " + ex.Message);
            }
        }

        public static void UpdateSeriesDoc(SeriesDoc _serieDoc, SqlConnection _con, SqlTransaction _tran)
        {
            string _sql = @"UPDATE SERIESDOC SET CONTADORB = @ContaB, CONTADORN = @ContaN WHERE TIPODOC = @TipoDoc and SERIE = @Serie";

            try
            {
                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;
                    _cmd.Transaction = _tran;

                    _cmd.Parameters.AddWithValue("@ContaB", _serieDoc.contadorb);
                    _cmd.Parameters.AddWithValue("@ContaN", _serieDoc.contadorn);
                    _cmd.Parameters.AddWithValue("@TipoDoc", _serieDoc.tipodoc);
                    _cmd.Parameters.AddWithValue("@Serie", _serieDoc.serie);

                    int _Q = _cmd.ExecuteNonQuery();
                }

            }
            catch (Exception ex)
            {
                //throw new Exception("UpdateSeriesDoc. " + ex.Message);
                throw new Exception("Se produjo un error al actualizar el contador de las Series del documento." + Environment.NewLine +
                    "Por favor comuniquese con ICG Agentina, para actualizar la tabla SERIESDOC");
            }
        }
    }
}
