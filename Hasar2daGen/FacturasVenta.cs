﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hasar2daGen
{
    public class FacturasVenta
    {
        public static bool InsertCancelacionFacturasVenta(string _numserie, int _numfactura, int _numfacturaNew, string _n, SqlConnection _con, SqlTransaction _tran)
        {
            bool _rta = false;
            string _sql = @"INSERT FACTURASVENTA
                (NUMSERIE, NUMFACTURA, N, CODCLIENTE, FECHA, HORA, PORTESPAG, CODMONEDA, FACTORMONEDA, IVAINCLUIDO, TRASPASADA, CODVENDEDOR, VIENEDEFO, TIPODOC, IDESTADO, FECHAMODIFICADO, Z, CAJA, FECHACREACION, IDMOTIVODTO, NUMIMPRESIONES, REGIMFACT, MMFIJADO, ENVIADOSII, MODIFIEDTOTALES)
                SELECT NUMSERIE, @numfacturaNew, N, CODCLIENTE, FECHA, HORA, PORTESPAG, CODMONEDA, FACTORMONEDA, IVAINCLUIDO, TRASPASADA, CODVENDEDOR, VIENEDEFO, TIPODOC, IDESTADO, FECHAMODIFICADO, Z, CAJA, FECHACREACION, IDMOTIVODTO, NUMIMPRESIONES, REGIMFACT, MMFIJADO, ENVIADOSII, MODIFIEDTOTALES
                FROM FACTURASVENTA
                WHERE NUMSERIE = @NumSerie and NUMFACTURA = @NumFactura and N = @N";
            try
            {
                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;
                    _cmd.Transaction = _tran;

                    _cmd.Parameters.AddWithValue("@numfacturaNew", _numfacturaNew);
                    _cmd.Parameters.AddWithValue("@NumSerie", _numserie);
                    _cmd.Parameters.AddWithValue("@NumFactura", _numfactura);
                    _cmd.Parameters.AddWithValue("@N", _n);

                    int _Q = _cmd.ExecuteNonQuery();

                    if (_Q > 0)
                        _rta = true;
                    else
                        _rta = false;
                }

                return _rta;
            }
            catch(Exception ex)
            {
                throw new Exception("" + ex.Message);
            }
        }

        public static void InsertCancelacionFacturasVentaNew(string _numserie, int _numfactura, int _numfacturaNew, string _n, SqlConnection _con, SqlTransaction _tran)
        {
            bool _rta = false;
            string _sql = @"INSERT FACTURASVENTA
                (NUMSERIE, NUMFACTURA, N, CODCLIENTE, FECHA, HORA, PORTESPAG, CODMONEDA, FACTORMONEDA, IVAINCLUIDO, TRASPASADA, CODVENDEDOR, VIENEDEFO, TIPODOC, IDESTADO, FECHAMODIFICADO, Z, CAJA, FECHACREACION, IDMOTIVODTO, NUMIMPRESIONES, REGIMFACT, MMFIJADO, ENVIADOSII, MODIFIEDTOTALES)
                SELECT NUMSERIE, @numfacturaNew, N, CODCLIENTE, FECHA, HORA, PORTESPAG, CODMONEDA, FACTORMONEDA, IVAINCLUIDO, TRASPASADA, CODVENDEDOR, VIENEDEFO, TIPODOC, IDESTADO, FECHAMODIFICADO, Z, CAJA, FECHACREACION, IDMOTIVODTO, NUMIMPRESIONES, REGIMFACT, MMFIJADO, ENVIADOSII, MODIFIEDTOTALES
                FROM FACTURASVENTA
                WHERE NUMSERIE = @NumSerie and NUMFACTURA = @NumFactura and N = @N";
            try
            {
                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;
                    _cmd.Transaction = _tran;

                    _cmd.Parameters.AddWithValue("@numfacturaNew", _numfacturaNew);
                    _cmd.Parameters.AddWithValue("@NumSerie", _numserie);
                    _cmd.Parameters.AddWithValue("@NumFactura", _numfactura);
                    _cmd.Parameters.AddWithValue("@N", _n);

                    int _Q = _cmd.ExecuteNonQuery();

                }

            }
            catch (Exception ex)
            {
                throw new Exception("InsertCancelacionFacturasVentaNew. Error: " + ex.Message);
            }
        }

        public static bool UpdateDocumentoAsociadoFacturasVenta(string _numserie, int _numfactura, string _n, string _docAsociado, SqlConnection _con)
        {
            bool _rta = false;
            string _sql = @"UPDATE FACTURASVENTA SET CLEANCASHCONTROLCODE2 = @DocAsoc WHERE NUMSERIE = @NumSerie and NUMFACTURA = @NumFactura and N = @N";
            try
            {
                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;
                    //_cmd.Transaction = _tran;

                    _cmd.Parameters.AddWithValue("@DocAsoc", _docAsociado);
                    _cmd.Parameters.AddWithValue("@NumSerie", _numserie);
                    _cmd.Parameters.AddWithValue("@NumFactura", _numfactura);
                    _cmd.Parameters.AddWithValue("@N", _n);

                    int _Q = _cmd.ExecuteNonQuery();

                    if (_Q > 0)
                        _rta = true;
                    else
                        _rta = false;
                }

                return _rta;
            }
            catch (Exception ex)
            {
                throw new Exception("UpdateDocumentoAsociadoFacturasVenta" + ex.Message);
            }
        }

        public static void UpdateDocumentoAsociadoFacturasVentaNew(string _numserie, int _numfactura, string _n, string _docAsociado, SqlConnection _con, SqlTransaction _transac)
        {
            string _sql = @"UPDATE FACTURASVENTA SET CLEANCASHCONTROLCODE2 = @DocAsoc WHERE NUMSERIE = @NumSerie and NUMFACTURA = @NumFactura and N = @N";
            try
            {
                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;
                    _cmd.Transaction = _transac;

                    _cmd.Parameters.AddWithValue("@DocAsoc", _docAsociado);
                    _cmd.Parameters.AddWithValue("@NumSerie", _numserie);
                    _cmd.Parameters.AddWithValue("@NumFactura", _numfactura);
                    _cmd.Parameters.AddWithValue("@N", _n);

                    int _Q = _cmd.ExecuteNonQuery();

                }
            }
            catch (Exception ex)
            {
                throw new Exception("UpdateDocumentoAsociadoFacturasVenta" + ex.Message);
            }
        }
    }
}
