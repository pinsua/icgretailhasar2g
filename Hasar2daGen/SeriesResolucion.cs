﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Hasar2daGen
{
    public class SeriesResolucion
    {
        public string serieresol { get; set; }
        public string numresol { get; set; }
        public DateTime fecha { get; set; }
        public int numeroinicial { get; set; }
        public int numerofinal { get; set; }
        public int activo { get; set; }
        public int contador { get; set; }
        public DateTime fechaingreso { get; set; }
        public DateTime fechavencimiento { get; set; }

        public static SeriesResolucion GetContador(string _ptovta, string _tipocompronate, SqlConnection _sqlconnection)
        {
            string _sql = @"SELECT SERIERESOL, NUMRESOL, FECHA, NUMEROINICIAL, NUMEROFINAL, ACTIVO, CONTADOR, FECHAINGRESO, FECHAVENCIMIENTO
                FROM SERIESRESOLUCION WHERE SERIERESOL = @serieresol and NUMRESOL = @numresol";

            SeriesResolucion _cls = new SeriesResolucion();

            try
            {
                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _sqlconnection;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;

                    _cmd.Parameters.AddWithValue("@serieresol", _ptovta);
                    _cmd.Parameters.AddWithValue("@numresol", _tipocompronate);

                    using (SqlDataReader _reader = _cmd.ExecuteReader())
                    {
                        if (_reader.HasRows)
                        {
                            while (_reader.Read())
                            {
                                _cls.activo = Convert.ToInt32(_reader["ACTIVO"]);
                                _cls.contador = Convert.ToInt32(_reader["CONTADOR"]);
                                _cls.fecha = Convert.ToDateTime(_reader["FECHA"]);
                                _cls.fechaingreso = Convert.ToDateTime(_reader["FECHAINGRESO"]);
                                _cls.fechavencimiento = Convert.ToDateTime(_reader["FECHAVENCIMIENTO"]);
                                _cls.numerofinal = Convert.ToInt32(_reader["NUMEROFINAL"]);
                                _cls.numeroinicial = Convert.ToInt32(_reader["NUMEROINICIAL"]);
                                _cls.numresol = _reader["NUMRESOL"].ToString();
                                _cls.serieresol = _reader["SERIERESOL"].ToString();
                            }
                        }
                    }
                }

                return _cls;
            }
            catch (Exception ex)
            {
                //throw new Exception("GetContador. " + ex.Message);
                throw new Exception("Se produjo un error al buscar el contador del Talonario Manual." + Environment.NewLine +
                    "Por favor comuniquese con ICG Agentina, para actualizar la tabla SERIESRESOLUCION");
            }
        }

        public static bool UpdateContadorSerieResolucion(SeriesResolucion _serieResol, SqlConnection _con)
        {
            string _sql = @"UPDATE SERIESRESOLUCION SET CONTADOR = @newcontador WHERE SERIERESOL = @serieresol and NUMRESOL = @numresol";

            bool _rta = false;

            try
            {
                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;

                    _cmd.Parameters.AddWithValue("@newcontador", _serieResol.contador);
                    _cmd.Parameters.AddWithValue("@serieresol", _serieResol.serieresol);
                    _cmd.Parameters.AddWithValue("@numresol", _serieResol.numresol);
                    
                    int _Q = _cmd.ExecuteNonQuery();
                    if (_Q > 0)
                        _rta = true;
                    else
                        _rta = false;

                }

                return _rta;
            }
            catch (Exception ex)
            {
                //throw new Exception("UpdateContadorSerieResolucion. " + ex.Message);
                throw new Exception("Se produjo un error al actualizar el contador del Talonario Manual." + Environment.NewLine +
                    "Por favor comuniquese con ICG Agentina, para actualizar la tabla SERIESRESOLUCION");
            }
        }
    }
}
