﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static Hasar2daGen.SiTefService.Common;

namespace Hasar2daGen
{
    public class SiTefService
    {
        public class Common
        {
            /// <summary>
            /// Clase que contiene la información para SiTef.
            /// </summary>
            public class InfoSitef
            {
                /// <summary>
                /// Id de la terminal
                /// </summary>
                public string sitefIdTerminal { get; set; }
                /// <summary>
                /// Id de la tienda
                /// </summary>
                public string sitefIdTienda { get; set; }
                /// <summary>
                /// Cuit de la tienda
                /// </summary>
                public string sitefCuit { get; set; }
                /// <summary>
                /// Cuit del ISV
                /// </summary>
                public string sitefCuitIsv { get; set; }
                /// <summary>
                /// Path donde se dejaran las invoice a enviar a la pasarela.
                /// </summary>
                public string pathSendInvoice { get; set; }
                /// <summary>
                /// Indica si el cliente USA o NO un clover.
                /// </summary>
                public bool usaClover { get; set; }
            }
            /// <summary>
            /// Clase del Json a enviar a IRSA con la venta
            /// </summary>
            public class InvoiceJson
            {
                public SEVersion SEVersion { get; set; }
                public Merchant Merchant { get; set; }
                public string saleTicketType { get; set; }
                public string merchantId { get; set; }
                public string Terminal { get; set; }
                public List<Invoice> Invoice { get; set; }
            }
            /// <summary>
            /// Clase que define la versión del JSON
            /// </summary>
            public class SEVersion
            {
                /// <summary>
                /// Numero de versión de SiTef
                /// </summary>
                public string Number { get; set; }
            }
            /// <summary>
            /// Clase que define el vendedor
            /// </summary>
            public class Merchant
            {
                public string SiTefCode { get; set; }
                public string DocumentMerchant { get; set; }
                public string DocumentMerchantType { get; set; }
            }
            /// <summary>
            /// Clase de los datos de la venta
            /// </summary>
            public class Invoice
            {
                public InvoiceNumber InvoiceNumber { get; set; }
                /// <summary>
                /// Fecha del ticket en formato AAAAMMDD
                /// </summary>
                public string Date { get; set; }
                /// <summary>
                /// Hora del Ticket en formato HHMMSS
                /// </summary>
                public string Time { get; set; }
                /// <summary>
                /// Lista de precios.
                /// </summary>
                public string PriceList { get; set; }
                /// <summary>
                /// Datos del cliente. DEPRECIADO.
                /// </summary>
                public Customer Customer { get; set; }
                /// <summary>
                /// Lista de productos del ticket.
                /// </summary>
                public List<Product> Products { get; set; }
                /// <summary>
                /// Lista de formas de pago del ticket.
                /// </summary>
                public List<PaymentValue> PaymentValues { get; set; }
                /// <summary>
                /// Subtotal del ticket.
                /// </summary>
                public string SubTotal { get; set; }
                /// <summary>
                /// Descuento comercial del ticket en porcentaje.
                /// </summary>
                public string DiscountPercentage { get; set; }
                /// <summary>
                /// Descuento comercial del ticket en monto.
                /// </summary>
                public string DiscountAmount { get; set; }
                /// <summary>
                /// Lista de IVAS del ticket.
                /// </summary>
                public List<Iva> IVA { get; set; }
                /// <summary>
                /// Otro impuestos.
                /// </summary>
                public string OtherTaxes { get; set; }
                /// <summary>
                /// Monto total del ticket.
                /// </summary>
                public string AmountTotal { get; set; }
            }
            /// <summary>
            /// Clase que define el tipo de venta.
            /// </summary>
            public class InvoiceNumber
            {
                public string Type { get; set; }
                public string FiscalPointSaleNumber { get; set; }
                public string Number { get; set; }
                public string InvoiceDeliveryMethod { get; set; }
                public string documentType { get; set; }
            }
            /// <summary>
            /// Clase del Cliente
            /// </summary>
            public class Customer
            {
                public string Name { get; set; }
                public string FiscalSituation { get; set; }
                public string Number { get; set; }
                public string DocumentType { get; set; }
            }
            /// <summary>
            /// Clase del producto de la venta.
            /// </summary>
            public class Product
            {
                public string ProductSKU { get; set; }
                public string ProductDescription { get; set; }
                public string Quantity { get; set; }
                public string Price { get; set; }
                public string DiscountPercentage { get; set; }
                public string DiscountAmount { get; set; }
                public string Amount { get; set; }
            }
            /// <summary>
            /// Clase de tipos de Pago.
            /// </summary>
            public class PaymentValue
            {
                public string PaymentCode { get; set; }
                public string PaymentDescription { get; set; }
                public string CardBrand { get; set; }
                public string Date { get; set; }
                public string Time { get; set; }
                public string Type { get; set; }
                public string Symbol { get; set; }
                public string Amount { get; set; }
                public string NSUSitef { get; set; }
                public string PaymentID { get; set; }
            }
            /// <summary>
            /// Clase de IVA
            /// </summary>
            public class Iva
            {
                public string Percentage { get; set; }
                public string Amount { get; set; }
            }
        }

        public class SiTefServiceRetail
        {
            /// <summary>
            /// Metodo que hace el envio de una venta. SQL12
            /// </summary>
            /// <param name="pSerie"></param>
            /// <param name="pNumero"></param>
            /// <param name="pN"></param>
            /// <param name="pIdTienda"></param>
            /// <param name="pIdTerminal"></param>
            /// <param name="pCUIT"></param>
            /// <param name="pCuitIsv"></param>
            /// <param name="pPathSend"></param>
            /// <param name="pesClover"></param>
            /// <param name="pConn"></param>
            /// <returns></returns>
            public static bool SendNormalInvoiceSql(string pSerie, string pNumero, string pN, string pIdTienda,
                string pIdTerminal, string pCUIT, string pCuitIsv, string pPathSend, bool pesClover, SqlConnection pConn)
            {
                string _delivery = "ELECTRONIC_INVOICE";
                string _letraFactura;
                string _tipoDocumento;
                int nrofiscal;
                string saleTicketType;


                string _cabecera = GetCabecera(pSerie, pNumero, pN, "N", pCuitIsv, pesClover, pConn,
                    out nrofiscal, out _letraFactura, out _tipoDocumento);
                string _fileName = "invoice_" + pSerie + pNumero + pN;

                if (_tipoDocumento == "EFACTURA")
                    saleTicketType = "SALE";
                else
                    saleTicketType = "RETURN";

                string _drta = GetJsonInvoice(nrofiscal, pIdTienda, pCUIT, "CUIT", saleTicketType, pCuitIsv, pIdTerminal, _letraFactura,
                    _delivery, _tipoDocumento, pSerie, pNumero, pN, pConn);

                pPathSend = pPathSend + _fileName + ".txt";

                _drta = Regex.Replace(_drta, @"\t|\n|\r", "");
                _drta = Regex.Replace(_drta, "(\"(?:[^\"\\\\]|\\\\.)*\")|\\s+", "$1");

                string envio = _cabecera + "ANF" + _drta.Length.ToString().PadLeft(5, '0') + _drta;
                envio = Regex.Replace(envio, @"\t|\n|\r", "");

                using (StreamWriter writer = new StreamWriter(pPathSend))
                {
                    writer.WriteLine(envio);
                }

                return true;
            }
            private static string GetCabecera(string serie, string numero, string n, string tipoOperacion, string cuitIsv, bool esClover, SqlConnection conection,
                    out int numeroFiscal, out string letraFactura, out string tipoDocumento)
            {
                //string _sql = @"SELECT CONVERT(varchar(8), TIQUETSCAB.FECHA,112) as FECHA, 
                //    replace(convert(varchar, TIQUETSCAB.HORAFIN, 108),':','') as hora, 
                //    TIQUETSCAB.SERIEFISCAL, TIQUETSCAB.NUMEROFISCAL, TIQUETSCAB.SERIEFISCAL2, 
                //    replace(LTRIM(RTRIM(str(Round(ABS(TIQUETSCAB.TOTALNETO), 2), 10, 2))), '.', '') as total, 
                //    replace(LTRIM(RTRIM(str(Round(ABS(TIQUETSPAG.IMPORTE), 2), 10, 2))), '.', '') as totalpag, 
                //    FORMASPAGO.DESCRIPCION as tipopago, TIQUETSPAG.TITULARTARJETA, FORMASPAGO.METALICO, FORMASPAGO.TEXTOIMP, TIQUETSPAG.NUMLINEA 
                //    FROM TIQUETSCAB Left join TIQUETSPAG on TIQUETSCAB.SERIE = TIQUETSPAG.SERIE and TIQUETSCAB.NUMERO = TIQUETSPAG.NUMERO 
                //    and TIQUETSCAB.N = TIQUETSPAG.N Left join FORMASPAGO on TIQUETSPAG.CODFORMAPAGO = FORMASPAGO.CODFORMAPAGO 
                //    WHERE TIQUETSCAB.SERIE = @serie and TIQUETSCAB.NUMERO = @numero and TIQUETSCAB.N = @n";

                string _sql = @"SELECT CONVERT(varchar(8), FACTURASVENTA.FECHA,112) as FECHA, 
                    replace(convert(varchar, FACTURASVENTA.HORA, 108),':','') as hora, 
                    FACTURASVENTASERIESRESOL.SERIEFISCAL1, FACTURASVENTASERIESRESOL.NUMEROFISCAL, FACTURASVENTASERIESRESOL.SERIEFISCAL2, 
                    replace(LTRIM(RTRIM(str(Round(ABS(FACTURASVENTA.TOTALNETO), 2), 10, 2))), '.', '') as total, 
                    replace(LTRIM(RTRIM(str(Round(ABS(TESORERIA.IMPORTE), 2), 10, 2))), '.', '') as totalpag, 
                    TIPOSPAGO.DESCRIPCION as tipopago, '' as TITULARTARJETA, TIPOSPAGO.METALICO, TIPOSPAGO.MARCASTARJETA, TESORERIA.POSICION,
                    FORMASPAGO.TEXTOIMP, FORMASPAGO.DESCIDCOBRO, FORMASPAGO.MARCASTARJETA
                    FROM FACTURASVENTA
                    inner join FACTURASVENTASERIESRESOL ON FACTURASVENTA.NUMSERIE = FACTURASVENTASERIESRESOL.NUMSERIE and FACTURASVENTA.NUMFACTURA = FACTURASVENTASERIESRESOL.NUMFACTURA and FACTURASVENTA.N = FACTURASVENTASERIESRESOL.N
                    inner join TESORERIA on FACTURASVENTA.NUMSERIE=TESORERIA.SERIE AND  FACTURASVENTA.NUMFACTURA = TESORERIA.NUMERO and FACTURASVENTA.N = TESORERIA.N 
                    INNER JOIN TIPOSPAGO ON TESORERIA.CODTIPOPAGO = TIPOSPAGO.CODTIPOPAGO 
                    inner join FORMASPAGO on TESORERIA.codformapago = FORMASPAGO.codformapago
                    WHERE FACTURASVENTA.NUMSERIE = @serie and FACTURASVENTA.NUMFACTURA = @numero and FACTURASVENTA.N = @n";

                string t002 = "";
                string rta = "";
                string nroFac = "";
                string fecha = "";
                string hora = "";
                string tipoVenta = "";
                string tipoOper = "";
                string icgRef = serie + numero + n;
                numeroFiscal = 0;
                letraFactura = "";
                tipoDocumento = "";
                string tipoOpe = "";

                if (conection.State == ConnectionState.Closed)
                    conection.Open();
                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = conection;
                    _cmd.CommandType = CommandType.Text;
                    _cmd.CommandText = _sql;
                    _cmd.Parameters.AddWithValue("@serie", serie);
                    _cmd.Parameters.AddWithValue("@numero", numero);
                    _cmd.Parameters.AddWithValue("@n", n);

                    using (SqlDataReader reader = _cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                //Agrupador
                                string _agrup = "";
                                //Vemos si es pago en Efectivo o no
                                bool esEfectivo = reader["METALICO"].ToString().ToUpper() == "T" ? true : false;
                                //Recuperamos el valor de TextImp
                                string textImp = reader["MARCASTARJETA"] == DBNull.Value ? "" : reader["MARCASTARJETA"].ToString();
                                bool esOtro = textImp == "O" ? true : false;
                                //Ahora tenemos que poner el TAG 002. MerchantID
                                if (String.IsNullOrEmpty(t002))
                                {
                                    t002 = "002" + cuitIsv.Length.ToString().PadLeft(5, '0') + cuitIsv;
                                    _agrup = t002;
                                }

                                //Nro de Factura. TAG 004. Solo cuando no es clover. Ahora va siempre.
                                if (String.IsNullOrEmpty(nroFac))
                                {
                                    string pa1 = reader["NUMEROFISCAL"].ToString().PadLeft(8, '0');
                                    string pa2 = reader["SERIEFISCAL1"].ToString().PadLeft(5, '0');
                                    string pa3 = pa2 + pa1;
                                    numeroFiscal = Convert.ToInt32(reader["NUMEROFISCAL"]);
                                    nroFac = "004" + pa3.Length.ToString().PadLeft(5, '0') + pa3;
                                    _agrup = _agrup + nroFac;
                                }
                                if (String.IsNullOrEmpty(fecha))
                                {
                                    string p1 = reader["FECHA"].ToString();
                                    fecha = "005" + p1.Length.ToString().PadLeft(5, '0') + p1;
                                    _agrup = _agrup + fecha;
                                }
                                //Hora. TAG 006. Solo cuando no es Clover. Ahora va siempre.
                                if (String.IsNullOrEmpty(hora))
                                {
                                    string p1 = reader["hora"].ToString();
                                    hora = "006" + p1.Length.ToString().PadLeft(5, '0') + p1;
                                    _agrup = _agrup + hora;
                                }
                                //Tipo de Venta. Tag 007
                                if (!esClover && !esEfectivo)
                                {
                                    if (String.IsNullOrEmpty(tipoVenta))
                                    {
                                        string p1 = reader["DESCIDCOBRO"].ToString();
                                        if (p1.ToUpper().Contains("CREDITO"))
                                        {
                                            string t007 = "C";
                                            tipoVenta = "007" + t007.Length.ToString().PadLeft(5, '0') + t007;
                                            _agrup = _agrup + tipoVenta;
                                        }
                                        if (p1.ToUpper().Contains("DEBITO"))
                                        {
                                            string t007 = "D";
                                            tipoVenta = "007" + t007.Length.ToString().PadLeft(5, '0') + t007;
                                            _agrup = _agrup + tipoVenta;
                                        }
                                        tipoVenta = "";
                                    }
                                }
                                //Tipo de Operación. TAG 009.
                                if (!esClover || esEfectivo)
                                {
                                    if (String.IsNullOrEmpty(tipoOper))
                                    {
                                        switch (reader["SERIEFISCAL2"].ToString().Substring(0, 3))
                                        {
                                            case "001": letraFactura = "A"; tipoDocumento = "EFACTURA"; tipoOpe = "N"; break;
                                            case "002": case "003": letraFactura = "A"; tipoDocumento = "NC_EFACTURA"; tipoOpe = "A"; break;
                                            case "006": letraFactura = "B"; tipoDocumento = "EFACTURA"; tipoOpe = "N"; break;
                                            case "007": case "008": letraFactura = "B"; tipoDocumento = "NC_EFACTURA"; tipoOpe = "A"; break;
                                            case "011": letraFactura = "C"; tipoDocumento = "EFACTURA"; tipoOpe = "N"; break;
                                            case "012": case "013": letraFactura = "C"; tipoDocumento = "NC_EFACTURA"; tipoOpe = "A"; break;
                                        }
                                        tipoOper = "00900001" + tipoOpe;
                                        _agrup = _agrup + tipoOper;
                                    }
                                }
                                else
                                {
                                    switch (reader["SERIEFISCAL2"].ToString().Substring(0, 3))
                                    {
                                        case "001": letraFactura = "A"; tipoDocumento = "EFACTURA"; tipoOpe = "N"; break;
                                        case "002": case "003": letraFactura = "A"; tipoDocumento = "NC_EFACTURA"; tipoOpe = "A"; break;
                                        case "006": letraFactura = "B"; tipoDocumento = "EFACTURA"; tipoOpe = "N"; break;
                                        case "007": case "008": letraFactura = "B"; tipoDocumento = "NC_EFACTURA"; tipoOpe = "A"; break;
                                        case "011": letraFactura = "C"; tipoDocumento = "EFACTURA"; tipoOpe = "N"; break;
                                        case "012": case "013": letraFactura = "C"; tipoDocumento = "NC_EFACTURA"; tipoOpe = "A"; break;
                                    }
                                }
                                //Total de cada pago. CUANDO NO ES CLOVER. TAG 013
                                if (!esClover || esEfectivo)
                                {
                                    string pago = reader["totalpag"].ToString();
                                    _agrup = _agrup + "013" + pago.Length.ToString().PadLeft(5, '0') + pago;
                                }
                                //Tipo de pago. Solo se envia si NO ES con CLOVER. TAG 014
                                string p2 = "";
                                if (!esClover || esEfectivo)
                                {
                                    if (esEfectivo)
                                        p2 = "E";
                                    else
                                    {
                                        if (esOtro)
                                            p2 = "O";
                                        else
                                            p2 = "T";
                                    }
                                    _agrup = _agrup + "014" + p2.Length.ToString().PadLeft(5, '0') + p2;
                                }
                                if (p2 == "T")
                                {
                                    //TAG 15. Tipo de Tarjeta.
                                    string tag15 = reader["TEXTOIMP"] == null ? "" : reader["TEXTOIMP"].ToString();
                                    if (!String.IsNullOrEmpty(tag15))
                                    {
                                        _agrup = _agrup + "015" + tag15.Length.ToString().PadLeft(5, '0') + tag15;
                                    }
                                    //TAG20. Cantidad de cuotas. Solo se informa cuando es Tarjeta.
                                    string tag20 = "1";
                                    _agrup = _agrup + "020" + tag20.Length.ToString().PadLeft(5, '0') + tag20;
                                    //TAG21. Dispositivo de Entrada. Solo se informa cuando es Tarjeta.
                                    string tag21 = "2";
                                    _agrup = _agrup + "021" + tag21.Length.ToString().PadLeft(5, '0') + tag21;
                                    //TAG22. HOST. Solo se informa cuando es Tarjeta.
                                    string tag22 = "1";
                                    _agrup = _agrup + "022" + tag22.Length.ToString().PadLeft(5, '0') + tag22;
                                    //TAG23. Seis primeros numeros de la tarjeta. Solo se informa cuando es tarjeta.
                                    string tag23 = "000000";
                                    //switch (tag15)
                                    //{
                                    //    //case "AM": { tag23 = "371595"; break; }
                                    //    case "AM": { tag23 = "371"; break; }
                                    //    case "EL": { tag23 = "451766"; break; }
                                    //    default: { tag23 = "000000"; break; }
                                    //}
                                    _agrup = _agrup + "023" + tag23.Length.ToString().PadLeft(5, '0') + tag23;
                                    //TAG24. Codigo de autorización.
                                    string tag24 = DateTime.Now.Ticks.ToString();
                                    string stag24 = tag24.Length.ToString().PadLeft(5, '0');
                                    _agrup = _agrup + "024" + stag24 + tag24;
                                }
                                //Referencia. SOLO VA CUANDO NO ES CLOVER. TAG 025.
                                string numlin = reader["POSICION"].ToString();
                                string titular2 = reader["TITULARTARJETA"] == null ? "" : reader["TITULARTARJETA"].ToString();
                                if (String.IsNullOrEmpty(titular2))
                                {
                                    string _ref = "H" + icgRef + numlin.Trim();
                                    _agrup = _agrup + "025" + _ref.Length.ToString().PadLeft(5, '0') + _ref;
                                }
                                else
                                {
                                    string p1 = "H" + titular2.Split('|')[1].ToString();
                                    if (!String.IsNullOrEmpty(p1))
                                    {
                                        _agrup = _agrup + "025" + p1.Length.ToString().PadLeft(5, '0') + p1;
                                    }
                                }
                                rta = rta + _agrup;
                            }
                        }
                    }
                }

                return rta;
            }

            /// <summary>
            /// Metodo que genera el Json con los datos de la venta.
            /// </summary>
            /// <param name="_nroFac"></param>
            /// <param name="_sitefCode">Codigo del Sitef</param>
            /// <param name="_merchantDoc">Documento del vendedor</param>
            /// <param name="_merchantDocType">Tipo de documento del vendedor</param>
            /// <param name="saleTicketType">Tipo de tiquet de venta</param>
            /// <param name="merchantId">Id del vendedor</param>
            /// <param name="terminalId">Id de la terminal</param>
            /// <param name="letraFac">Letra de la venta</param>
            /// <param name="invoiceDelivery"></param>
            /// <param name="invoicedocType"></param>
            /// <param name="serie"></param>
            /// <param name="numero"></param>
            /// <param name="n"></param>
            /// <param name="connection">Conexion SQL</param>
            /// <returns>Json en string</returns>
            private static string GetJsonInvoice(int _nroFac, string _sitefCode, string _merchantDoc, string _merchantDocType, string saleTicketType,
                string merchantId, string terminalId, string letraFac, string invoiceDelivery, string invoicedocType,
                string serie, string numero, string n, SqlConnection connection)
            {
                string json;

                string _sqlInvoiceCustomer = @"SELECT REPLACE(STR(FACTURASVENTASERIESRESOL.SERIEFISCAL1, 5), SPACE(1), '0') as [InvoiceNumber.FiscalPointSaleNumber], 
                    REPLACE(STR(FACTURASVENTASERIESRESOL.NUMEROFISCAL, 8), SPACE(1), '0') as [InvoiceNumber.Number], 
                    CONVERT(varchar(8), FACTURASVENTA.FECHA, 112) as [Date], 
                    REPLACE(convert(varchar, FACTURASVENTA.HORA, 108), ':', '') as [Time], 
                    CASE WHEN FACTURASVENTA.CODCLIENTE = 0 THEN 'Consumidor Final' ELSE clientes.nombrecliente END as 'Customer.Name', 
                    CASE WHEN FACTURASVENTA.CODCLIENTE = 0 THEN 'Consumidor Final' ELSE REGIMENFACTURACION.DESCRIPCION END as 'Customer.FiscalSituation', 
                    CASE WHEN FACTURASVENTA.CODCLIENTE = 0 THEN '0' ELSE clientes.cif END as 'Customer.Number', 
                    CASE WHEN FACTURASVENTA.CODCLIENTE = 0 THEN 'DNI' else 'DNI' END as 'Customer.DocumentType', 
                    LTRIM(RTRIM(str(Round(isnull(ABS(FACTURASVENTA.TOTALNETO), 0), 2), 10, 2))) as [AmountTotal], 
                    LTRIM(RTRIM(str(Round(0, 2), 10, 2))) as [OtherTaxes]
                    FROM FACTURASVENTA inner join FACTURASVENTASERIESRESOL on FACTURASVENTA.NUMSERIE = FACTURASVENTASERIESRESOL.NUMSERIE and 
                    FACTURASVENTA.NUMFACTURA = FACTURASVENTASERIESRESOL.NUMFACTURA and FACTURASVENTA.N = FACTURASVENTASERIESRESOL.N
                    LEFT JOIN CLIENTES ON FACTURASVENTA.CODCLIENTE = CLIENTES.CODCLIENTE
                    LEFT JOIN REGIMENFACTURACION ON CLIENTES.REGIMFACT = REGIMENFACTURACION.CODREGIMEN
                    WHERE FACTURASVENTA.NUMSERIE = @serie and FACTURASVENTA.NUMFACTURA = @numero and FACTURASVENTA.N = @n";

                string _sqlProducts = @"SELECT LTRIM(RTRIM(STR(ALBVENTALIN.CODARTICULO))) as 'ProductSKU', 
                    ALBVENTALIN.DESCRIPCION as 'ProductDescription', 
                    LTRIM(RTRIM(str(Cast(ALBVENTALIN.UNIDADESTOTAL as decimal(10, 2)), 10, 2))) as 'Quantity', 
                    LTRIM(RTRIM(str(Round(ABS(ALBVENTALIN.PRECIOIVA), 2), 10, 2))) as 'Price', 
                    LTRIM(RTRIM(str(round(ABS(ALBVENTALIN.DTO), 2), 10, 2))) as 'DiscountPercentage', 
                    LTRIM(RTRIM(str(round(ABS(ALBVENTALIN.PRECIOIVA) * ABS(ALBVENTALIN.DTO) / 100, 2), 10, 2))) as  'DiscountAmount', 
                    LTRIM(RTRIM(str(Round(ABS(ALBVENTALIN.PRECIOIVA) * ALBVENTALIN.UNIDADESTOTAL, 2) - round(ABS(ALBVENTALIN.PRECIOIVA) * ABS(ALBVENTALIN.DTO) / 100, 2), 10, 2))) as 'Amount' 
                    FROM FACTURASVENTA inner join ALBVENTACAB on FACTURASVENTA.NUMSERIE = ALBVENTACAB.NUMSERIEFAC and
                    FACTURASVENTA.NUMFACTURA = ALBVENTACAB.NUMFAC and FACTURASVENTA.N = ALBVENTACAB.NFAC
                    inner join ALBVENTALIN ON ALBVENTACAB.NUMSERIE = ALBVENTALIN.NUMSERIE and ALBVENTACAB.NUMALBARAN = ALBVENTALIN.NUMALBARAN and ALBVENTACAB.N = ALBVENTALIN.N
                    WHERE FACTURASVENTA.NUMSERIE = @serie and FACTURASVENTA.NUMFACTURA = @numero and FACTURASVENTA.N = @n";

                string _sqlPayments = @"SELECT CASE WHEN FORMASPAGO.TEXTOIMP = 'E' THEN 'P' ELSE FORMASPAGO.TEXTOIMP + '1' END as 'PaymentCode', 
                    CASE WHEN FORMASPAGO.TEXTOIMP = 'E' THEN 'Pesos' ELSE LTRIM(RTRIM(FORMASPAGO.DESCRIPCION)) END AS 'PaymentDescription', 
                    CASE WHEN FORMASPAGO.TEXTOIMP = 'E' THEN 'P' ELSE FORMASPAGO.TEXTOIMP END as 'CardBrand', 
                    FORMASPAGO.DESCIDCOBRO AS 'Type',
                    '$' as 'Symbol',
                    LTRIM(RTRIM(str(Round(ABS(TESORERIA.IMPORTE), 2), 10, 2))) as 'Amount',
                    LTRIM(RTRIM(str(TESORERIA.POSICION))) as 'PaymentID1', 
                    '' as 'PaymentID2'
                    FROM FACTURASVENTA inner join TESORERIA on FACTURASVENTA.NUMSERIE = TESORERIA.SERIE and FACTURASVENTA.NUMFACTURA = TESORERIA.NUMERO and FACTURASVENTA.N = TESORERIA.N
                    INNER JOIN FORMASPAGO ON TESORERIA.CODFORMAPAGO = FORMASPAGO.CODFORMAPAGO 
                    WHERE FACTURASVENTA.NUMSERIE = @serie and FACTURASVENTA.NUMFACTURA = @numero and FACTURASVENTA.N = @n";

                string _sqlIva = @"SELECT LTRIM(RTRIM(str(Round(ABS(IVA),2),10,2))) as 'Percentage', 
                    BASEIMPONIBLE, 
                    LTRIM(RTRIM(str(Round(ABS(TOTIVA),2),10,2))) as 'Amount' 
                    FROM FACTURASVENTATOT 
                    WHERE IVA >= 0 AND CODDTO = -1 AND SERIE = @serie AND NUMERO = @numero AND N = @n";

                string _sqlTotales = @"SELECT LTRIM(RTRIM(str(Round(ABS(FACTURASVENTA.TOTALBRUTO) - ((FACTURASVENTA.TOTDTOCOMERCIAL + FACTURASVENTA.TOTDTOPP)), 2), 10, 2))) as [SubTotal], 
                    LTRIM(RTRIM(str(Round((FACTURASVENTA.DTOCOMERCIAL + FACTURASVENTA.DTOPP), 2), 10, 2))) as [DiscountPercentage], 
                    LTRIM(RTRIM(str(Round((FACTURASVENTA.TOTDTOCOMERCIAL + FACTURASVENTA.TOTDTOPP), 2), 10, 2))) as [DiscountAmount]
                    FROM FACTURASVENTA
                    WHERE FACTURASVENTA.NUMSERIE = @serie and FACTURASVENTA.NUMFACTURA = @numero and FACTURASVENTA.N = @n";

                try
                {
                    InvoiceJson _invoiceJson = new InvoiceJson();
                    SEVersion sEVersion = new SEVersion { Number = "2" };
                    Merchant merchant = new Merchant { DocumentMerchant = _merchantDoc, DocumentMerchantType = _merchantDocType, SiTefCode = _sitefCode };
                    _invoiceJson.SEVersion = sEVersion;
                    _invoiceJson.Merchant = merchant;
                    _invoiceJson.saleTicketType = saleTicketType;
                    _invoiceJson.merchantId = merchantId;
                    _invoiceJson.Terminal = terminalId;
                    Invoice invoice = new Invoice();
                    InvoiceNumber invoiceNumber = new InvoiceNumber();
                    Customer customer = new Customer();

                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    //InvoiceNumber y Customer.
                    using (SqlCommand _cmd = new SqlCommand())
                    {
                        _cmd.Connection = connection;
                        _cmd.CommandType = CommandType.Text;
                        _cmd.CommandText = _sqlInvoiceCustomer;
                        _cmd.Parameters.AddWithValue("@serie", serie);
                        _cmd.Parameters.AddWithValue("@numero", numero);
                        _cmd.Parameters.AddWithValue("@n", n);

                        using (SqlDataReader reader = _cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                if (reader.HasRows)
                                {
                                    invoiceNumber.documentType = invoicedocType;
                                    invoiceNumber.FiscalPointSaleNumber = reader["InvoiceNumber.FiscalPointSaleNumber"].ToString();
                                    invoiceNumber.InvoiceDeliveryMethod = invoiceDelivery;
                                    invoiceNumber.Number = reader["InvoiceNumber.Number"].ToString();
                                    invoiceNumber.Type = letraFac;

                                    invoice.Date = reader["Date"].ToString();
                                    invoice.Time = reader["Time"].ToString();
                                    invoice.PriceList = "Publico";
                                    invoice.AmountTotal = reader["AmountTotal"].ToString();
                                    invoice.OtherTaxes = reader["OtherTaxes"].ToString();

                                    //Se saca porque no se informa mas.
                                    //customer.DocumentType = reader["Customer.DocumentType"].ToString();
                                    //customer.FiscalSituation = reader["Customer.FiscalSituation"].ToString();
                                    //customer.Name = reader["Customer.Name"].ToString();
                                    //customer.Number = reader["Customer.Number"].ToString();
                                    customer.DocumentType = "";
                                    customer.FiscalSituation = "";
                                    customer.Name = "";
                                    customer.Number = "";
                                }
                            }
                        }
                    }
                    //Productos
                    List<Product> products = new List<Product>();
                    using (SqlCommand _cmd = new SqlCommand())
                    {
                        _cmd.Connection = connection;
                        _cmd.CommandType = CommandType.Text;
                        _cmd.CommandText = _sqlProducts;
                        _cmd.Parameters.AddWithValue("@serie", serie);
                        _cmd.Parameters.AddWithValue("@numero", numero);
                        _cmd.Parameters.AddWithValue("@n", n);

                        using (SqlDataReader reader = _cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                if (reader.HasRows)
                                {
                                    Product product = new Product();
                                    if (saleTicketType == "RETURN")
                                        product.Amount = reader["Amount"].ToString().Replace("-", "");
                                    else
                                        product.Amount = reader["Amount"].ToString();
                                    product.DiscountAmount = reader["DiscountAmount"].ToString();
                                    product.DiscountPercentage = reader["DiscountPercentage"].ToString();
                                    product.Price = reader["Price"].ToString();
                                    product.ProductDescription = reader["ProductDescription"].ToString();
                                    product.ProductSKU = reader["ProductSKU"].ToString();
                                    if (saleTicketType == "RETURN")
                                        product.Quantity = reader["Quantity"].ToString().Replace("-", "").Split('.')[0];
                                    else
                                        product.Quantity = reader["Quantity"].ToString().Split('.')[0];

                                    products.Add(product);
                                }
                            }
                        }
                    }
                    //Pagos
                    List<PaymentValue> paymentValues = new List<PaymentValue>();
                    using (SqlCommand _cmd = new SqlCommand())
                    {
                        _cmd.Connection = connection;
                        _cmd.CommandType = CommandType.Text;
                        _cmd.CommandText = _sqlPayments;
                        _cmd.Parameters.AddWithValue("@serie", serie);
                        _cmd.Parameters.AddWithValue("@numero", numero);
                        _cmd.Parameters.AddWithValue("@n", n);

                        using (SqlDataReader reader = _cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                if (reader.HasRows)
                                {
                                    PaymentValue paymentValue = new PaymentValue();
                                    paymentValue.Amount = reader["Amount"].ToString();
                                    paymentValue.CardBrand = reader["CardBrand"].ToString() == "P" ? "" : reader["CardBrand"].ToString();
                                    paymentValue.Date = invoice.Date;
                                    paymentValue.PaymentCode = reader["PaymentCode"].ToString();
                                    paymentValue.PaymentDescription = reader["PaymentDescription"].ToString();
                                    paymentValue.PaymentID = String.IsNullOrEmpty(reader["PaymentID2"].ToString()) ? "H" + serie + numero + n + reader["PaymentID1"].ToString() : "H" + serie + numero + n + reader["PaymentID2"].ToString();
                                    paymentValue.Symbol = reader["Symbol"].ToString();
                                    paymentValue.Time = invoice.Time;
                                    paymentValue.Type = reader["Type"].ToString();
                                    paymentValue.NSUSitef = " ";

                                    paymentValues.Add(paymentValue);
                                }
                            }
                        }
                    }
                    //IVAS
                    List<Iva> ivas = new List<Iva>();
                    using (SqlCommand _cmd = new SqlCommand())
                    {
                        _cmd.Connection = connection;
                        _cmd.CommandType = CommandType.Text;
                        _cmd.CommandText = _sqlIva;
                        _cmd.Parameters.AddWithValue("@serie", serie);
                        _cmd.Parameters.AddWithValue("@numero", numero);
                        _cmd.Parameters.AddWithValue("@n", n);

                        using (SqlDataReader reader = _cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                if (reader.HasRows)
                                {
                                    Iva iva = new Iva();
                                    iva.Amount = reader["Amount"].ToString();
                                    iva.Percentage = reader["Percentage"].ToString();

                                    ivas.Add(iva);
                                }
                            }
                        }
                    }
                    //Totales
                    using (SqlCommand _cmd = new SqlCommand())
                    {
                        _cmd.Connection = connection;
                        _cmd.CommandType = CommandType.Text;
                        _cmd.CommandText = _sqlTotales;
                        _cmd.Parameters.AddWithValue("@serie", serie);
                        _cmd.Parameters.AddWithValue("@numero", numero);
                        _cmd.Parameters.AddWithValue("@n", n);

                        using (SqlDataReader reader = _cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                if (reader.HasRows)
                                {
                                    invoice.SubTotal = reader["SubTotal"].ToString();
                                    //invoice.DiscountPercentage = reader["DiscountPercentage"].ToString();
                                    invoice.DiscountPercentage = "0.00";
                                    //invoice.DiscountAmount = reader["DiscountAmount"].ToString();
                                    invoice.DiscountAmount = "0.00";
                                }
                            }
                        }
                    }

                    invoice.InvoiceNumber = invoiceNumber;
                    invoice.Customer = customer;
                    invoice.Products = products;
                    invoice.PaymentValues = paymentValues;
                    invoice.IVA = ivas;
                    List<Invoice> _lst = new List<Invoice>();
                    _lst.Add(invoice);
                    _invoiceJson.Invoice = _lst;

                    json = JsonConvert.SerializeObject(_invoiceJson, Formatting.Indented);
                }
                catch (Exception ex)
                {

                    json = "";
                }

                return json;
            }
        }
    }
}
