﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Hasar2daGen
{
    public class Items
    {
        public string numserie { get; set; }
        public int numalbaran { get; set; }
        public string n { get; set; }
        public int numlin { get; set; }
        public string _codArticulo { get; set; }
        public string _descripcion { get; set; }
        public decimal _cantidad { get; set; }
        public decimal _precio { get; set; }
        public decimal _iva { get; set; }
        public decimal _descuento { get; set; }
        public string _textoPromo { get; set; }
        public string _abonoDeNumseie { get; set; }
        public int _abonoDeNumAlbaran { get; set; }
        public string _abonoDe_N { get; set; }
        public string _referencia { get; set; }
        public decimal _precioDefecto { get; set; }
        public decimal _dto2 { get; set; }
        public int _tipoImpuesto { get; set; }
        public List<ItemsPromos> Promosiones { get; set; }

        public static List<Items> GetItems(string _numSerie, string _n, int _nroAlbaran, SqlConnection _con)
        {
            //string _sql = @"SELECT ALBVENTALIN.NUMSERIE, ALBVENTALIN.NUMALBARAN, ALBVENTALIN.N, ALBVENTALIN.NUMLIN,
            //        ALBVENTALIN.CODARTICULO, ALBVENTALIN.DESCRIPCION, ALBVENTALIN.UNIDADESTOTAL, ALBVENTALIN.PRECIO, 
            //        ALBVENTALIN.IVA, ALBVENTALIN.DTO, PROMOCIONES.TEXTOIMPRIMIR, ALBVENTALIN.ABONODE_NUMSERIE, ALBVENTALIN.ABONODE_NUMALBARAN, 
            //        ALBVENTALIN.ABONODE_N, ALBVENTALIN.REFERENCIA, ALBVENTALIN.PRECIODEFECTO, ALBVENTALINPROMOCIONES.DTO as DTO2, ALBVENTALIN.TIPOIMPUESTO
            //        FROM ALBVENTALIN left join ALBVENTALINPROMOCIONES ON ALBVENTALIN.NUMSERIE = ALBVENTALINPROMOCIONES.NUMSERIE 
            //        AND ALBVENTALIN.NUMALBARAN = ALBVENTALINPROMOCIONES.NUMALBARAN 
            //        AND ALBVENTALIN.N = ALBVENTALINPROMOCIONES.N 
            //        AND ALBVENTALIN.NUMLIN = ALBVENTALINPROMOCIONES.NUMLIN and ALBVENTALINPROMOCIONES.IMPORTEDTO > 0 
            //        LEFT JOIN PROMOCIONES ON ALBVENTALINPROMOCIONES.IDPROMOCION = PROMOCIONES.IDPROMOCION 
            //        WHERE ALBVENTALIN.NUMSERIE = @Numserie and ALBVENTALIN.N = @N and ALBVENTALIN.NUMALBARAN = @NumAlbaran 
            //        ORDER BY ALBVENTALINPROMOCIONES.DTO asc";
            string _sql = @"SELECT ALBVENTALIN.NUMSERIE, ALBVENTALIN.NUMALBARAN, ALBVENTALIN.N, ALBVENTALIN.NUMLIN,
                    ALBVENTALIN.CODARTICULO, ALBVENTALIN.DESCRIPCION, ALBVENTALIN.UNIDADESTOTAL, ALBVENTALIN.PRECIO, 
                    ALBVENTALIN.IVA, ALBVENTALIN.DTO, '' as TEXTOIMPRIMIR, ALBVENTALIN.ABONODE_NUMSERIE, ALBVENTALIN.ABONODE_NUMALBARAN, 
                    ALBVENTALIN.ABONODE_N, ALBVENTALIN.REFERENCIA, ALBVENTALIN.PRECIODEFECTO, 0 as DTO2, ALBVENTALIN.TIPOIMPUESTO
                    FROM ALBVENTALIN 
                    WHERE ALBVENTALIN.NUMSERIE = @Numserie and ALBVENTALIN.N = @N and ALBVENTALIN.NUMALBARAN = @NumAlbaran
                    ORDER BY ALBVENTALIN.DTO asc";

            List<Items> _items = new List<Items>();

            using (SqlCommand _cmd = new SqlCommand())
            {
                _cmd.Connection = _con;
                _cmd.CommandType = System.Data.CommandType.Text;
                _cmd.CommandText = _sql;

                _cmd.Parameters.AddWithValue("@NumSerie", _numSerie);
                _cmd.Parameters.AddWithValue("@N", _n);
                _cmd.Parameters.AddWithValue("@NumAlbaran", _nroAlbaran);

                using (SqlDataReader _reader = _cmd.ExecuteReader())
                {
                    if (_reader.HasRows)
                    {
                        while (_reader.Read())
                        {
                            Items _cls = new Items();
                            _cls.numserie = _reader["NUMSERIE"].ToString();
                            _cls.numalbaran = Convert.ToInt32(_reader["NUMALBARAN"]);
                            _cls.n = _reader["N"].ToString();
                            _cls.numlin = Convert.ToInt32(_reader["NUMLIN"]);
                            _cls._cantidad = Convert.ToDecimal(_reader["UNIDADESTOTAL"]);
                            _cls._codArticulo = _reader["CODARTICULO"].ToString();
                            _cls._descripcion = _reader["DESCRIPCION"].ToString();
                            _cls._descuento = Convert.ToDecimal(_reader["DTO"]);
                            _cls._iva = Convert.ToDecimal(_reader["IVA"]);
                            _cls._precio = Convert.ToDecimal(_reader["PRECIO"]);
                            _cls._textoPromo = _reader["TEXTOIMPRIMIR"].ToString();
                            _cls._abonoDeNumseie = _reader["ABONODE_NUMSERIE"].ToString();
                            _cls._abonoDeNumAlbaran = Convert.ToInt32(_reader["ABONODE_NUMALBARAN"]);
                            _cls._abonoDe_N = _reader["ABONODE_N"].ToString();
                            _cls._referencia = _reader["REFERENCIA"].ToString();
                            _cls._precioDefecto = Convert.ToDecimal(_reader["PRECIODEFECTO"]);
                            _cls._dto2 = _reader["DTO2"] == DBNull.Value ? 0 : Convert.ToDecimal(_reader["DTO2"]);
                            _cls._tipoImpuesto = Convert.ToInt32(_reader["TIPOIMPUESTO"]);
                            //List<ItemsPromos> _lst = ItemsPromos.GetItemsPromos(_cls.numserie, _cls.n, _cls.numalbaran, _cls.numlin, _con);
                            //_cls.Promosiones = _lst;
                            _items.Add(_cls);
                        }
                    }
                }
            }

            foreach (Items it in _items)
            {
                List<ItemsPromos> _lst = ItemsPromos.GetItemsPromos(it.numserie, it.n, it.numalbaran, it.numlin, _con);
                it.Promosiones = _lst;
            }

            return _items;
        }
    }

    public class ItemsPromos
    {
        public string numserie { get; set; }
        public int numalbaran { get; set; }
        public string n { get; set; }
        public int numlin { get; set; }
        public decimal dto2 { get; set; }
        public decimal importedto { get; set; }
        public decimal importedtoiva { get; set; }
        public string textoimprimir { get; set; }

        public static List<ItemsPromos> GetItemsPromos(string _numSerie, string _n, int _nroAlbaran, int _numlin, SqlConnection _con)
        {
            string _sql = @"SELECT ALBVENTALINPROMOCIONES.NUMSERIE, ALBVENTALINPROMOCIONES.NUMALBARAN, ALBVENTALINPROMOCIONES.N, ALBVENTALINPROMOCIONES.NUMLIN,
                    ALBVENTALINPROMOCIONES.DTO, PROMOCIONES.TEXTOIMPRIMIR, ALBVENTALINPROMOCIONES.IMPORTEDTO, ALBVENTALINPROMOCIONES.IMPORTEDTOIVA
                    FROM ALBVENTALIN left join ALBVENTALINPROMOCIONES ON ALBVENTALIN.NUMSERIE = ALBVENTALINPROMOCIONES.NUMSERIE 
                    AND ALBVENTALIN.NUMALBARAN = ALBVENTALINPROMOCIONES.NUMALBARAN 
                    AND ALBVENTALIN.N = ALBVENTALINPROMOCIONES.N 
                    AND ALBVENTALIN.NUMLIN = ALBVENTALINPROMOCIONES.NUMLIN and ALBVENTALINPROMOCIONES.IMPORTEDTO > 0 
                    LEFT JOIN PROMOCIONES ON ALBVENTALINPROMOCIONES.IDPROMOCION = PROMOCIONES.IDPROMOCION 
                    WHERE ALBVENTALIN.NUMSERIE = @Numserie and ALBVENTALIN.N = @N and ALBVENTALIN.NUMALBARAN = @NumAlbaran AND ALBVENTALIN.NUMLIN = @NumLin
                    ORDER BY ALBVENTALINPROMOCIONES.DTO asc";


            List<ItemsPromos> _items = new List<ItemsPromos>();

            using (SqlCommand _cmd = new SqlCommand())
            {
                _cmd.Connection = _con;
                _cmd.CommandType = System.Data.CommandType.Text;
                _cmd.CommandText = _sql;

                _cmd.Parameters.AddWithValue("@NumSerie", _numSerie);
                _cmd.Parameters.AddWithValue("@N", _n);
                _cmd.Parameters.AddWithValue("@NumAlbaran", _nroAlbaran);
                _cmd.Parameters.AddWithValue("@NumLin", _numlin);

                using (SqlDataReader _reader = _cmd.ExecuteReader())
                {
                    if (_reader.HasRows)
                    {
                        while (_reader.Read())
                        {
                            if (!String.IsNullOrEmpty(_reader["NUMSERIE"].ToString()))
                            {
                                ItemsPromos _cls = new ItemsPromos();
                                _cls.numserie = _reader["NUMSERIE"].ToString();
                                _cls.numalbaran = Convert.ToInt32(_reader["NUMALBARAN"]);
                                _cls.n = _reader["N"].ToString();
                                _cls.numlin = Convert.ToInt32(_reader["NUMLIN"]);
                                _cls.textoimprimir = _reader["TEXTOIMPRIMIR"].ToString();
                                _cls.dto2 = Convert.ToDecimal(_reader["DTO"]);
                                _cls.importedto = Convert.ToDecimal(_reader["IMPORTEDTO"]);
                                _cls.importedtoiva = Convert.ToDecimal(_reader["IMPORTEDTOIVA"]);

                                _items.Add(_cls);
                            }
                        }
                    }
                }
            }

            return _items;
        }
    }
}
