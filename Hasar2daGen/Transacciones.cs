﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hasar2daGen
{
    public class Transacciones
    {
        public static void TransaccionOK(Cabecera _cab, string _serieFiscal1, string _serieFiscal2, int _numeroTiquet, 
            int _zNumero, List<DocumentoAsociado> _docAsoc, SqlConnection _sqlConn)
        {
            SqlTransaction _tran;
            //Comienzo la transaccion
            _tran = _sqlConn.BeginTransaction();
            try
            {
                FacturasVentaSerieResol _fvsr = new FacturasVentaSerieResol
                {
                    n = _cab.n,
                    numSerie = _cab.numserie,
                    numFactura = _cab.numfac,
                    numeroFiscal = _numeroTiquet,
                    serieFiscal1 = _serieFiscal1,
                    serieFiscal2 = _serieFiscal2
                };
                //Grabo FacturasVentaSerieResol.
                FacturasVentaSerieResol.GrabarTiquetNew(_fvsr, _sqlConn, _tran);
                //Grabo FacturasVentaCamposLibres
                FacturasVentaCamposLibres _fvcl = new FacturasVentaCamposLibres
                {
                    numSerie = _cab.numserie,
                    numFactura = _cab.numfac,
                    N = _cab.n,
                    zNro = _zNumero
                };
                FacturasVentaCamposLibres.UpdateFacturasVentaCamposLibres_Tiquet(_fvcl, _sqlConn, _tran);
                //Grabo el Nro de Z en facturasventa.cleancashcontrolcode1
                Cabecera.GrabarZetaNew(_fvcl, _sqlConn, _tran);
                //Documentos asociados
                string _fcAsoc = "";
                foreach (DocumentoAsociado _doc in _docAsoc)
                {
                    if (String.IsNullOrEmpty(_fcAsoc))
                        _fcAsoc = _doc._seriefiscal1 + "-" + _doc._numerofiscal.PadLeft(8, '0');
                    else
                        _fcAsoc = _fcAsoc + "/" + _doc._seriefiscal1 + "-" + _doc._numerofiscal.PadLeft(8, '0');
                }
                if (!String.IsNullOrEmpty(_fcAsoc))
                {
                    //Grabo las facturas asociadas.
                    Hasar2daGen.FacturasVenta.UpdateDocumentoAsociadoFacturasVentaNew(_cab.numserie, _cab.numfac, _cab.n, _fcAsoc, _sqlConn, _tran);
                }
                //Grabo RemTransacciones. Enviamos los cambios a central.
                Hasar2daGen.RemTransacciones.InsertRemTransacciones(Environment.MachineName, _cab.numserie, _cab.numfac, _cab.n, 1, _sqlConn, _tran);

                _tran.Commit();
            }
            catch(Exception ex)
            {
                _tran.Rollback();
                throw new Exception("Transaccion OK. Error: " + ex.Message);
            }
        }

        public static void TransaccionCancelacion(Cabecera _cab, string _serieFiscal1, string _serieFiscal2, int _numeroTiquet,
            int _zNumero, SqlConnection _sql)
        {
            SqlTransaction _tran;
            //Comienzo la transaccion
            _tran = _sql.BeginTransaction();
            //
            try
            {
                //Recupero el Nro d comprobante.
                SeriesDoc _seriesDoc = SeriesDoc.GetSeriesDoc(_cab.numserie, _cab.tipodoc, _sql, _tran);
                //Inserto FActurasVenta
                FacturasVenta.InsertCancelacionFacturasVentaNew(_cab.numserie, _cab.numfac, _seriesDoc.contadorb, _cab.n, _sql, _tran);
                 FacturasVentaSerieResol _fvsr = new FacturasVentaSerieResol();
                _fvsr.numSerie = _cab.numserie;
                _fvsr.numFactura = _seriesDoc.contadorb;
                _fvsr.n = _cab.n;
                _fvsr.numeroFiscal = _numeroTiquet;
                _fvsr.serieFiscal1 = _serieFiscal1;
                _fvsr.serieFiscal2 = _serieFiscal2 + " Cancelada";
                //Inserto FacturasVentaSerieResol.
                FacturasVentaSerieResol.GrabarTiquetNew(_fvsr, _sql, _tran);
                //Grabo FacturasVentaCamposLibres
                FacturasVentaCamposLibres _fvcl = new FacturasVentaCamposLibres
                {
                    numSerie = _cab.numserie,
                    numFactura = _cab.numfac,
                    N = _cab.n,
                    zNro = _zNumero
                };
                FacturasVentaCamposLibres.UpdateFacturasVentaCamposLibres_Tiquet(_fvcl, _sql, _tran);
                //Grabo el Nro de Z en facturasventa.cleancashcontrolcode1
                Cabecera.GrabarZetaNew(_fvcl, _sql, _tran);

                //Vemos cual contador es el contador.
                if (_cab.n == "B")
                {
                    //Grabo RemTransacciones. Enviamos los cambios a central.
                    Hasar2daGen.RemTransacciones.InsertRemTransacciones(Environment.MachineName, _cab.numserie, _seriesDoc.contadorb, _cab.n, 1, _sql, _tran);
                    _seriesDoc.contadorb = _seriesDoc.contadorb + 1;                    
                }
                else
                {
                    //Grabo RemTransacciones. Enviamos los cambios a central.
                    Hasar2daGen.RemTransacciones.InsertRemTransacciones(Environment.MachineName, _cab.numserie, _seriesDoc.contadorn, _cab.n, 1, _sql, _tran);
                    _seriesDoc.contadorn = _seriesDoc.contadorn + 1;                    
                }
                //Actualizamos el contador.
                SeriesDoc.UpdateSeriesDoc(_seriesDoc, _sql, _tran);

                _tran.Commit();
            }
            catch (Exception ex)
            {
                _tran.Rollback();
                throw new Exception("TransaccionCancelacion. Error: " + ex.Message);
            }
        }

        public static void TransaccionBlack(Cabecera _cab, SqlConnection _sql)
        {
                string _serieFiscal1 = "0000";
                string _serieFiscal2 = "";
                int _numeroFiscal = -1;

                SqlTransaction _tran;
                //Comienzo la transaccion
                _tran = _sql.BeginTransaction();
            try
            {
                FacturasVentaSerieResol _fvsr = new FacturasVentaSerieResol
                {
                    n = _cab.n,
                    numSerie = _cab.numserie,
                    numFactura = _cab.numfac,
                    numeroFiscal = _numeroFiscal,
                    serieFiscal1 = _serieFiscal1,
                    serieFiscal2 = _serieFiscal2
                };
                //Grabo FacturasVentaSerieResol.
                FacturasVentaSerieResol.GrabarTiquetNew(_fvsr, _sql, _tran);
                //Inserto en Rem_transacciones.
                //Grabo RemTransacciones. Enviamos los cambios a central.
                Hasar2daGen.RemTransacciones.InsertRemTransacciones(Environment.MachineName, _cab.numserie, _cab.numfac, _cab.n, 1, _sql, _tran);
                //Comiteo los cambios
                _tran.Commit();
            }
            catch (Exception ex)
            {
                _tran.Rollback();
                throw new Exception("TransaccionBlack. Error: " + ex.Message);
            }
        }
    }
}
