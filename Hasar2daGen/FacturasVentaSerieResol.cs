﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hasar2daGen
{
    public class FacturasVentaSerieResol
    {
        public string numSerie { get; set; }
        public int numFactura { get; set; }
        public string n { get; set; }
        public string serieFiscal1 {get;set;}
        public string serieFiscal2 { get; set; }
        public int numeroFiscal { get; set; }

        public static bool GrabarTiquet(string _numSerie, int _numFactura, string _n, string _serieFiscal1, 
            string _serieFiscal2, int _numeroFiscal, SqlConnection _con)
         {
            string _sql = "INSERT INTO FACTURASVENTASERIESRESOL ( NUMSERIE, NUMFACTURA, N, SERIEFISCAL1, SERIEFISCAL2, NUMEROFISCAL) "+
                    "VALUES(@numSerie, @numFactura, @n, @serieFiscal1, @serieFiscal2, @numeroFiscal)";

            bool _rta = false;

            try
            {
                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;

                    _cmd.Parameters.AddWithValue("@Numserie", _numSerie);
                    _cmd.Parameters.AddWithValue("@numFactura", _numFactura);
                    _cmd.Parameters.AddWithValue("@n", _n);
                    _cmd.Parameters.AddWithValue("@serieFiscal1", _serieFiscal1);
                    _cmd.Parameters.AddWithValue("@serieFiscal2", _serieFiscal2);
                    _cmd.Parameters.AddWithValue("@numeroFiscal", _numeroFiscal);

                    int _insert = _cmd.ExecuteNonQuery();
                    _rta = true;
                }
            }
            catch (Exception )
            {
                _rta = false;
            }

            return _rta;
        }

        public static bool GrabarTiquet(FacturasVentaSerieResol _fvsr, SqlConnection _con, SqlTransaction _tran)
        {
            string _sql = "INSERT INTO FACTURASVENTASERIESRESOL ( NUMSERIE, NUMFACTURA, N, SERIEFISCAL1, SERIEFISCAL2, NUMEROFISCAL) " +
                    "VALUES(@numSerie, @numFactura, @n, @serieFiscal1, @serieFiscal2, @numeroFiscal)";

            bool _rta = false;

            try
            {
                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;
                    _cmd.Transaction = _tran;
                    
                    _cmd.Parameters.AddWithValue("@Numserie", _fvsr.numSerie);
                    _cmd.Parameters.AddWithValue("@numFactura", _fvsr.numFactura);
                    _cmd.Parameters.AddWithValue("@n", _fvsr.n);
                    _cmd.Parameters.AddWithValue("@serieFiscal1", _fvsr.serieFiscal1);
                    _cmd.Parameters.AddWithValue("@serieFiscal2", _fvsr.serieFiscal2);
                    _cmd.Parameters.AddWithValue("@numeroFiscal", _fvsr.numeroFiscal);

                    int _insert = _cmd.ExecuteNonQuery();
                    _rta = true;
                }
            }
            catch (Exception ex)
            {
                string _mes = ex.Message;
                _rta = false;
            }

            return _rta;
        }

        public static void GrabarTiquetNew(FacturasVentaSerieResol _fvsr, SqlConnection _con, SqlTransaction _tran)
        {
            string _sql = "INSERT INTO FACTURASVENTASERIESRESOL ( NUMSERIE, NUMFACTURA, N, SERIEFISCAL1, SERIEFISCAL2, NUMEROFISCAL) " +
                    "VALUES(@numSerie, @numFactura, @n, @serieFiscal1, @serieFiscal2, @numeroFiscal)";

            try
            {
                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;
                    _cmd.Transaction = _tran;

                    _cmd.Parameters.AddWithValue("@Numserie", _fvsr.numSerie);
                    _cmd.Parameters.AddWithValue("@numFactura", _fvsr.numFactura);
                    _cmd.Parameters.AddWithValue("@n", _fvsr.n);
                    _cmd.Parameters.AddWithValue("@serieFiscal1", _fvsr.serieFiscal1);
                    _cmd.Parameters.AddWithValue("@serieFiscal2", _fvsr.serieFiscal2);
                    _cmd.Parameters.AddWithValue("@numeroFiscal", _fvsr.numeroFiscal);

                    int _insert = _cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static FacturasVentaSerieResol GetTiquet(string _numSerie, int _numFactura, string _n, SqlConnection _con)
        {
            string _sql = "SELECT NUMSERIE, NUMFACTURA, N, SERIEFISCAL1, SERIEFISCAL2, NUMEROFISCAL FROM FACTURASVENTASERIESRESOL " +
                    "WHERE NUMSERIE = @numSerie AND NUMFACTURA = @numFactura AND N = @n";

            FacturasVentaSerieResol _cls = new FacturasVentaSerieResol();

            using (SqlCommand _cmd = new SqlCommand())
            {
                _cmd.Connection = _con;
                _cmd.CommandType = System.Data.CommandType.Text;
                _cmd.CommandText = _sql;

                _cmd.Parameters.AddWithValue("@Numserie", _numSerie);
                _cmd.Parameters.AddWithValue("@numFactura", _numFactura);
                _cmd.Parameters.AddWithValue("@n", _n);

                using (SqlDataReader _reader = _cmd.ExecuteReader())
                {
                    if (_reader.HasRows)
                    {
                        while (_reader.Read())
                        {
                            _cls.n = _reader["N"].ToString();
                            _cls.numeroFiscal = Convert.ToInt32(_reader["NUMEROFISCAL"]);
                            _cls.numFactura = Convert.ToInt32(_reader["NUMFACTURA"]);
                            _cls.numSerie = _reader["NUMSERIE"].ToString();
                            _cls.serieFiscal1 = _reader["SERIEFISCAL1"].ToString();
                            _cls.serieFiscal2 = _reader["SERIEFISCAL2"].ToString();
                        }
                    }
                }
            }


            return _cls;
        }

        public static bool GrabarNumeroTiquetCancelado(string _serie, int _numero, string _n, string _serieFiscal1,
            string _serieFiscal2, int _numeroCancel, SqlConnection _con)
        {
            string _sql = "UPDATE FACTURASVENTASERIESRESOL SET SERIEFISCAL = @serieFiscal, SERIEFISCAL2 = @serieFiscal2, NUMEROFISCAL = 0 " +
                "WHERE NUMSERIE = @serie AND NUMERO = NUMFACTURA AND N = @n";

            string _sql2 = "UPDATE FACTURASVENTACAMPOSLIBRES SET ANULACION_IRSA = @numeroCancel " +
                "WHERE NUMSERIE = @serie AND NUMFACTURA = @numero AND N = @n";

            bool _rta = false;

            try
            {
                //TiquetsCab
                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;

                    _cmd.Parameters.AddWithValue("@serie", _serie);
                    _cmd.Parameters.AddWithValue("@numero", _numero);
                    _cmd.Parameters.AddWithValue("@n", _n);
                    _cmd.Parameters.AddWithValue("@serieFiscal", _serieFiscal1);
                    _cmd.Parameters.AddWithValue("@serieFiscal2", _serieFiscal2);

                    int _update = _cmd.ExecuteNonQuery();
                    if (_update > 0)
                        _rta = true;
                    else
                        _rta = false;
                }
                //TiquetsVentaCamposLibres
                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql2;

                    _cmd.Parameters.AddWithValue("@serie", _serie);
                    _cmd.Parameters.AddWithValue("@numero", _numero);
                    _cmd.Parameters.AddWithValue("@n", _n);
                    _cmd.Parameters.AddWithValue("@numeroCancel", _numeroCancel);

                    int _update = _cmd.ExecuteNonQuery();
                    if (_update > 0)
                        _rta = true;
                    else
                        _rta = false;
                }
            }
            catch (Exception)
            {
                _rta = false;
            }

            return _rta;
        }

        public static bool ValidoPtoVtaNroFiscal(string _ptovta, int _nro, SqlConnection _connection)
        {
            string _sql = @"SELECT COUNT(*) FROM FACTURASVENTASERIESRESOL WHERE SERIEFISCAL1 = @serie and NUMEROFISCAL = @nro";

            try
            {
                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _connection;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;

                    _cmd.Parameters.AddWithValue("@serie", _ptovta);
                    _cmd.Parameters.AddWithValue("@nro", _nro);
                    
                    int _Q = Convert.ToInt32(_cmd.ExecuteScalar());

                    if (_Q > 0)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

    }
}
