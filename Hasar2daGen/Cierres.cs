﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Hasar2daGen
{
    public class Cierres
    {
        public static string ImprimirCierreZ(string _ip, bool _hasarLog)
        {
            hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarJornadaFiscal _cierre = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarJornadaFiscal();
            hfl.argentina.HasarImpresoraFiscalRG3561.CerrarJornadaFiscalZ _zeta = new hfl.argentina.HasarImpresoraFiscalRG3561.CerrarJornadaFiscalZ();
            string _msj;

            try
            {
                //Conectamos.
                Impresiones.Conectar(hasar, _hasarLog, _ip);
                //ejecutamos el cierre Z.
                _cierre = hasar.CerrarJornadaFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.TipoReporte.REPORTE_Z);
                _zeta = _cierre.Z;

                _msj = "INFORME DIARIO DE CIERRE :" + Environment.NewLine +
                  "Cierre 'Z' Nº            =[" + _zeta.getNumero() + "]" + Environment.NewLine +
                  "Fecha del Cierre         =[" + _zeta.getFecha() + "]" + Environment.NewLine +
                  "DF Emitidos              =[" + _zeta.getDF_CantidadEmitidos() + "]" + Environment.NewLine +
                  "DF Cancelados            =[" + _zeta.getDF_CantidadCancelados() + "]" + Environment.NewLine +
                  "DF Total                 =[" + string.Format("{0:0.00}", _zeta.getDF_Total() / 100) + "]" + Environment.NewLine +
                  "DF Total Gravado         =[" + string.Format("{0:0.00}", _zeta.getDF_TotalGravado() / 100) + "]" + Environment.NewLine +
                  "DF Total No Gravado      =[" + string.Format("{0:0.00}", _zeta.getDF_TotalNoGravado() / 100) + "]" + Environment.NewLine +
                  "DF Total Exento          =[" + string.Format("{0:0.00}", _zeta.getDF_TotalExento() / 100) + "]" + Environment.NewLine +
                  "DF Total IVA             =[" + string.Format("{0:0.00}", _zeta.getDF_TotalIVA() / 100) + "]" + Environment.NewLine +
                  "DF Total Otros Tributos  =[" + string.Format("{0:0.00}", _zeta.getDF_TotalTributos() / 100) + "]" + Environment.NewLine +
                  "NC Emitidas              =[" + _zeta.getNC_CantidadEmitidos() + "]" + Environment.NewLine +
                  "NC Canceladas            =[" + _zeta.getNC_CantidadCancelados() + "]" + Environment.NewLine +
                  "NC Total                 =[" + string.Format("{0:0.00}", _zeta.getNC_Total() / 100) + "]" + Environment.NewLine +
                  "NC Total Gravado         =[" + string.Format("{0:0.00}", _zeta.getNC_TotalGravado() / 100) + "]" + Environment.NewLine +
                  "NC Total No Gravado      =[" + string.Format("{0:0.00}", _zeta.getNC_TotalNoGravado() / 100) + "]" + Environment.NewLine +
                  "NC Total Exento          =[" + string.Format("{0:0.00}", _zeta.getNC_TotalExento() / 100) + "]" + Environment.NewLine +
                  "NC Total IVA             =[" + string.Format("{0:0.00}", _zeta.getNC_TotalIVA() / 100) + "]" + Environment.NewLine +
                  "NC Total Otros Tributos  =[" + string.Format("{0:0.00}", _zeta.getNC_TotalTributos() / 100) + "]" + Environment.NewLine +
                  "DNFH Emitidos            =[" + _zeta.getDNFH_CantidadEmitidos() + "]" + Environment.NewLine +
                  "DNFH Total               =[" + _zeta.getDNFH_Total() + "]";

                return _msj;

            }
            catch (Exception ex)
            {
                //Cancelamos
                Impresiones.Cancelar(hasar);
                //Mandamos la Excepcion
                throw new Exception(ex.Message);
            }
        }

        public static string ImprimirCierreZ(string _ip, bool _hasarLog, string _caja, SqlConnection _sql)
        {
            hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarJornadaFiscal _cierre = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarJornadaFiscal();
            hfl.argentina.HasarImpresoraFiscalRG3561.CerrarJornadaFiscalZ _zeta = new hfl.argentina.HasarImpresoraFiscalRG3561.CerrarJornadaFiscalZ();
            string _msj;

            try
            {
                //Conectamos.
                Impresiones.Conectar(hasar, _hasarLog, _ip);
                //ejecutamos el cierre Z.
                _cierre = hasar.CerrarJornadaFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.TipoReporte.REPORTE_Z);                
                _zeta = _cierre.Z;
                //Recupero el Total.
                double _total = _zeta.getDF_Total() / 100;
                //Recupero total Impuestos
                double _total_Impuestos = (_zeta.getDF_TotalIVA() / 100) + (_zeta.getDF_TotalTributos() / 100);
                //Recupero Fecha
                DateTime _fecha = _zeta.getFecha();
                //Recupero la hora del controlador Fiscal
                TimeSpan _hora = hasar.ConsultarFechaHora().getHora();
                //Sumo la fecha y la hora
                DateTime _fechaHora = _fecha + _hora;
                //Recupero numero Z.
                int _nroZeta = _zeta.getNumero();
                //Recuperro Total NC.
                double _totalNC = _zeta.getNC_Total() / 100;
                //Recupero Total NC Iva.
                double _totalNC_Iva = _zeta.getNC_TotalIVA() / 100;
                int _cantidadTiquets = _zeta.getDF_CantidadEmitidos();
                //Descontamos de total las notas de credito
                double _totalNeto = _total - _totalNC;
                string _dato = _zeta.getNumero() + ";" + _zeta.getDF_CantidadEmitidos() + ";" + _zeta.getDF_Total();
                ////Grabo FacturasVentaCamposLibres
                //FacturasVentaCamposLibres _fvcl = new FacturasVentaCamposLibres
                //{
                //    numSerie = "Cierre",
                //    numFactura = 0,
                //    N = "Z",
                //    zNro = _nroZeta,
                //    zTotal = _total,
                //    zImpuestos = _total_Impuestos,
                //    zTiquets = _cantidadTiquets,
                //    zFechaHora = _fecha
                //};
                ////Grabamos la información en los campos libres.
                //FacturasVentaCamposLibres.UpdateFacturasVentaCamposLibres_CierreZ(_fvcl, _caja, _sql);
                //Insertamos en la tabla Arqueos
                int _fo = GetFo(_sql);
                InsertArqueos(_fo, _caja, _nroZeta, _fechaHora, _totalNeto, _total_Impuestos, _cantidadTiquets, _dato, _sql);
                //Recupero nombre terminal
                string _terminal = Environment.MachineName;
                //Mandamos a Rem_transacciones.
                RemTransacciones.InsertRemTransaccionesCierreZ_X(_terminal, _nroZeta, _caja, _sql);
                int _rta = Arqueo.UpdateArqueosOK("Z", _dato, _sql);
                RemTransacciones.InsertRemTransaccionesCierreZ(_terminal, _nroZeta, _caja, _sql);                

                _msj = "INFORME DIARIO DE CIERRE :" + Environment.NewLine +
                  "Cierre 'Z' Nº            =[" + _zeta.getNumero().ToString() + "]" + Environment.NewLine +
                  "Fecha del Cierre         =[" + _zeta.getFecha().ToString() + "]" + Environment.NewLine +
                  "DF Emitidos              =[" + _zeta.getDF_CantidadEmitidos().ToString() + "]" + Environment.NewLine +
                  "DF Cancelados            =[" + _zeta.getDF_CantidadCancelados().ToString() + "]" + Environment.NewLine +
                  "DF Total                 =[" + string.Format("{0:0.00}", _zeta.getDF_Total() / 100) + "]" + Environment.NewLine +
                  "DF Total Gravado         =[" + string.Format("{0:0.00}", _zeta.getDF_TotalGravado() / 100) + "]" + Environment.NewLine +
                  "DF Total No Gravado      =[" + string.Format("{0:0.00}", _zeta.getDF_TotalNoGravado() / 100) + "]" + Environment.NewLine +
                  "DF Total Exento          =[" + string.Format("{0:0.00}", _zeta.getDF_TotalExento() / 100) + "]" + Environment.NewLine +
                  "DF Total IVA             =[" + string.Format("{0:0.00}", _zeta.getDF_TotalIVA() / 100) + "]" + Environment.NewLine +
                  "DF Total Otros Tributos  =[" + string.Format("{0:0.00}", _zeta.getDF_TotalTributos() / 100) + "]" + Environment.NewLine +
                  "NC Emitidas              =[" + _zeta.getNC_CantidadEmitidos().ToString() + "]" + Environment.NewLine +
                  "NC Canceladas            =[" + _zeta.getNC_CantidadCancelados().ToString() + "]" + Environment.NewLine +
                  "NC Total                 =[" + string.Format("{0:0.00}", _zeta.getNC_Total() / 100) + "]" + Environment.NewLine +
                  "NC Total Gravado         =[" + string.Format("{0:0.00}", _zeta.getNC_TotalGravado() / 100) + "]" + Environment.NewLine +
                  "NC Total No Gravado      =[" + string.Format("{0:0.00}", _zeta.getNC_TotalNoGravado() / 100) + "]" + Environment.NewLine +
                  "NC Total Exento          =[" + string.Format("{0:0.00}", _zeta.getNC_TotalExento() / 100) + "]" + Environment.NewLine +
                  "NC Total IVA             =[" + string.Format("{0:0.00}", _zeta.getNC_TotalIVA() / 100) + "]" + Environment.NewLine +
                  "NC Total Otros Tributos  =[" + string.Format("{0:0.00}", _zeta.getNC_TotalTributos() / 100) + "]" + Environment.NewLine +
                  "DNFH Emitidos            =[" + _zeta.getDNFH_CantidadEmitidos().ToString() + "]" + Environment.NewLine +
                  "DNFH Total               =[" + _zeta.getDNFH_Total().ToString() + "]";

                return _msj;

            }
            catch (Exception ex)
            {
                //Cancelamos
                Impresiones.Cancelar(hasar);
                //Mandamos la Excepcion
                throw new Exception(ex.Message);
            }
        }

        public static int InsertArqueos(int _fo, string _caja, int _nroZ, DateTime _fecha, double _totalZ, double _impuestosZ, int _tiquetsZ, string _dato, SqlConnection _conn)
        {

            string _sql = @"IF(SELECT COUNT(1) FROM ARQUEOS WHERE ARQUEO = 'X' AND CAJA = @caja AND NUMERO = @nroZ) = 0
            BEGIN
            INSERT INTO [ARQUEOS] ([FO],[ARQUEO],[CAJA],[NUMERO],[CODVENDEDOR],[FECHA],[HORA],[TOTAL],[DESCUADRE],[PUNTEO],[SESION],[ACUMULADO]
           ,[ACUMULADON],[NUMMESASABIERTAS],[IMPORTEMESASABIERTAS],[NUMVENTASIMPRESAS],[IMPORTEVENTASIMPRESAS],[OBSERVACIONES],[CLEANCASHCONTROLCODE1]
           ,[CERRADO]) VALUES(@fo,'X',@caja,@nroZ,-99,@fecha,@hora,@total,0,0,@totalImpuestos,0,0,0,0,@cantTiquets,@total,'Cierre Z',@dato,0)
            END
            ELSE
            BEGIN
            UPDATE ARQUEOS SET CODVENDEDOR = -99, FECHA = @fecha, HORA = @hora, TOTAL = @total, ACUMULADO = @totalImpuestos,
            NUMVENTASIMPRESAS = @cantTiquets, IMPORTEVENTASIMPRESAS = total, OBSERVACIONES = 'Cierre Z', CLEANCASHCONTROLCODE1 = @dato
            WHERE ARQUEO = 'X' and CAJA = @caja and NUMERO = @nroZ
            END";

            int _rta = 0;

            using (SqlCommand _cmd = new SqlCommand())
            {
                _cmd.Connection = _conn;
                _cmd.CommandType = System.Data.CommandType.Text;
                _cmd.CommandText = _sql;

                _cmd.Parameters.AddWithValue("@fo", _fo);
                _cmd.Parameters.AddWithValue("@caja", _caja);
                _cmd.Parameters.AddWithValue("@nroZ", _nroZ);
                _cmd.Parameters.AddWithValue("@fecha", _fecha.Date);
                _cmd.Parameters.AddWithValue("@hora", _fecha.ToLongTimeString());
                _cmd.Parameters.AddWithValue("@total", _totalZ);
                _cmd.Parameters.AddWithValue("@totalImpuestos", _impuestosZ);
                _cmd.Parameters.AddWithValue("@cantTiquets", _tiquetsZ);
                _cmd.Parameters.AddWithValue("@dato", _dato);

                _rta = _cmd.ExecuteNonQuery();

            }

            return _rta;
        }

        public static int GetFo(SqlConnection _connection)
        {
            string _sql = "SELECT VALOR FROM REM_CONFIG WHERE IDCONFIG = 31";
            int _rta = 1;

            using (SqlCommand _cmd = new SqlCommand())
            {
                _cmd.Connection = _connection;
                _cmd.CommandType = System.Data.CommandType.Text;
                _cmd.CommandText = _sql;

                using (SqlDataReader _reader = _cmd.ExecuteReader())
                {
                    if (_reader.HasRows)
                    {
                        while (_reader.Read())
                        {
                            _rta = Convert.ToInt32(_reader["VALOR"]);
                        }
                    }
                }
            }

            return _rta;
        }
    }
}
